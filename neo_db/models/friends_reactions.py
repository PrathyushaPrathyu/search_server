from neo_db import *

class FriendsReactions:
  def __init__(self, reactions):
    self.reactions = self.decode_reactions(reactions=reactions)

  def to_dict(self):
    return [{
      'user': o['user'].to_dict(simplified=True),
      'reaction': o['reaction'],
    } for o in self.reactions]

  @staticmethod
  def decode_reactions(reactions):
    return [{
      'user': User.from_node(o['user']),
      'reaction': 'loves this track!',
    } for o in reactions]
