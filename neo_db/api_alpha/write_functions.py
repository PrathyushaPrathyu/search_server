
from recommender.api import make_ngrams
from neo_db import *
import re

def write(code: str, **kwargs):
    func = lambda tx, **kwargs: tx.run(code, **kwargs)
    with driver.session() as session:
        return session.write_transaction(func, **kwargs)


def update_track_thresholds(love_threshold, listen_threshold):
    code = """
    MATCH(n:Track)
    SET n.love_threshold = $love_threshold,
        n.listen_threshold = $listen_threshold
    """
    write(code, love_threshold=love_threshold, listen_threshold=listen_threshold)

def set_friend_notified(user, friend, shared_track_id):
    print("setting friend notified")
    code="""
    MATCH (p:User {username: $username}),
    (f:User {username: $friend_name})
    MERGE (f)-[r:SHARES_TRACK {track_mongo_id: $track_mongo_id}]->(p)
    SET r.notified = TRUE
    """
    write(code, username=user.name, friend_name=friend.name, track_mongo_id=shared_track_id)
    print("set friend notified in neo4j")

def set_track_name_from_id(track_id, track_name):
    print("setting track name from id")
    code = """
            MERGE (t:Track{id:$track_id})
            set t.name = $track_name
            """
    write(code, track_name=track_name, track_id=track_id)

def load_track_to_neo4j(_id, name_lower, popularity, genre, cover_image_url):
    title_ngrams_set = make_ngrams(word=name_lower)
    title_ngrams = ' '.join(list(title_ngrams_set))

    name_lower = re.sub(r" ?\([^)]+\)", " ", name_lower)
    name_lower = re.sub(r'\[[^()]*\]', ' ', name_lower)
    name_lower = name_lower.replace('"', ' ')

    code = """
    MERGE (t:Track {id:$_id})
    ON CREATE SET t.name = $name_lower, t.cover_image_url = $cover_image_url

    WITH t
    UNWIND $genre AS genre_dict
    MERGE (genre:TrackFeature:Genre {name: genre_dict.name})
    MERGE (t)-[r:HAS_GENRE]->(genre)
    ON CREATE SET r.score = genre_dict.score

    MERGE (s:TitleNgrams{mongoId:$_id})
    SET s.popularity = $popularity,
        s.title_ngrams=$title_ngrams
     """
    write(code, _id=_id, name_lower=name_lower, popularity=popularity, genre=genre, cover_image_url=cover_image_url,
                             title_ngrams=title_ngrams)