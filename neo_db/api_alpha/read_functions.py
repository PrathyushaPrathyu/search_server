from neo_db import *
from collections import defaultdict
from collections import OrderedDict
import db_manager as dm
from pprint import pprint
from pandas.io.json import json_normalize
import json

def read(code:str, **kwargs):
    func = lambda tx, **kwargs: tx.run(code, **kwargs)
    with driver.session() as session:
          return session.read_transaction(func, **kwargs)


def get_track_expertise(user_id, track_id):
    # code = """
    # MATCH (u:User {id: $user_id})
    # MATCH (t:Track {id: $track_id})
    # OPTIONAL MATCH (u)-[ter:IS_EXPERT_IN]->(t)
    # WITH u, t, count(ter) > 0 as is_track_expert
    # OPTIONAL MATCH (u)-[:IS_EXPERT_IN]->(art:Artist)<-[:HAS_ARTIST]-(t)
    # WITH u, t, is_track_expert, collect(DISTINCT art.name) as artists
    # OPTIONAL MATCH (u)-[:IS_EXPERT_IN]->(alb:Album)<-[:HAS_ALBUM]-(t)
    # WITH u, t, is_track_expert, alb.name as album, artists
    # OPTIONAL MATCH (u)-[:IS_EXPERT_IN]->(g:Genre)
    # WITH u, is_track_expert,  album, artists,  collect(DISTINCT g.name) as genre_expert
    # OPTIONAL MATCH (u)-[:IS_EXPERT_IN]->(a:About)
    # WITH u, is_track_expert,  album, artists, genre_expert, collect(DISTINCT a.name) as about_expert
    # OPTIONAL MATCH (u)-[:IS_EXPERT_IN]->(i:Instrument)
    # WITH u, is_track_expert,  album, artists, genre_expert, about_expert, collect(DISTINCT i.name) as instrument_expert
    # OPTIONAL MATCH (u)-[track_feature_edit:HAS_EDITED]->(tf:TrackFeature)
    # WITH u, is_track_expert,  album, artists, genre_expert, about_expert, instrument_expert, count(tf) AS edit_count
    # OPTIONAL MATCH (u)-[:LOVES]->(loved_track:Track)
    # RETURN is_track_expert, album as track_album_expertise, artists as track_artist_expertise,
    # genre_expert as genre_expertise, about_expert as about_expertise, instrument_expert as instrument_expertise,
    #  edit_count, count(loved_track) as love_count
    # """
    #todo:edited by aditya..new algo
    #todo: right now we are returning is_expert_in fields also in neo4j query..its currently not being used, but since our initial algo was based on, we've kept it for now...will remove if required
    code = """
    MATCH (u:User {id: $user_id})
    MATCH (t:Track {id: $track_id})
    MATCH (u)-[:HAS_BADGE]->(b:Badge)
    OPTIONAL MATCH (u)-[r:LISTENED_TO]->(t)
    WITH u, t, min(b.priority) as bp_min, max(r.count) as l2_track
    OPTIONAL MATCH (u)-[ter:IS_EXPERT_IN]->(t)
    WITH u, t, bp_min, l2_track, count(ter) > 0 as is_track_expert
    OPTIONAL MATCH (u)-[:IS_EXPERT_IN]->(art:Artist)<-[:HAS_ARTIST]-(t)
    WITH u, t, bp_min, l2_track, is_track_expert, collect(DISTINCT art.name) as artists
    OPTIONAL MATCH (u)-[:IS_EXPERT_IN]->(alb:Album)<-[:HAS_ALBUM]-(t)
    WITH u, t, bp_min, l2_track, is_track_expert, alb.name as album, artists

    OPTIONAL MATCH (t)-[:HAS_GENRE]->(genre:Genre)
    OPTIONAL MATCH (u)-[l2:LISTENED_TO]->(genre)
    WITH u, t, bp_min, l2_track, is_track_expert, album, artists, l2.count as l2_genre, collect(DISTINCT genre.name) as genres
    OPTIONAL MATCH (u)-[ex:IS_EXPERT_IN]->(g:Genre)<-[:HAS_GENRE]-(t)
    WITH u, t, bp_min, is_track_expert, album, artists, genres,
        l2_genre, l2_track,
        g, ex, collect(DISTINCT g.name) AS genre_expert
    OPTIONAL MATCH (u2:User)-[ed:HAS_EDITED{tid:t.id}]->(gedit:Genre)<-[:HAS_GENRE]-(t)
    OPTIONAL MATCH(u2)-[:HAS_BADGE]->(b2:Badge)
    WITH u, t, bp_min, is_track_expert, album, artists, genres,
        l2_genre, l2_track,
        min(b2.priority) AS b2p_g, max(ex.expertise) AS exp_g, max(ed.expertise) AS edp_g,
        genre_expert, collect(DISTINCT gedit.name) as genre_edited

    OPTIONAL MATCH (t)-[:HAS_ABOUT]->(about:About)
    OPTIONAL MATCH (u)-[l2:LISTENED_TO]->(about)
    WITH u, t, bp_min, is_track_expert, album, artists, genres, collect(DISTINCT about.name) as abouts,
        l2_genre, l2.count as l2_about, l2_track,
        b2p_g, exp_g, edp_g, genre_expert, genre_edited
    OPTIONAL MATCH (u)-[ex:IS_EXPERT_IN]->(a:About)<-[:HAS_ABOUT]-(t)
    WITH u, t, bp_min, is_track_expert, album, artists, genres, abouts,
        l2_genre, l2_about, l2_track,
        b2p_g, exp_g, edp_g, genre_expert, genre_edited, 
        a, ex, collect(DISTINCT a.name) AS about_expert
    OPTIONAL MATCH (u2:User)-[ed:HAS_EDITED{tid:t.id}]->(aedit:About)<-[:HAS_ABOUT]-(t)
    OPTIONAL MATCH(u2)-[:HAS_BADGE]->(b2:Badge)
    WITH u, t, bp_min, is_track_expert, album, artists, genres, abouts,
        l2_genre, l2_about, l2_track,
        b2p_g, exp_g, edp_g, genre_expert, genre_edited, 
        min(b2.priority) AS b2p_a, max(ex.expertise) AS exp_a, max(ed.expertise) AS edp_a,
        about_expert, collect(DISTINCT aedit.name) as about_edited

    OPTIONAL MATCH (t)-[:HAS_LANGUAGE]->(language:Language)
    OPTIONAL MATCH (u)-[l2:LISTENED_TO]->(language)
    WITH u, t, bp_min, is_track_expert, album, artists, genres, abouts, collect(DISTINCT language.name) as languages,
        l2_genre, l2_about, l2.count as l2_language, l2_track,
        b2p_g, exp_g, edp_g, genre_expert, genre_edited, 
        b2p_a, exp_a, edp_a, about_expert, about_edited
    OPTIONAL MATCH (u)-[ex:IS_EXPERT_IN]->(l:Language)<-[:HAS_LANGUAGE]-(t)
    WITH u, t, bp_min, is_track_expert, album, artists, genres, abouts, languages,
        l2_genre, l2_about, l2_language, l2_track,
        b2p_g, exp_g, edp_g, genre_expert, genre_edited, 
        b2p_a, exp_a, edp_a, about_expert, about_edited,
        l, ex, collect(DISTINCT l.name) AS language_expert
    OPTIONAL MATCH (u2:User)-[ed:HAS_EDITED{tid:t.id}]->(ledit:Language)<-[:HAS_LANGUAGE]-(t)
    OPTIONAL MATCH(u2)-[:HAS_BADGE]->(b2:Badge)
    WITH u, t, bp_min, is_track_expert, album, artists, genres, abouts, languages,
        l2_genre, l2_about, l2_language, l2_track,
        b2p_g, exp_g, edp_g, genre_expert, genre_edited,
        b2p_a, exp_a, edp_a, about_expert, about_edited, 
        min(b2.priority) AS b2p_l, max(ex.expertise) AS exp_l, max(ed.expertise) AS edp_l,
        language_expert, collect(DISTINCT ledit.name) as language_edited

    OPTIONAL MATCH (t)-[:HAS_INSTRUMENT]->(instrument:Instrument)
    OPTIONAL MATCH (u)-[l2:LISTENED_TO]->(instrument)
    WITH u, t, bp_min, is_track_expert, album, artists, genres, abouts, languages, collect(DISTINCT instrument.name) as instruments,
        l2_genre, l2_about, l2_language, l2.count as l2_instrument, l2_track,
        b2p_g, exp_g, edp_g, genre_expert, genre_edited, 
        b2p_a, exp_a, edp_a, about_expert, about_edited,
        b2p_l, exp_l, edp_l, language_expert, language_edited
    OPTIONAL MATCH (u)-[ex:IS_EXPERT_IN]->(i:Instrument)<-[:HAS_INSTRUMENT]-(t)
    WITH u, t, bp_min, is_track_expert, album, artists, genres, abouts, languages, instruments,
        l2_genre, l2_about, l2_language, l2_instrument, l2_track,
        b2p_g, exp_g, edp_g, genre_expert, genre_edited, 
        b2p_a, exp_a, edp_a, about_expert, about_edited,
        b2p_l, exp_l, edp_l, language_expert, language_edited,
        i, ex, collect(DISTINCT i.name) AS instrument_expert
    OPTIONAL MATCH (u2:User)-[ed:HAS_EDITED{tid:t.id}]->(iedit:Instrument)<-[:HAS_INSTRUMENT]-(t)
    OPTIONAL MATCH(u2)-[:HAS_BADGE]->(b2:Badge)
    WITH u, t, bp_min, is_track_expert, album, artists, genres, abouts, languages, instruments,
        l2_genre, l2_about, l2_language, l2_instrument, l2_track,
        b2p_g, exp_g, edp_g, genre_expert, genre_edited,
        b2p_a, exp_a, edp_a, about_expert, about_edited,
        b2p_l, exp_l, edp_l, language_expert, language_edited,
        min(b2.priority) AS b2p_i, max(ex.expertise) AS exp_i, max(ed.expertise) AS edp_i,
        instrument_expert, collect(DISTINCT iedit.name) as instrument_edited

    OPTIONAL MATCH (u)-[track_feature_edit:HAS_EDITED]->(tf:TrackFeature)
    WITH u, t, bp_min, is_track_expert, album, artists, genres, abouts, languages, instruments,
        l2_genre, l2_about, l2_language, l2_instrument, l2_track,
        b2p_g, exp_g, edp_g, genre_expert, genre_edited,
        b2p_a, exp_a, edp_a, about_expert, about_edited,
        b2p_l, exp_l, edp_l, language_expert, language_edited,
        b2p_i, exp_i, edp_i, instrument_expert, instrument_edited,
        count(tf) AS edit_count
    OPTIONAL MATCH (u)-[:LOVES]->(loved_track:Track)
    RETURN is_track_expert, album as track_album_expertise, artists as track_artist_expertise, genres, abouts, languages, instruments,
        l2_genre as l2_g, l2_about as l2_a, l2_language as l2_l, l2_instrument as l2_i, l2_track as l2_t,
        bp_min, b2p_g, exp_g, edp_g, genre_expert as genre_expertise, genre_edited,
        b2p_a, exp_a, edp_a, about_expert as about_expertise, about_edited,
        b2p_l, exp_l, edp_l, language_expert as language_expertise, language_edited,
        b2p_i, exp_i, edp_i, instrument_expert as instrument_expertise, instrument_edited,
        edit_count, count(loved_track) as love_count
    """
    res = read(code,user_id=user_id, track_id=track_id).data()

    #converting the result to pandas data frame so that we can manipulate the data as required
    expertises = {}
    expertise_df = json_normalize(res)

    expertise_df[['exp_g', 'edp_g', 'exp_a', 'edp_a', 'exp_l', 'edp_l', 'exp_i', 'edp_i']] = expertise_df[
        ['exp_g', 'edp_g', 'exp_a', 'edp_a', 'exp_l', 'edp_l', 'exp_i', 'edp_i']].fillna(value=0)
    expertise_df[['bp_min', 'b2p_g', 'b2p_a', 'b2p_l', 'b2p_i']] = expertise_df[
        ['bp_min', 'b2p_g', 'b2p_a', 'b2p_l', 'b2p_i']].fillna(value=5)
    expertise_df[['l2_g', 'l2_a', 'l2_l', 'l2_i', 'l2_t']] = expertise_df[
        ['l2_g', 'l2_a', 'l2_l', 'l2_i', 'l2_t']].fillna(value=0)

    for ind in expertise_df.index:
        if (len(expertise_df['genres'][ind]) > 0):
            for gen in expertise_df['genres'][ind]:
                expertise_key = "genres::" + gen
                if (expertise_key not in expertises.keys()):
                    expertises[expertise_key] = get_expertise_dict(my_badge=expertise_df['bp_min'][ind],
                                                                   ed_badge=expertise_df['b2p_g'][ind],
                                                                   my_exp=expertise_df['l2_g'][ind],
                                                                   ed_exp=expertise_df['edp_g'][ind])

        if (len(expertise_df['abouts'][ind]) > 0):
            for ab in expertise_df['abouts'][ind]:
                expertise_key = "about::" + ab
                if (expertise_key not in expertises.keys()):
                    expertises[expertise_key] = get_expertise_dict(my_badge=expertise_df['bp_min'][ind],
                                                                   ed_badge=expertise_df['b2p_a'][ind],
                                                                   my_exp=expertise_df['l2_a'][ind],
                                                                   ed_exp=expertise_df['edp_a'][ind])

        if (len(expertise_df['languages'][ind]) > 0):
            for ln in expertise_df['languages'][ind]:
                expertise_key = "languages::" + ln
                if (expertise_key not in expertises.keys()):
                    expertises[expertise_key] = get_expertise_dict(my_badge=expertise_df['bp_min'][ind],
                                                                   ed_badge=expertise_df['b2p_l'][ind],
                                                                   my_exp=expertise_df['l2_l'][ind],
                                                                   ed_exp=expertise_df['edp_l'][ind])

        if (len(expertise_df['instruments'][ind]) > 0):
            for inst in expertise_df['instruments'][ind]:
                expertise_key = "instruments::" + inst
                if (expertise_key not in expertises.keys()):
                    expertises[expertise_key] = get_expertise_dict(my_badge=expertise_df['bp_min'][ind],
                                                                   ed_badge=expertise_df['b2p_i'][ind],
                                                                   my_exp=expertise_df['l2_i'][ind],
                                                                   ed_exp=expertise_df['edp_i'][ind])

    #to give founder extra privilages
    if expertise_df['bp_min'][0] == 1:
        is_track_expert = True
    else:
        is_track_expert = expertise_df['is_track_expert'][0]

    is_track_exp_key = "is_track_expert"
    expertises[is_track_exp_key] = {'canEdit': bool(is_track_expert), 'myBadge': int(expertise_df['bp_min'][0]), 'hasEditedBadge': int(0),
                             "myExpertise": int(expertise_df['l2_t'][ind]), 'hasEditedExpertise': int(0)}


    #todo:delete dataframe...


    return expertises


def get_expertise_dict(my_badge, ed_badge, my_exp, ed_exp):
    #todo: only allow to edit if user listens to this feature more than threshold...hard coded as 10; will make it dynamic later with love_count
    #todo: change
    expertise_threshold = 10
    has_superpowers_override = False #setting badge privilages as false by default...should only be given to founder for now



    #user must atleast have heard the song threshold number of times to be able to edit
    if (my_exp > expertise_threshold):
        if (my_badge < ed_badge):
            key_expertise = {'canEdit': bool(True), 'myBadge': int(my_badge), 'hasEditedBadge': int(ed_badge),
                             "myExpertise": int(my_exp), 'hasEditedExpertise': int(ed_exp)}

        elif (my_badge == ed_badge):
            if (my_exp > ed_exp):
                key_expertise = {'canEdit': bool(True), 'myBadge': int(my_badge), 'hasEditedBadge': int(ed_badge),
                                 "myExpertise": int(my_exp), 'hasEditedExpertise': int(ed_exp)}
            else:
                key_expertise = {'canEdit': bool(False), 'myBadge': int(my_badge), 'hasEditedBadge': int(ed_badge),
                                 "myExpertise": int(my_exp), 'hasEditedExpertise': int(ed_exp)}

        elif (my_badge > ed_badge):
            key_expertise = {'canEdit': bool(False), 'myBadge': int(my_badge), 'hasEditedBadge': int(ed_badge),
                             "myExpertise": int(my_exp), 'hasEditedExpertise': int(ed_exp)}

    else:
        key_expertise = {'canEdit': bool(False), 'myBadge': int(my_badge), 'hasEditedBadge': int(ed_badge),
                         "myExpertise": int(my_exp), 'hasEditedExpertise': int(ed_exp)}

    if my_badge == 1:
        key_expertise['canEdit'] = True


    return key_expertise

# def get_friends_who_love_this_track(user_id, track_id):
#     code = """
#     MATCH(me:User {id: $user_id})
#     MATCH(track:Track {id: $track_id})
#     WITH me,track
#     MATCH (me)-[:IS_FRIENDS_WITH]->(u:User)-[:LOVES]->(track)
#     WITH me,track,u
#     MATCH (u)-[r:LISTENED_TO]->(track)
#     RETURN u.name as name, r.score as score
#     """
#     res = read(code, user_id=user_id, track_id=track_id).data()
#     return [{'friend': o['name'] , 'score': o['score']} for o in res]

#added by aditya
def get_friends_who_love_this_shared_track(user_id, shared_track_id):
    code = """
    MATCH(me:User {id: $user_id})
    MATCH(track:Track {id: $track_id})
    WITH me,track
    MATCH (me)-[s:SHARES_TRACK]->(f:User)-[:LOVES]->(track)
    WITH me,track,u
    MATCH (u)-[r:LISTENED_TO]->(track)
    RETURN u.name as name, r.score as score
    """
    res = read(code, user_id=user_id, track_id=shared_track_id).data()
    return [{'friend': o['name'] , 'score': o['score']} for o in res]

def get_top_people_recommendations(user_id):
    code = """
        MATCH (me:User {id: $user_id})
        MATCH (me)-[:LOVES]->(n)<-[:LOVES]-(u:User)
        OPTIONAL MATCH (me)-[re:IS_FRIENDS_WITH]-(u)
        RETURN count(re) > 0 AS isFriend, n.name AS item, u.username AS user
        """
    data = read(code, user_id=user_id).data()
    users = defaultdict(list)
    for o in data:
        users["{}&{}".format(o['user'],o['isFriend'])].append(o['item'])
    users = OrderedDict((k, v) for k, v in sorted(users.items(), key=lambda x: len(x), reverse=True))
    res = []
    for k,v in users.items():
        name, is_friend = k.split('&')
        res.append({'name': name, 'is_friend': is_friend, 'items': v})
    return res

def get_top_expert_people(user_id,tag_type, name):
    code = """
        MATCH (u:User)-[:IS_EXPERT_IN]->(:TrackFeature {tag_type: $tag_type, name: $name})
        MATCH (me {id: $user_id})-[:LOVES]->(n)<-[:LOVES]-(u)
        OPTIONAL MATCH (me)-[re:IS_FRIENDS_WITH]-(u)
        RETURN count(re) > 0 AS isFriend, n.name AS item, u.name AS user
        """
    data = read(code, user_id=user_id, tag_type=tag_type, name=name).data()
    users = defaultdict(list)
    for o in data:
        users["{}&{}".format(o['user'],o['isFriend'])].append(o['item'])
    users = OrderedDict((k, v) for k, v in sorted(users.items(), key=lambda x: len(x), reverse=True))
    res = []
    for k,v in users.items():
        name, is_friend = k.split('&')
        res.append({'name': name, 'is_friend': is_friend, 'items': v})
    return res

def get_top_badge_people(user_id,badge_name):
    code = """
        MATCH (u:User)-[:HAS_BADGE]->(:Badge {name: $badge_name})
        MATCH (me {id: $user_id})-[:LOVES]->(n)<-[:LOVES]-(u)
        OPTIONAL MATCH (me)-[re:IS_FRIENDS_WITH]-(u)
        RETURN count(re) > 0 AS isFriend, n.name AS item, u.name AS user
        """
    data = read(code, user_id=user_id, badge_name=badge_name).data()
    users = defaultdict(list)
    for o in data:
        users["{}&{}".format(o['user'],o['isFriend'])].append(o['item'])
    users = OrderedDict((k, v) for k, v in sorted(users.items(), key=lambda x: len(x), reverse=True))
    res = []
    for k,v in users.items():
        name, is_friend = k.split('&')
        res.append({'name': name, 'is_friend': is_friend, 'items': v})
    return res

def get_tracks_user_shared(user):
    # code="""
    # MATCH (p:User {username: $name})-[r:SHARES_TRACK]->(f:User)
    # RETURN collect(f.username) AS friends, r.track_mongo_id AS track_mongo_id, r.visualized AS visualized
    # """
    #res = read(code, name=user.name).data()

    # return [{'users': [User.from_name(frnd).to_dict(simplified=True) for frnd in o['friends']],
    #          # frnd for frnd in o['friends']
    #          'track_mongo_id': o['track_mongo_id'],
    #          'visualized': o['visualized']} for o in res]

    code = """
    MATCH (p:User {username: $name})-[r:SHARES_TRACK]->(f:User)
    MATCH (t:Track{id:r.track_mongo_id})
    RETURN collect(f.username) AS friends, r.track_mongo_id AS id, r.visualized AS visualized,t.cover_image_url as cover_image_url, t.name as name
    """
    res=read(code, name=user.name).data()
    #pprint(res)
    #todo:shared_by is actually shared_to..i've written like this because its written like this in firestore
    return [{'users': [User.from_name(frnd).to_dict(simplified=True) for frnd in o['friends']],   #frnd for frnd in o['friends']
             'id': o['id'],
             'name' : o['name'],
             'cover_image_url' : o['cover_image_url'],
             } for o in res]

def get_friends_who_love_this_track(user_id,track_id):

    code="""
    MATCH (me:User{id: $user_id})
    MATCH (t:Track{id:$track_mongo_id})
    MATCH (me)-[:IS_FRIENDS_WITH]-(friend:User)
    WITH me, friend, t
    MATCH (friend)-[:LOVES]->(t)
    RETURN friend.id as friend_id, friend.username as friend_name
    """
    #todo:add to track_friends in next_track_pipeline
    res = read(code,user_id=user_id, track_mongo_id=track_id).data()


    return [{'friend': o['friend_name'],
             'friendId': o['friend_id']
             } for o in res]

def get_loved_songs(user):
    print("Loved songs")
    code="""
    MATCH (me:User{username: $username})
    WITH me
    MATCH (me)-[:LOVES]->(t)
    RETURN t.id as track_mongo_id
    """
    res = read(code,username=user.name).data()

    return [{'track_mongo_id': o['track_mongo_id']} for o in res]

# Added by aditya
def notify_friends_share_track(user, shared_track_id: str):
    print("notify_friends_share_track")
    #tdoc = mm.Track.from_id(id=shared_track_id).to_dict()

    code ="""
    MATCH (me:User{username: $username})
    MATCH (t:Track{id:$track_mongo_id})
    WITH me,t
    MATCH (f:User)-[s:SHARES_TRACK{track_mongo_id:$track_mongo_id}]->(me:User)-[:LOVES]->(t:Track)
    RETURN f.id AS friend_id, t.name AS track_name, s.notified as notified,  s.visualized AS visualized
    """

    res = read(code, username=user.name, track_mongo_id=shared_track_id).data()
    pprint(res)
    return [{'friend_id': dm.UserManager.from_id(o['friend_id']),
             'track_name': o['track_name'],
             'visualized': o['visualized'],
             'notified': o['notified']} for o in res]

# def set_friend_notified(user, friend, shared_track_id):
#     print("setting friend notified")
#     code="""
#     MATCH (p:User {username: $username}),
#     (f:User {username: $friend_name})
#     MERGE (p)-[r:SHARES_TRACK {track_mongo_id: $track_mongo_id}]->(f)
#     SET r.notified = TRUE
#     RETURN f.username as friend, r.track_mongo_id as track_mongo_id, r.notified as notified
#     """
#
#     res = read(code, username=user.name, friend_name=friend.name, track_mongo_id=shared_track_id).data()
#     print("setting friend notified")

def get_tracks_for_improve_recommendation(user):
    #todo: for now we are only considering disliked tracks, can be distilled for even liked or bookmarked later on

    #todo:change score to last_score
    code="""
    MATCH (u:User {username: $user_name})
    MATCH (t:Track)
    MATCH (u)-[r:LISTENED_TO]->(t)
    WHERE r.last_score < 0
    RETURN t.id as disliked_mongo_id
    """
    res = read(code, user_name=user.name).data()
    return  [{
             'disliked_mongo_id': o['disliked_mongo_id'],
             } for o in res]

