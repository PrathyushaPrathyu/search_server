from neo_db import *

# TODO:REFACTOR This dont actually constitutes a node, but rather just some
# Convenience functions to use with the database
class Node:
  @staticmethod
  def write(code:str, **kwargs):
    func = lambda tx, **kwargs: tx.run(code, **kwargs)
    with driver.session() as session:
      return session.write_transaction(func, **kwargs)

  @staticmethod
  def read(code:str, **kwargs):
    func = lambda tx, **kwargs: tx.run(code, **kwargs)
    with driver.session() as session:
      return session.read_transaction(func, **kwargs)
