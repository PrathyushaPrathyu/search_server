from ..track import TrackFeature

class Expertise:
  def __init__(self, track_feature: TrackFeature, count):
    self.track_feature = track_feature
    self.count = count
    self.minimum_count = 10
    self.has_expertise = bool(self.minimum_count <= self.count)

  def to_dict(self):
    return {
      'tag': self.track_feature.to_dict(),
      'count': self.count,
      'minimum_count': self.minimum_count,
      'has_expertise': self.has_expertise,
    }

  def __repr__(self):
    return f'<{self.__class__.__name__}' \
           f' track_feature: {self.track_feature},' \
           f' count: {self.count},' \
           f' minimum_count: {self.minimum_count},' \
           f' has_expertise: {self.has_expertise}>'
