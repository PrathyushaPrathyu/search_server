class TrackFeatureDeprecated:
  def __init__(self, name, score=None):
    self.name = name
    self.score = score

  @property
  def tag_type(self) -> str: raise NotImplementedError

  def to_dict(self):
    return {'tagType': self.tag_type, 'name': self.name}
