from neo_db import *
from .track_feature import TrackFeatureDeprecated

class Genre(TrackFeatureDeprecated):
  @property
  def tag_type(self) -> str: return 'genres'

  def to_dict(self):
    return {'tagType': 'genres', 'name': self.name}

  @staticmethod
  def create_constraints():
    with driver.session() as session:
      func = lambda tx: tx.run('CREATE CONSTRAINT ON (genre:Genre) ASSERT genre.name IS UNIQUE')
      session.write_transaction(func)

  def __repr__(self):
    return f'{self.__class__.__name__}: {self.name}'
