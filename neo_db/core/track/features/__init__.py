from .artist import Artist
from .genre import Genre
from .language import Language
from .about import About
from .instrument import Instrument
