from neo_db import *
from .track_feature import TrackFeatureDeprecated

# TODO: Artist class name is taken by Person:Artist, need to rename this class
class Artist(TrackFeatureDeprecated):
  @property
  def tag_type(self) -> str: return 'artist'

  @staticmethod
  def create_constraints():
    with driver.session() as session:
      func = lambda tx: tx.run('CREATE CONSTRAINT ON (a:Artist) ASSERT a.name IS UNIQUE')
      session.write_transaction(func)

  def __repr__(self):
    return f'{self.__class__.__name__}: {self.name}'