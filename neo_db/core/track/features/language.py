from neo_db import *
from .track_feature import TrackFeatureDeprecated

class Language(TrackFeatureDeprecated):
  @property
  def tag_type(self) -> str: return 'language'

  @staticmethod
  def create_constraints():
    with driver.session() as session:
      func = lambda tx: tx.run('CREATE CONSTRAINT ON (l:Language) ASSERT l.name IS UNIQUE')
      session.write_transaction(func)

  def __repr__(self):
    return f'{self.__class__.__name__}: {self.name}'
