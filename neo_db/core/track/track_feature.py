class TrackFeature:
  def __init__(self, name:str, tag_type:str):
    self.name = name
    self.tag_type = tag_type

  def to_dict(self):
    return {'tagType': self.tag_type, 'name': self.name}

  def __repr__(self):
    return f'<{self.__class__.__name__} name: {self.name}, tag_type: {self.tag_type}>'
