from neo_db import *
from .features import *

class Track:
  def __init__(
      self,
      mongo_id:str,
      name:str,
      artists:List[Artist],
      genres:List[Genre],
      instruments:List[Instrument],
      languages:List[Language],
      valence:float,
      loudness:float,
      deepness:float,
      topics=List[About],
  ):
    self.mongo_id = mongo_id
    self.valence = valence
    self.loudness = loudness
    self.deepness = deepness
    self.name = name
    self.artists = artists
    self.instruments = instruments
    self.genres = genres
    self.languages = languages
    self.topics = topics

  def create(self):
    with driver.session() as session:
      session.write_transaction(self._create, track=self)
    return self

  @classmethod
  def from_mongodb_doc(cls, doc):
    return cls(
      mongo_id=str(doc['_id']),
      name=doc['name'],
      # TODO: Should use enums instead of strings
      loudness=doc['loudness'],
      valence=doc['valence'],
      deepness=doc['deepness'],
      genres=[Genre(name=o['name'].title(), score=o['score']) for o in doc['genres']],
      instruments=[Instrument(name=o['name'].title(), score=o['score']) for o in doc['instruments']],
      languages=[Language(name=o['name'].title(), score=o['score']) for o in doc['languages']],
      artists=[Artist(name=o['name'].title()) for o in doc['artists']],
      topics=[About(name=o['name'].title(), score=o['score']) for o in doc['about']],
    )

  @staticmethod
  def _create(tx, track:'Track'):
    # TODO: Remove hardcoded tag types
    return tx.run("""
    MERGE (t:Track {id: $mongo_id, name: $name, loudness: $loudness, deepness: $deepness, valence: $valence})
    
    WITH t 
    UNWIND $artists_names AS artist_name
    MERGE (artist:TrackFeature:Person:Artist {name: artist_name})
    MERGE (t)-[:BY_ARTIST]->(artist)
    
    WITH t
    UNWIND $genre_dicts AS genre_dict
    MERGE (genre:TrackFeature:Genre {name: genre_dict.name})
    MERGE (t)-[r:HAS_GENRE]->(genre)
    ON CREATE SET r.score = genre_dict.score
    
    
    WITH t
    UNWIND $language_dicts AS language_dict
    MERGE (language:TrackFeature:Language {name: language_dict.name})
    MERGE (t)-[r:IN_LANGUAGE]->(language)
    ON CREATE SET r.score = language_dict.score
        
    WITH t
    UNWIND $about_dicts AS about_dict
    MERGE (about:TrackFeature:About {name: about_dict.name})
    MERGE (t)-[r:IS_ABOUT]->(about)
    ON CREATE SET r.score = about_dict.score
    
    WITH t
    UNWIND $instrument_dicts AS instrument_dict
    MERGE (instrument:TrackFeature:Instrument {name: instrument_dict.name})
    MERGE (t)-[r:HAS_INSTRUMENT]->(instrument)
    ON CREATE SET r.score = instrument_dict.score
    """,
                  mongo_id=track.mongo_id,
                  name=track.name,
                  valence=track.valence,
                  loudness=track.loudness,
                  deepness=track.deepness,
                  artists_names=[o.name for o in track.artists],
                  genre_dicts=[{'name': o.name, 'score': o.score} for o in track.genres],
                  language_dicts=[{'name': o.name, 'score': o.score} for o in track.languages],
                  instrument_dicts=[{'name': o.name, 'score': o.score} for o in track.instruments],
                  about_dicts=[{'name': o.name, 'score': o.score} for o in track.topics],
                  )

  @staticmethod
  def create_constraints():
    with driver.session() as session:
      func = lambda tx: tx.run('CREATE CONSTRAINT ON (t:Track) ASSERT t.mongo_id IS UNIQUE')
      session.write_transaction(func)

  def __repr__(self):
    return f'<{self.__class__.__name__}: {self.name}>'
