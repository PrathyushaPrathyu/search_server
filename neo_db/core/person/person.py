from ..node import Node

class Person(Node):
  def __init__(self, name:str, mongo_id:str):
    # TODO: Change name to username
    self.name = name
    self.mongo_id = mongo_id

  def to_dict(self, simplified:bool): raise NotImplementedError
