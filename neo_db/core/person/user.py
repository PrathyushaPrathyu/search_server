from neo_db import *
from .person import Person
from ..badge import Badge
from ..expertise import Expertise
from ..track import TrackFeature, Track

class User(Person):
  def __init__(self, name: str, mongo_id: str, currency:float=0.0):
    super().__init__(name=name, mongo_id=mongo_id)
    self.currency = currency

  def create(self) -> 'User':
    code = "CREATE (:Person:User {username: $name, id: $mongo_id, currency: $currency})"
    self.write(code, name=self.name, mongo_id=self.mongo_id, currency=self.currency)
    return self

  def delete(self) -> None:
    code = 'MATCH (u:User {username: $user_name}) DETACH DELETE u'
    self.write(code, user_name=self.name)

  def modify_currency(self, v:float):
    code = """
    MATCH (u:User {username: $user_name})
    SET u.currency = u.currency + $v
    """
    self.write(code, user_name=self.name, v=v)

  def add_badge(self, badge:Badge) -> None:
    code = """
    MATCH (u:User {id: $mongo_id})
    MATCH (b:Badge {name: $badge_name})
    MERGE (u)-[:HAS_BADGE]->(b)
    """
    self.write(code, mongo_id=self.mongo_id, badge_name=badge.name)

  def get_badges(self) -> List[Badge]:
    code = 'MATCH (:User {id: $mongo_id})-[:HAS_BADGE]->(b:Badge) RETURN b'
    data = self.read(code=code, mongo_id=self.mongo_id).data()
    return [Badge.from_node(o['b']) for o in data]

  def get_expertises(self) -> List[Expertise]:
    code = """
    MATCH (u:User {username: $user_name})-[:IS_EXPERT_IN]->(t)
    WITH u,t
    MATCH (u)-[r:LISTENED_TO]->(t) 
    RETURN t, r.count as count, r.score as score
    """
    data = self.read(code=code, user_name=self.name).data()
    return [{'name': o['t']['name'], 'count': o['count'], 'score': o['score']} for o in data] if data else []

  def get_expertises_for_track(self, track:Track):
    code = """
    MATCH (t:Track {id: $mongo_id})--(tf:TrackFeature)
    OPTIONAL MATCH (u:User {username: $user_name})-[rl:LISTENED_TO]->(t)
    WITH u,t
    OPTIONAL MATCH (u)-[re:IS_EXPERT_IN]->(t:Track {id: $mongo_id})
    RETURN re, r.count as count
    """
    data = self.read(code=code, user_name=self.name, mongo_id=track.mongo_id).single()
    track_expertise = [Expertise(TrackFeature(name=track.name, tag_type='track'), count=data['count'])] if data else []
    return track_expertise + tracks_features_expertise

  def add_friend(self, friend:'User') -> None:
    code = """
    MATCH (p:User {username: $name})
    MATCH (f:User {username: $friend_name})
    MERGE (p)-[:IS_FRIENDS_WITH]-(f)
    """
    self.write(code, name=self.name, friend_name=friend.name)

  def get_friends(self):
    code = """
    MATCH (u:User {username: $user_name})-[:IS_FRIENDS_WITH]-(f:User)
    RETURN f as user
    """
    data = self.read(code, user_name=self.name).data()
    return [User(mongo_id=o['user']['id'], name=o['user']['username']) for o in data] if data else []

  def get_users_with_query_openbeta(self,query:str):
    # code = """
    # Match (u:User) WHERE u.username STARTS WITH $prefix
    # RETURN u AS user
    # """

    code = """
    Match (u:User) WHERE u.username = $username
    RETURN u AS user
    """

    data = self.read(code, username=query).data()
    return [User(mongo_id=o['user']['id'], name=o['user']['username']) for o in data] if data else []


  def register_listen(self, track_id, score, time):
      # max_score here can be 2

      increment = 1
      feature_expertise_threshold = 10
      track_expertise_threshold = 2
      track_love_threshold = 2      # One love gives 2
      feature_love_threshold = 10

      self._register_track_listen(track_id=track_id, increment=increment, love_threshold=track_love_threshold,
                                  expertise_threshold=track_expertise_threshold,score=score, time=time)
      self._register_feature_listen(track_id=track_id, increment=increment, score=score, time=time,
                                    love_threshold=feature_love_threshold, expertise_threshold=feature_expertise_threshold)


  def _register_track_listen(self,
                               track_id:str,
                               increment:int,
                             love_threshold:float,
                             expertise_threshold:float,
                             time,
                               score):

    code = """
    MATCH (t:Track {id: $mongo_id})
    MATCH (u:User {username: $user_name})
    // Mark song as listened
    MERGE (u)-[r:LISTENED_TO]->(t)
    ON CREATE SET r.count=0, r.score=0.0, r.last_listened= $time
    SET r.count = r.count + $increment,
        r.score = r.score + $score,
        r.last_listened = $time,
        r.last_score = $score
    WITH u,r,t
    MATCH (u)-[r]->(t)
    WHERE (r.score >= $love_threshold)
    MERGE (u)-[rl:LOVES]->(t)
    WITH u,r,t
    MATCH (u)-[r]->(t)
    WHERE (r.count >= $listen_threshold)
    MERGE (u)-[e:IS_EXPERT_IN]->(t)
    SET e.expertise = r.count
    //TODO : Remove at places where love count is no longer true
    """
    #todo: when r.score goes below love_threshold, DELETE LOVE RELATIONSHIP
    code_delete_love ="""
    MATCH (t:Track {id: $mongo_id})
    MATCH (u:User {username: $user_name})
    MATCH (u)-[r:LISTENED_TO]->(t)
    WITH u,r,t
    MATCH (u)-[lov:LOVES]->(t)
    WHERE (r.score < $love_threshold)
    DELETE lov
    """

    #todo:r.last_score = $.score

    self.write(code, mongo_id=track_id,
                user_name=self.name,
                score=score,
                increment=increment,
               love_threshold=love_threshold,
               listen_threshold=expertise_threshold,
               time=time)

    self.write(code_delete_love, mongo_id=track_id,
            user_name=self.name,
            score=score,
           love_threshold=love_threshold)


  def _register_feature_listen(self,
                               track_id:str,
                               increment:int,
                               love_threshold:int,
                               expertise_threshold:int,
                               time,
                               score):

    code = """
    MATCH (t:Track {id: $mongo_id})--(tf:TrackFeature)
    MATCH (u:User {username: $user_name})
    MERGE (u)-[r:LISTENED_TO]->(tf)
    ON CREATE SET r.count = 0, r.score=0, r.last_listened= $time
    SET r.count = r.count + $increment,
        r.score = r.score + $score,
        r.last_listened = $time,
        r.last_score = $score
    WITH u,r,tf
    MATCH (u)-[r]->(tf)
    WHERE (r.score > $love_threshold)
    MERGE (u)-[:LOVES]->(tf)
    WITH u,r,tf
    MATCH (u)-[r]->(tf)
    WHERE (r.count > $expertise_threshold)
    MERGE (u)-[e:IS_EXPERT_IN]->(tf)
    SET e.expertise = r.count
    """

    code_delete_love ="""
    MATCH (t:Track {id: $mongo_id})--(tf:TrackFeature)
    MATCH (u:User {username: $user_name})
    MATCH (u)-[r:LISTENED_TO]->(tf)
    WITH u,r,tf
    MATCH (u)-[lov:LOVES]->(tf)
    WHERE (r.score < $love_threshold)
    DELETE lov
    """
    #todo:SET e.expertise = r.count SET e.expertise = r.count
    # set e.count added by aditya
    self.write(code,
               mongo_id=track_id,
               user_name = self.name,
               increment=increment,
               love_threshold=love_threshold,
               expertise_threshold=expertise_threshold,
               time = time,
               score=score)

    self.write(code_delete_love, mongo_id=track_id,
            user_name=self.name,
            score=score,
           love_threshold=love_threshold)


  def reset_listening_history(self):
      code = """
      MATCH (u:User {username : $user_name})
      WITH u
      MATCH (u)-[rt]->(:Track)
      MATCH (u)-[rtf]->(:TrackFeature)
      DELETE rt,rtf
      """
      self.write(code, user_name=self.name)

  def to_dict(self, simplified:bool=True) -> Dict[str, str]:
    if simplified: return {'username': self.name, 'mongo_id': self.mongo_id}
    return {
      'username': self.name,
      'mongo_id': self.mongo_id,
      'currency': self.currency,
      'badges': [b.to_dict() for b in self.get_badges()],
      'expertises': [o for o in self.get_expertises()],
    }


  @classmethod
  def from_mongo_id(cls, mongo_id):
    code = "MATCH (p:User {id: $mongo_id}) RETURN p"
    res = cls.read(code, mongo_id=mongo_id).single()[0]
    name = res['username']
    mongo_id = res['id']
    return cls(mongo_id=mongo_id, name=name)

  @classmethod
  def from_name(cls, name):
    code = "MATCH (p:User {username: $name}) RETURN p"
    res = cls.read(code, name=name).single()[0]
    name = res['username']
    mongo_id = res['id']
    return cls(mongo_id=mongo_id, name=name)

  @classmethod
  def from_node(cls, node):
    return cls(**node)

  def __repr__(self):
    return f'<{self.__class__.__name__} (name: {self.name})>'
