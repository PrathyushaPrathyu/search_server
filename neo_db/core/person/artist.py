from .person import Person

class Artist(Person):
  def get_num_streams(self):
    code = """
    MATCH (art:Artist {name: $name})<-[:HAS_ARTIST]-()<-[listened:LISTENED]-(u:User)
    RETURN SUM(listened.n_played) as num_streams
    """
    return self.read(code, name=self.name).single()['num_streams']

  def to_dict(self, simplified: bool):
    raise NotImplementedError

