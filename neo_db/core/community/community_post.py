from ..node import Node
from ..person import Person, User

class CommunityPost(Node):
  def __init__(self, person:Person, title:str, content:str, id:str=None, added_at:str=None):
    self.person = person
    self.title, self.content = title, content
    self._id = id
    self._added_at = added_at

  @property
  def id(self) -> str:
    if self._id is None: raise ValueError('First create post before getting its id')
    return self._id

  @property
  def added_at(self) -> str:
    if self._added_at is None: raise ValueError('Added at is null, is this node already created?')
    return self._added_at

  def create(self) -> 'CommunityPost':
    code = """
    MATCH (p:Person {name: $posted_by})
    CREATE (post:CommunityPost {id: randomUUID(), title: $title, content: $content, added_at: datetime()})
    MERGE (post)<-[:COMMUNITY_POSTED]-(p)
    RETURN post.id AS id
    """
    res = self.write(code, title=self.title, content=self.content, posted_by=self.person.name).single()
    self._id = res['id']
    return self

  def to_dict(self):
    return {
      'id': self.id,
      'created_by': self.person.to_dict(simplified=True),
      'title': self.title,
      'content': self.content,
      'added_at': self.added_at,
    }

  @classmethod
  def from_node(cls, node):
    # TODO: Currently just handles users
    code = 'MATCH (u:User)-[:COMMUNITY_POSTED]->(:CommunityPost {id: $post_id}) RETURN u'
    user_node = cls.read(code, post_id=node['id']).single()[0]
    user = User.from_node(node=user_node)
    return cls(person=user, **node)

  @staticmethod
  def create_constraints():
    code = 'CREATE CONSTRAINT ON (c:CommunityPost) ASSERT c.id IS UNIQUE'
    CommunityPost.write(code)

  def __repr__(self):
    return f'<{self.__class__.__name__} (title: {self.title})>'
