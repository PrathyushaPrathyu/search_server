from ..node import Node
from ..person import Person, User
from .community_post import CommunityPost
from neo_db import *

class Community(Node):
  def __init__(self, name:str, id:str=None):
    self.name = name
    self._id = id

  @property
  def id(self) -> str:
    if self._id is None: raise ValueError('First create post before getting its id')
    return self._id

  def create(self, person:Person):
    code = """
    MATCH (p:Person {name: $person_name})
    CREATE (c:Community {id: randomUUID(), name: $community_name})-[:CREATED_BY]->(p)
    CREATE (c)-[:HAS_PERSON]->(p)
    """
    self.write(code, community_name=self.name, person_name=person.name)
    return self

  def delete(self):
    code = 'MATCH (c:Community {name: $name}) DETACH DELETE (c)'
    self.write(code, name=self.name)

  def add_post(self, post:CommunityPost):
    code = """
    MATCH (c:Community {name: $name})
    MATCH (p:CommunityPost {id: $post_id})
    MERGE (p)<-[:HAS_POST]-(c)
    """
    self.write(code, name=self.name, post_id=post.id)

  def add_user(self, user:Person):
    code = """
    MATCH (p:Person {name: $person_name})
    MATCH (c:Community {name: $community_name})
    MERGE (p)<-[:HAS_PERSON {joined_at: datetime()}]-(c)
    """
    self.write(code, person_name=user.name, community_name=self.name)

  def get_posts(self) -> List[CommunityPost]:
    # TODO: Uses ORDER BY that is not well optimized for huge lists. Consider using MongoDB for storing posts.
    code = """
    MATCH (c:Community {name: $community_name})-[:HAS_POST]->(post:CommunityPost)
    RETURN post ORDER BY post.added_at DESC
    """
    res = self.read(code, community_name=self.name).data()
    return [CommunityPost.from_node(o['post']) for o in res]

  def get_users(self) -> List[Person]:
    # TODO: Currently only get Users
    code = """
    MATCH (c:Community {name: $community_name})-[:HAS_PERSON]->(p:Person)
    RETURN p
    """
    res = self.read(code, community_name=self.name).data()
    return [User.from_node(node=o['p']) for o in res]

  def to_dict(self, simplified=True):
    if simplified:
      return {'name': self.name}
    else:
      return {
        'id': self.id,
        'name': self.name,
        'posts': [o.to_dict() for o in self.get_posts()],
      }

  @classmethod
  def from_name(cls, name:str):
    code = 'MATCH (c:Community {name: $community_name}) RETURN c'
    node = cls.read(code, community_name=name).single()[0]
    return cls.from_node(node=node)

  @classmethod
  def from_node(cls, node):
    return cls(**node)

  @classmethod
  def get_communities(cls) -> List['Community']:
    code = 'MATCH (c:Community) RETURN c'
    nodes = [o['c'] for o in cls.read(code).data()]
    return [Community.from_node(node) for node in nodes]

  @staticmethod
  def create_constraints():
    code = 'CREATE CONSTRAINT ON (c:Community) ASSERT c.name IS UNIQUE'
    Community.write(code)
