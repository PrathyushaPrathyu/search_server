from neo_db.core.node import Node

class Badge(Node):
  def __init__(self, name:str, priority):
    self.name = name
    self.priority = priority

  def create(self):
    code = 'MERGE (:Badge {name: $name})'
    self.write(code, name=self.name)

  def delete(self):
    code = 'MATCH (b:Badge {name: $name}) DETACH DELETE b'
    self.write(code, id=self.name)

  def to_dict(self) -> dict:
    return {'name': self.name, 'priority':self.priority}

  @classmethod
  def from_node(cls, node):
    return cls(**node)

  @staticmethod
  def delete_all():
    code = 'MATCH (b:Badge) DETACH DELETE b'
    Badge.write(code)

  def __repr__(self):
    return f'<{self.__class__.__name__} id: {self.id}, name: {self.name}>'

class Badges:
  beta_user_badge = Badge(name='Champion', priority=3)
  founder_badge = Badge(name='Founder', priority = 1)

  @staticmethod
  def all(): return [Badges.beta_user_badge, Badges.founder_badge]
