from .badges import *

def create_badges(delete:bool=False):
  if delete: Badge.delete_all()
  for badge in Badges.all(): badge.create()
