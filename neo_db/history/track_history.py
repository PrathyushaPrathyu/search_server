from sortedcollections import ValueSortedDict
from neo_db import *
from .track_history_item import TrackHistoryItem

# TODO: track_history_items can have repeated tracks if using global or multiple_contexts
class TrackHistory:
  def __init__(self, track_history_items:List[TrackHistoryItem]):
    self.items = track_history_items
    self.date_sorted = ValueSortedDict({o.track_id: o.last_played for o in self.items})
    self.score_sorted = ValueSortedDict({o.track_id: o.score for o in self.items})

  @property
  def all_played_tracks(self):
    return list(self.score_sorted)

  @property
  def favorites(self):
    left = self.score_sorted.bisect_key_left(2.0)
    ids = self.score_sorted.keys()[left:][::-1]
    return ids

  @property
  def short(self):
    ids = self.date_sorted.keys()[::-1][:30]
    return ids

  @property
  def long(self):
    ids = self.score_sorted.keys()[::-1][:30]
    return ids

  @classmethod
  def from_context(cls, user:User, context:Context):
    with driver.session() as session:
      res = session.read_transaction(cls._get_history, user=user, context=context)
    items = [TrackHistoryItem.from_neo_relationship(track_id=o['track_id'],
                                                    track_name=o['track_name'],
                                                    relationship=o['relationship'],)
             for o in res.data()]
    return cls(track_history_items=items)

  @staticmethod
  def _get_history(tx, *, user:User, context:Context):
    return tx.run("""
    MATCH (u:User {username: $user_name})-[r:LISTENED]-(t:Track)
    WHERE ($context_name is NULL) or (r.context=$context_name)
    RETURN t.mongo_id as track_id, t.name as track_name, r as relationship
    """, user_name=user.name, context_name=context.name)
