class TrackHistoryItem:
  def __init__(
      self,
      *,
      track_id:str,
      track_name:str,
      reward:float,
      score:float,
      last_played:float,  # TODO: Switch to datetime
      last_action:str,
      n_played:int,
      last_reward:float,
  ):
    self.track_id = track_id
    self.track_name = track_name
    self.reward = reward
    self.score = score
    self.last_played = last_played
    self.last_action = last_action
    self.n_played = n_played
    self.last_reward = last_reward

  @classmethod
  def from_neo_relationship(cls, track_id:str, track_name:str, relationship):
    return cls(
      track_id=track_id,
      track_name=track_name,
      reward=relationship['reward'],
      score=relationship['score'],
      last_played=relationship['last_played'],
      n_played=relationship['n_played'],
      last_reward=relationship['last_reward'],
      last_action=relationship['last_action'],
    )

  def __repr__(self):
    return f'{self.__class__.__name__}: ({self.track_name}, {self.track_id})'
