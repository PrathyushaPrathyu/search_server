from neo_db import *

def search_user(query:str) -> List[User]:
  with driver.session() as session:
    res = session.read_transaction(_search_user, query=query).data()
  return [User.from_node(o['user']) for o in res]

def _search_user(tx, query:str):
  return tx.run("""
    Match (u:User) WHERE u.username STARTS WITH $prefix
    RETURN u AS user
    """, prefix=query)
