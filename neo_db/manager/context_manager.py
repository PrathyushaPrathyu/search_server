from neo_db import *

class ContextManager(Node):
  def __init__(self, user:User, context:Context):
    self.user = user
    self.context = context
    self.history = None
    # self.update()

  @property
  def all_played_tracks(self):
    return self.history.all_played_tracks

  @property
  def favorites(self):
    return self.history.favorites

  @property
  def short(self):
    return self.history.short

  @property
  def long(self):
    return self.history.long

  def register_action(self, *, track:Track, action:str, reward:float):
    increment = 0
    expertise_threshold = 10
    love_threshold = 3
    if action != 'Search':
      increment = 1

    with driver.session() as session:
      session.write_transaction(
        self._register_action,
        user=self.user,
        context=self.context,
        track=track,
        action=action,
        reward=reward,
        increment=increment,
        love_threshold=love_threshold,
        expertise_threshold=expertise_threshold
      )

    # TODO: Removing update here can improve performance
    self._register_feature_expertise(track=track,
                                     increment=increment,
                                     reward=reward,
                                     love_threshold=love_threshold,
                                     expertise_threshold=expertise_threshold
                                     )
    # self.update()


  def update(self):
    self.history = TrackHistory.from_context(user=self.user, context=self.context)

  @staticmethod
  def _register_action(tx, *, user:User, context:Context, track:Track, action:str, reward:float, increment:int, love_threshold, expertise_threshold):
    return tx.run("""
    MATCH (t:Track {id: $mongo_id})
    MATCH (u:User {username: $user_name})
    // Mark song as listened
    MERGE (u)-[r:LISTENED {context: $context_name}]->(t)
    ON CREATE SET r.n_played=0, r.reward=0.0, r.action_history=[]
    SET r.n_played = r.n_played + $increment,
        r.last_played = datetime(),
        r.last_action = $action_name,
        r.last_reward = $reward,
        r.reward = r.reward + $reward,
        r.action_history = r.action_history + $action_name,
        r.score = r.reward
    // Check whether the user loves this song
    WITH u,r,t
    MATCH (u)-[r]->(t)
    WHERE (r.reward > $love_threshold)
    MERGE (u)-[:LOVES]->(t)
    // Check for expertise for song
    WITH u,r,t
    MATCH (u)-[r]->(t)
    WHERE (r.n_played > $expertise_threshold)
    MERGE (u)-[:IS_EXPERT_IN]->(t)
    """,
                  mongo_id=track.mongo_id,
                  user_name=user.name,
                  context_name=context.name,
                  reward=reward,
                  action_name=action,
                  increment=increment,
                  love_threshold=love_threshold,
                  expertise_threshold=expertise_threshold
                  )

  def _register_feature_expertise(self,
                                  track:Track,
                                  increment:int,
                                  love_threshold:int,
                                  expertise_threshold:int,
                                  reward):
    code = """
    MATCH (t:Track {id: $mongo_id})--(tf:TrackFeature)
    MATCH (u:User {username: $user_name})
    MERGE (u)-[r:EXPERTISE]->(tf)
    ON CREATE SET r.count = 0
    SET r.count = r.count + $increment
    WITH u,r,tf
    MATCH (u)-[r]->(tf)
    WHERE (r.count > $expertise_threshold)
    MERGE (u)-[:IS_EXPERT_IN]->(tf)
    """

    self.write(code,
               mongo_id=track.mongo_id,
               user_name = self.user.name,
               increment=increment,
               love_threshold=love_threshold,
               expertise_threshold=expertise_threshold,
               reward=reward)

    # code = """
    # MATCH (t:Track {mongo_id: $mongo_id})
    # MATCH (u:User {name: $user_name})
    # MERGE (u)-[r:EXPERTISE]->(t)
    # ON CREATE SET r.count = 0
    # SET r.count = r.count + $increment
    # """
    # self.write(code,
    #            mongo_id=track.mongo_id,
    #            user_name = self.user.name,
    #            increment=increment,
    #            love_threshold=love_threshold,
    #            expertise_threshold=expertise_threshold)