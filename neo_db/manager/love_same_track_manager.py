from neo_db import *

class LoveSameTrackManager:
  def __init__(self, user:User):
    self.user = user

  def visualized(self):
    with driver.session() as session:
      session.write_transaction(self._visualized, user=self.user)

  def get_received_broadcasted_tracks(self):
    """Tracks shared to me"""
    with driver.session() as session:
      res = session.read_transaction(self._get_tracks_broadcasted_to_user, user=self.user)
    return [{
      'users': [User.from_name(name).to_dict(simplified=True) for name in o['friends']],
      'track_mongo_id': o['track_mongo_id'],
      'visualized': o['visualized'],
    } for o in res]

  def get_sent_broadcasted_tracks(self):
    """Tracks that this user has shared with other people"""
    with driver.session() as session:
      res = session.read_transaction(self._get_tracks_user_broadcasted, user=self.user)
    return res

  @staticmethod
  def _get_tracks_user_broadcasted(tx, user:User):
    return tx.run("""
    MATCH (p:User {username: $name})-[r:BROADCASTS_TRACK]->(f:User)
    RETURN f.name AS user, r.track_mongo_id AS track_mongo_id, r.visualized AS visualized
    """, name=user.name)

  @staticmethod
  def _get_tracks_broadcasted_to_user(tx, user:User):
    """Get the tracks that were shared to this user"""
    return tx.run("""
    MATCH (p:User {username: $user_name})<-[r:BROADCASTS_TRACK]-(f:User)
    WITH r.track_mongo_id AS track_mongo_id, collect(f.name) AS friends, r.visualized AS visualized
    RETURN friends, track_mongo_id, size(friends) AS score, visualized
    """, user_name=user.name)

  @staticmethod
  def _broadcast_track(tx, user:User, track:Track):
    return tx.run("""
    MATCH (p:User {username: $user_name})-[:FRIENDS]-(f:User)
    MERGE (p)-[:BROADCASTS_TRACK {track_mongo_id: $track_mongo_id, visualized: FALSE}]->(f)
    """, user_name=user.name, track_mongo_id=track.mongo_id)

  @staticmethod
  def _visualized(tx, user:User):
    return tx.run("""
    MATCH (u:User {username: $user_name})<-[r:BROADCASTS_TRACK]-(:User)
    SET r.visualized = TRUE
    """, user_name=user.name)
