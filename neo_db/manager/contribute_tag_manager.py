from mongo_manager import MusicTagType
from neo_db import *


class ContributeTagManager(Node):
  def add_tag(self, track_id:str, music_tag):
    relationship_label = self._get_relationship_label(tag_type=music_tag.tag_type)
    code = """
    MATCH (t:Track {id: $track_id})
    MERGE (tf:TrackFeature {name: $feature_name, tag_type:$tag_type})
    MERGE (t)-[:%s]->(tf)
    """ % relationship_label
    self.write(
      code,
      track_id=track_id,
      feature_name=music_tag.name,
      tag_type=music_tag.tag_type.name,
    )

  def _get_relationship_label(self, tag_type: MusicTagType):
    _tag2relation = {
      MusicTagType.genres: 'HAS_GENRE',
      MusicTagType.language: 'HAS_LANGUAGE',
      MusicTagType.artist: 'HAS_ARTIST',
      MusicTagType.about: 'HAS_ABOUT',
      MusicTagType.emotion: 'HAS_EMOTION',
      MusicTagType.instrument: 'HAS_INSTRUMENT',
    }
    return _tag2relation[tag_type]
