from neo_db import *

class FriendsHistoryManager:
  def __init__(self, user:User):
    self.user = user

  def friends_reaction(self, track_id) -> FriendsReactions:
    with driver.session() as session:
      res = session.write_transaction(self._friends_reaction, user=self.user, track_id=track_id).data()
    return FriendsReactions(reactions=res)

  @staticmethod
  def _friends_reaction(tx, user:User, track_id:Track):
    return tx.run("""
    MATCH (u:User {username: $user_name})-[:FRIENDS]-(f:User)-[r:LISTENED]->(t:Track {id: $track_id})
    WHERE r.score > 1.0
    RETURN f AS user, r AS reaction, t AS track
    """, user_name=user.name, track_id=track_id)

