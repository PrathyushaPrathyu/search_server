from neo_db import *

class SessionManager:
  def __init__(self, user:User):
    self.user = user
    self.seed_track_id = None
    self.seed_track_reward = None
    self.played_track_ids = []
    self.update()

  # TODO: Use action enum instead of string
  def register_action(self, track:Track, action:str, reward:float):
    # Update seed track
    if (self.seed_track_id is None
        or action == 'Search'
        or reward >= self.seed_track_reward):
      if action != 'LeftSwipe':
        self.set_seed_track(track=track, reward=reward)
    self.mark_as_played(track=track)
    self.update()

  def set_seed_track(self, track:Track, reward:float):
    with driver.session() as session:
      session.write_transaction(self._delete_seed_track, user=self.user)
      session.write_transaction(self._set_seed_track, user=self.user, track=track, reward=reward)
    self.update()

  def mark_as_played(self, track:Track):
    with driver.session() as session:
      session.write_transaction(self._mark_as_played, user=self.user, track=track)

  def update(self):
    with driver.session() as session:
      seed_track = session.read_transaction(self._get_seed_track, user=self.user).single()
      played = session.read_transaction(self._get_played_tracks, user=self.user).data()
    if seed_track:
      self.seed_track_id = seed_track['track_id']
      self.seed_track_reward = seed_track['reward']
    else:
      self.seed_track_id = None
      self.seed_track_reward = None
    self.played_track_ids = [o['track_id'] for o in played]

  def reset_session(self):
    with driver.session() as session:
      session.write_transaction(self._delete_seed_track, user=self.user)
      session.write_transaction(self._reset_listened_tracks, user=self.user)
    self.update()

  @staticmethod
  def _delete_seed_track(tx, user:User):
    return tx.run("""
    MATCH (u:User {name: $user_name})-[r:SEED_TRACK]-(:Track)
    DELETE r
    """, user_name=user.name)

  @staticmethod
  def _get_seed_track(tx, user:User):
    return tx.run("""
    MATCH (u:User {name: $user_name})-[r:SEED_TRACK]-(t:Track)
    RETURN t.mongo_id AS track_id, r.reward AS reward
    """, user_name=user.name)

  @staticmethod
  def _set_seed_track(tx, user:User, track:Track, reward:float):
    return tx.run("""
    MATCH (u:User {name: $user_name})
    MATCH (t:Track {id: $mongo_id})
    MERGE (u)-[new_r:SEED_TRACK {reward: $reward}]->(t)
    RETURN t.mongo_id AS track_id, new_r.reward AS reward
    """,
                  user_name=user.name,
                  reward=reward,
                  mongo_id=track.mongo_id,
                  )

  @staticmethod
  def _get_played_tracks(tx, user:User):
    return tx.run("""
    MATCH (:User {name: $user_name})-[:SESSION_LISTENED]->(t:Track)
    RETURN t.mongo_id as track_id
    """, user_name=user.name)

  @staticmethod
  def _reset_listened_tracks(tx, user:User):
    return tx.run("""
    MATCH (:User {name: $user_name})-[r:SESSION_LISTENED]->(:Track)
    DELETE r
    """, user_name=user.name)

  @staticmethod
  def _mark_as_played(tx, user:User, track:Track):
    return tx.run("""
    MATCH (u:User {name: $user_name})
    MATCH (t:Track {id: $mongo_id})
    MERGE (u)-[:SESSION_LISTENED]->(t)
    """, user_name=user.name, mongo_id=track.mongo_id)
