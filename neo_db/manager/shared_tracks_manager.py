from neo_db import *

class SharedTracksManager:
  def __init__(self, user:User):
    self.user = user

  def share_track(self, track:Track, friend:'User'):
    with driver.session() as session:
      session.write_transaction(self._share_track, user=self.user, friend=friend, track=track)

  def mark_as_listened(self, track:Track):
    with driver.session() as session:
      session.write_transaction(self._mark_as_listened, user=self.user, track=track)

  def track_shared_by(self, track:Track):
    with driver.session() as session:
      res = session.write_transaction(self._track_shared_by, user=self.user, track=track).data()
    return [o['name'] for o in res]

  def visualized(self):
    with driver.session() as session:
      session.write_transaction(self._visualized, user=self.user)

  def get_received_shared_tracks(self):
    """Tracks shared to me"""
    with driver.session() as session:
      res = session.read_transaction(self._get_tracks_shared_to_user, user=self.user).data()
    return [{
      'users': [User.from_name(name).to_dict(simplified=True) for name in o['friends']],
      'track_mongo_id': o['track_mongo_id'],
      'visualized': o['visualized'],
    } for o in res]

  def get_sent_shared_tracks(self):
    """Tracks that this user has shared with other people"""
    with driver.session() as session:
      res = session.read_transaction(self._get_tracks_user_shared, user=self.user)
    return res

  @staticmethod
  def _get_tracks_user_shared(tx, user:User):
    return tx.run("""
    MATCH (p:User {username: $name})-[r:SHARES_TRACK]->(f:User)
    RETURN f.name AS user, r.track_mongo_id AS track_mongo_id, r.visualized AS visualized
    """, name=user.name)

  @staticmethod
  def _get_tracks_shared_to_user(tx, user:User):
    """Get the tracks that were shared to this user"""
#    return tx.run("""
 #   MATCH (p:User {username: $user_name})<-[r:SHARES_TRACK]-(f:User)
  #  WITH r.track_mongo_id AS track_mongo_id, r.visualized AS visualized, collect(f.username) AS friends
   # RETURN friends, track_mongo_id, size(friends) AS score, visualized
    #""", user_name=user.name)
  #edited by Aditya
    return tx.run("""
    MATCH(p:User{username: $user_name})<-[r:SHARES_TRACK]-(f:User)
    WHERE NOT(p)-[:LISTENED_TO]->(:Track{id:r.track_mongo_id})
    WITH r.track_mongo_id AS track_mongo_id, r.visualized AS visualized, collect(f.username) AS friends
    RETURN friends, track_mongo_id, size(friends) AS score, visualized
    """,user_name=user.name)

  @staticmethod
  def _share_track(tx, user:User, friend:User, track:Track):
    return tx.run("""
    MATCH (p:User {username: $name}),
          (f:User {username: $friend_name})
    MERGE (p)-[r:SHARES_TRACK {track_mongo_id: $track_mongo_id}]->(f)
    ON CREATE SET r.visualized=FALSE, r.notified = FALSE
    """, name=user.name, friend_name=friend.name, track_mongo_id=track.mongo_id)
#TODO:r.lastlistened set from alpha_playback->register_track_listen...
  @staticmethod
  def _mark_as_listened(tx, user:User, track:Track):
    return tx.run("""
    MATCH (u:User {username: $user_name})<-[r:SHARES_TRACK {track_mongo_id: $track_mongo_id}]-(:User)
    DELETE r
    """, user_name=user.name, track_mongo_id=track.mongo_id)

  @staticmethod
  def _track_shared_by(tx, user:User, track:Track):
    return tx.run("""
    MATCH (u:User {username: $user_name})<-[r:SHARES_TRACK {track_mongo_id: $track_mongo_id}]-(f:User)
    RETURN f.name AS name
    """, user_name=user.name, track_mongo_id=track.mongo_id)

  @staticmethod
  def _visualized(tx, user:User):
    return tx.run("""
    MATCH (u:User {username: $user_name})<-[r:SHARES_TRACK]-(:User)
    SET r.visualized = TRUE
    """, user_name=user.name)
