from neo_db import *

class PeopleRecommender(Node):
  def __init__(self, user:User):
    self.user = user

  def get_top_recommendations(self):
    code = """
        MATCH (me:User {username: $user_name})
        MATCH (me)-[:LOVES]->(n)<-[:LOVES]-(u:User)
        WHERE NOT (me)-[:IS_FRIENDS_WITH]-(u)
        RETURN u as Recommended, count(*) AS Strength ORDER BY Strength DESC
        """
    data = self.read(code, user_name=self.user.name).data()
    users = [User.from_node(o['Recommended']) for o in data]
    return users

  def by_expertise(self, expertise_name) -> List[User]:
    code = """
    MATCH (me:User {username: $user_name})
    MATCH (u:User)-[:IS_EXPERT_IN]->(:TrackFeature {name: $expertise_name})
    WHERE NOT (me)-[:IS_FRIENDS_WITH]-(u)
    RETURN u as Recommended, count(*) AS Strength ORDER BY Strength DESC
    """
    data = self.read(code, user_name=self.user.name, expertise_name=expertise_name).data()
    users = [User.from_node(o['Recommended']) for o in data]
    return users

  def by_badge(self, badge_name) -> List[User]:
    code = """
    MATCH (me:User {username: $user_name})
    MATCH (u:User)-[:HAS_BADGE]->(:Badge {name=$badge_name})
    WHERE NOT (me)-[:FRIENDS]-(u)
    RETURN u as Recommended, count(*) AS Strength ORDER BY Strength DESC
    """
    data = self.read(code, user_name=self.user.name, badge=badge_name).data()
    users = [User.from_node(o['Recommended']) for o in data]
    return users
