import flask
from os import getenv
import connexion
from recommender import *

app = connexion.App(__name__)
app.add_api('swagger.yml')
application = app.app
DEBUG = bool(int(getenv('DEBUG')))
# 2318 for stable, 2317 for debug
port = 2317

def drop_into_pdb(app, exception):
    import sys
    import pdb
    import traceback
    traceback.print_exc()
    pdb.post_mortem(sys.exc_info()[2])

# somewhere in your code (probably if DEBUG is True)

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    # U.v2.initialize_scripts(debug=DEBUG)
    set_debug(v=DEBUG)
    api.set_debug_mode(v=DEBUG)
    app.run(port=port, debug=DEBUG)
    if DEBUG:
      flask.got_request_exception.connect(drop_into_pdb)

