# import lishutils as U
import concurrent.futures, pymongo, random
import faiss
import numpy as np
import mongo_manager as mm
import urllib
import os
from pymongo import MongoClient
import db_manager as dm
from typing import *
# from .helpers import *
# from .reward import *
from flask import abort, request
# from .helpers_new import context_helper
# from .preload_spot import preload_spot
from collections import namedtuple
from pathlib import Path

from recommender import *


import time
from functools import wraps


def log_time(f):
  @wraps(f)
  def wrapper(*args, **kwds):
    start = time.time()
    res = f(*args, **kwds)
    end = time.time()
    # TODO: Use logger instead of print
    # TODO: Format output
    print(f'{f.__module__}.{f.__name__} time: {end - start}')
    return res
  return wrapper


username = urllib.parse.quote_plus(os.getenv('USERNAME'))
password = urllib.parse.quote_plus(os.getenv('PASSWORD'))
alpha_client = MongoClient('mongodb://%s:%s@localhost:27421' % (username, password))
alpha_db = alpha_client.music


class TrackSpace:
    path = '/home/admin/embeds_v2.npy'

    def __init__(self, embeds, gembeds):
        print('initializing track space')
        self.embeds = embeds
        self.gembeds = gembeds
        self.count = 0
        self.threshold = 10

    def shuffle(self,k):
        return np.random.randint(low=0, high=len(self.embeds), size=k).tolist()

    def search(self, seed_embed, k):
        """ wrapper around the faiss knn functions without index """
        distance_type = faiss.METRIC_L2
        query = np.array([seed_embed]).astype(np.float32)
        nq, d = query.shape
        nb, d2 = self.embeds.shape
        assert d == d2

        I = np.empty((nq, k), dtype='int64')
        D = np.empty((nq, k), dtype='float32')

        if distance_type == faiss.METRIC_L2:
            heaps = faiss.float_maxheap_array_t()
            heaps.k = k
            heaps.nh = nq
            heaps.val = faiss.swig_ptr(D)
            heaps.ids = faiss.swig_ptr(I)
            faiss.knn_L2sqr(
                faiss.swig_ptr(query), faiss.swig_ptr(self.embeds),
                d, nq, nb, heaps
            )
        elif distance_type == faiss.METRIC_INNER_PRODUCT:
            heaps = faiss.float_minheap_array_t()
            heaps.k = k
            heaps.nh = nq
            heaps.val = faiss.swig_ptr(D)
            heaps.ids = faiss.swig_ptr(I)
            faiss.knn_inner_product(
                faiss.swig_ptr(query), faiss.swig_ptr(self.embeds),
                d, nq, nb, heaps
            )
        return D, I

    def update_index(self, tdoc:mm.Track,feat_type:str):
        if feat_type in ['genre', 'valence', 'loudness', 'deepness', 'speechiness']:
            gdict = defaultdict(float)
            for i in tdoc.genres:
                gdict[i['name']] += len(tdoc.art_gens) / float(len(tdoc.genres))

            for i in tdoc.art_gens:
                gdict[i['name']] += 1.

            all_ges = []
            scores = []
            for g, score in gdict.items():
                if g in self.gembeds:
                    all_ges.append(self.gembeds[g])
                    scores.append(score)
            if not all_ges:
                ges = [0, 0, 0, 0, 0]
            else:
                ges = list(np.average(all_ges, axis=0, weights=scores))

            ges.append(tdoc.valence)
            ges.append(tdoc.loudness)
            ges.append(tdoc.deepness)
            ges.append(tdoc.speechiness)
            tdoc.embed = ges
            self.embeds[tdoc.seq_id] = np.array(ges).astype('float32')
            self.count += 1
            print(f"Edit Count For This Worker : {self.count}")
            if self.count == self.threshold:
                print("saving numpy embeds")
                np.save(self.path, self.embeds)
                self.count = 0

        tdoc.save()

    @classmethod
    def from_file(cls):
        embeds = np.load(cls.path)
        gembeds = {}
        for gdoc in alpha_db.genre.find():
            gembeds[gdoc['name']] = gdoc['embed']
        return cls(embeds=embeds, gembeds=gembeds)


 # all_tspace = TrackSpace.from_file()


def reset_listening_history(token:str):
    return api.reset_listening_history(token=token)

def recommend_people(token:str, recommendBy:str):
    return api.recommend_people(token=token, recommend_by=recommendBy)

def tag_feedback(token: str, trackId: str, feedback: str, body):
    return api.tag_feedback(token=token, track_id=trackId, feedback=feedback, body=body)

def get_user(token:str):
    return api2.get_user(token=token)

def top_contexts():
    return api.top_contexts()

def get_tags_of_type(token:str, type:str):
    print("### I HAVE BEEN STARTED ###")
    res = api.get_tags_of_type(token=token, type=type)
    print("### I AM RETURNING ###")
    return res

def register_tags(token:str, body):
    print("### I HAVE BEEN STARTED ###")
    res = api.register_tags(token=token, body=body)
    print("### I AM RETURNING ###")
    return res

def edit_track_features(token, trackId, actionType, featType, featValue):
    print("### I HAVE BEEN STARTED ###")
    api.edit_track_features(token=token, tid=trackId, action_type=actionType,
                                feat_type=featType, feat_value=featValue)
    #all_tspace.update_index(tdoc, feat_type=featType)
    print("### I AM RETURNING ###")


def simulate_actions(token, trackId, sessionId, body):
    print ("######### I HAVE BEEN STARTED ##############")
    seq_ids = []
    seq_dists = []
    session_doc = mm.MusicSession.objects().get(id=ObjectId(sessionId))

    # if 'secondaryMode' in body:
    #     if body['secondaryMode'].lower() == 'organic':
    #         if session_doc.seed_track:
    #             seed_track_embed = session_doc.seed_track.embed
    #             seq_dists, seq_ids = all_tspace.search(seed_embed=seed_track_embed, k= 100)
    #             seq_ids = seq_ids[0].tolist()
    #             seq_dists = seq_dists[0].tolist()
    #
    #     elif body['secondaryMode'].lower() == 'wild':
    #         seq_ids = all_tspace.shuffle(k=100)

    res = api.simulate_actions(token=token, track_id=trackId, session_doc=session_doc,
                                seq_ids=seq_ids, seq_dists = seq_dists,
                                body=body)

    print("######### I AM RETURNING ##############")

    return res

def broadcast_track(token:str, trackId:str):
    return api.broadcast_track(token=token, track_id=trackId)

def visualized_broadcasted_tracks(token:str):
    return api.visualized_broadcasted_tracks(token=token)

def received_broadcasted_tracks(token:str):
    return api.received_broadcasted_tracks(token=token)

def create_community(token:str, communityName):
    return api.create_community(token=token, community_name=communityName)

def join_community(token:str, communityName):
    return api.join_community(token=token, community_name=communityName)

def add_community_post(token:str, communityName, body):
    return api.add_community_post(token=token, community_name=communityName, body=body)

def community_posts(token:str, communityName):
    return api.community_posts(token=token, community_name=communityName)

def community_users(token:str, communityName):
    return api.community_users(token=token, community_name=communityName)

def get_communities(token:str):
    return api.get_communities(token=token)

def friends_reactions(token:str, trackId:str):
    return api.friends_reactions(token=token, track_id=trackId)

def add_friend(token:str, friendId:str):
    return api.add_friend(token=token, friend_id=friendId)

def visualized_shared_tracks(token:str):
    return api.visualized_shared_tracks(token=token)

def received_shared_tracks(token:str):
    return api.received_shared_tracks(token=token)

#Added by aditya
def sent_shared_tracks(token:str):
    return api.sent_shared_tracks(token=token)

def improve_recommendation_tracks(token:str):
    return api.tracks_for_improve_recommendation(token=token)

def share_track(token:str, toUserId:str, trackId:str):
    return api.share_track(token=token, to_user=toUserId, track_id=trackId)

def send_chat_notification(token:str,toUserId:str, trackId: str,message:str):
    return api.send_chat_notification(token=token,to_user=toUserId, track_id= trackId, message=message)

def get_friends(token:str):
    return api.get_friends(token=token)

def search_user(token: str, query: str):
    return api.search_user(token=token, query=query)

def find_users(token: str, query: str):
    return api.get_users_with_query_openbeta(token=token, query=query)

def track_preview(token: str, body: list):
    return api.track_preview(token=token, body=body)


def start(token: str, body: list):
    return api.start(token=token, body=body)


def reset_session(token: str, body: dict):
    return api.reset_session(token=token, body=body)


def remove_track_tag(token: str, trackId: str, body: dict):
    return api.remove_track_tag(token=token, track_id=trackId, body=body)


def add_track_tag(token: str, trackId: str, body: dict):
    return api.add_track_tag(token=token, track_id=trackId, body=body)


def modify_track_features(token: str, trackId: str, body: dict):
    return api.modify_track_features(token=token, track_id=trackId, body=body)


def track_metadata(token: str, trackId: str):
    return api.track_metadata(token=token, track_id=trackId)


def seed_track_metadata(token: str):
    return api.seed_track_metadata(token=token)


def bandit_plot(token: str):
    return api.bandit_plot(token=token)


def feat_importance_plot(token: str, body):
    return api.feat_importance_plot(token=token, body=body)


def track_info(token: str, trackId: str):
    return api.track_info(token=token, track_id=trackId)

def add_or_get_track_for_song(token:str, trackId:str):
    return api.add_or_get_track_for_song(token=token, trackId=trackId)

def get_track2_from_id(token:str, trackId:str):
    return api.get_track2_from_id(token=token, track_id=trackId)


@log_time
def register_right_swipe(
    token: str, trackId: str, sessionId:str, loveCount: int, playRatio: float, body: dict
) -> None:
    print("######### I HAVE BEEN STARTED ##############")
    # return U.v2.register_right_swipe(token=token, track_id=trackId, love_count=loveCount, play_ratio=playRatio, info=body)
    res = api.register_right_swipe2(
        token=token,
        track_id=trackId,
        session_id=sessionId,
        love_count=loveCount,
        play_ratio=playRatio,
        body=body,
    )
    print("######### I AM RETURNING ##############")
    return res

def search_while_playing(
    token: str, trackId: str, loveCount: int, playRatio: float, body: dict
) -> None:
    print("######### I HAVE BEEN STARTED ##############")
    # return U.v2.register_right_swipe(token=token, track_id=trackId, love_count=loveCount, play_ratio=playRatio, info=body)
    res =  api.search_while_playing(
        token=token,
        track_id=trackId,
        love_count=loveCount,
        play_ratio=playRatio,
        body=body,
    )
    print("######### I AM RETURNING ##############")
    return res


def register_left_swipe(
    token: str, trackId: str, sessionId:str, loveCount: int, playRatio: float, body: dict
) -> None:
    print("######### I HAVE BEEN STARTED ##############")
    # return U.v2.register_left_swipe(token=token, track_id=trackId, love_count=loveCount, play_ratio=playRatio, info=body)
    res = api.register_left_swipe2(
        token=token,
        track_id=trackId,
        session_id=sessionId,
        love_count=loveCount,
        play_ratio=playRatio,
        body=body,
    )
    print("######### I AM RETURNING ##############")
    return res


def register_complete(token: str, trackId: str, sessionId:str, loveCount: int, body: dict) -> None:
    # return U.v2.register_complete(token=token, track_id=trackId, love_count=loveCount, info=body)
    print("######### I HAVE BEEN STARTED ##############")
    res =  api.register_complete2(
        token=token, track_id=trackId, session_id=sessionId, love_count=loveCount, body=body
    )
    print("######### I AM RETURNING ##############")
    return res


def register_track_search(token: str, trackId: str, body: dict) -> None:
    # return U.v2.register_track_search(token=token, track_id=trackId, info=body)
    return api.register_track_search2(token=token, track_id=trackId, body=body)


def next_tracks(
    *,
    token,
    trackId: str,
    tcol=None,
    ucol=None,
    tspace=None,
    update_n: int = 20,
    body: dict
) -> dict:
    raise NotImplementedError


def register_context(token: str, context: str, body: dict) -> dict:
    # return U.v2.register_context(token=token, context=context, info=body)
    return api.register_context(token=token, context=context, info=body)


def get_user_tags(token: str, maxTop:int, maxRecent:int, currentHour:int):
    print("######### I HAVE BEEN STARTED ##############")
    res = api.get_user_tags(token=token, max_top=maxTop, max_recent=maxRecent, current_hour=currentHour)
    print("######### I AM RETURNING ##############")
    return res

# def next_track(token:str, body:dict) -> dict:
# return U.v2.next_track(token=token, info=body)


def next_track(token: str, trackId, body: dict) -> dict:
    return api.next_track3(token=token, last_track_id=trackId, body=body)


def start_from_features(token: str, features: str, body: dict) -> dict:
    return U.v2.start_from_features(token=token, features=features, info=body)


def start_from_features_all(token: str, features: str, body: dict) -> dict:
    return U.v2.start_from_features_all(token=token, features=features, info=body)


def search_features(token: str, query: str, body: dict) -> List[str]:
    return U.v2.search_features(query=query)


# TODO: Context not needed here anymore
def start_from_context(token: str, context: str, body: dict) -> dict:
    # return U.v2.start_from_context(token=token, info=body)
    return api.start_from_context(token=token, info=body)


def start_from_nothing(token: str, body: dict) -> dict:
    return U.v2.start_from_nothing(token=token, info=body)


def sign_in(email: str, password: str, deviceToken:str, body: dict, tempFix: str) -> str:
    # return U.v2.sign_in(username=username, password=password, info=body)
    print("######### I HAVE BEEN STARTED ##############")
    assert(tempFix == 'Golu')
    res = api2.sign_in(email=email, password=password, info=body, device_token = deviceToken)
    print(res)
    print("######### I AM RETURNING ##############")
    return res

def sign_up(username: str, password: str, name:str, email:str, deviceToken:str, body: dict):
    # return U.v2.sign_up(username=username, password=password, info=body)
    print("######### I HAVE BEEN STARTED ##############")
    res = api2.sign_up(username=username, password=password, name=name, email=email, info=body, device_token= deviceToken)
    print("######### I AM RETURNING ##############")
    return res

def username_exists_check(username:str):
    res = api2.username_exists_check(username=username)
    return res

def search_item(token: str, query: str, limit: int = 2, type: str = None):
    print("######### I HAVE BEEN STARTED ##############")
    res = api.search_item(token=token, query=query, item_type=type, limit=limit)
    print("######### I AM RETURNING ##############")
    return res

def search_me(token: str, query: str, limit: int):
    print("######### I HAVE BEEN STARTED ##############")
    res = api.search_me(token=token, query=query, limit=limit)
    print("######### I AM RETURNING ##############")
    return res


def previous_contexts(token: str, body: dict) -> List[str]:
    # return U.v2.previous_contexts(token=token, info=body)
    return api.previous_contexts(token=token, info=body)


###### Still need to refactor ######


def start_from_genre(token: str, genreId: str):
    global _UCOL, _TCOL, _GCOL
    gdoc = _GCOL.find_one({"_id": ObjectId(genreId)}, projection=["name_lower"])
    return start_from_features(token=token, features=gdoc["name_lower"])


def start_from_artist(token: str, lishashId: str, spotifyId: str):
    global _TCOL, _UCOL
    tdocs = list(
        _TCOL.find(
            {"most_popular_track.artists.id": spotifyId},
            projection=["most_popular_track"],
            limit=10,
        )
    )
    tdoc = random.choice(tdocs)
    # Add history #
    # udoc = _UCOL.find_one({'_id': ObjectId(token)})
    # info = U.db.user.History.start_from_artist(udoc=udoc, track_id=str(tdoc['id']))
    # _UCOL.update_one({'_id': udoc['_id']}, {'$push': {'music.history': info}})
    # Ends add history #
    return get_tid_map(_id=tdoc["_id"], spotify_id=tdoc["most_popular_track"]["id"])


def start_from_album(token: str, name: str):
    udoc = _UCOL.find_one({"_id": ObjectId(token)})
    udoc = U.db.get_user_col().find_one({"_id": ObjectId(token)})
    #     tdocs = list(_TCOL.find({'album_lower': name.lower()}, projection=['popularity', 'seq_id', 'ids'], limit=1))
    tdocs = list(
        _TCOL.find(
            {"most_popular_track.album.name_lower": name.lower()},
            projection=["seq_id", "most_popular_track"],
            limit=10,
        )
    )
    seq_id2tdoc = get_seq_id2tdoc(tdocs=tdocs)
    best_track = get_best_track(seq_id2tdoc, user_track_ws=udoc["music"]["tracks_ws"])
    return get_tid_map(_id=best_track["_id"], spotify_id=best_track["spotify_id"])


def register_service_token(token: str, serviceToken: str, body: dict):
    ucol = U.db.get_user_col()
    udoc = ucol.find_one({"_id": ObjectId(token)})
    if "spot_token" not in udoc["music"]:
        preload_spot(
            ucol=ucol, tcol=_TCOL, token=token, spot_token=serviceToken, info=body
        )

def referral_add_friends_and_rewards(token:str, referredBy:str,trackId:str):
    #todo:add as friend and increment friends fire user currency..FRIEND_ID IS ACTUALLY FRIEND NAME!!!
    referredBy_neo = nd.User.from_mongo_id(mongo_id=referredBy)
    api.add_friend(token=token,friend_id=referredBy_neo.name)

    fire_user_referred_by = dm.FirebaseUser.from_id(id=referredBy)
    fire_user_referred_by.modify_currency(v=50)

    #sharing track sent along while referring
    api.share_track(token=referredBy, to_user=token, track_id=trackId)





