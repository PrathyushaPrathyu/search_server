import json
from mongo_manager import *
from mongo_manager.core.driver import get_tdoc


# TODO: Should be a document
class MusicTag:
  def __init__(
      self,
      name: str,
      tag_type: MusicTagType,
      filter_state: FilterState = None,
      id: str = None,
      uri: str = None,
      preview_url=None,
      cover_image=None,
      recommended_by=None,
      artist_name=None,
  ):
    self.name, self.tag_type, self.filter_state, self.id, self.uri = name, tag_type, filter_state, id, uri
    self.preview_url, self.cover_image = preview_url, cover_image
    self.recommended_by = recommended_by
    self.artist_name = artist_name

  def __eq__(self, other):
    if isinstance(other, self.__class__):
      return (self.name.lower() == other.name.lower()
              and self.tag_type == other.tag_type)
    return False

  def __hash__(self):
    return hash((self.name.lower(), self.tag_type))

  def to_filter(self, seeding: bool):
    if self.filter_state is None: raise ValueError('filter_state cannot be None')
    # TODO: If it's track, filter by _id
    if seeding and self.tag_type == MusicTagType.track:
      return {'_id': ObjectId(self.id)}
    if self.filter_state == FilterState.include:
      return {self.tag_type.db_field_hack: self.name.lower()}
    if self.filter_state in FilterState.exclude:
      return {self.tag_type.db_field_hack: {'$ne': self.name.lower()}}
    if self.filter_state == FilterState.seed:
      if seeding:
        return {self.tag_type.db_field_hack: self.name.lower()}
      else:
        return {}
    raise ValueError(f'FilterState {self.filter_state} not supported')

  def to_json(self):
    return json.loads(json.dumps({snake2lowerCamel(k): v for k, v in self.__dict__.items()}))

  @classmethod
  def from_json(cls, x):
    # TODO: Cannot do try/except because KeyError can be thrown on x or FilterState
    if 'filterState' in x and x['filterState'] is not None:
      filter_state = FilterState[x['filterState']]
    else:
      filter_state = None
    return cls(name=x['name'],
               tag_type=MusicTagType[x['tagType']],
               filter_state=filter_state,
               id=x.get('id'),
               uri=x.get('uri'),
               )

  @classmethod
  def from_doc(cls, x):
    try:
      filter_state = FilterState[x['filterState']]
    except KeyError:
      filter_state = None
    return cls(name=x['name'],
               tag_type=MusicTagType[x['tag_type']],
               filter_state=filter_state,
               id=str(x.get('_id')),
               uri=x.get('uri'),
               )

  @classmethod
  def from_tdoc(cls, tdoc, recommended_by:list):
    print(f'Playing song from url: {tdoc["sample_url"]}')
    return cls(
      name=tdoc['name'],
      tag_type=MusicTagType.track,
      id=str(tdoc['_id']),
      uri=tdoc['uri'],
      preview_url=tdoc['sample_url'],
      cover_image=tdoc['album']['images'][0]['url'],
      recommended_by=recommended_by,
      artist_name=', '.join([o['name'] for o in tdoc['artists']]),
    )

  @classmethod
  def tags_from_tdoc(cls, tdoc):
    # TODO:HACK MusicTagType name hack
    return {o: [MusicTag(name=name['name'], tag_type=o) for name in tdoc[o.db_field]]
            for o in MusicTagType if o != MusicTagType.track}

  @classmethod
  def from_artist_doc(cls, doc):
    return cls(
      name=doc['name_lower'],
      tag_type=MusicTagType.artist,
    )

  @classmethod
  def from_tdoc_id(cls, track_id: str, shared_by:list=None):
    tdoc = get_tdoc(_id=track_id)
    return cls.from_tdoc(tdoc=tdoc, recommended_by=shared_by)

  @classmethod
  def from_language_doc(cls, doc):
    return cls(
      name=doc['name_lower'],
      tag_type=MusicTagType.language,
    )

  @staticmethod
  def from_list_json(list_json):
    return [MusicTag.from_json(o) for o in list_json]

  def __repr__(self):
    return f'<{self.__class__.__name__}{self.__dict__}>'
