from db_manager import *
from typing import *
from datetime import datetime, timedelta, timezone



class FirebaseUser:
    def __init__(self, username: str, id: str, device_tokens: List[str] = None, currency: float = 200):
        self.username = username
        self.id = id
        self.currency = currency
        self.device_tokens = device_tokens or []


    @property
    def doc_ref(self):
        return firebase.db.document(f'users/{self.id}')

    @property
    def doc_ref_received_shared(self, track_id):
        return firebase.db.document(f'users/{self.id}/shared_songs/{track_id}')

    def create_notification(self, message_title, message_body, data):
        data['click_action'] = 'FLUTTER_NOTIFICATION_CLICK'
        message = firebase.messaging.MulticastMessage(
            notification=firebase.messaging.Notification(
                title=message_title,
                body=message_body,
            ),
            data=data,
            tokens=self.device_tokens,
        )
        print(self.device_tokens)
        firebase.messaging.send_multicast(message)
        # print(response.responses.success_count)
        # push_service.notify_multiple_devices(registration_ids=registration_ids, message_title=message_title,
        #                                               message_body=message_body)

    def send_new_friend_add_message(self, name):
        message = firebase.messaging.MulticastMessage(
            data={'type': 'newFriend', 'friendName': name},
            tokens=self.device_tokens
        )
        firebase.messaging.send_multicast(message)

    def create(self):
        self.doc_ref.set({
            'id': self.id,
            'username': self.username,
            'currency': self.currency,
            'last_updated': datetime.utcnow()
        })
        return self

    def modify_currency(self, v: float):
        self.currency += v
        self.doc_ref.set({
            'currency': self.currency,
        }, merge=True)

    def modify_last_updated(self):
        self.doc_ref.set({
            'last_updated' : datetime.now(timezone.utc)
        },merge=True)

    @classmethod
    def from_dict(cls, d: dict):
        return cls(**d)

    @classmethod
    def from_id(cls, id: str):
        doc_ref = firebase.db.document(f'users/{id}')
        doc_ref_converted = doc_ref.get().to_dict()

        doc_ref_dict = {
            'username': doc_ref_converted['username'],
            'id': doc_ref_converted['id'],
            'currency': doc_ref_converted['currency'],
            'device_tokens': doc_ref_converted['deviceTokens'],
        }
        return cls.from_dict(d=doc_ref_dict)

    @classmethod
    def from_username(cls, username: str):
        doc_ref = firebase.db.document(f'users/{id}')
        return cls.from_dict(d=doc_ref.get().to_dict())

    def register_device_token(self, device_token):
        # message = firebase.messaging.MulticastMessage(
        #   notification=firebase.messaging.Notification(
        #     title='$GOOG up 1.43% on the day',
        #     body='$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
        #   ),
        # data = {'type': 'signin', 'name': 'akshay'},
        # tokens = [device_token]
        # )
        # firebase.messaging.send_multicast(message)
        self.doc_ref.set({
            'deviceTokens': [device_token],
        }, merge=True)

    def register_session_id(self, session_id):
        self.doc_ref.set({
            'sessionId': session_id
        }, merge=True)

    def write_share_track_to_friends_firestore(self, track2, shared_by):
        shared_by_firestore = []
        print("writing shared track to friend's firestore")

        doc_ref_received_shared = firebase.db.collection('users').document(self.id).collection('shared_songs').document(track2.id)
        if(doc_ref_received_shared.get().exists):
            doc_fields = doc_ref_received_shared.get().to_dict()
            shared_by_friends = doc_fields['shared_by']
            for friend in shared_by_friends:
                shared_by_firestore.append(friend)
            print(shared_by_firestore)
        else:
            print("song not shared yet")
        if shared_by not in shared_by_firestore:
            shared_by_firestore.append(shared_by)
        doc_ref_received_shared.set({
            'id': track2.id,
            'name': track2.name,
            'cover_image_url': track2.cover_image_url,
            'shared_by': shared_by_firestore,
            'last_updated': datetime.now(timezone.utc)
        }, merge=True)

    # def write_recently_shared_track_to_my_firestore(self, track2, shared_to):
    #     shared_to_firestore = []
    #     print("writing shared track to friend's firestore")
    #
    #     doc_ref_sent_shared = firebase.db.collection('users').document(self.id).collection('recently_shared').document(track2.id)
    #     if(doc_ref_sent_shared.get().exists):
    #         doc_fields = doc_ref_sent_shared.get().to_dict()
    #         shared_to_friends = doc_fields['shared_to']
    #         for friend in shared_to_friends:
    #             shared_to_firestore.append(friend)
    #         print(shared_to_firestore)
    #     else:
    #         print("song not shared yet")
    #     if shared_to not in shared_to_firestore:
    #         shared_to_firestore.append(shared_to)
    #     doc_ref_sent_shared.set({
    #         'id': track2.id,
    #         'name': track2.name,
    #         'cover_image_url': track2.cover_image_url,
    #         'shared_to': shared_to_firestore,
    #         'last_updated': datetime.now(timezone.utc)
    #     }, merge=True)

    def last_login_deduct_currency(self):
        user_doc = firebase.db.document(f'users/{self.id}')
        user_fields = user_doc.get().to_dict()

        decrement_interval = timedelta(hours=1)
        #todo:only for testing
        #decrement_interval = timedelta(minutes=5)

        if 'last_updated' not in user_fields.keys():
            user_fields['last_updated'] = datetime.now(timezone.utc)
            user_doc.set({
                'last_updated':  datetime.now(timezone.utc)
            },merge=True)

        last_updated = user_fields['last_updated']

        time_since_last_login = datetime.now(timezone.utc) - last_updated
        decrement_multiplier = time_since_last_login // decrement_interval
        currency_decrement = -2 * decrement_multiplier
        self.modify_currency(v=currency_decrement)
        self.modify_last_updated()
        #todo:neeed to modify last_updated with other operations also?

