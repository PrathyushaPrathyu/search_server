import pathlib
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import messaging
from boltons.cacheutils import cachedproperty

_KEY_PATH = str(pathlib.Path(__file__).parent / 'firestore_key.json')

_cred = credentials.Certificate(_KEY_PATH)
firebase_admin.initialize_app(_cred)


class _FirebaseManager:
  @cachedproperty
  def db(self):
    return firestore.client()

  @cachedproperty
  def messaging(self):
    return messaging


firebase = _FirebaseManager()
