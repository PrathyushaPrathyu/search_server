import db_manager as dm
import neo_db as nd
import mongo_manager as mm
from recommender import *
from recommender.api.track2 import Track2

RESERVED_USERNAMES = ['shash', 'timmy', 'svsobh', 'shosho',
                      'lishash', 'shashwat', 'shash@lishash.com', 'svsobh@gmail.com']

class UserManager:
  def __init__(
      self,
      mongo_user: mm.UserInfo,
      neo_user: nd.User,
      fire_user: dm.FirebaseUser
  ):
    self.mongo_user = mongo_user
    self.neo_user = neo_user
    self.fire_user = fire_user

  @property
  def id(self): return str(self.mongo_user.id)

  def to_dict(self, simplified=True):
    res_dict =  self.neo_user.to_dict(simplified=simplified)
    res_dict['hasSpotify'] = self.mongo_user.has_spotify
    res_dict['hasAppleMusic'] = self.mongo_user.has_apple_music
    return res_dict

  def modify_currency(self, v: float):
    self.fire_user.modify_currency(v=v)

  def share_track(self, tdoc, to_user:str):
    from_user = self.neo_user
    from_user_fire = self.fire_user
    for each_to_user in to_user.split(","):
      to_user_dm = dm.UserManager.from_id(each_to_user)
      to_user_neo = to_user_dm.neo_user
      to_user_fire = to_user_dm.fire_user
      track = nd.Track.from_mongodb_doc(doc=tdoc)
      shared_tracks_manager = nd.SharedTracksManager(user=from_user)
      #share track and add to neo4j
      shared_tracks_manager.share_track(track=track, friend=to_user_neo)

      music_tag = MusicTag.from_tdoc_id(track_id=str(tdoc['_id']))
      track2 = Track2.from_music_tag(token=self.id, music_tag=music_tag)

      #to write shared track to friend's firestore as shared_songs and my firestore as recently shared
      to_user_fire.write_share_track_to_friends_firestore(track2=track2, shared_by=from_user.name)


      #share track friend notification body
      message_title = from_user.name + " shared a song with you!"
      message_body = track.name.capitalize()
      data = {'page': '/peoplePage',
              'username': str(from_user.name),
              'user_id': str(from_user.mongo_id)}

      to_user_fire.create_notification(message_title=message_title, message_body=message_body, data=data)


      #todo: add to friend's firebase document/snapshot containing the required

  @classmethod
  def from_id(cls, user_id: str):
    mongo_user = mm.UserInfo.from_user_id(id=user_id)
    neo_user = nd.User.from_mongo_id(mongo_id=user_id)
    fire_user = dm.FirebaseUser.from_id(id=user_id)
    return cls(mongo_user=mongo_user, neo_user=neo_user, fire_user=fire_user)

  def username_exists_check(username = str):
    pipeline = [{"$match": {"username": username}}]

    user_cur = mm.UserInfo.objects.aggregate(*pipeline)
    users_list = list(user_cur)
    # udoc = mm.UserInfo.from_username(username=username)
    if (len(users_list) == 0):
      print("username not present")
      username_exists = False
    elif (len(users_list) > 0):
      print("username is present")
      username_exists = True
      #print(users_list)
      # raise Exception("username already exists")
    return username_exists

  @classmethod
  def sign_up(cls, email, name, username, password: str, device_token):
    if username in RESERVED_USERNAMES:
      raise Exception(f'{username} is a reserved username')

    mongo_user = mm.UserInfo(username=username, email=email, name=name).save()
    uid = str(mongo_user.id)
    neo_user = nd.User(name=username, mongo_id=uid).create()
    if username == 'shash':
      neo_user.add_badge(badge=nd.Badges.founder_badge)
    neo_user.add_badge(badge=nd.Badges.beta_user_badge)
    fire_user = dm.FirebaseUser(username=username, id=uid).create()
    fire_user.register_device_token(device_token=device_token)
    return cls(mongo_user=mongo_user, neo_user=neo_user, fire_user=fire_user)

  @classmethod
  def sign_in(cls, email: str, password: str, device_token):
    mongo_user = mm.UserInfo.from_email(email=email)
    uid = str(mongo_user.id)
    neo_user = nd.User.from_mongo_id(mongo_id=uid)
    fire_user = dm.FirebaseUser.from_id(id=uid)
    fire_user.register_device_token(device_token=device_token)
    return cls(mongo_user=mongo_user, neo_user=neo_user, fire_user=fire_user)

  def reset_listening_history(self):
    nd.User.from_mongo_id(mongo_id=self.id).reset_listening_history()
    for history in mm.History.objects(user_id=self.id):
      history.delete()



