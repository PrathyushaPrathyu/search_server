import neo_db as nd
import mongo_manager as mm
from db_manager import *


class ContributeTagManager:
  def __init__(self):
    self.neo_manager = nd.ContributeTagManager()
    self.mongo_manager = mm.ContributeTagManager()

  def add_tag(self, track_id: str, tag: MusicTag):
    self.neo_manager.add_tag(track_id=track_id, music_tag=tag)
    self.mongo_manager.add_tag(track_id=track_id, music_tag=tag)
