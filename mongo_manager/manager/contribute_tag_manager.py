from bson import ObjectId
from mongo_manager import *
from mongo_manager.tmp.music_tag import MusicTag, MusicTagType


class ContributeTagManager:
  def add_tag(self, track_id: str, music_tag: MusicTag):
    tag = {'name': music_tag.name, 'count': 200}
    # TODO:HACKY
    up = {'$push': {music_tag.tag_type.db_field: tag}}
    MongoDBManager.tcol.update_one({'_id': ObjectId(track_id)}, update=up)
