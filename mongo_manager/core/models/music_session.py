from mongoengine import *

class AlphaUserHistory(EmbeddedDocument):
    all_history = ListField(StringField())
    general_long = ListField(DictField())
    general_short = ListField(DictField())
    context_long = ListField(DictField())
    context_short = ListField(DictField())


class BaseTrack(EmbeddedDocument):
    score = FloatField()
    embed = ListField(FloatField())
    feats = ListField(StringField())

class SearchSeed(EmbeddedDocument):
    name = StringField()
    loves = FloatField()    # The number of times the song has been loved since
    completes = FloatField()
    rights = FloatField()

class MusicSession(Document):
    user_history = EmbeddedDocumentField(AlphaUserHistory)
    seed_track = EmbeddedDocumentField(BaseTrack)
    thorn_track = EmbeddedDocumentField(BaseTrack)
    search_seed = EmbeddedDocumentField(SearchSeed)
    # schema {(type, name) : float(score)}
    positive_interests = ListField(DictField())
    negative_interests = ListField(DictField())
    session_listened_tracks = ListField(StringField())

