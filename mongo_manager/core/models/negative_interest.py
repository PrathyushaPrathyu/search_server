from boltons.cacheutils import cachedproperty
from mongoengine import *
from collections import defaultdict
from mongo_manager import *
from mongo_manager.tmp.music_tag import MusicTag
from .context_document import ContextDocument
from .ema import EMA
from .exponential_decay import ExponentialDecay


class NegativeInterest(ContextDocument):
  user_id = StringField()
  context = StringField()
  _tag2sched = ListField()
  _sampled_tags = ListField()

  @cachedproperty
  def tag2sched(self):
    return defaultdict(
      lambda: ExponentialDecay(add_smooth=0.5, ema=EMA(smooth=0.95, value=0.0)),
      {MusicTag(name=o['tag']['name'], tag_type=MusicTagType[o['tag']['tag_type']]): o['sched']
       for o in self._tag2sched})

  @cachedproperty
  def sampled_tags(self):
    return [MusicTag(name=o['name'], tag_type=MusicTagType[o['tag_type']]) for o in self._sampled_tags]

  @property
  def tags_type(self):
    return [MusicTagType.language, MusicTagType.genres, MusicTagType.artist, MusicTagType.about, MusicTagType.emotion]

  def _step(self):
    [o.step() for o in self.tag2sched.values()]
    self.sampled_tags = []

  def reset_tdoc_tags(self, tdoc):
    tags = MusicTag.tags_from_tdoc(tdoc)
    tags = flatten_2d_list([v for k, v in tags.items() if k in self.tags_type])
    for o in tags: self.tag2sched[o].set(0.0)

  def update(self, tdoc, seed_tdoc=None):
    if seed_tdoc:
      seed_tags = MusicTag.tags_from_tdoc(seed_tdoc)
      # TODO: Check this line, k is a MusicTag and "k in self.tags_type" will always be false
      seed_tags = flatten_2d_list([v for k, v in seed_tags.items() if k in self.tags_type])
    else:
      seed_tags = []
    tags = MusicTag.tags_from_tdoc(tdoc)
    # TODO: Check this line, k is a MusicTag and "k in self.tags_type" will always be false
    tags = flatten_2d_list([v for k, v in tags.items() if k in self.tags_type])
    self.update_from_tags(seed_tags=seed_tags, tags=tags)

  def update_from_tags(self, seed_tags, tags):
    diff = set(tags) - set(seed_tags)
    intersect = set(tags).intersection(set(seed_tags))
    # Add difference to negative list
    # seed track is edm, current is rock. This value of 1.
    # is going to give a very high probabilty of this rock tag being sampled,
    # and because this is negative intereset, rock is going to not be played.
    for o in diff: self.tag2sched[o].add(1.0)
    # Also add intersection to negative list, but with smaller weight
    for o in intersect: self.tag2sched[o].add(0.4)
    # for o in seed_tags: self.tag2sched[o].set(0.0)
    self._step()

  def sample(self):
    # print("######### SAMPLING A NEGATIVE INTEREST TAG")
    # logger.debug('Negative tags: %s', [(k.name, v.value) for k, v in self.tag2sched.items()])
    tags = [k for k, v in self.tag2sched.items() if v.value > np.random.uniform() if k not in self.sampled_tags]
    self.sampled_tags.extend(tags)
    info = [(o.name, self.tag2sched[o].value) for o in tags]
    # print('Negative sampled tags %s', [f'{name}, {w:.2f}' for name, w in info])
    # import pdb; pdb.set_trace()
    return tags

  def get_pipeline_stages(self):
    tags = self.sample()
    for tag in tags: tag.filter_state = FilterState.exclude
    filters = [t.to_filter(seeding=False) for t in tags]
    if filters:
      return [{'$match': {'$and': filters}}]
    else:
      return [{'$match': {}}]

  def save(self):
    self._tag2sched = [{'tag': {'name': k.name, 'tag_type': k.tag_type.name}, 'sched': v}
                       for k, v in self.tag2sched.items()]
    self._sampled_tags = [{'name': o.name, 'tag_type': o.tag_type.name} for o in self.sampled_tags]
    super().save()
