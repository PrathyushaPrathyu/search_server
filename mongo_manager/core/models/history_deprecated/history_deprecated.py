from bson import ObjectId
from mongoengine import *
from datetime import datetime
from boltons.cacheutils import cachedproperty

from sortedcollections import *
from typing import *

from mongo_manager import *
from .history_item import HistoryItem
from .history_item_controller import HistoryItemController


class History_Deprecated(Document):
  user_id = StringField()
  context = StringField()
  data = MapField(EmbeddedDocumentField(HistoryItem))
  # TODO: Below items should not be on this class, they're about the current session
  seed_track = EmbeddedDocumentField(HistoryItem)
  seed_track_reward = FloatField()
  played_tracks = EmbeddedDocumentListField(HistoryItem)

  def __len__(self):
    # print('#### Careful!!! History.__len__ has changed!! #####')
    return super().__len__()

  @cachedproperty
  def date_sorted(self):
    return ValueSortedDict({k: v.last_played for k, v in self.data.items()})

  @cachedproperty
  def score_sorted(self):
    return ValueSortedDict({k: v.score for k, v in self.data.items()})

  @property
  def favorites(self):
    left = self.score_sorted.bisect_key_left(2.0)
    ids = self.score_sorted.keys()[left:][::-1]
    return self.get_items(ids=ids)

  @property
  def short(self):
    ids = self.date_sorted.keys()[::-1][:30]
    return self.get_items(ids=ids)

  @property
  def long(self):
    ids = self.score_sorted.keys()[::-1][:30]
    return self.get_items(ids=ids)

  def get_items(self, ids):
    return HistoryItemController(items=[self.data[id] for id in ids])

  def set_seed_track(self, track_id, reward):
    try:
      item = self.data[track_id]
    except KeyError:
      item = HistoryItem(id=track_id)
    # Simulate reward for this item
    item.last_reward = reward
    self.seed_track = item

  def register_action(self, action: str, track_id: str, reward: float):
    try:
      item = self.data[track_id]
    except KeyError:
      item = HistoryItem(id=track_id)
    item.register_action(action=action, reward=reward)
    self._update(item=item)
    self._register_played(item=item, action=action)

  def reset_session(self):
    self.seed_track: HistoryItem = None
    self.played_tracks: List[HistoryItem] = []

  def get_played_tracks_pipeline_stage(self):
    return {'$match': {'_id': {'$nin': [ObjectId(o.id) for o in self.played_tracks]}}}

  def get_new_tracks_pipeline_stage(self):
    return {'$match': {'_id': {'$nin': [ObjectId(o.id) for o in self.data.values()]}}}

  def _register_played(self, item: HistoryItem, action: str):
    if self.seed_track is None or action == 'Search' or item.last_reward >= self.seed_track_reward:
      if action != 'LeftSwipe':
        self.seed_track = item
        self.seed_track_reward = item.last_reward
    self.played_tracks.append(item)

  def _update(self, item: HistoryItem):
    self.data[item.id] = item
    self.date_sorted[item.id] = datetime.utcnow()
    self.score_sorted[item.id] = item.score

  @classmethod
  def from_context(cls, user_id: str, context: str):
    try:
      return cls.objects().get(user_id=user_id, context=context)
    except DoesNotExist:
      return cls(user_id=user_id, context=context)

  @classmethod
  def from_global(cls, user_id: str):
    try:
      return cls.objects().get(user_id=user_id, context=GLOBAL_CONTEXT)
    except DoesNotExist:
      return cls(user_id=user_id, context=GLOBAL_CONTEXT)
