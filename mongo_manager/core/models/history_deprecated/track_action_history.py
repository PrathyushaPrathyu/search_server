from mongoengine import *
from .action_log import ActionLog


class TrackActionHistory(EmbeddedDocument):
  action2log = MapField(field=EmbeddedDocumentField(ActionLog))

  def register_action(self, action: str):
    try:
      action_log = self.action2log[action]
    except KeyError:
      action_log = ActionLog()
      self.action2log[action] = action_log
    action_log.register_new()
    if action == 'love': self.action2log['love'].register_new()

  def __add__(self, other):
    raise NotImplementedError
    # if not isinstance(other, self.__class__): raise NotImplemented
    # action2log = combine_dicts(self.action2log, other.action2log, values_reduce=lambda x: reduce(operator.add, x))
    # new = self.__class__(action2log=action2log)
    # return new

  def __repr__(self):
    return f'<{self.__class__.__name__} ({dict(self.action2log)})>'
