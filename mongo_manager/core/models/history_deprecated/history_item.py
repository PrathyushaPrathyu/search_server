from datetime import datetime
from mongoengine import *

from .track_action_history import TrackActionHistory


class HistoryItem(EmbeddedDocument):
  id = StringField()
  reward = FloatField(default=0)
  n_played = IntField(default=0)
  last_played = DateTimeField()
  last_action = StringField()
  last_reward = FloatField()
  action_history = EmbeddedDocumentField(TrackActionHistory, default=TrackActionHistory())

  @property
  def score(self):
    return self.reward

  def register_action(self, action: str, reward):
    self.reward += reward
    self.n_played += 1
    self.last_played = datetime.utcnow()
    self.last_action = action
    self.last_reward = reward
    self.action_history.register_action(action=action)

  def __add__(self, other):
    raise NotImplementedError
    # if not isinstance(other, self.__class__): raise NotImplemented
    # assert self.id == other.id
    # new = self.__class__(id=self.id)
    # new.reward = self.reward + other.reward
    # new.n_played = self.n_played + other.n_played
    # new.last_played = max(self.last_played, other.last_played)
    # new.action_history = self.action_history + other.action_history
    # return new

  def __repr__(self):
    return f'<{self.__class__.__name__} (id:{self.id}, score:{self.score}, last_played:{self.last_played}>'
