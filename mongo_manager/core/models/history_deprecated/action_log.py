from datetime import datetime

from mongoengine import *


class ActionLog(EmbeddedDocument):
  n = IntField(min_value=0, default=1)
  last = DateTimeField(datetime=datetime.utcnow())

  def register_new(self):
    self.n += 1
    self.last = datetime.utcnow()

  def __add__(self, other):
    if not isinstance(other, self.__class__): raise NotImplemented
    new = self.__class__()
    new.n = self.n + other.n
    new.last = max(self.last, other.last)
    return new

  def __repr__(self):
    return f'<{self.__class__.__name__} (n:{self.n}, last:{self.last})>'
