from mongoengine import *


class EMA(EmbeddedDocument):
  smooth = FloatField(min_value=0.0, max_value=1.0)
  value = FloatField()

  def set(self, v):
    value = v

  def __call__(self, v, smooth: float = None):
    smooth = smooth or self.smooth
    if self.value is None:
      self.value = v
    else:
      self.value = smooth * self.value + (1 - smooth) * v
    return self.value

  def __repr__(self):
    return f'<{self.__class__.__name__}: (value:{self.value}, smooth:{self.smooth})>'
