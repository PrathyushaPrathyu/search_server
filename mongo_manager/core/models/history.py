from mongoengine import *

class AlphaAction(EmbeddedDocument):
    action = StringField()
    love_count = FloatField()
    play_ratio = FloatField()
    score = FloatField()

class Time(EmbeddedDocument):
    server = DateTimeField()
    user = DictField()

class Context(EmbeddedDocument):
    name = StringField(default='UNSPECIFIED')
    type = StringField(default='UNSPECIFIED')

class History(Document):
    user_id = StringField(required=True)  # also in neo
    track_id = StringField(required=True)  # also in neo
    session_id = StringField(required=True)
    context = EmbeddedDocumentField(Context)
    action = EmbeddedDocumentField(AlphaAction)
    time = EmbeddedDocumentField(Time)
    location = PointField()

    meta = {
        'db-alias' : 'user-alias',
        'indexes' : [
            'user_id',
            'track_id',
            'context',
            'time.server',
            'time.user.minute',
            'time.user.hour',
            'time.user.day',
            'time.user.month'
        ]
    }