from mongoengine import *

from mongo_manager import GLOBAL_CONTEXT


class ContextDocument(Document):
  user_id = StringField()
  context = StringField()
  meta = {
    'allow_inheritance': True,
    'indexes': [('user_id', 'context')],
  }

  @classmethod
  def from_context(cls, user_id: str, context: str):
    try:
      return cls.objects().get(user_id=user_id, context=context)
    except DoesNotExist:
      return cls(user_id=user_id, context=context)

  @classmethod
  def from_global(cls, user_id: str):
    try:
      return cls.objects().get(user_id=user_id, context=GLOBAL_CONTEXT)
    except DoesNotExist:
      return cls(user_id=user_id, context=GLOBAL_CONTEXT)
