from boltons.cacheutils import cachedproperty
from mongoengine import *
from collections import defaultdict
from mongo_manager import *
from mongo_manager.tmp.music_tag import MusicTag
from .context_document import ContextDocument
from .ema import EMA
from .exponential_increase import ExponentialIncrease


class PositiveInterest(ContextDocument):
  user_id = StringField()
  context = StringField()
  _tag2sched = ListField()
  _sampled_tags = ListField()

  @cachedproperty
  def tag2sched(self):
    return defaultdict(
      lambda: ExponentialIncrease(add_smooth=0.8, ema=EMA(smooth=0.75, value=0.0)),
      {MusicTag(name=o['tag']['name'], tag_type=MusicTagType[o['tag']['tag_type']]): o['sched']
       for o in self._tag2sched})

  @cachedproperty
  def sampled_tags(self):
    return [MusicTag(name=o['name'], tag_type=MusicTagType[o['tag_type']]) for o in self._sampled_tags]

  def _step(self):
    [o.step() for o in self.tag2sched.values()]
    self.sampled_tags = []

  def update(self, tdoc, v: float, seed_tdoc=None):
    tags = MusicTag.tags_from_tdoc(tdoc)
    # Only artists tags
    tags = tags[MusicTagType.artist]
    self.update_from_tags(tags=tags, v=v)

  def update_from_tags(self, tags, v: float):
    # print('Adding positive tags: %s with value %s', [t.name for t in tags], v)
    for tag in tags: self.tag2sched[tag].update(v)
    self._step()

  def sample(self):
    # print("####### SAMPLING A POSITIVE INTEREST TAG ###########")
    # logger.debug('Positive tags: %s', [(k.name, v.value) for k, v in self.tag2sched.items()])
    tags = [k for k, v in self.tag2sched.items() if v.value > np.random.uniform() if k not in self.sampled_tags]
    # TODO: We can take only one of them, currently take the first one
    # tags = random.choices(tags) if tags else []
    tags = [max(tags, key=lambda x: self.tag2sched[x].value)] if tags else []
    # Keep track of sampled tracks so we can get away when the songs from this tag is over
    self.sampled_tags.extend(tags)
    info = [(o.name, self.tag2sched[o].value) for o in tags]
    # print('Positive sampled tags %s', [f'{name}, {w:.2f}' for name, w in info])
    # import pdb; pdb.set_trace()
    return tags

  def get_pipeline_stages(self):
    tags = self.sample()
    for tag in tags: tag.filter_state = FilterState.include
    filters = [t.to_filter(seeding=False) for t in tags]
    if filters:
      return [{'$match': {'$and': filters}}]
    else:
      return None

  def reset_tdoc_tags(self, tdoc):
    tags = MusicTag.tags_from_tdoc(tdoc)
    tags = flatten_2d_list([v for k, v in tags.items() if k in [MusicTagType.artist]])
    for o in tags:
      if o in self.tag2sched:
        self.tag2sched[o].step_negative()
    self._step()

  def register_search(self, tdoc):
    tags = MusicTag.tags_from_tdoc(tdoc)
    tags = flatten_2d_list([v for k, v in tags.items() if k in [MusicTagType.artist]])
    for o in tags: self.tag2sched[o].set(1.2)

  def save(self):
    self._tag2sched = [{'tag': {'name': k.name, 'tag_type': k.tag_type.name},
                        'sched': v}
                       for k, v in self.tag2sched.items()]
    self._sampled_tags = [{'name': o.name, 'tag_type': o.tag_type.name} for o in self.sampled_tags]
    super().save()
