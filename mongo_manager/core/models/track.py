from mongoengine import *
from bson import ObjectId

class Track(Document):
    uri = StringField(unique=True)
    name = StringField()
    name_lower = StringField()
    popularity = IntField()
    valence = FloatField()
    loudness = FloatField()
    deepness = FloatField()
    embed = ListField(FloatField())
    artists = ListField(DictField())
    genres = ListField(DictField())
    languages = ListField(DictField())
    about = ListField(DictField())
    instruments = ListField(DictField())
    speechiness = FloatField()
    seq_id = IntField()
    album = DictField()
    duration_ms = FloatField()
    release_date = StringField(required=False)
    preview_url = StringField()
    cover_image_url = StringField()
    art_gens = ListField(DictField())
    release_year = IntField(required=False)
    applemusic_uri = IntField(required=False)
    popularity_playlist = IntField(required=False)
    name_ngrams = StringField(required=False)
    all_ngrams = StringField(required=False)
    isrc = StringField(required=False)

    meta = {'collection': 'track_final'}

    @classmethod
    def from_id(cls, id:str):
        try:
            return cls.objects().get(id=ObjectId(id))
        except:
            import pdb; pdb.set_trace()

    def to_dict(self):
        return self.to_mongo().to_dict()
