from mongoengine import *
import numpy as np


# TODO: This class __call__ is doing something more than just moving average
class AcceleratedEMA(EmbeddedDocument):
  _smooth = FloatField()
  _value = FloatField()
  meta = {'allow_inheritance': True}

  @property
  def smooth(self):
    return self._smooth

  @property
  def value(self):
    return self._value

  def set(self, v):
    self._value = v

  def __call__(self, v, smooth_frac: float = 1.):
    smooth = self.smooth * smooth_frac
    if self.value is None:
      self._value = v
    else:
      aceleration = v - self.value
      mult = np.interp(aceleration, (0, 2.5), (1, 2.5))
      _value = (1 - smooth) * v + smooth * self.value
      change = _value - self.value
      change *= mult
      self._value += change
    return self.value

  def __repr__(self):
    return f'<{self.__class__.__name__}: (value:{self.value}, smooth:{self.smooth})>'


class EMASmoothEMA(AcceleratedEMA):
  _smooth_ema = EmbeddedDocumentField(AcceleratedEMA)

  @property
  def smooth(self): return self._smooth_ema(self._smooth)

  def set_smooth(self, v: float): self._smooth_ema.set(v)
