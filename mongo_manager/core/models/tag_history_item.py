from mongoengine import *
from datetime import datetime
from mongo_manager import *


class TagHistoryItem(EmbeddedDocument):
  # tag = EmbeddedDocumentField(MusicTagSimplified)
  last_used = DateTimeField(default=datetime.utcnow())
  _count = DictField(IntField(), default={'seed': 0, 'include': 0, 'exclude': 0})

  @property
  def score(self):
    return self._count['seed'] + self._count['include']

  def register_use(self, filter_state: FilterState, increment: int = 1):
    self._count[filter_state.name] += increment
    self.last_used = datetime.utcnow()

  # def to_json(self):
  #   return self.tag.to_json()

  # def __repr__(self):
  #   return f'<{self.__class__.__name__}: (name: {self.tag.name})>'
