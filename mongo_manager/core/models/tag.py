from mongoengine import *
from bson import ObjectId

class Tag(Document):
  name = StringField()
  tag_type = StringField()

  @classmethod
  def from_id(cls, id: str):
    return cls.objects().get(id=ObjectId(id))

  def to_dict(self):
    return self.to_mongo().to_dict()