# from mongoengine import *
# from bson import ObjectId
#
#
# class TagWithCount(EmbeddedDocument):
#     name = StringField()
#     count = IntField()
#
#
# class TrackDeprecated(Document):
#     # name_lower = StringField()
#     tag_type = StringField()
#     type = StringField()
#     # name = StringField()
#     # loudness = FloatField()
#     # valence = FloatField()
#     # deepness = FloatField()
#     speechiness = FloatField()
#     emotion = EmbeddedDocumentListField(TagWithCount)
#     about = EmbeddedDocumentListField(TagWithCount)
#     genres = EmbeddedDocumentListField(TagWithCount)
#     language = EmbeddedDocumentListField(TagWithCount)
#     instrument = EmbeddedDocumentListField(TagWithCount)
#     artist = EmbeddedDocumentListField(TagWithCount)
#     album = DictField()
#     artists = ListField(DictField())
#     genres_embed = ListField(StringField())
#     # duration_ms = FloatField()
#     track_cover_url = URLField()
#     sample_url = URLField()
#     # embed = ListField(FloatField())
#     # uri =  StringField()
#
#     @classmethod
#     def from_id(cls, id):
#         return cls.objects().get(id=ObjectId(id))
#
#     def to_dict(self):
#         return self.to_mongo().to_dict()
#
#     meta = {
#         'indexes': [
#             'name_lower',
#             'loudness',
#             'valence',
#             'deepness',
#             'speechiness',
#             'about.name',
#             'instrument.name',
#             'genres.name',
#             'language.name',
#             'artist.name'
#         ],
#     }
#
#
#
#
#
