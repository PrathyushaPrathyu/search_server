from mongoengine import *


class TagsInfo(EmbeddedDocument):
  seed = IntField(min_value=0, default=0)
  include = IntField(min_value=0, default=0)
  exclude = IntField(min_value=0, default=0)
