from mongoengine import *
from .me_tag_simplified import MeTagSimplified
from .tags_info import TagsInfo


class MeTagsHistory(Document):
  tag = EmbeddedDocumentField(MeTagSimplified)
  count = IntField(min_value=0, default=0)

  meta = {'allow_inheritance': True,
          'abstract': True,
          'db-alias': 'user-alias'}
