from mongoengine import *


class MusicTagSimplified(EmbeddedDocument):
  name = StringField()
  tag_type = StringField(choices=[
    'language',
    'genres',
    'emotion',
    'about',
    'instrument',
    'track',
    'artist',
  ])
