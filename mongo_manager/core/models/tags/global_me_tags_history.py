from typing import *
from .me_tags_history import MeTagsHistory
from .me_tag_simplified import MeTagSimplified


class GlobalMeTagsHistory(MeTagsHistory):
  @staticmethod
  def register_tag(tag: MeTagSimplified):
    up = {
      'set__tag': tag,
      'inc__count': 1,
    }
    GlobalMeTagsHistory.objects(tag=tag).update_one(upsert=True, **up)

  @staticmethod
  def get_tags() -> List['GlobalMeTagsHistory']:
    tags = GlobalMeTagsHistory.objects().order_by('-count')
    return tags
