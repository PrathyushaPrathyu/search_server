from mongo_manager import *
from .music_tag_simplified import MusicTagSimplified
from .music_tags_history import MusicTagsHistory


class GlobalMusicTagsHistory(MusicTagsHistory):
  @staticmethod
  def register_tag(tag: MusicTagSimplified, filter_state: FilterState):
    up = {
      'set__tag': tag,
      f'inc__info__{filter_state.name}': 1,
    }
    GlobalMusicTagsHistory.objects(tag=tag).update_one(upsert=True, **up)

  @staticmethod
  def get_tags_of_type(tag_type: str, order_by: FilterState = None) -> List['GlobalMusicTagsHistory']:
    tags = GlobalMusicTagsHistory.objects(tag__tag_type=tag_type)
    if order_by: tags = tags.order_by(f'-info__{order_by.name}')
    return tags
