from mongoengine import *

from mongo_manager import *
from .music_tag_simplified import MusicTagSimplified
from .tags_info import TagsInfo


class MusicTagsHistory(Document):
  tag = EmbeddedDocumentField(MusicTagSimplified)
  info = EmbeddedDocumentField(TagsInfo, default=TagsInfo(seed=0, include=0, exclude=0))

  meta = {'allow_inheritance': True, 'abstract': True}
