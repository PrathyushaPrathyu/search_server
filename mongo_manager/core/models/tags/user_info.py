from mongoengine import *
from mongoengine.context_managers import switch_db


class UserInfo(Document):
  username = StringField(unique=True)
  email = StringField(required=False)
  name = StringField(required=False)
  base_pref = DictField()
  has_spotify = BooleanField(default=False)
  has_apple_music = BooleanField(default=False)

  meta = {
    'indexes': ['username'],
  }

  @classmethod
  def from_user_id(cls, id: str):
    return UserInfo.objects().get(id=id)

  @classmethod
  def from_username(cls, username: str):
    return UserInfo.objects().get(username=username)

  @classmethod
  def from_email(cls, email: str):
    return UserInfo.objects().get(email=email)
