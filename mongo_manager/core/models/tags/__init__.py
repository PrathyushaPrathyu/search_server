from .me_tag_simplified import MeTagSimplified
from .music_tag_simplified import MusicTagSimplified
from .music_tags_history import MusicTagsHistory
from .me_tags_history import MeTagsHistory
from .global_me_tags_history import GlobalMeTagsHistory
from .global_music_tags_history import GlobalMusicTagsHistory
from .user_info import UserInfo
