from mongoengine import *
from bson import ObjectId

class Artist(Document):
  name = StringField()
  name_lower = StringField()
  genres = ListField(DictField())
  service_id = StringField()
  cover_image_url = StringField(required = False)

  @classmethod
  def from_id(cls, id: str):
      return cls.objects().get(id=ObjectId(id))

  def to_dict(self):
      return self.to_mongo().to_dict()