from scipy.special import softmax
from typing import *
from mongoengine import *
from mongo_manager import *
from .context_document import ContextDocument
from .ema_smooth_ema import EMASmoothEMA


class FeatureImportance(ContextDocument):
  user_id = StringField()
  context = StringField()
  _actions_react = DictField()
  _feats_w = DictField()
  _vs = ListField()
  _importance = MapField(EmbeddedDocumentField(EMASmoothEMA))

  @property
  def importance(self):
    for k, v in self._importance.items():
      # TODO: Can be either Features or MusicTag, need to refactor this
      try:
        tag = Features[k]
      except KeyError:
        tag = MusicTagType[k]
    return {tag: v.value or 0.}

  @property
  def feat2w(self) -> Dict[Features, float]:
    feats, vs = zip(*list(self.importance.items()))
    ws = self._importance2w(vs)
    feat2w = {feat: w for feat, w in zip(feats, ws)}
    return feat2w

  def set_importance(self, feats: List[Features], value: float = 3.0, smooth=.9):
    for feat in feats:
      self._importance[feat.name].set(v=value)
      self._importance[feat.name].set_smooth(v=smooth)

  def register_action(self, track_v: np.array, action: str, feat_diff_fn):
    return self._update_importance(track_v=track_v, action=action, feat_diff_fn=feat_diff_fn)

  def _update_importance(self, track_v, action: str, feat_diff_fn):
    if len(self._vs) == 0:
      self._vs.append(track_v)
      return
    v_last = self._vs[-1]
    change = self.feat_change(v1=track_v, v2=v_last, feat_diff_fn=feat_diff_fn)
    smooth_frac = 1. / self._actions_react[action]
    if action == 'LeftSwipe':
      for k, v in change.items(): self._importance[k.name](v, smooth_frac=smooth_frac)
    elif action == 'RightSwipe' or action == 'Complete':
      for k, v in change.items(): self._importance[k.name](-v, smooth_frac=smooth_frac)
      self._vs.append(track_v)
    elif action == 'Search':
      for k, v in change.items(): self._importance[k.name](-v, smooth_frac=smooth_frac)
      self._vs.append(track_v)

  # TODO: v1, v2 to diff
  def feat_change(self, v1, v2, feat_diff_fn: dict):
    feat_diff = feat_diff_fn(v1=v1, v2=v2)
    return {k: v.mean() * self._feats_w[k.name] for k, v in feat_diff.items()}

  def _importance2w(self, a: np.array, w: float = 3.):
    a = np.asarray(a, dtype=float) * w
    return softmax(a)

  @classmethod
  def load(cls, *, user_id: str, context: str, importance, feats_w, actions_react):
    try:
      return cls.objects().get(user_id=user_id, context=context)
    except DoesNotExist:
      return cls(
        user_id=user_id,
        context=context,
        _importance=importance,
        _feats_w=feats_w,
        _actions_react=actions_react,
      )

  @classmethod
  def from_context(cls, user_id: str, context: str, **kwargs):
    return cls.load(user_id=user_id, context=context, **kwargs)

  @classmethod
  def from_global(cls, user_id: str, **kwargs):
    return cls.load(user_id=user_id, context=GLOBAL_CONTEXT, **kwargs)
