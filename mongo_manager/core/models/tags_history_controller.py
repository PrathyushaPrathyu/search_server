import pandas as pd
from boltons.cacheutils import cachedproperty
from mongoengine import *

from .context_document import ContextDocument

from mongo_manager import *
from mongo_manager.tmp.music_tag import MusicTag
from mongo_manager.tmp.me_tag import MeTag
from .tag_history_item import TagHistoryItem
from .tags import GlobalMusicTagsHistory, GlobalMeTagsHistory, MusicTagSimplified, MeTagSimplified


class TagsHistoryController(ContextDocument):
  _tags_session = ListField()
  _tags_history = ListField()

  def _get_tag(self, tag_dict):
    if tag_dict['_tag_class'] == MusicTag.__name__:
      tag = MusicTag(name=tag_dict['name'], tag_type=MusicTagType[tag_dict['tag_type']],
                     filter_state=FilterState[tag_dict['filter_state']])
    elif tag_dict['_tag_class'] == MeTag.__name__:
      tag = MeTag(name=tag_dict['name'])
    else:
      raise ValueError(f'Tag class {tag_dict["_tag_class"]} is not supported')
    return tag

  @cachedproperty
  def tags_history(self):
    return {self._get_tag(o['tag']): o['item'] for o in self._tags_history}

  @cachedproperty
  def tags_session(self):
    return [self._get_tag(o) for o in self._tags_session]

  # TODO:BUG: Might silently fail if we have two tracks with same name and tag_type but are still different
  # Example: A track that have the same name but are from different artists
  def save(self, **kwargs):
    self._tags_session.clear()
    self._tags_history.clear()
    for k, v in self.tags_history.items():
      if isinstance(k, MusicTag):
        tag = {'name': k.name, 'tag_type': k.tag_type.name, 'filter_state': k.filter_state.name,
               '_tag_class': MusicTag.__name__}
      elif isinstance(k, MeTag):
        tag = {'name': k.name, '_tag_class': MeTag.__name__}
      else:
        raise ValueError(f'Tag class {k.__class__.__name__} is not supported')
      self._tags_history.append({'tag': tag, 'item': v})
      self._tags_session.append(tag)
    super().save(**kwargs)

  # TODO: Do we need to call reset on login?
  def reset_session(self):
    self.tags_session.clear()

  def _get_top_recent(self, items, max_top: int, max_recent):
    if items:
      data = [{'score': t.score, 'last_used': t.last_used} for t in items]
      df = pd.DataFrame(data)
      df_top = df[df['score'] > 1].sort_values(['score', 'last_used'], ascending=[False, False])
      top_idxs = list(df_top.index)[:max_top]
      df_recent = df.drop(index=top_idxs).sort_values(['last_used'], ascending=False)
      recent_idxs = list(df_recent.index)[:max_recent]
    else:
      top_idxs, recent_idxs = [], []
    return {'top': top_idxs, 'recent': recent_idxs}

  def get_items(self, tag_class):
    items = [(k, v) for k, v in self.tags_history.items() if isinstance(k, tag_class)]
    if len(items) == 0: return [], []
    tags, history_items = zip(*[(k, t) for k, t in items])
    return tags, history_items

  def get_music_top_recent(self, max_top: int, max_recent: int):
    tags, items = self.get_items(tag_class=MusicTag)
    idxs = self._get_top_recent(items=items, max_top=max_top, max_recent=max_recent)
    return {
      'favoritesMusicTags': [tags[i].to_json() for i in idxs['top']],
      'recentMusicTags': [tags[i].to_json() for i in idxs['recent']],
    }

  def get_me_top_recent(self, max_top: int, max_recent: int):
    tags, items = self.get_items(tag_class=MeTag)
    idxs = self._get_top_recent(items=items, max_top=max_top, max_recent=max_recent)
    return {
      'favoritesMeTags': [tags[i].to_json() for i in idxs['top']],
      'recentMeTags': [tags[i].to_json() for i in idxs['recent']],
    }

  def get_recent(self, n=None):
    items = {'recentMeTags': [], 'recentMusicTags': []}
    for tag, o in sorted(self.tags_history.items(), key=lambda kv: kv[1].last_used, reverse=True)[:n]:
      if isinstance(tag, MeTag):
        items['recentMeTags'].append(tag.to_json())
      elif isinstance(tag, MusicTag):
        items['recentMusicTags'].append(tag.to_json())
      else:
        raise ValueError(f'Tag type {type(tag)} not supported')
    return items

  def get_favorites(self, n=None):
    items = {'favoritesMeTags': [], 'favoritesMusicTags': []}
    for tag, o in sorted(self.tags_history.items(), key=lambda kv: kv[1].score, reverse=True)[:n]:
      if isinstance(tag, MeTag):
        items['favoritesMeTags'].append(tag.to_json())
      elif isinstance(tag, MusicTag):
        items['favoritesMusicTags'].append(tag.to_json())
      else:
        raise ValueError(f'Tag type {type(tag)} not supported')
    return items

  def register_tags(self, tags, increment: int = 1):
    # TODO: Transform tag to MusicTagSimplified
    new_tags = []
    for tag in tags:
      if tag in self.tags_session: continue
      new_tags.append(tag)
      self.tags_session.append(tag)
    # Add new tags to history_deprecated
    for tag in new_tags:
      try:
        item = self.tags_history[tag]
      except KeyError:
        item = TagHistoryItem()
        self.tags_history[tag] = item
      if isinstance(tag, MusicTag):
        item.register_use(filter_state=tag.filter_state, increment=increment)
        tag_simple = MusicTagSimplified(name=tag.name, tag_type=tag.tag_type.name)
        GlobalMusicTagsHistory.register_tag(tag=tag_simple, filter_state=tag.filter_state)
      else:
        item.register_use(filter_state=FilterState.include, increment=increment)
        tag_simple = MeTagSimplified(name=tag.name)
        GlobalMeTagsHistory.register_tag(tag=tag_simple)

  @classmethod
  def load(cls, user_id: str):
    return cls.from_global(user_id=user_id)
