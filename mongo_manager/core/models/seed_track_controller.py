from mongoengine import *
from mongo_manager import *
from .context_document import ContextDocument


class SeedTrackController(ContextDocument):
  user_id = StringField()
  seed_tdoc = DictField()
  seed_v = ListField(FloatField())

  def reset_session(self):
    self.seed_v.clear()
    self.seed_tdoc.clear()

  def get_seed_v(self):
    if len(self.seed_v) == 0: raise SeedTrackNotAvailable
    return self.seed_v

  def register_action(
      self,
      seed_tdoc,
      seed_v,
      action: str,
  ):
    if seed_tdoc is None: return
    # TODO: For showing artists
    if action == 'LeftSwipe':
      pass
      # seed_feats.update(feat_preference_control.sample())
    self.seed_tdoc = seed_tdoc
    self.seed_v = list(seed_v)

  def set_seed(self, seed_tdoc, seed_v):
    self.seed_tdoc = seed_tdoc
    self.seed_v = list(seed_v)

  @classmethod
  def load(cls, user_id):
    try:
      return cls.objects().get(user_id=user_id)
    except DoesNotExist:
      return cls(user_id=user_id)
