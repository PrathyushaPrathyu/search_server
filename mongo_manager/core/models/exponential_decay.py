from mongoengine import *
from .ema import EMA


class ExponentialDecay(EmbeddedDocument):
  add_smooth = FloatField(min_value=0.0, max_value=1.0)
  ema = EmbeddedDocumentField(EMA)

  @property
  def value(self): return self.ema.value

  def set(self, v: float): return self.ema.set(v)

  def add(self, v):
    current = self.ema.value
    return self.ema(current + v, smooth=self.add_smooth)

  def step(self):
    return self.ema(0)

  def __repr__(self): return f'<ExponentialDecay: {self.value}>'
