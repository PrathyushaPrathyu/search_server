import os, pymongo
from bson import ObjectId
from typing import *


class _DBManager:
  DB_IP = 'localhost'
  DB_PORT = '27421'

  def __init__(self):
    self._username = os.getenv('USERNAME')
    self._password = os.getenv('PASSWORD')
    self._debug = False
    self._client = pymongo.MongoClient(host=self.DB_IP,
                                       username=self._username,
                                       password=self._password,
                                       port=int(self.DB_PORT))

  @property
  def db_music(self): return self._client.music

  @property
  def col_track(self): return self.db_music.track_v0_0

  @property
  def col_language(self): return self.db_music.language

  @property
  def col_playlist_name(self): return self.db_music.playlist_name

  @property
  def col_playlist_tags(self): return self.db_music.playlist_tags

  @property
  def col_track_tags(self): return self.db_music.track_tags

  @property
  def col_tags(self): return self.db_music.tags

  @property
  def col_genre_hierarchy(self): return self.db_music.genre_hierarchy

  @property
  def col_artist(self): return self.db_music.artist

  @property
  def col_friends(self): return self.db_music.friends


_db_manager = _DBManager()


def get_music_db(): return _db_manager.db_music


def get_tcol(): return _db_manager.col_track


def get_language_col(): return _db_manager.col_language


def get_pcol_name(): return _db_manager.col_playlist_name


def get_pcol_tags(): return _db_manager.col_playlist_tags


def get_tcol_tags(): return _db_manager.col_track_tags


def get_tags_col(): return _db_manager.col_tags


def get_genre_hierarchy_col(): return _db_manager.col_genre_hierarchy


def get_artist_col(): return _db_manager.col_artist


def get_friends_col() -> Collection: return _db_manager.col_friends


def get_tdoc(_id: Union[str, ObjectId] = None, name: str = None, name_sheet: str = None, uri: str = None):
  tcol = get_tcol()
  if _id:
    return tcol.find_one({'_id': ObjectId(_id)})
  elif name:
    return tcol.find_one({'name_lower': name.lower()})
  elif name_sheet:
    return tcol.find_one({'name_sheet': name_sheet})
  elif uri:
    return tcol.find_one({'_id': uri.split(':')[-1]})
  else:
    raise ValueError('At least one parameter should be supplied')


def get_tdocs_filt(_id: List[Union[str, ObjectId]] = None, name: List[str] = None, name_sheet: List[str] = None,
                   uri: List[str] = None):
  if _id:
    return {'_id': {'$in': [ObjectId(o) for o in _id]}}
  elif name:
    return {'name_lower': {'$in': [o.lower() for o in name]}}
  elif name_sheet:
    return {'name_sheet': {'$in': name_sheet}}
  elif uri:
    return {'_id': {'$in': [o.split(':')[-1] for o in uri]}}
  else:
    raise ValueError('At least one parameter should be supplied')


def get_tdocs(_id: List[Union[str, ObjectId]] = None, name: List[str] = None, name_sheet: List[str] = None,
              uri: List[str] = None):
  tcol = get_tcol()
  return tcol.find(get_tdocs_filt(_id=_id, name=name, name_sheet=name_sheet, uri=uri))
