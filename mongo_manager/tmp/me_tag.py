import json
from mongo_manager import *


# Should be a document
class MeTag:
  def __init__(self, name: str, type:MeTagType):
    self.name = name
    self.type = type

  def to_json(self): return json.loads(json.dumps({snake2lowerCamel(k): v for k, v in self.__dict__.items()}))

  def __eq__(self, other):
    if isinstance(other, self.__class__):
      return self.name.lower() == other.name.lower()
    return False

  def __hash__(self):
    return hash(self.name.lower())

  @classmethod
  def from_json(cls, x):
    return cls(name=x['name'].lower(), type=MeTagType[x['type']])

  @staticmethod
  def from_list_json(list_json):
    return [MeTag.from_json(o) for o in list_json]
