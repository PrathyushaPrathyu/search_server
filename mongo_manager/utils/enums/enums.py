from mongo_manager import *
from enum import Enum
from ..string_utils import *


class DBEnum(str, Enum):
  @property
  def db_field(self):
    return camel2snake(self.name)

  @classmethod
  def from_db_field(cls, name):
    return cls[snake2lowerCamel(name)]


class Features(DBEnum):
  valence = 'valence'
  loudness = 'loudness'
  deepness = 'deepness'
  speechiness = 'speechiness'
  popularity = 'popularity'
  date = 'date'


class FilterState(DBEnum):
  seed = 'seed'
  include = 'include'
  exclude = 'exclude'


# TODO:HACK Using this class to quick fix tag formulation (they now have a count)
class AddNameHackMixin(DBEnum):
  @property
  def db_field_hack(self):
    if self.db_field == 'track': return self.db_field
    return self.db_field + '.name'

  @property
  def db_field_count(self):
    if self.db_field == 'track': raise ValueError('Cannot get score for tag of type track')
    return self.db_field + '.count'


class MusicTagType(AddNameHackMixin, DBEnum):
  languages = 'languages'
  genres = 'genres'
  emotion = 'emotion'
  about = 'about'
  instruments = 'instruments'
  track = 'track'
  artists = 'artists'
  album = 'album'


class MeTagType(DBEnum):
  time = 'time'
  location = 'location'
  activity = 'activity'
  feeling = 'feeling'