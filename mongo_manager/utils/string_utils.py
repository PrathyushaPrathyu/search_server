import re


def camel2snake(name):
  s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
  return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def snake2UpperCamel(word):
  'Same as camel case, but first letter is also capitalized'
  return ''.join(x.capitalize() or '_' for x in word.split('_'))


def snake2lowerCamel(snake_str):
  components = snake_str.split('_')
  return components[0] + ''.join(x.title() for x in components[1:])
