def flatten_2d_list(l: list) -> list: return [v for row in l for v in row]


def remove_duplicates(l: list) -> list: return list(set(l))
