from recommender import *

def right_swipe_reward(*, udoc:dict, love_count:float, play_ratio:float) -> float:
  if play_ratio < 0.15: reward = .5 + love_count
  else:                 reward = .8 + love_count
  return _reward_hooks(udoc=udoc, reward=reward)

def left_swipe_reward(*, udoc:dict, love_count:float, play_ratio:float) -> float:
  return 0

def complete_reward(*, udoc:dict, love_count:float):
  reward = .8 + love_count
  return _reward_hooks(udoc=udoc, reward=reward)

def search_reward(udoc:dict) -> float:
  return 1.

def _reward_hooks(udoc, reward):
  reward = _add_bonus_reward(udoc=udoc, reward=reward)
  reward = _bound_reward(reward=reward)
  return reward

# def _add_bonus_reward(udoc, reward):
#   return reward + db.get_bonus_reward(doc=udoc)

def _bound_reward(reward): return min(1., reward)
