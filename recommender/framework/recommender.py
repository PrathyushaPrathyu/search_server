import numpy as np
from typing import *

from recommender.bandit import *
from recommender.errors import *
from recommender.index import SavedIndex
from recommender.interest import Interest
from recommender.enums import *


class Recommender:
  def __init__(self, bandit:Bandit, interests:Dict[InterestTag, Interest]):
    self.bandit,self.interests = bandit,interests

  def next_track(self, last_track_id, played) -> Tuple[int, Arm]:
    while True:
      arm, dist = self.bandit.sample_arm()
      try:
        if arm.mode == ModeTag.exploit:  # Sample a song from interest
          # TODO: last_track_id can be None when starting from context, is this the best way to handle that?
          if last_track_id:
            seq_id = self.interests[arm.interest].sample_near(id=last_track_id, played=played, index=SavedIndex.comb_index)
          else:
            seq_id = self.interests[arm.interest].sample()
        if arm.mode == ModeTag.explore:  # Find a nn of a song for now
          i = self.interests[arm.interest].sample()
          idxs, dists = SavedIndex.comb_index.get_nns_by_name(name=i, n=10+len(played))
          idxs = list(set(idxs) - set(played))
          seq_id = np.random.choice(idxs)
        if arm.mode == ModeTag.immediate:
          seq_id = self.interests[arm.interest].sample_near(index=SavedIndex.comb_index, played=played)
        break
      except ExhaustedTracksError:
        print(f'{arm} exhausted')
      except EmptyInterestError:
        print(f'{arm} has no tracks yet')
    return seq_id, arm

  def register_action(self, *, track_id, action:ActionTag, arm:Arm, reward:float):
    # Update pulled arm
    self.bandit.update_arm(arm=arm, reward=reward)
    # Update arms that also contain song
    for i_arm in self.bandit.arms:
      if track_id in self.interests[i_arm.interest]:
        # Only the pulled arm should receive the full reward
        if i_arm == arm: continue
        self.bandit.update_arm(arm=i_arm, reward=0.7*reward)
    if action in [ActionTag.right, ActionTag.complete] and arm != Arm(ModeTag.immediate, InterestTag.immediate):
      self.bandit.update_arm(arm=Arm(ModeTag.immediate, InterestTag.immediate), reward=.7*reward)
    # Update track in containers
    for interest in self.interests.values():
      if action in [ActionTag.right, ActionTag.complete]:
        interest.register_right(id=track_id, reward=reward)
      elif action == ActionTag.left:
        interest.register_left(id=track_id, reward=reward)

  def save(self, user_id, context):
    for interest in self.interests.values(): interest.context_save(user_id=user_id, context=context)
    self.bandit.context_save(user_id=user_id, context=context)

