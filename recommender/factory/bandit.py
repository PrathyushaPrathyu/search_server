from recommender.bandit import Bandit
from .arms import new_arms


def get_bandit(user_id, context) -> Bandit:
  try: return Bandit.from_context(user_id=user_id, context=context)
  except FileNotFoundError: ### Create new bandit
    arms = new_arms()
    return Bandit(arms=arms)
