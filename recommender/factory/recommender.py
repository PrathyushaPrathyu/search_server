from recommender.framework import Recommender
from recommender.factory import *


def get_recommender(user_id, context) -> Recommender:
  'Gets the recommender for the specific user and context'
  bandit = get_bandit(user_id=user_id, context=context)
  interests = get_interests(user_id=user_id, context=context)
  return Recommender(bandit=bandit, interests=interests)
