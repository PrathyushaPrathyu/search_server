from recommender import *

def get_feat_importance() -> FeatureImportance:
  scaler = EmbedScaler.from_file('/home/admin/embed_models/')
  embedder = TrackEmbedder.from_file('/home/admin/embed_models/')
  feat_importance = FeatureImportancePlot(
    embedder=embedder,
    smooth=0.8,
    scaler=scaler,
    feats_w={
      MusicTagType.genres: 2.0, # The final change in genre will be multiplied by 2.5
    },
    actions_react={
      Search: 4., # Importance changes caused by search are 4x more reactive
      LeftSwipe: 2.,
    }
  )
  return feat_importance
