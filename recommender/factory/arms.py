from typing import *

from recommender.bandit import Arm
from recommender.enums import *
from recommender.dists import *

# def new_arms() -> Dict[Arm, Distribution]:
#   arms = {
#     Arm(ModeTag.exploit, InterestTag.glt): BetaReactive(1, 1, entropy=.01, react=.4),
#     Arm(ModeTag.exploit, InterestTag.gst): BetaReactive(1, 1, entropy=.01, react=.4),
#     Arm(ModeTag.exploit, InterestTag.slt): BetaReactive(1, 1, entropy=.01, react=.4),
#     Arm(ModeTag.exploit, InterestTag.sst): BetaReactive(1, 1, entropy=.01, react=.4),
#     Arm(ModeTag.explore, InterestTag.glt): BetaReactive(1, 1, entropy=.01, react=.4),
#     Arm(ModeTag.explore, InterestTag.gst): BetaReactive(1, 1, entropy=.01, react=.4),
#     Arm(ModeTag.explore, InterestTag.slt): BetaReactive(1, 1, entropy=.01, react=.4),
#     Arm(ModeTag.explore, InterestTag.sst): BetaReactive(1, 1, entropy=.01, react=.4),
#     Arm(ModeTag.immediate, InterestTag.immediate): UnbalancedBetaReactive(
#       1, 1, entropy=.05, react_left=.2, react_right=.7),
#   }
#   return arms

# def new_arms() -> Dict[Arm, Distribution]:
#   arms = {
#     Arm(ModeTag.exploit, InterestTag.glt): BetaReactive.from_mean(mean=.3, std=.2, entropy=1., react=.4),
#     Arm(ModeTag.exploit, InterestTag.gst): BetaReactive.from_mean(mean=.4, std=.2, entropy=1., react=.4),
#     Arm(ModeTag.exploit, InterestTag.slt): BetaReactive.from_mean(mean=.5, std=.2, entropy=1., react=.4),
#     Arm(ModeTag.exploit, InterestTag.sst): BetaReactive.from_mean(mean=.6, std=.15, entropy=1., react=.4),
#     Arm(ModeTag.explore, InterestTag.glt): BetaReactive.from_mean(mean=.3, std=.2, entropy=1., react=.4),
#     Arm(ModeTag.explore, InterestTag.gst): BetaReactive.from_mean(mean=.4, std=.2, entropy=1., react=.4),
#     Arm(ModeTag.explore, InterestTag.slt): BetaReactive.from_mean(mean=.5, std=.2, entropy=1., react=.4),
#     Arm(ModeTag.explore, InterestTag.sst): BetaReactive.from_mean(mean=.6, std=.15, entropy=1., react=.4),
#     # Arm(ModeTag.immediate, InterestTag.immediate): UnbalancedBetaReactive.from_mean(
#     #   mean=.7, std=.15, entropy=1., react_left=.2, react_right=.7),
#     Arm(ModeTag.immediate, InterestTag.immediate): BetaReactive.from_mean(mean=.7, std=.15, entropy=1., react=.4),
#   }
#   return arms

def new_arms() -> Dict[Arm, Distribution]:
  arms = {
    Arm(ModeTag.exploit, InterestTag.glt): BetaMeanEMA(mean=.3, std=.15, smooth=.6),
    Arm(ModeTag.exploit, InterestTag.gst): BetaMeanEMA(mean=.4, std=.15, smooth=.6),
    Arm(ModeTag.exploit, InterestTag.slt): BetaMeanEMA(mean=.5, std=.15, smooth=.6),
    Arm(ModeTag.exploit, InterestTag.sst): BetaMeanEMA(mean=.6, std=.15, smooth=.6),
    Arm(ModeTag.explore, InterestTag.glt): BetaMeanEMA(mean=.3, std=.15, smooth=.6),
    Arm(ModeTag.explore, InterestTag.gst): BetaMeanEMA(mean=.4, std=.15, smooth=.6),
    Arm(ModeTag.explore, InterestTag.slt): BetaMeanEMA(mean=.5, std=.15, smooth=.6),
    Arm(ModeTag.explore, InterestTag.sst): BetaMeanEMA(mean=.6, std=.15, smooth=.6),
    Arm(ModeTag.immediate, InterestTag.immediate): BetaMeanMultEMA(mean=.7, std=.2, smooths=[.7, .3]),
  }
  return arms
