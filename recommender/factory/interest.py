from typing import *

from recommender.enums import *
from recommender.interest import *

_interests_fn = {
  InterestTag.glt: GlobalLongInterest,
  InterestTag.gst: GlobalShortInterest,
  InterestTag.slt: SpecificLongInterest,
  InterestTag.sst: SpecificShortInterest,
  InterestTag.immediate: ImmediateInterest,
}

def get_interests(user_id, context, load:bool=True) -> Dict[InterestTag, Interest]:
  interests = {}
  for tag, interest in _interests_fn.items():
    if load:
      try: interests[tag] = interest.from_context(user_id=user_id, context=context)
      except FileNotFoundError: interests[tag] = interest()
    else:
      interests[tag] = interest()
  return interests

def new_interests() -> Dict[InterestTag, Interest]:
  interests = {
    InterestTag.glt: GlobalLongInterest(),
    InterestTag.gst: GlobalShortInterest(),
    InterestTag.slt: SpecificLongInterest(),
    InterestTag.sst: SpecificShortInterest(),
    InterestTag.immediate: ImmediateInterest(),
  }
  return interests

def get_interest_controller(user_id, context, load:bool=True) -> InterestController:
  interests = get_interests(user_id=user_id, context=context, load=load)
  return InterestController(interests=interests)
