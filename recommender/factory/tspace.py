from recommender import *
from recommender import cache
from collections import defaultdict

def get_tspace(
    *,
    embedder:TrackEmbedder,
    scaler:EmbedScaler,
    feat2w:Dict[TrackFeaturesTag, float]=None,
    must_have:List[str]=None,
    exclude:List[str]=None,
    index:Index=None,
    tcol:Collection=None,
    languages:List[str]=None,
    filt:dict=None,
    use_cache:bool=False,
) -> TSpace:
  index = index or FaissIndex(dim=embedder.dim, metric='euclidean')
  index = IndexMap(index)
  tspace = TSpace(
    index=index,
    embedder=embedder,
    scaler=scaler,
    feat2w=feat2w,
  )
  # Create DB queryself._data[k] for k in self._sorted.keys()[i:]
  ops = {}
  if must_have: ops.update({'$all': must_have})
  if exclude: ops.update({'$nin': exclude})
  filt = filt or {}
  if ops: filt['tags'] = ops
  if languages: filt['language'] = {'$in': languages + [None]}
  # filt = {'tags': ops} if ops else {}
  # Add tracks to index
  tcol = tcol or db.get_tcol()
  # IF cache dont use filt
  if use_cache:
    raise ValueError('Not compatible with any kind of filtering')
    if filt: raise ValueError('Currently Filt cannot be used with cache')
    tspace.populate_index(cursor=cache.get_tcol())
  else:
    tspace.populate_index(cursor=tcol.find(filt))
  return tspace

def get_tspace2(token, cursor:None) -> TSpace:
  raise DeprecationWarning
  feat_importance = cache.get_feat_importance(key=token)
  index = FaissIndex(dim=feat_importance.embedder.dim, metric='euclidean')
  index = IndexMap(index)
  tspace = TSpace(
    index=index,
    embedder=feat_importance.embedder,
    scaler=feat_importance.scaler,
    feat2w=feat_importance.feat2w,
  )
  cursor = cursor or db.get_tcol().find({})
  tspace.populate_index(cursor=cursor)
  return tspace

def get_tspace3(feat_importance, cursor:None) -> TSpace:
  index = FaissIndex(dim=feat_importance.embedder.dim, metric='euclidean')
  index = IndexMap(index)
  tspace = TSpace(
    index=index,
    embedder=feat_importance.embedder,
    scaler=feat_importance.scaler,
    feat2w=feat_importance.feat2w,
  )
  cursor = cursor or mm.Track.objects().aggregate({"$match": {}})
  tspace.populate_index(cursor=cursor)
  return tspace
