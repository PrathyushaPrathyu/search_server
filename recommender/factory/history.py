from recommender.history import History


def get_context_history(user_id, context) -> History:
  return History.from_context(user_id=user_id, context=context.lower())

def get_global_history(user_id:str) -> History:
  return History.from_global(user_id=user_id)
