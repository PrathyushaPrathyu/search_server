import mongo_manager as mm
from recommender import *

class PositiveInterest:
  def __init__(self, model:mm.PositiveInterest):
    self.model = model

  def reset_tdoc_tags(self, tdoc):
    return self.model.reset_tdoc_tags(tdoc=tdoc)

  def register_search(self, tdoc):
    return self.model.register_search(tdoc=tdoc)

  def update(self, tdoc, v: float, seed_tdoc=None):
    return self.model.update(tdoc=tdoc, seed_tdoc=seed_tdoc, v=v)

  def update_from_tags(self, tags, v: float):
    return self.model.update_from_tags(tags=tags, v=v)

  def sample(self):
    return self.model.sample()

  def get_pipeline_stages(self):
    return self.model.get_pipeline_stages()

  def save_as_global(self):
    self.model.save()

  def save_as_context(self):
    self.model.save()

  @classmethod
  def from_context(cls, user_id:str, context:str):
    model = mm.PositiveInterest.from_context(user_id=user_id, context=context)
    return cls(model=model)

  @classmethod
  def from_global(cls, user_id:str):
    model = mm.PositiveInterest.from_global(user_id=user_id)
    return cls(model=model)

# from recommender import *
# import random
#
# class PositiveInterest(ContextSerializable2):
#   def __init__(self, user_id: str, context: str):
#     super().__init__(user_id=user_id, context=context)
#     self.tag2sched = defaultdict(lambda: math.ExponentialIncrease(smooth=0.75, add_smooth=0.8))
#     self._sampled_tags = []
#
#   @staticmethod
#   def get_base_path() -> Path:
#     return Path(f'/mnt/disks/dev/tmp/positive_interest')
#
#   def _step(self):
#     [o.step() for o in self.tag2sched.values()]
#     self._sampled_tags = []
#
#   def update(self, tdoc, v:float, seed_tdoc=None):
#     tags = MusicTag.tags_from_tdoc(tdoc)
#     # Only artists tags
#     tags = tags[MusicTagType.artist]
#     self.update_from_tags(tags=tags, v=v)
#
#   def update_from_tags(self, tags, v:float):
#     # logger.debug('Adding positive tags: %s with value %s', [t.name for t in tags], v)
#     for tag in tags: self.tag2sched[tag].update(v)
#     self._step()
#
#   def sample(self):
#     # logger.debug('Positive tags: %s', [(k.name, v.value) for k, v in self.tag2sched.items()])
#     tags = [k for k, v in self.tag2sched.items() if v.value > np.random.uniform() if k not in self._sampled_tags]
#     # TODO: We can take only one of them, currently take the first one
#     # tags = random.choices(tags) if tags else []
#     tags = [max(tags, key=lambda x: self.tag2sched[x].value)] if tags else []
#     # Keep track of sampled tracks so we can get away when the songs from this tag is over
#     self._sampled_tags.extend(tags)
#     info = [(o.name, self.tag2sched[o].value) for o in tags]
#     # logger.debug('Positive sampled tags %s', [f'{name}, {w:.2f}' for name, w in info])
#     return tags
#
#   def get_pipeline_stages(self):
#     tags = self.sample()
#     for tag in tags: tag.filter_state = FilterState.include
#     filters = [t.to_filter(seeding=False) for t in tags]
#     if filters:
#       return [{'$match': {'$and': filters}}]
#     else:
#       return None
#
#   def reset_tdoc_tags(self, tdoc):
#     tags = MusicTag.tags_from_tdoc(tdoc)
#     tags = flatten_2d_list([v for k, v in tags.items() if k in [MusicTagType.artist]])
#     for o in tags: self.tag2sched.pop(o, None)
#     self._step()
