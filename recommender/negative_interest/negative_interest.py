import mongo_manager as mm
from recommender import *

class NegativeInterest:
  def __init__(self, model:mm.NegativeInterest):
    self.model = model

  def reset_tdoc_tags(self, tdoc):
    return self.model.reset_tdoc_tags(tdoc=tdoc)

  def update(self, tdoc, seed_tdoc=None):
    return self.model.update(tdoc=tdoc, seed_tdoc=seed_tdoc)

  def update_from_tags(self, seed_tags, tags):
    return self.model.update_from_tags(seed_tags=seed_tags, tags=tags)

  def sample(self):
    return self.model.sample()

  def get_pipeline_stages(self):
    return self.model.get_pipeline_stages()

  def save_as_global(self):
    self.model.save()

  def save_as_context(self):
    self.model.save()

  @classmethod
  def from_context(cls, user_id:str, context:str):
    model = mm.NegativeInterest.from_context(user_id=user_id, context=context)
    return cls(model=model)

  @classmethod
  def from_global(cls, user_id:str):
    model = mm.NegativeInterest.from_global(user_id=user_id)
    return cls(model=model)

# class NegativeInterest(ContextSerializable2):
#   def __init__(self, user_id: str, context: str):
#     super().__init__(user_id=user_id, context=context)
#     self.tag2sched = defaultdict(lambda: math.ExponentialDecay(smooth=0.95, add_smooth=0.5))
#     self._sampled_tags = []
#
#   @staticmethod
#   def get_base_path() -> Path:
#     return Path(f'/mnt/disks/dev/tmp/negative_interest')
#
#   @property
#   def tags_type(self):
#     return [MusicTagType.language, MusicTagType.genres, MusicTagType.artist, MusicTagType.about, MusicTagType.emotion]
#
#   def _step(self):
#     [o.step() for o in self.tag2sched.values()]
#     self._sampled_tags = []
#
#   def reset_tdoc_tags(self, tdoc):
#     tags = MusicTag.tags_from_tdoc(tdoc)
#     tags = flatten_2d_list([v for k, v in tags.items() if k in self.tags_type])
#     for o in tags: self.tag2sched[o].set(0.0)
#
#   def update(self, tdoc, seed_tdoc=None):
#     if seed_tdoc:
#       seed_tags = MusicTag.tags_from_tdoc(seed_tdoc)
#       seed_tags = flatten_2d_list([v for k, v in seed_tags.items() if k in self.tags_type])
#     else: seed_tags = []
#     tags = MusicTag.tags_from_tdoc(tdoc)
#     tags = flatten_2d_list([v for k, v in tags.items() if k in self.tags_type])
#     self.update_from_tags(seed_tags=seed_tags, tags=tags)
#
#   def update_from_tags(self, seed_tags, tags):
#     diff = set(tags) - set(seed_tags)
#     intersect = set(tags).intersection(set(seed_tags))
#     # Add difference to negative list
#     for o in diff: self.tag2sched[o].add(1.0)
#     # Also add intersection to negative list, but with smaller weight
#     for o in intersect: self.tag2sched[o].add(0.4)
#     # for o in seed_tags: self.tag2sched[o].set(0.0)
#     self._step()
#
#   def sample(self):
#     tags = [k for k, v in self.tag2sched.items() if v.value > np.random.uniform() if k not in self._sampled_tags]
#     self._sampled_tags.extend(tags)
#     info = [(o.name, self.tag2sched[o].value) for o in tags]
#     # logger.debug('Negative sampled tags %s', [f'{name}, {w:.2f}' for name, w in info])
#     return tags
#
#   def get_pipeline_stages(self):
#     tags = self.sample()
#     for tag in tags: tag.filter_state = FilterState.exclude
#     filters = [t.to_filter(seeding=False) for t in tags]
#     if filters:
#       return [{'$match': {'$and': filters}}]
#     else:
#       return [{'$match': {}}]
