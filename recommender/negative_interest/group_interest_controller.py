from recommender import *
from .interest_controller_interface import InterestControllerInterface
from .negative_interest_controller import NegativeInterestController

class GroupInterestController(InterestControllerInterface):
  def __init__(self, interest_controllers:List[NegativeInterestController]):
    self.controllers = interest_controllers

  def register_action(self, action: Action, seed_tdoc, tdoc):
    for o in self.controllers: o.register_action(action=action, seed_tdoc=seed_tdoc, tdoc=tdoc)

  def update_positive(self, tdoc, action: Action, seed_tdoc=None):
    for o in self.controllers: o.update_positive(tdoc=tdoc, action=action, seed_tdoc=seed_tdoc)

  def update(self, tdoc, seed_tdoc=None):
    for o in self.controllers: o.update(tdoc=tdoc, seed_tdoc=seed_tdoc)

  def reset_tdoc_tags(self, tdoc):
    for o in self.controllers: o.reset_tdoc_tags(tdoc=tdoc)

  def positive_reset_tdoc_tags(self, tdoc):
    for o in self.controllers: o.reset_tdoc_tags(tdoc=tdoc)

  def get_pipeline_stages(self):
    return [stage for o in self.controllers for stage in o.get_pipeline_stages()]

  def save(self):
    for o in self.controllers: o.save()

  @classmethod
  def load(cls, user_id: str, contexts: List[str]):
    raise NotImplementedError

  @classmethod
  def from_users(cls, users_ids:List[str]):
    controllers = [NegativeInterestController.load(user_id=user_id, contexts=[]) for user_id in users_ids]
    return cls(interest_controllers=controllers)
