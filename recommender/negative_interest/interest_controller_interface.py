from recommender import *

class InterestControllerInterface:
  def register_action(self, action:Action, seed_tdoc, tdoc): raise NotImplementedError

  def update_positive(self, tdoc, action:Action, seed_tdoc=None): raise NotImplementedError

  def update(self, tdoc, seed_tdoc=None): raise NotImplementedError

  def reset_tdoc_tags(self, tdoc): raise NotImplementedError

  def positive_reset_tdoc_tags(self, tdoc): raise NotImplementedError

  def get_pipeline_stages(self): raise NotImplementedError

  def save(self): raise NotImplementedError

  @classmethod
  def load(cls, user_id:str, contexts:List[str]): raise NotImplementedError

