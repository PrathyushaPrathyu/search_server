from recommender import *
from .interest_controller_interface import InterestControllerInterface
from .negative_interest import NegativeInterest
from .positive_interest import PositiveInterest

# TODO: Rename to InterestController or something similar. This now controls both negative and positive interests
class NegativeInterestController(InterestControllerInterface):
  def __init__(self, global_, locals_, positive_global:PositiveInterest, positive_locals:List[PositiveInterest]):
    self.global_, self.locals_ = global_, locals_
    self.positive_global, self.positive_locals = positive_global, positive_locals

  def register_action(self, action:Action, seed_tdoc, tdoc):
    # For negative interest
    if action == LeftSwipe:
      self.update(seed_tdoc=seed_tdoc, tdoc=tdoc)
      self.positive_reset_tdoc_tags(tdoc=tdoc)
    if action == Search or action == RightSwipe or action == Complete:
      self.reset_tdoc_tags(tdoc=tdoc)
      self.update_positive(tdoc=tdoc, seed_tdoc=seed_tdoc, action=action)

  def register_search(self, tdoc):
    self.reset_tdoc_tags(tdoc=tdoc)
    self.positive_global.register_search(tdoc=tdoc)
    for o in self.positive_locals: o.register_search(tdoc=tdoc)

  def update_positive(self, tdoc, action:Action, seed_tdoc=None):
    # v = reward_from_action(action) + 0.1
    if action == RightSwipe or action == Complete: v = action.love + 0.4
    elif action == Search: v = 3.0
    self.positive_global.update(seed_tdoc=seed_tdoc, tdoc=tdoc, v=v)
    for o in self.positive_locals: o.update(seed_tdoc=seed_tdoc, tdoc=tdoc, v=v)

  def update(self, tdoc, seed_tdoc=None):
    self.global_.update(seed_tdoc=seed_tdoc, tdoc=tdoc)
    for o in self.locals_: o.update(seed_tdoc=seed_tdoc, tdoc=tdoc)

  def reset_tdoc_tags(self, tdoc):
    self.global_.reset_tdoc_tags(tdoc=tdoc)
    for o in self.locals_: o.reset_tdoc_tags(tdoc=tdoc)

  def positive_reset_tdoc_tags(self, tdoc):
    self.positive_global.reset_tdoc_tags(tdoc=tdoc)
    for o in self.positive_locals: o.reset_tdoc_tags(tdoc=tdoc)

  # TODO: Handle multiple contexts
  def get_pipeline_stages(self):
    if len(self.locals_) == 0:
      return self.positive_global.get_pipeline_stages() or self.global_.get_pipeline_stages()
    return self.positive_locals[0].get_pipeline_stages() or self.locals_[0].get_pipeline_stages()

  def save(self):
    self.global_.save_as_global()
    for o in self.locals_: o.save_as_context()
    self.positive_global.save_as_global()
    for o in self.positive_locals: o.save_as_context()

  @classmethod
  def load(cls, user_id:str, contexts:List[str]):
    global_ = NegativeInterest.from_global(user_id=user_id)
    locals_ = [NegativeInterest.from_context(user_id=user_id, context=c) for c in contexts]
    positive_global = PositiveInterest.from_global(user_id=user_id)
    positive_locals = [PositiveInterest.from_context(user_id=user_id, context=c) for c in contexts]
    return cls(global_=global_, locals_=locals_, positive_global=positive_global, positive_locals=positive_locals)



