import time
from functools import wraps


def log_time(f):
  @wraps(f)
  def wrapper(*args, **kwds):
    start = time.time()
    res = f(*args, **kwds)
    end = time.time()
    # TODO: Use logger instead of print
    # TODO: Format output
    print(f'{f.__module__}.{f.__name__} time: {end - start}')
    return res
  return wrapper
