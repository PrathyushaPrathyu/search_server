from collections import deque

class Deque:
    def __init__(self, maxlen:int):
        self._deque = deque(maxlen=maxlen)

    def add(self, v):
        'Append items to deque and return possible removed item'
        removed = None
        try:
            self._deque.remove(v)
        except ValueError:
            if len(self._deque) == self._deque.maxlen:
                removed = self._deque.popleft()
        self._deque.append(v)
        return removed

    def __repr__(self): return self._deque.__repr__()
