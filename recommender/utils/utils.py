def get_nested_key(d: dict, k: str, default=None):
  'k should be in the form of key1.key2.key3...'
  try:
    for x in k.split('.'): d = d[x]
  except KeyError as e:
    if default is not None: return default
    raise e
  return d


def flatten_2d_list(l: list) -> list: return [v for row in l for v in row]


def remove_duplicates(l: list) -> list: return list(set(l))
