import concurrent.futures
from concurrent.futures import ThreadPoolExecutor

class FutureHandler:
  def __init__(self):
    self.futures = []
    self.thread_pool = None

  def fire_and_forget(self, func):
    self.thread_pool = self.thread_pool or ThreadPoolExecutor()
    future = self.thread_pool.submit(func)
    self.watch(future=future)

  def watch(self, future):
    self.futures.append(future)

  def wait(self):
    return [f.result() for f in concurrent.futures.as_completed(self.futures)]

  def raise_any_error(self):
    ongoing_futures = []
    for future in self.futures:
      if future.done():
        future.result()
      else:
        ongoing_futures.append(future)
    self.futures = ongoing_futures

_future_handler = FutureHandler()

def fire_and_forget(func):
  return _future_handler.fire_and_forget(func)
