import pandas as pd


def df_from_gsheet(sheet_id:str, gid:int=0, **kwargs):
  url = f'https://docs.google.com/spreadsheets/d/{sheet_id}/export?gid={gid}&format=csv'
  return pd.read_csv(url, **kwargs)

def df_from_gdoc(doc_id:str, gid:int=0, **kwargs):
  url = f'https://docs.google.com/document/d/{doc_id}/export?gid={gid}&format=txt'
  return pd.read_csv(url, **kwargs)
