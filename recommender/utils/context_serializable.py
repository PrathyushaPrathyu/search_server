from pathlib import Path
from .serializable import Serializable2

class ContextSerializable2(Serializable2):
  def __init__(self, user_id:str, context:str):
    super().__init__()
    self.user_id, self.context = user_id, context

  @staticmethod
  def get_base_path() -> Path: raise NotImplementedError

  def save_as_context(self):
    if not self.context: raise ValueError('Context cannot be None')
    return self.save(self._get_context_path(cls=self.__class__, user_id=self.user_id, context=self.context))

  def save_as_global(self):
    return self.save(self._get_global_path(cls=self.__class__, user_id=self.user_id))

  @classmethod
  def _load(cls, path:Path, user_id:str, context:str):
    try:                      return super(ContextSerializable2, cls).from_file(path)
    except FileNotFoundError: return cls(user_id=user_id, context=context)

  @classmethod
  def from_context(cls, user_id:str, context:str):
    return cls._load(
      path=cls._get_context_path(cls=cls, user_id=user_id, context=context),
      user_id=user_id,
      context=context,
    )

  @classmethod
  def from_global(cls, user_id:str):
    return cls._load(
      path=cls._get_global_path(cls, user_id=user_id),
      user_id=user_id,
      context=None,
    )

  @staticmethod
  def _get_context_path(cls, user_id:str, context:str): return cls.get_base_path()/f'{user_id}/context/{context}'

  @staticmethod
  def _get_global_path(cls, user_id:str):
    return cls.get_base_path()/f'{user_id}/global'
