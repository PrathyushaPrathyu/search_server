from recommender import *

class InstructionsRecorder:
  def __init__(self):
    self._instructions: List[str] = []

  def add(self, x:str) -> None:
    # logger.debug(f'InstructionsRecorder - add | instruction: {x}')
    self._instructions.append(x)

  def to_dict(self) -> dict:
    return self._instructions
