from bson import ObjectId


class Serializable:
  def to_dict(self): return NotImplementedError

class SerializableClass(Serializable):
  def to_dict(self, apply_ObjectId:bool=False):
    d = {}
    for k, v in self.__dict__.items():
      if k == '_id': d[k] = ObjectId(v) if apply_ObjectId else v
      elif k.startswith('_'): continue
      elif isinstance(v, Serializable): d[k] = v.to_dict(apply_ObjectId=apply_ObjectId)
      else: d[k] = v
    return d

class SerializableList(Serializable, list):
  def to_dict(self, apply_ObjectId:bool=False):
    return [o.to_dict(apply_ObjectId=apply_ObjectId) if isinstance(o, Serializable) else o for o in self]
