from tqdm.auto import tqdm

class PBar:
  def __init__(self, gen, length=None, start=0, **kwargs):
    self.length = length or len(gen)
    self.gen,self.start,self.kw = gen,start,kwargs

  def __len__(self): return self.length

  def __iter__(self):
    self.tqdm = tqdm(self.gen, total=len(self), initial=self.start, **self.kw)
    yield from self.tqdm

  @property
  def n(self): return self.tqdm.n