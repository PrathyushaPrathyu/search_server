import numpy as np
from typing import *
from recommender.utils import Serializable2


class VarianceScaler(Serializable2):
  def __init__(self, variances:list):
    super().__init__()
    self.variances = np.asarray(variances)

  @property
  def stds(self): return self.variances ** 0.5

  def get_normalizers(self, slices:List[slice], ws:List[float]):
    sliced_vars = np.array([self.variances[slc].sum() for slc in slices])
    target_var = sliced_vars.mean() * np.asarray(ws)
    normalizer = target_var / sliced_vars
    return normalizer

  # TODO: scale and unscale should call this function to not repeat code
  def _apply(self, v, slices:List[slice], ws=List[float]): pass

  def unscale(self, v, slices, ws):
    assert len(slices) == len(ws)
    v = np.asarray(v).copy()
    if len(v.shape) != 1: raise ValueError(f'v should be 1 dimensional, got {v.shape}')
    assert len(self.variances) == len(v)
    norms = self.get_normalizers(slices=slices, ws=ws)
    assert len(norms) == len(ws)
    for slc, norm in zip(slices, norms): v[slc] /= norm**0.5
    return v

  def scale(self, v, slices:List[slice], ws=List[float]):
    assert len(slices) == len(ws)
    v = np.asarray(v).copy()
    if len(v.shape) != 1: raise ValueError(f'v should be 1 dimensional, got {v.shape}')
    assert len(self.variances) == len(v)
    norms = self.get_normalizers(slices=slices, ws=ws)
    assert len(norms) == len(ws)
    for slc, norm in zip(slices, norms): v[slc] *= norm**0.5
    return v

  @classmethod
  def from_data(cls, data, precomputed:Dict[slice, np.ndarray]=None):
    precomputed = precomputed or {}
    data = np.asarray(data)
    data_var = data.var(axis=0)
    for slc, var in precomputed.items(): data_var[slc] = var
    return cls(variances=data_var)


# class VarianceScaler(Serializable):
#   def __init__(self, slices:List[slice], variances:list, ws:list):
#     assert len(slices) == len(variances) == len(ws)
#     self.slices, self.variances, self.ws = slices, np.asarray(variances), np.asarray(ws)
#     target_var = self.variances.mean() * self.ws
#     self.normalizer = target_var / variances
#
#   def __call__(self, v):
#     v = np.asarray(v).copy()
#     if len(v.shape) != 1: raise ValueError(f'v should be 1 dimensional, got {v.shape}')
#     for slc, norm in zip(self.slices, self.normalizer): v[slc] *= norm**0.5
#     return v
#
#   @classmethod
#   def from_data(cls, data, slices:List[slice], ws:list):
#     assert len(slices) == len(ws)
#     data = np.asarray(data)
#     data_var = data.var(axis=0)
#     variances = [data_var[slc].sum() for slc in slices]
#     return cls(slices=slices, variances=variances, ws=ws)

