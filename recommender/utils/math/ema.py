class SimpleEMA:
  def __init__(self, smooth:float):
    self._smooth = smooth
    self._value = None

  @property
  def smooth(self): return self._smooth

  @property
  def value(self): return self._value

  def set(self, v):
    self._value = v

  def __call__(self, v, smooth:float=None):
    smooth = smooth or self.smooth
    if self._value is None: self._value = v
    else: self._value = smooth * self._value + (1 - smooth) * v
    return self._value

  def __repr__(self):
    return f'<{self.__class__.__name__}: (value:{self.value}, smooth:{self.smooth})>'