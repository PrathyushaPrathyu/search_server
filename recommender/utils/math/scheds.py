from .ema import SimpleEMA

class Schedule:
  def add(self, v): raise NotImplementedError
  def step(self): raise NotImplementedError

class ExponentialDecay(Schedule):
  def __init__(self, smooth:float, add_smooth):
    self.add_smooth = add_smooth
    self.ema = SimpleEMA(smooth=smooth)
    self.ema.set(0)

  def __repr__(self): return f'<ExponentialDecay: {self.value}>'

  @property
  def value(self): return self.ema.value

  def set(self, v:float): return self.ema.set(v)

  def add(self, v):
    # TODO: This should be ema.value instead?
    # current = self.ema(0)
    current = self.ema.value
    return self.ema(current + v, smooth=self.add_smooth)

  def step(self):
    return self.ema(0)

class ExponentialIncrease(Schedule):
  def __init__(self, smooth:float, add_smooth):
    self.add_smooth = add_smooth
    self.ema = SimpleEMA(smooth=smooth)
    self.ema.set(0)

  def __repr__(self): return f'<ExponentialDecay: {self.value}>'

  @property
  def value(self): return self.ema.value

  def set(self, v:float): return self.ema.set(v)

  def add(self, v):
    # TODO: This should be ema.value instead?
    # current = self.ema(0)
    current = self.ema.value
    return self.ema(current + v, smooth=self.add_smooth)

  def update(self, v):
    self.ema(v, smooth=self.add_smooth)

  def step(self):
    return self.ema(1)

def simulate_sched(sched:Schedule):
  import matplotlib.pyplot as plt
  vs = []
  sched.add(v=1)
  vs += [sched.step() for _ in range(5)]
  sched.add(v=1)
  vs += [sched.step() for _ in range(5)]
  sched.add(v=1)
  vs += [sched.step() for _ in range(10)]
  sched.add(v=1)
  vs += [sched.step() for _ in range(10)]
  sched.add(v=1)
  vs += [sched.step() for _ in range(3)]
  sched.add(v=1)
  vs += [sched.step() for _ in range(20)]
  plt.plot(vs)
