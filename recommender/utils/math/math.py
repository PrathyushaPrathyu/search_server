import numpy as np

def weighted_nanmean(v, ws):
  ws = np.asarray(ws)
  ws = ~np.isnan(v) * ws[:, None]
  ws[ws == 0] = np.nan
  ws = ws / np.nansum(ws, axis=0)
  return np.nansum((v * ws), axis=0)

def zero_divide(a, b):
  return np.divide(a, b, out=np.zeros_like(a), where=b!=0)

def unit_v(v):
  shape = v.shape
  if len(shape) == 1: v = v[None]
  v_new = (v / np.linalg.norm(v, axis=1, keepdims=True))
  return v_new.reshape(shape)

# TODO: This class __call__ is doing something more than just moving average
class EMA:
  def __init__(self, smooth:float):
    self._smooth = smooth
    self._value = None

  @property
  def smooth(self): return self._smooth

  @property
  def value(self): return self._value

  def set(self, v):
    self._value = v

  def __call__(self, v, smooth_frac:float=1.):
    smooth = self.smooth * smooth_frac
    if self._value is None:
      self._value = v
      self._aceleration = v
    else:
      aceleration = v - self._value
      mult = np.interp(aceleration, (0, 2.5), (1, 2.5))
      _value = (1-smooth) * v + smooth * self._value
      change = _value - self._value
      change *= mult
      self._value += change
    return self._value

  def __repr__(self):
    return f'<{self.__class__.__name__}: (value:{self.value}, smooth:{self.smooth})>'

class EMASum(EMA):
  def __call__(self, v, smooth_frac:float=1.):
    smooth = self.smooth * smooth_frac
    if self._value is None: self._value = v
    else: self._value = (1-smooth) * (v + self._value) + smooth * self._value
    return self._value

class EMASmoothEMA(EMA):
  def __init__(self, smooth:float, smooth_smooth:float):
    super().__init__(smooth=smooth)
    self._smooth_ema = EMA(smooth=smooth_smooth)

  @property
  def smooth(self): return self._smooth_ema(self._smooth)

  def set_smooth(self, v:float): self._smooth_ema.set(v)

class EMASumSmoothEMA(EMASmoothEMA, EMASum): pass
