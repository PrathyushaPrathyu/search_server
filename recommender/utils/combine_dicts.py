from collections import defaultdict
from typing import *

def combine_dicts(*dicts, values_reduce, keys_reduce=set.union):
  keys:List[set] = []
  default = defaultdict(set)
  for d in dicts:
    keys.append(set(d.keys()))
    for k, v in d.items():
      default[k].add(v)
  if default: return {k: values_reduce(default[k]) for k in keys_reduce(*keys)}
  else:       return {}
