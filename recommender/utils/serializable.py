import dill
from pathlib import Path
from typing import *
from recommender.logger import logger


class Serializable2:
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self._serializable_attrs_cls = {}

  @property
  def serializable_attrs(self):
    return {k: v for k, v in self.__dict__.copy().items() if isinstance(v, Serializable2)}

  @property
  def non_serializable_attrs(self):
    return {k: v for k, v in self.__dict__.copy().items() if not isinstance(v, Serializable2)}

  @property
  def name(self): return self.__class__.__name__

  def __getstate__(self):
    return self.non_serializable_attrs

  def _get_path(self, path):
    path = (Path(path)/self.name).with_suffix('.dill')
    path.parent.mkdir(exist_ok=True, parents=True)
    return path

  # TODO: If a class have multiple of the same serializable attributes it will overwrite
  def save(self, path:Union[str, Path]) -> None:
    # logger.debug(f'{self.name} saving to {path}')
    for k, v in self.serializable_attrs.items():
      v.save(path)
      self._serializable_attrs_cls[k] = v.__class__
    path = self._get_path(path)
    with open(str(path), 'wb') as f: dill.dump(self, f)

  @classmethod
  def from_file(cls, path:Union[str, Path]):
    # logger.debug(f'{cls.__name__} loading from {path}')
    obj = cls._from_file(cls, path)
    loaded = {k: v.from_file(path) for k, v in obj._serializable_attrs_cls.items()}
    obj.__dict__.update(loaded)
    return obj

  @staticmethod
  def _from_file(cls, path:Union[str, Path]):
    path = (Path(path)/cls.__name__).with_suffix('.dill')
    with open(str(path), 'rb') as f:
      obj = dill.load(f)
    if not isinstance(obj, cls):
      raise ValueError(f'Type inconsistency between loaded object {type(obj)} and class {cls}')
    return obj

class Serializable:
  def save(self, path:Union[str, Path]) -> None:
    path = Path(path).with_suffix('.dill')
    path.parent.mkdir(exist_ok=True, parents=True)
    with open(str(path), 'wb') as f:
      dill.dump(self, f)

  @classmethod
  def from_file(cls, path:Union[str, Path]):
    path = Path(path).with_suffix('.dill')
    with open(str(path), 'rb') as f:
      obj = dill.load(f)
    if not isinstance(obj, cls):
      raise ValueError(f'Type inconsistency between loaded object {type(obj)} and class {cls}')
    return obj

class ContextSerializable(Serializable):
  def context_save(self, user_id, context, name):
    path = self._get_path(user_id=user_id, context=context, name=name)
    return self.save(path=path)

  @classmethod
  def from_context(cls, user_id, context, name):
    path = cls._get_path(user_id=user_id, context=context, name=name)
    return cls.from_file(path=path)

  @staticmethod
  def _get_path(user_id, context, name):
    return Path(f'/mnt/disks/dev/data/user/{user_id}/{context}')/name

class ClassNameSerializableMixin:
  def context_save(self, user_id, context):
    name = self.__class__.__name__
    return super().context_save(user_id=user_id, context=context, name=name)

  @classmethod
  def from_context(cls, user_id, context):
    name = cls.__name__
    return super(ClassNameSerializableMixin, cls).from_context(user_id=user_id, context=context, name=name)

