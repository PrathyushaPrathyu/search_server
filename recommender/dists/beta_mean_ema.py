from .beta_mean import BetaMean
from recommender.utils import math

class BetaMeanEMA(BetaMean):
  def __init__(self, mean:float, std:float, smooth:float):
    super().__init__(mean=mean, std=std)
    self.smooth = smooth
    self._mean_ema = math.SimpleEMA(smooth=smooth)

  def _update_mean(self, reward:float):
    self._mean = self._mean_ema(reward)
