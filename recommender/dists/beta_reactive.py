from .beta import Beta
from collections import deque

# class BetaReactive(Beta):
#   def __init__(self, alpha, beta, entropy:float=1., horizon:int=5):
#     super().__init__(alpha=alpha, beta=beta, entropy=entropy)
#     self.horizon = horizon
#     self._alphas = deque([alpha], maxlen=horizon)
#     self._betas = deque([beta], maxlen=horizon)
#
#   @property
#   def alpha(self): return max(1e-6, sum(self._alphas)) / self.entropy
#   @property
#   def beta(self): return max(1e-6, sum(self._betas)) / self.entropy
#
#   def update(self, reward):
#     self._alphas.append(reward)
#     self._betas.append(1-reward)

class BetaReactive(Beta):
  def __init__(self, alpha, beta, entropy:float=1., react:float=.2):
    super().__init__(alpha=alpha, beta=beta, entropy=entropy)
    self.react = react

  def update(self, reward):
    self._alpha = self._update(v_new=reward*self._init_alpha, v_old=self._alpha)
    self._beta = self._update(v_new=(1-reward)*self._init_beta, v_old=self._beta)

  def _update(self, v_new, v_old):
    return (v_new * self.react) + (v_old * (1-self.react))
