from scipy.stats import beta as beta_dist
from .distribution import Distribution

class Beta(Distribution):
  dist_fn = beta_dist

  def __init__(self, alpha, beta, entropy:float=1.):
    self._init_alpha, self._init_beta = alpha, beta
    self._alpha, self._beta, self.entropy = alpha, beta, entropy

  @property
  def alpha(self): return max(self._alpha, 1e-6) / self.entropy
  @property
  def beta(self): return max(self._beta, 1e-6) / self.entropy
  @property
  def mean(self): return self.alpha / (self.alpha + self.beta)
  @property
  def dist(self): return self.dist_fn(self.alpha, self.beta)

  def update(self, reward):
    self._alpha += reward * self._init_alpha
    self._beta += (1 - reward) * self._init_beta

  def set(self, reward):
    self._alpha = reward * self._init_alpha
    self._beta = (1 - reward) * self._init_beta

  @classmethod
  def from_mean(cls, mean:float, std:float, **kwargs):
    # https://stats.stackexchange.com/questions/12232/calculating-the-parameters-of-a-beta-distribution-using-the-mean-and-variance
    assert 0 < mean < 1
    assert 0 < std < 0.25
    # Calculate alpha
    alpha = (1 - mean) / std**2
    alpha -= 1 / mean
    alpha *= mean**2
    # Calculate beta
    beta = (1/mean) - 1
    beta *= alpha
    return cls(alpha=alpha, beta=beta, **kwargs)

  @staticmethod
  def calc_mean(alpha, beta): return alpha / (alpha + beta)

  def __repr__(self):
    return f'<Beta: (alpha:{self.alpha}, beta:{self.beta})>'

