from typing import *

from recommender.utils import math
from .beta_mean import BetaMean

class BetaMeanMultEMA(BetaMean):
  def __init__(self, mean:float, std:float, smooths:List[float], reduce:Callable[[List[float]], float]=max):
    super().__init__(mean=mean, std=std)
    self.smooths, self._reduce = smooths, reduce
    self._mean_emas = [math.SimpleEMA(smooth=smooth) for smooth in smooths]

  def _update_mean(self, reward:float):
    means = [ema(reward) for ema in self._mean_emas]
    mean = self._reduce(means)
    for ema in self._mean_emas: ema.set(v=mean)
    self._mean = mean
