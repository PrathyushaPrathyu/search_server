from .beta import Beta

class UnbalancedBetaReactive(Beta):
  def __init__(self, alpha, beta, *, entropy:float, react_left:float, react_right:float):
    super().__init__(alpha=alpha, beta=beta, entropy=entropy)
    self.react_left, self.react_right = react_left, react_right
    self._alpha1, self._beta1 = alpha, beta
    self._alpha2, self._beta2 = alpha, beta

  def update(self, reward):
    self._alpha1 = self.react_right * reward + (1-self.react_right) * self._alpha
    self._beta1 = self.react_right * (1-reward) + (1-self.react_right) * self._beta
    mean1 = Beta.calc_mean(alpha=self._alpha1, beta=self._beta1)
    self._alpha2 = self.react_left * reward + (1-self.react_left) * self._alpha
    self._beta2 = self.react_left * (1-reward) + (1-self.react_left) * self._beta
    mean2 = Beta.calc_mean(alpha=self._alpha2, beta=self._beta2)

    if mean1 > mean2:
      self._alpha, self._beta = self._alpha1, self._beta1
    else:
      self._alpha, self._beta = self._alpha2, self._beta2

  # TODO: Replicated code from beta
  @classmethod
  def from_mean(cls, mean:float, std:float, **kwargs):
    # https://stats.stackexchange.com/questions/12232/calculating-the-parameters-of-a-beta-distribution-using-the-mean-and-variance
    assert 0 < mean < 1
    assert 0 < std < 0.25
    # Calculate alpha
    alpha = (1 - mean) / std**2
    alpha -= 1 / mean
    alpha *= mean**2
    # Calculate beta
    beta = (1/mean) - 1
    beta *= alpha
    return cls(alpha=alpha, beta=beta, **kwargs)

