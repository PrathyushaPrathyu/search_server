class Distribution:
  @property
  def dist(self): raise NotImplementedError

  def pdf(self, *args, **kwargs): return self.dist.pdf(*args, **kwargs)
  def ppf(self, *args, **kwargs): return self.dist.ppf(*args, **kwargs)
  def sample(self, n:int=None): return self.dist.rvs(size=n)
