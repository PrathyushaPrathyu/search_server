import numpy as np
from .distribution import Distribution
from scipy.stats import beta as beta_dist

class Beta(Distribution):
  @property
  def alpha(self) -> float: raise NotImplementedError
  @property
  def beta(self) -> float: raise NotImplementedError
  @property
  def mean(self) -> float: raise NotImplementedError
  @property
  def std(self) -> float: raise NotImplementedError
  @property
  def dist(self): return beta_dist(self.alpha, self.beta)

  def __repr__(self):
    return f'<Beta: (alpha:{self.alpha}, beta:{self.beta}, mean:{self.mean}, std:{self.std})>'

class BetaMean(Beta):
  def __init__(self, mean:float, std:float):
    self._mean,self._std = mean,std

  def update(self, reward:float):
    self._update_mean(reward)
    self._update_std(reward)

  def _update_mean(self, reward:float):
    self._mean = reward

  def _update_std(self, reward:float):
    pass

  @property
  def std(self) -> float: return min(self._std, self.max_std)
  @property
  def var(self) -> float: return self.std ** 2

  @property
  def mean(self) -> float:
    assert 0 <= self._mean <= 1
    return float(np.clip(self._mean, 0.1, 0.9))

  @property
  def max_var(self) -> float: return self.mean * (1-self.mean)
  @property
  def max_std(self) -> float: return self.max_var ** 0.5

  @property
  def alpha(self) -> float:
    alpha = (1 - self.mean) / self.var
    alpha -= 1 / self.mean
    alpha *= self.mean**2
    return alpha
  @property
  def beta(self) -> float:
    beta = (1/self.mean) - 1
    beta *= self.alpha
    return beta

