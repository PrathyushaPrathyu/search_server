class ExhaustedTracksError(Exception): pass

class EmptyInterestError(Exception): pass

class EmptyTagsError(Exception): pass

class SeedTrackNotAvailable(Exception): pass

class NoTracksAvailable(Exception): pass
