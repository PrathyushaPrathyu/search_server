import hashlib
from bson import ObjectId

def gen_objectid(s):
  "Generates objectid from given string"
  return ObjectId(hashlib.shake_128(str(s).encode('utf-8')).digest(12))
