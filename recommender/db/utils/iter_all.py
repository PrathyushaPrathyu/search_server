import pymongo
from bson import ObjectId

from recommender import *


class IterAll:
  # TODO: add exists option
  # TODO: If we pass start_idx tqdm initial value is wrong
  def __init__(self, col, query=None, projection=None, start_idx:ObjectId=None, limit:int=0):
    self.col,self.query,self.projection,self.start_idx,self.limit = col,query,projection,start_idx,limit
    self.cursor = self._create_cursor()

  def __iter__(self):
    try:
      for doc in self.cursor:
        self.start_idx = doc['_id']
        yield doc
    except pymongo.errors.CursorNotFound as e:
      self.cursor = self._create_cursor(self.cursor)
      yield from self
    except BaseException as e:
      raise e

  def _create_cursor(self, old_cursor=None):
    last_i = old_cursor.n if old_cursor else self.col.find({'_id': {'$lt': self.start_idx}}).count()
    query = self.query or {}
    if self.start_idx: query.update({'_id': {'$gte': self.start_idx}})
    cursor = self.col.find(query, projection=self.projection, limit=self.limit).sort('_id', 1)
    cursor = PBar(gen=cursor,length=self.col.estimated_document_count(),start=last_i)
    return cursor