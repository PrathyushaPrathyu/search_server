from collections import defaultdict
from pymongo.collection import Collection
from recommender import *
from .iter_all import IterAll


class Cache:
  def __init__(self):
    self.cache = defaultdict(dict)

  def __getitem__(self, key): return self.cache[key]

  def __len__(self): return len(self.cache)

  def __iter__(self): yield from PBar(self.cache.values(), length=len(self))

  def __contains__(self, key): return key in self.cache

  def add(self, key, **kwargs):
    self.cache[key].update(kwargs)

  def get(self, key): return self.cache[key]

  def insert_new_col(self, col, ordered=False):
    col.insert_many(self.cache.values(), ordered=ordered)

  @classmethod
  def from_col(cls, col:Collection):
    self = cls()
    for d in IterAll(col): self.add(key=d[TSPACE_ID_FIELD], **d)
    return self
