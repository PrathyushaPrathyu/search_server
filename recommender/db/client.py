import os, pymongo
from bson import ObjectId

_USERNAME = os.getenv('USERNAME')
_PASSWORD = os.getenv('PASSWORD')

_DB_IP = 'localhost'
_DB_PORT = '27421'
_DEBUG = False

_CLIENT = pymongo.MongoClient(host=_DB_IP, username=_USERNAME, password=_PASSWORD, port=int(_DB_PORT))

def is_debug() -> bool: return _DEBUG
def set_debug_mode(v:bool):
  global _DEBUG
  _DEBUG = v

def get_client(): return _CLIENT

def get_user_col() -> pymongo.collection.Collection:
  return get_client().users.user if not _DEBUG else get_client().users.user_dev

def get_udoc(token:str):
  ucol = get_user_col()
  return ucol.find_one({'_id': ObjectId(token)})

def get_udoc_by_credentials(username:str, password:str):
  ucol = get_user_col()
  return ucol.find_one({'username': username})
