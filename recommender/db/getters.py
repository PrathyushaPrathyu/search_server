from .keys import *
from recommender.bandit import Arm
from recommender.utils import get_nested_key

def get_current_context(doc): return get_nested_key(d=doc, k=CURRENT_CONTEXT)

def get_past_contexts(doc): return get_nested_key(d=doc, k=PAST_CONTEXTS)

def get_last_arm(doc): return Arm.from_dict(get_nested_key(d=doc, k=LAST_ARM))

def get_played_tracks(doc): return get_nested_key(d=doc, k=PLAYED_TRACKS)

def get_bonus_reward(doc): return get_nested_key(d=doc, k=BONUS_REWARD)

def get_seed_track(doc): return get_nested_key(d=doc, k=SEED_TRACK)
