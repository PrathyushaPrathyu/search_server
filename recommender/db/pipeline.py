import pprint
from copy import deepcopy
import mongo_manager as mm
from recommender import *
from .music import get_tcol


class Pipeline:
  def __init__(self):
    self._stages = []

  def __str__(self):
    return pprint.pformat(self._stages)

  def add_stage(self, stage):
    self._stages.append(stage)

  def add_stages(self, stages:list):
    self._stages.extend(stages)

  def run(self, projection:List[str]=None):
    # if projection: self._stages.append({'$project': {o: True for o in projection}})
    projection = [{'$project': {o: True for o in projection}}] if projection else []
    return mm.Track.objects().aggregate(*(self._stages + projection))

  def copy(self):
    return deepcopy(self)
