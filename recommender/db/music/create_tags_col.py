from recommender import *
from recommender.db.utils import *
from .client import *

def create_tags_col():
  tags = []
  for tdoc in IterAll(get_tcol()):
    # TODO: Maybe even track should be included
    tags.extend([{
      'tag_type': o.db_field,
      'name': name,
    } for o in MusicTagType if (o != MusicTagType.track and o != MusicTagType.artist) for name in tdoc[o.db_field]])
    # Remove duplicates
  tags = [dict(o) for o in set(frozenset(tag.items()) for tag in tags)]

  tags_col = get_tags_col()
  tags_col.drop()
  tags_col.create_index([('name', 1)])
  tags_col.create_index([('tag_type', 1)])
  tags_col.insert_many(tags)

