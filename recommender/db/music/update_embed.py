import pymongo
from recommender import *
from recommender.tspace import TrackEmbedder
from recommender.db.utils import *
from .client import *


def update_track_embed() -> None:
  embedder = TrackEmbedder()
  ups = []
  for tdoc in IterAll(get_tcol()):
    try: embed = embedder.tdoc2v(tdoc).tolist()
    except EmptyTagsError: continue
    ups.append(pymongo.UpdateOne({'_id': tdoc['_id']} ,{'$set': {'embed': embed}}))
  get_tcol().bulk_write(ups)
