from .client import *

def create_friends_col(drop:bool=False):
  col = get_friends_col()
  if drop: col.drop()
  col.create_index([(FriendsKeys.user_ids, 1)])
  col.create_index([(FriendsKeys.usernames, 1)])

def get_friend_doc(user_id1, user_id2):
  col = get_friends_col()
  return col.find_one({'$and': [
    {FriendsKeys.user_ids: ObjectId(user_id1)},
    {FriendsKeys.user_ids: ObjectId(user_id2)},
  ]})


class FriendsKeys:
  user_ids = 'users._id'
  usernames = 'users.username'