from .client import get_music_db, get_tcol
from recommender.db.utils import IterAll

def create_backup():
  backup = get_music_db().track_backup_v0
  docs = [doc for doc in IterAll(get_tcol())]
  backup.drop()
  backup.insert_many(docs)
