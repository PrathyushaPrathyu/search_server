import pymongo
from bson import ObjectId
from typing import *

from .client import get_user_col
from .music.client import get_tcol, get_friends_col

class DBOperation(dict): pass

class DBWriter:
  def __init__(self, _id:str, col: pymongo.collection.Collection):
    self._id, self._col = ObjectId(_id), col
    self._updates: List[DBOperation] = []

  def register_update(self, op:DBOperation) -> None:
    self._updates.append(op)

  def write(self) -> None:
    ups = []
    for up in self._updates:
      if up: ups.append(pymongo.UpdateOne({'_id': self._id}, update=up))
    if ups: self._col.bulk_write(ups)
    self._updates = []

class UserDBWriter(DBWriter):
  def __init__(self, _id:str):
    super().__init__(_id=_id, col=get_user_col())

class TrackDBWriter(DBWriter):
  def __init__(self, _id:str):
    super().__init__(_id=_id, col=get_tcol())

class FriendsDBWriter(DBWriter):
  def __init__(self, _id:str):
    super().__init__(_id=_id, col=get_friends_col())

  @staticmethod
  def insert_new(doc):
    return get_friends_col().insert_one(doc)
