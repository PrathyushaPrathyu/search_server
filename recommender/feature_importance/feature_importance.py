import mongo_manager as mm
from recommender import *

class FeatureImportance:
  def __init__(self, model:mm.FeatureImportance):
    self.model = model
    self.scaler = EmbedScaler.from_file('/home/admin/embed_models/')
    self.embedder = TrackEmbedder.from_file('/home/admin/embed_models/')

  @property
  def importance(self): self.model.importance

  @property
  def feat2w(self) -> Dict[TrackFeaturesTag, float]:
    return self.model.feat2w

  def set_importance(self, feats:List[TrackFeaturesTag], value:float=3.0, smooth=.9):
    return self.model.set_importance(feats=feats, value=value, smooth=smooth)

  def register_action(self, track_v:np.array, action:Action):
    return self.model.register_action(track_v=track_v, action=action.name, feat_diff_fn=self._calc_feat_diff)

  def feat_change(self, v1, v2):
    return self.model.feat_change(v1=v1, v2=v2, feat_diff_fn=self._calc_feat_diff())

  def save_as_context(self):
    self.model.save()

  def save_as_global(self):
    self.model.save()

  def _calc_feat_diff(self, v1, v2):
    v1, v2 = map(np.asarray, (v1, v2))
    diff = np.abs(v1 - v2) / self.scaler.stds
    feat_diff = self.embedder.v2feats(diff)
    return feat_diff

  @classmethod
  def _load_parameters(cls):
    scaler = EmbedScaler.from_file('/home/admin/embed_models/')
    embedder = TrackEmbedder.from_file('/home/admin/embed_models/')
    # Initialize importance
    initial_importance = {MusicTagType.genres: 3.0}
    importance = {k.name: mm.EMASmoothEMA(_smooth=0.8, _smooth_ema=mm.AcceleratedEMA(_smooth=0.8))
                  for k in embedder.feats2slices}
    for k, v in initial_importance.items():
      importance[k.name].set(v)
      importance[k.name].set_smooth(v=0.95)
    # Initialize feats_w
    feats_w = {MusicTagType.genres: 2.0}
    _feats_w = {k.name: feats_w.get(k, 1.) for k in embedder.feats2slices}
    # Initialize actions_react
    actions_react = {Search: 4., LeftSwipe: 1.}
    _actions_react = {k.__name__: actions_react.get(k.name, 1.) for k in Action.get_values()}
    return dict(
      importance=importance,
      feats_w=_feats_w,
      actions_react=_actions_react,
    )

  @classmethod
  def from_context(cls, user_id:str, context:str):
    kwargs = cls._load_parameters()
    model = mm.FeatureImportance.from_context(user_id=user_id, context=context, **kwargs)
    return cls(model=model)

  @classmethod
  def from_global(cls, user_id:str):
    kwargs = cls._load_parameters()
    model = mm.FeatureImportance.from_global(user_id=user_id, **kwargs)
    return cls(model=model)

# from scipy.special import softmax
#
# from recommender import *
# from recommender.tspace import TrackEmbedder
#
#
# class FeatureImportance(Serializable2):
#   base_path = Path(f'/mnt/disks/dev/tmp/features_importance')
#   def __init__(
#       self,
#       *,
#       user_id:str,
#       embedder:TrackEmbedder,
#       scaler:math.VarianceScaler,
#       smooth:float,
#       context:str=None,
#       feats_w: Dict[TrackFeaturesTag, float]=None,
#       actions_react:Dict[Action, float]=None,
#       initial_importance:Dict[TrackFeaturesTag, float]=None,
#   ):
#     super().__init__()
#     self.user_id, self.context = user_id, context
#     self.embedder, self.scaler = embedder, scaler
#     self._vs = []
#     initial_importance = initial_importance or {}
#     self._importance = {k: math.EMASmoothEMA(smooth=smooth, smooth_smooth=0.9) for k in embedder.feats2slices}
#     for k, v in initial_importance.items():
#       self._importance[k].set(v)
#       self._importance[k].set_smooth(v=0.95)
#     feats_w = feats_w or {}
#     self._feats_w = {k: feats_w.get(k, 1.) for k in embedder.feats2slices}
#     actions_react = actions_react or {}
#     self._actions_react = {k: actions_react.get(k, 1.) for k in Action.get_values()}
#
#   @property
#   def importance(self): return {k: v.value or 0. for k, v in self._importance.items()}
#
#   @property
#   def feat2w(self) -> Dict[TrackFeaturesTag, float]:
#     feats, vs = zip(*list(self.importance.items()))
#     ws = self._importance2w(vs)
#     feat2w = {feat: w for feat, w in zip(feats, ws)}
#     return feat2w
#
#   def set_importance(self, feats:List[TrackFeaturesTag], value:float=3.0, smooth=.9):
#     for feat in feats:
#       self._importance[feat].set(v=value)
#       self._importance[feat].set_smooth(v=smooth)
#
#   def register_action(self, track_v:np.array, action:Action):
#     return self._update_importance(track_v=track_v, action=action)
#
#   def _update_importance(self, track_v, action:ActionTag):
#     if len(self._vs) == 0:
#       self._vs.append(track_v)
#       return
#     v_last = self._vs[-1]
#     change = self.feat_change(v1=track_v, v2=v_last)
#     smooth_frac = 1. / self._actions_react[action]
#     if action == LeftSwipe:
#       for k, v in change.items(): self._importance[k](v, smooth_frac=smooth_frac)
#     # elif action == ActionTag.quick_right:
#     #   for k, v in change.items(): self._importance[k](v, smooth_frac=smooth_frac)
#     elif (action == RightSwipe or action == Complete):
#       for k, v in change.items(): self._importance[k](-v, smooth_frac=smooth_frac)
#       self._vs.append(track_v)
#     elif action == Search:
#       for k, v in change.items(): self._importance[k](-v, smooth_frac=smooth_frac)
#       self._vs.append(track_v)
#
#   def feat_change(self, v1, v2):
#     v1, v2 = map(np.asarray, (v1, v2))
#     diff = np.abs(v1 - v2) / self.scaler.stds
#     feat_diff = self.embedder.v2feats(diff)
#     return {k: v.mean() * self._feats_w[k] for k, v in feat_diff.items()}
#
#   def _importance2w(self, a:np.array, w:float=3.):
#     a = np.asarray(a, dtype=float) * w
#     return softmax(a)
#
#   def save_as_context(self):
#     if not self.context: raise ValueError('Context cannot be None')
#     return self.save(self._get_context_path(user_id=self.user_id, context=self.context))
#
#   def save_as_global(self):
#     return self.save(self._get_global_path(user_id=self.user_id))
#
#   @classmethod
#   def _load(cls, path:Path, user_id:str, context:str):
#     try:                      return super(FeatureImportance, cls).from_file(path)
#     except FileNotFoundError:
#       scaler = EmbedScaler.from_file('/mnt/disks/dev/tmp/v5/var_scaler/base_5genre_3emot_v3_3')
#       embedder = TrackEmbedder.from_file('/mnt/disks/dev/tmp/v5/embedder/base_5genre_3emot_v3_3')
#       return cls(
#         user_id=user_id,
#         context=context,
#         embedder=embedder,
#         smooth=0.8,
#         scaler=scaler,
#         feats_w={
#           MusicTagType.genres: 2.0, # The final change in genre will be multiplied by 2.5
#         },
#         actions_react={
#           Search: 4., # Importance changes caused by search are 4x more reactive
#           LeftSwipe: 1.,
#         },
#         initial_importance={
#           MusicTagType.genres: 3.0,
#         },
#       )
#
#   @classmethod
#   def from_context(cls, user_id:str, context:str):
#     return cls._load(
#       path=cls._get_context_path(user_id=user_id, context=context),
#       user_id=user_id,
#       context=context,
#     )
#
#   @classmethod
#   def from_global(cls, user_id:str):
#     return cls._load(
#       path=cls._get_global_path(user_id=user_id),
#       user_id=user_id,
#       context=None,
#     )
#
#   @staticmethod
#   def _get_context_path(user_id:str, context:str): return FeatureImportance.base_path/f'{user_id}/context/{context}'
#
#   @staticmethod
#   def _get_global_path(user_id:str): return FeatureImportance.base_path/f'{user_id}/global'

