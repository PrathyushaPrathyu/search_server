import matplotlib.pyplot as plt
from abc import ABC
from collections import defaultdict
from .feature_importance import FeatureImportance

from recommender import *


class FeatureImportancePlotMixin(FeatureImportance, ABC):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.history = defaultdict(list)
    self._names = []

  def _update_importance(self, *args, **kwargs):
    super()._update_importance(*args, **kwargs)
    for k, v in self.importance.copy().items(): self.history[k].append(v)

  def register_action(self, track_v, action:ActionTag, name:str):
    super().register_action(track_v=track_v, action=action)
    self._names.append(f'{name} - {action.name}')

  def plot(self):
    fig, ax = plt.subplots()
    for k, v in self.history.items(): self._plot_line(ax=ax, name=k.name, vs=v)
    ax.set_xticklabels(self._names, rotation=45, ha='right')
    plt.legend(loc='best')
    plt.show()
    return fig, ax

  def _plot_line(self, ax, name, vs):
    xs = list(range(len(vs)))
    ax.plot(xs, vs, 'o-', label=name)
    ax.set_xticks(xs)


class FeatureImportancePlot(FeatureImportancePlotMixin, FeatureImportance): pass
