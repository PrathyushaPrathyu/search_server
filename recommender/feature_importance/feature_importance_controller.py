from typing import *

from recommender import *
from .feature_importance_plot_mixin import FeatureImportancePlot

# TODO: Manage multiple contexts
class FeatureImportanceController:
  def __init__(self, global_feat_imp:FeatureImportancePlot, contexts_feat_imp:List[FeatureImportancePlot]):
    self.global_feat_imp = global_feat_imp
    self.contexts_feat_imp = contexts_feat_imp

  @property
  def embedder(self) -> TrackEmbedder: return self.global_feat_imp.embedder

  @property
  def scaler(self) -> EmbedScaler: return self.global_feat_imp.scaler

  @property
  def importance(self):
    if len(self.contexts_feat_imp) > 0: return self.contexts_feat_imp[0].importance
    return self.global_feat_imp.importance

  @property
  def feat2w(self) -> Dict[TrackFeaturesTag, float]:
    if len(self.contexts_feat_imp) > 0: return self.contexts_feat_imp[0].feat2w
    return self.global_feat_imp.feat2w

  def set_importance(self, feats:List[TrackFeaturesTag], value:float=3.0, smooth=.9):
    for o in self.contexts_feat_imp: o.set_importance(feats=feats, value=value, smooth=smooth)
    self.global_feat_imp.set_importance(feats=feats, value=value, smooth=smooth)

  def register_action(self, track_v:np.array, action:Action, name:str):
    for o in self.contexts_feat_imp: o.register_action(track_v=track_v, action=action, name=name)
    self.global_feat_imp.register_action(track_v=track_v, action=action, name=name)

  def feat_change(self, v1, v2):
    for o in self.contexts_feat_imp: o.feat_change(v1=v1, v2=v2)
    self.global_feat_imp.feat_change(v1=v1, v2=v2)

  def plot(self):
    if len(self.contexts_feat_imp) > 0: return self.contexts_feat_imp[0].plot()
    return self.global_feat_imp.plot()

  def save(self):
    self.global_feat_imp.save_as_global()
    for o in self.contexts_feat_imp: o.save_as_context()

  @classmethod
  def load(cls, user_id:str, contexts:List[str]):
    global_feat_imp = FeatureImportancePlot.from_global(user_id=user_id)
    contexts_feat_imp = [FeatureImportancePlot.from_context(user_id=user_id, context=c) for c in contexts]
    return cls(global_feat_imp=global_feat_imp, contexts_feat_imp=contexts_feat_imp)
