import datetime
import mongo_manager as mm
from datetime import datetime
from datetime import timedelta
from collections import defaultdict, Counter
from bson import ObjectId
from recommender.tag.me_tag import MeTag

class UserHistoryPipeline:
    def __init__(self, user_id, group_id, group_user_ids, time, user_minute_hour):
        self.user_id = user_id
        self.group_id = group_id
        self.group_user_ids = group_user_ids
        self.user_minute_hour = user_minute_hour
        self.time = time

    def get_user_history(self,
                         general_long_min_score = 8.,
                         general_short_min_score = 2.,
                         context_long_min_score = 4.,
                         context_short_min_score = 2.,
                         general_long_min_last_score = 2.,
                         general_short_min_last_score = 2.,
                         context_long_min_last_score = 2.,
                         context_short_min_last_score = 1.,
                         general_long_limit = 10,
                         context_long_limit = 10,
                         general_short_limit = 10,
                         context_short_limit = 10,
                         context_short_delta_days = 7,
                         general_short_delta_days = 30
                         ):
        facet_dict = {}

        facet_dict['all_history'] = self._get_all_history_pipeline()

        facet_dict['general_long'] = self._get_general_long_favorites_pipeline(min_score=general_long_min_score,
                                                                               min_last_score = general_long_min_last_score,
                                                                               limit=general_long_limit)

        facet_dict['general_short'] = self._get_general_short_favorites_pipeline(min_score=general_short_min_score,
                                                                                 min_last_score = general_short_min_last_score,
                                                                                 delta_days=general_short_delta_days,
                                                                                 limit=general_short_limit)

        facet_dict['context_long'] = self._get_context_long_favorites_pipeline(min_score=context_long_min_score,
                                                                               min_last_score = context_long_min_last_score,
                                                                               limit= context_long_limit)

        facet_dict['context_short'] = self._get_context_short_favorites_pipeline(min_score=context_short_min_score,
                                                                                 min_last_score = context_short_min_last_score,
                                                                                 delta_days=context_short_delta_days,
                                                                                 limit=context_short_limit)

        pipeline = [
            {"$facet": facet_dict},
        ]

        res = mm.History.objects().aggregate(*pipeline)

        for user_history in res:
            if user_history['all_history']:
                user_history['all_history'] = user_history['all_history'][0]['track_ids']

        return user_history

    def _get_all_history_pipeline(self):
        user_id = self.group_id if self.group_id else self.user_id
        pipeline = [
            {"$match": {"user_id": user_id}},
            {"$project": {"track_id": 1, "_id": 0}},
            {"$group": {
                '_id': None,
                'track_ids': {"$addToSet": "$track_id"}
            }
            },
            {"$project": {"track_ids": 1, "_id": 0}},
        ]
        return pipeline

    def _get_general_long_favorites_pipeline(self, limit, min_score, min_last_score):
        user_id = self.group_id if self.group_id else self.user_id
        general_long_pipeline = [
            {"$match": {"user_id": user_id}},
            {"$project": {"track_id": 1, "_id": 0, "action.score": 1}},
            {"$group": {"_id": "$track_id",
                        "score": {"$sum": "$action.score"},
                        "last_score": {"$last": "$action.score"},
                        }
             },
            {"$match": {"score": {"$gte": min_score}}},
            {"$match": {"last_score": {"$gte": min_last_score}}},
            {"$sort": {"score": -1}},
            {"$limit": 3 * limit},
            {"$sample": {"size": limit}}
        ]

        return general_long_pipeline

    def _get_general_short_favorites_pipeline(self, min_score, min_last_score ,delta_days, limit):
        user_id = self.group_id if self.group_id else self.user_id

        general_short_pipeline = [
            {"$match": {"user_id": user_id}},
            {"$project": {"track_id": 1, "_id": 0, "action.score": 1, "time.server": 1}},
            {"$group": {"_id": "$track_id",
                        "score": {"$sum": "$action.score"},
                        "last_score": {"$last": "$action.score"},
                        "first_listen": {"$min": "$time.server"}
                        }
             },
            {"$match": {"first_listen": {"$gte": datetime.utcnow() - timedelta(days=delta_days)}}},
            {"$match": {"score": {"$gte": min_score}}},
            {"$match": {"last_score": {"$gte": min_last_score}}},
            {"$sort": {"score": -1, "first_listen": -1}},
            {"$limit": limit},
        ]

        return general_short_pipeline

    def _get_context_long_favorites_pipeline(self, min_score, min_last_score, limit):
        user_id = self.group_id if self.group_id else self.user_id

        context_long_pipeline = [
            {"$match": {"user_id": user_id}},
            {"$project": {"track_id": 1, "_id": 0, "action.score": 1, "time": 1}},
            {"$group": {"_id": "$track_id",
                        "min_time_diff": {
                            "$min": {"$abs": {"$subtract": [self.user_minute_hour, "$time.user.minute_hour"]}}},
                        "avg_time": {"$avg": "$time.user.minute_hour"},
                        "score": {"$sum": "$action.score"},
                        "last_score": {"$last": "$action.score"},
                        }
             },
            {"$match": {"score": {"$gte": min_score}}},
            {"$match": {"last_score": {"$gte": min_last_score}}},
            {"$addFields": {"avg_time_diff": {"$abs": {"$subtract": [self.user_minute_hour, "$avg_time"]}}}},
            {"$project": {"_id": 1,
                          "sort_score": {"$divide": [1,
                                                     {"$add": [0.001,
                                                               {"$round": ["$min_time_diff", 0]},
                                                               {"$round": ["$avg_time_diff", 0]},
                                                               ]}
                                                     ]},
                          "score": {"$divide": ["$score",
                                                {"$add": [0.001,
                                                          "$min_time_diff",
                                                          "$avg_time_diff",
                                                          ]}
                                                ]},
                          "min_time_diff": {"$round": ["$min_time_diff", 0]},
                          "avg_time_diff": {"$round": ["$avg_time_diff", 0]},
                          }},
            {"$sort": {"min_time_diff": 1, "sort_score": -1, "score": -1}},
            {"$limit": limit}
        ]

        return context_long_pipeline

    def _get_context_short_favorites_pipeline(self, min_score, min_last_score, delta_days, limit):
        user_id = self.group_id if self.group_id else self.user_id

        delta_millis = delta_days * 24 * 60 * 60 * 1000

        context_short_pipeline = [
            {"$match": {"user_id": user_id}},
            {"$project": {"track_id": 1, "_id": 0, "action.score": 1, "time": 1}},
            {"$group": {"_id": "$track_id",
                        "min_time_diff": {
                            "$min": {"$abs": {"$subtract": [self.user_minute_hour, "$time.user.minute_hour"]}}},
                        "avg_time": {"$avg": "$time.user.minute_hour"},
                        "score": {"$sum": "$action.score"},
                        "last_listen": {"$max": "$time.server"},
                        "last_score": {"$last": "$action.score"}
                        }
             },
            {"$match": {"last_listen": {"$gte": datetime.utcnow() - timedelta(days=delta_days)}}},
            {"$match": {"score": {"$gte": min_score}}},
            {"$match": {"last_score": {"$gte": min_last_score}}},
            {"$addFields": {"avg_time_diff": {"$abs": {"$subtract": [self.user_minute_hour, "$avg_time"]}},
                            "last_listen_ratio": {"$divide": [
                                {"$subtract": [datetime.utcnow(), "$last_listen"]},
                                delta_millis]
                            },
                            }},
            {"$project": {"_id": 1,
                          "last_listen": 1,
                          "last_score": 1,
                          "sort_score": {"$divide": [1,
                                                     {"$add": [0.001,
                                                               {"$round": ["$min_time_diff", 0]},
                                                               {"$round": ["$last_listen_ratio", 0]},
                                                               {"$round": ["$avg_time_diff", 0]},
                                                               ]}
                                                     ]},
                          "score": {"$divide": ["$score",
                                                {"$add": [0.001,
                                                          "$min_time_diff",
                                                          "$last_listen_ratio",
                                                          "$avg_time_diff"]}
                                                ]},
                          "min_time_diff": {"$round": ["$min_time_diff", 0]},
                          "last_listen_ratio": {"$round": ["$last_listen_ratio", 0]},
                          "avg_time_diff": {"$round": ["$avg_time_diff", 0]},
                          }},
            {"$sort": {"min_time_diff": 1, "sort_score": -1, "score": -1}},
            {"$limit": limit},
        ]

        return context_short_pipeline

    def _get_best_tracks_for_group(self, min_score=0.5, limit=50):

        best_tracks_pipeline = [
            {"$match": {"user_id": {"$in": self.group_user_ids}}},
            {"$group": {
                '_id': {'track': "$track_id", 'user': "$user_id"},
                'score': {"$max": "$action.score"},
                'time': {"$max": "$time.server"}
            }},
            {"$match": {"score": {"$gte": min_score}}},
            {"$group": {
                '_id': "$_id.track",
                'users': {"$addToSet": "$_id.user"},
                'score': {"$sum": "$score"},
                'last_listen': {"$max": "$time"},
            }},
            {"$addFields": {"nusers": {"$size": "$users"}}},
            {"$project": {"score": {"$multiply": ["$score", {"$pow": ["$nusers", 3]}]}, "last_listen": 1}},
            {"$sort": {"score": -1, "last_listen": -1}},
            {"$limit": limit},
        ]

        best_tracks = [doc for doc in mm.History.objects.aggregate(*best_tracks_pipeline)]
        best_tracks_dict = {}
        for track in best_tracks: best_tracks_dict[ObjectId(track['_id'])] = track['score']
        return best_tracks_dict

    def _get_threshold_datetime(self, max_days):
        current_timestamp = self.time['server'].timestamp()
        duration_in_seconds = max_days * 24 * 60 * 60
        threshold_timestamp = current_timestamp - duration_in_seconds
        return datetime.datetime.fromtimestamp(threshold_timestamp)

    def get_group_positive_interests(self, limit=50):
        best_tracks = self._get_best_tracks_for_group()
        best_features_pipeline = [
            {"$match": {"_id": {"$in": [k for k in best_tracks]}}},
            {"$project": {"genres": 1, "artists": 1}}
        ]

        features = Counter()

        for tdoc in mm.Track.objects.aggregate(*best_features_pipeline):
            score = best_tracks[tdoc['_id']]
            for genre in tdoc['genres']:
                key = "{},{}".format('genres', genre['name'])
                features[key] += score
            for artist in tdoc['artists']:
                key = "{},{}".format('artists', artist['name'])
                features[key] += 2 * score

        return {k: v for k, v in features.most_common(limit)}

    def get_positive_interests(self, user_history,
                               general_long_weight=1,
                               context_long_weight=5,
                               general_short_weight=5,
                               context_short_weight=50,
                               ):
        favorites = defaultdict(float)
        score_multiplier = {'general_long': general_long_weight,
                            'context_long': context_long_weight,
                            'general_short': general_short_weight,
                            'context_short': context_short_weight}

        for k, v in user_history.items():
            if k != 'all_history' and v:
                max_score = max(o['score'] for o in v)  # yes this can be less than 1, but so will be the other values
                for track in v:
                    favorites[track['_id']] += (track['score'] / max_score) * score_multiplier[k]

        pipeline = [
            {"$match": {"_id": {"$in": [ObjectId(k) for k in favorites]}}},
            {"$project": {"genres": 1, "artists": 1, "languages": 1,
                          "about": 1, "instruments": 1, "album": 1,
                          "valence": 1, "loudness": 1, "speechiness": 1, "deepness": 1}}
        ]
        cursor = mm.Track.objects().aggregate(*pipeline)

        positive_interests = defaultdict(float)
        for track in cursor:
            score = favorites[str(track["_id"])]
            for tag_type, names in track.items():
                if tag_type != '_id' and names:
                    # if tag_type == 'album':
                    #     key = "{},{}".format('album', names['name'])
                    #     positive_interests[key] += score
                    # elif tag_type == 'artists':
                    #     for name in names:
                    #         key = "{},{}".format('artist', name['name'])
                    #         positive_interests[key] += score
                    if tag_type == 'genres':
                        for name in names:
                            key = "{},{}".format('genres', name['name'])
                            positive_interests[key] += score
                    if tag_type == 'artists':
                        for name in names:
                            key = "{},{}".format('artists', name['name'])
                            positive_interests[key] += score
                    elif tag_type == 'languages':
                        for name in names:
                            key = "{},{}".format('language', name['name'])
                            positive_interests[key] += score
                    elif tag_type == 'about':
                        for name in names:
                            key = "{},{}".format('about', name['name'])
                            positive_interests[key] += score
                    elif tag_type == 'instruments':
                        for name in names:
                            key = "{},{}".format('instrument', name['name'])
                            positive_interests[key] += score
                    elif tag_type == 'valence':
                        key = "{},{}".format('valence', names)
                        positive_interests[key] += score
                    elif tag_type == 'loudness':
                        key = "{},{}".format('loudness', names)
                        positive_interests[key] += score
                    elif tag_type == 'deepness':
                        key = "{},{}".format('deepness', names)
                        positive_interests[key] += score
                    elif tag_type == 'speechiness':
                        key = "{},{}".format('speechiness', names)
                        positive_interests[key] += score

        if not positive_interests:
            if self.group_id:
                group_history = self._get_best_tracks_for_group()
                positive_interests = self.get_positive_interests(user_history=group_history)
            else:
                user_info_doc = mm.UserInfo.objects().get(id=ObjectId(self.user_id))
                base_interests = []
                base_interests += [f'genres,{g}' for g in user_info_doc.base_pref['genre']]
                base_interests += [f'language,{g}' for g in user_info_doc.base_pref['language']]

                # this is our backup option, in update interest mults are used with 1.,
                # so this makes sure these do not interfere if actual positive interests are there
                for i in base_interests:
                    positive_interests[i] += 0.1

        return positive_interests