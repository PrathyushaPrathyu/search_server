from recommender import *
import neo_db as nd
import random
import math
from recommender.tag.db_enums import Features, MusicTagType, FilterState
from recommender.tag.music_tag import MusicTag
from collections import Counter
from pprint import pprint

class NeoNextTrackPipeline():
    def __init__(self,
                 user_history,
                 action,
                 simulating=False,
                 positive_interests=None,
                 negative_interests=None,
                 session_listened_tracks=None,
                 primary_mode=None,
                 secondary_mode=None,
                 tag_filters=None,
                 feature_filters=None,
                 ):

        self.user_history = user_history
        self.primary_mode = primary_mode
        self.secondary_mode = secondary_mode
        self.tag_filters = tag_filters
        self.feature_filters = feature_filters
        self.positive_interests = Counter(positive_interests)
        self.negative_interests = negative_interests
        self.session_listened_tracks = session_listened_tracks
        self.action = action
        self.params = {}
        self.is_simulating = simulating

    def run(self, searched_tag:MusicTag = None, seq_ids = None):
        pipeline = []

        # Apply All The Filters Together
        if self.primary_mode == PrimaryMode.Favorites:
            pipeline = self.apply_favorite_tracks_stage(pipeline=pipeline)

        if self.tag_filters:
            pipeline = self.apply_tag_filters_stage(pipeline=pipeline)

        if searched_tag:
            pipeline = self.apply_searched_tag_stage(searched_tag=searched_tag, pipeline=pipeline)

        if self.feature_filters:
            pipeline = self.apply_feature_filters_stage(pipeline=pipeline)

        if self.primary_mode == PrimaryMode.Discover:
            pipeline = self.apply_discover_tracks_stage(pipeline=pipeline)

        if self.session_listened_tracks:
            pipeline = self.apply_session_listened_tracks_stage(pipeline=pipeline)

        # all the steps till here are for filtering, and are set by the user
        filter_pipeline = []
        filter_pipeline.extend(pipeline)

        if self.secondary_mode == SecondaryMode.Wild:
            if self.primary_mode != PrimaryMode.Favorites:
                # These seq_ids are the shuffled ids from numpy
                pipeline = self.apply_seq_ids_stage(pipeline=pipeline, seq_ids=seq_ids)

        # MAGIC ALGORITHM STARTS HERE
        else:
            # best case positive interests, filters and seq_ids, all are happy with each other
            if seq_ids and self.positive_interests:
                print("Applying both tree and and pos")
                pipeline = self.apply_seq_ids_stage(pipeline=pipeline, seq_ids=seq_ids)
                pipeline = self.apply_positive_interests_stage(pipeline=pipeline)
            elif seq_ids:
                print("No pos, applying only tree")
                pipeline = self.apply_seq_ids_stage(pipeline=pipeline, seq_ids=seq_ids)
            elif self.positive_interests:
                print("No tree, applying only pos")
                pipeline = self.apply_positive_interests_stage(pipeline=pipeline)

            if self.negative_interests:
                pipeline = self.apply_negative_interests_stage(pipeline=pipeline)

        pipeline = self.apply_pipeline_return_stage(pipeline=pipeline)

        base_code = ' '.join(pipeline)
        res = nd.api_alpha.read(base_code, **self.params).data()

        if not res:
            # if tree was applied do not apply and see if we get any results
            if seq_ids:
                print("Tree with pos_neg gave no result. Applying only pos_neg")
                no_seq_ids_pipeline = []
                no_seq_ids_pipeline.extend(filter_pipeline)
                if self.positive_interests:
                    no_seq_ids_pipeline = self.apply_positive_interests_stage(pipeline=no_seq_ids_pipeline)
                if self.negative_interests:
                    no_seq_ids_pipeline = self.apply_negative_interests_stage(pipeline=no_seq_ids_pipeline)

                pos_neg_code = ' '.join(self.apply_pipeline_return_stage(pipeline=no_seq_ids_pipeline))
                res = nd.api_alpha.read(pos_neg_code, **self.params).data()

                if not res:
                    print("pos_neg gave no result still. Applying only tree and neg")
                    # assume that our positive / negative_interests code fucked up, so use tree as backup
                    seq_ids_pipeline = []
                    seq_ids_pipeline.extend(filter_pipeline)
                    seq_ids_pipeline = self.apply_seq_ids_stage(seq_ids=seq_ids, pipeline=seq_ids_pipeline)
                    if self.negative_interests:
                        seq_ids_pipeline = self.apply_negative_interests_stage(pipeline=seq_ids_pipeline)

                    tspace_code = ' '.join(self.apply_pipeline_return_stage(pipeline=seq_ids_pipeline))
                    res = nd.api_alpha.read(tspace_code, **self.params).data()

            else:
                print("No tree available, and pos_neg gave no result.")

            # if even this fails, it means our interest logic is just not working, only start with filters then
            if not res:
                print("Applying Only Filters.")
                only_filter_pipeline = self.apply_pipeline_return_stage(pipeline=filter_pipeline)
                only_filter_code = ' '.join(only_filter_pipeline)
                res = nd.api_alpha.read(only_filter_code, **self.params).data()

                # if applying only filters fail as well, it means the filters are too restrictive, raise exception
                if not res:
                    print("No Tracks Available")
                    raise NoTracksAvailable


        return [o['track_id'] for o in res]


    def apply_seq_ids_stage(self, seq_ids, pipeline):
        self.params['seq_ids'] = seq_ids
        if pipeline:
            pipeline.append("MATCH (t) WHERE t.seq_id IN $seq_ids")
        else:
            pipeline.append("MATCH (t:Track) WHERE t.seq_id IN $seq_ids")
        return  pipeline

    def apply_favorite_tracks_stage(self, pipeline):
        favorite_track_ids = set()
        for track in self.user_history.general_long: favorite_track_ids.add(track['_id'])
        for track in self.user_history.general_short: favorite_track_ids.add(track['_id'])
        for track in self.user_history.context_long: favorite_track_ids.add(track['_id'])
        for track in self.user_history.context_short: favorite_track_ids.add(track['_id'])

        # This will be useful when the mode is wild
        favorite_track_ids = list(favorite_track_ids)
        random.shuffle(favorite_track_ids)

        if favorite_track_ids:
            self.params['favorite_track_ids'] = favorite_track_ids
            if pipeline:
                code = """
                MATCH (t) WHERE t.id IN $favorite_track_ids
                """
            else:
                code = """
                MATCH (t:Track) WHERE t.id IN $favorite_track_ids
                """
            pipeline.append(code)

        return pipeline

    def apply_discover_tracks_stage(self, pipeline):
        all_history = [track_id for track_id in self.user_history['all_history']]
        if all_history:
            self.params['all_history'] = all_history
            if pipeline:
                code = """
                MATCH (t) WHERE NOT t.id IN $all_history
                """
            else:
                code = """
                MATCH (t:Track) WHERE NOT t.id IN $all_history
                """
            pipeline.append(code)

        return pipeline

    def apply_session_listened_tracks_stage(self, pipeline):
        if self.session_listened_tracks:
            self.params['session_listened_ids'] = self.session_listened_tracks
            if pipeline:
                code = """
                MATCH (t) WHERE NOT t.id IN $session_listened_ids
                """
            else:
                code = """
                MATCH (t:Track) WHERE NOT t.id IN $session_listened_ids
                """
            pipeline.append(code)

        return pipeline

    def apply_pipeline_return_stage(self, pipeline):
        if pipeline:
            stage_code = "RETURN t.id as track_id LIMIT 10"
        else:
            stage_code = "MATCH (t:Track) WHERE rand() < 0.00001 RETURN t.id as track_id LIMIT 10"
        pipeline.append(stage_code)

        return pipeline

    def apply_feature_filters_stage(self, pipeline):

        stage_conditions = []

        for feature_filter in self.feature_filters:

            if feature_filter.feature == Features.valence:
                min_valence = feature_filter.min
                max_valence = feature_filter.max
                if not (min_valence == 1. and max_valence == 5.):

                    stage_condition = []
                    if stage_conditions:
                        stage_condition.append("MATCH (t) WHERE")
                    else:
                        if pipeline:
                            stage_condition.append("MATCH (t) WHERE")
                        else:
                            stage_condition.append("MATCH (t:Track) WHERE")

                    if (min_valence == max_valence):
                        stage_condition.append("t.valence = $valence")
                        self.params['valence'] = max_valence
                    elif (min_valence == 1.):
                        stage_condition.append("t.valence <= $max_valence")
                        self.params['max_valence'] = max_valence
                    elif (max_valence == 5.):
                        stage_condition.append("t.valence >= $min_valence")
                        self.params['min_valence'] = min_valence
                    else:
                        stage_condition.append("t.valence >= $min_valence AND t.valence <= $max_valence")
                        self.params['min_valence'] = min_valence
                        self.params['max_valence'] = max_valence

                    stage_conditions.append(' '.join(stage_condition))

            if feature_filter.feature == Features.loudness:
                min_loudness = feature_filter.min
                max_loudness = feature_filter.max
                if not (min_loudness == 1. and max_loudness == 8.):

                    stage_condition = []
                    if stage_conditions:
                        stage_condition.append("MATCH (t) WHERE")
                    else:
                        if pipeline:
                            stage_condition.append("MATCH (t) WHERE")
                        else:
                            stage_condition.append("MATCH (t:Track) WHERE")

                    if (min_loudness == max_loudness):
                        stage_condition.append("t.loudness = $loudness")
                        self.params['loudness'] = max_loudness
                    elif (min_loudness == 1.):
                        stage_condition.append("t.loudness <= $max_loudness")
                        self.params['max_loudness'] = max_loudness
                    elif (max_loudness == 8.):
                        stage_condition.append("t.loudness >= $min_loudness")
                        self.params['min_loudness'] = min_loudness
                    else:
                        stage_condition.append("t.loudness >= $min_loudness AND t.loudness <= $max_loudness")
                        self.params['min_loudness'] = min_loudness
                        self.params['max_loudness'] = max_loudness

                    stage_conditions.append(' '.join(stage_condition))

            if feature_filter.feature == Features.deepness:
                min_deepness = feature_filter.min
                max_deepness = feature_filter.max
                if not (min_deepness == 1. and max_deepness == 4.):

                    stage_condition = []
                    if stage_conditions:
                        stage_condition.append("MATCH (t) WHERE")
                    else:
                        if pipeline:
                            stage_condition.append("MATCH (t) WHERE")
                        else:
                            stage_condition.append("MATCH (t:Track) WHERE")

                    if (min_deepness == max_deepness):
                        stage_condition.append("t.deepness = $deepness")
                        self.params['deepness'] = max_deepness
                    elif (min_deepness == 1.):
                        stage_condition.append("t.deepness <= $max_deepness")
                        self.params['max_deepness'] = max_deepness
                    elif (max_deepness == 4.):
                        stage_condition.append("t.deepness >= $min_deepness")
                        self.params['min_deepness'] = min_deepness
                    else:
                        stage_condition.append("t.deepness >= $min_deepness AND t.deepness <= $max_deepness")
                        self.params['min_deepness'] = min_deepness
                        self.params['max_deepness'] = max_deepness

                    stage_conditions.append(' '.join(stage_condition))

            if feature_filter.feature == Features.speechiness:
                min_speechiness = feature_filter.min
                max_speechiness = feature_filter.max
                if not (min_speechiness == -1. and max_speechiness == 1.):

                    stage_condition = []
                    if stage_conditions:
                        stage_condition.append("MATCH (t) WHERE")
                    else:
                        if pipeline:
                            stage_condition.append("MATCH (t) WHERE")
                        else:
                            stage_condition.append("MATCH (t:Track) WHERE")

                    if (min_speechiness == max_speechiness):
                        stage_condition.append("t.speechiness = $speechiness")
                        self.params['speechiness'] = max_speechiness
                    elif (min_speechiness == -1.):
                        stage_condition.append("t.speechiness <= $max_speechiness")
                        self.params['max_speechiness'] = max_speechiness
                    elif (max_speechiness == 1.):
                        stage_condition.append("t.speechiness >= $min_speechiness")
                        self.params['min_speechiness'] = min_speechiness
                    else:
                        stage_condition.append("t.speechiness >= $min_speechiness AND t.speechiness <= $max_speechiness")
                        self.params['min_speechiness'] = min_speechiness
                        self.params['max_speechiness'] = max_speechiness

                    stage_conditions.append(' '.join(stage_condition))


        if stage_conditions:
            pipeline.append(' '.join(stage_conditions))

        return pipeline

    def apply_tag_filters_stage(self, pipeline):

        include_genres = []
        exclude_genres = []
        include_languages = []
        exclude_languages = []
        include_artists = []
        exclude_artists = []
        include_about = []
        exclude_about = []
        include_instruments = []
        exclude_instruments = []

        for tag_filter in self.tag_filters:
            filter_state = tag_filter.filter_state
            tag_name = tag_filter.name
            tag_type = tag_filter.tag_type
            if tag_type == MusicTagType.genres:
                if filter_state == FilterState.exclude: exclude_genres.append(tag_name)
                else: include_genres.append(tag_name)
            if tag_filter.tag_type == MusicTagType.artists:
                if filter_state == FilterState.exclude: exclude_artists.append(tag_name)
                else: include_artists.append(tag_name)
            if tag_filter.tag_type == MusicTagType.languages:
                if filter_state == FilterState.exclude: exclude_languages.append(tag_name)
                else: include_languages.append(tag_name)
            if tag_filter.tag_type == MusicTagType.about:
                if filter_state == FilterState.exclude: exclude_about.append(tag_name)
                else: include_about.append(tag_name)
            if tag_filter.tag_type == MusicTagType.instruments:
                if filter_state == FilterState.exclude: exclude_instruments.append(tag_name)
                else: include_instruments.append(tag_name)

        if include_artists:
            for i, artist in enumerate(include_artists):
                a = f'a{i}'
                if pipeline:
                    pipeline.append(f"MATCH (t)-[:HAS_ARTIST]->({a}:Artist) WHERE {a}.name = '{artist}'")
                else:
                    pipeline.append(f"MATCH (t:Track)-[:HAS_ARTIST]->({a}:Artist) WHERE {a}.name = '{artist}'")

        if include_genres:
            for i, genre in enumerate(include_genres):
                g = f'g{i}'
                if pipeline:
                    pipeline.append(f"MATCH (t)-[:HAS_GENRE]->({g}:Genre) WHERE {g}.name = '{genre}'")
                else:
                    pipeline.append(f"MATCH (t:Track)-[:HAS_GENRE]->({g}:Genre) WHERE {g}.name = '{genre}'")

        if include_languages:
            for i, language in enumerate(include_languages):
                l = f'l{i}'
                if pipeline:
                    pipeline.append(f"MATCH (t)-[:HAS_LANGUAGE]->({l}:Language) WHERE {l}.name = '{language}'")
                else:
                    pipeline.append(f"MATCH (t:Track)-[:HAS_LANGUAGE]->({l}:Language) WHERE {l}.name = '{language}'")

        if include_about:
            for i, about in enumerate(include_about):
                abo = f'abo{i}'
                if pipeline:
                    pipeline.append(f"MATCH (t)-[:HAS_ABOUT]->({abo}:About) WHERE {abo}.name = '{about}'")
                else:
                    pipeline.append(f"MATCH (t:Track)-[:HAS_ABOUT]->({abo}:About) WHERE {abo}.name = '{about}'")

        if include_instruments:
            for i, instrument in enumerate(include_instruments):
                instru = f'instru{i}'
                if pipeline:
                    pipeline.append(f"MATCH (t)-[:HAS_INSTRUMENT]->({instru}:Instrument) WHERE {instru}.name = '{instrument}'")
                else:
                    pipeline.append(f"MATCH (t:Track)-[:HAS_INSTRUMENT]->({instru}:Instrument) WHERE {instru}.name = '{instrument}'")

        if exclude_languages:
            self.params['exclude_languages'] = exclude_languages
            language_stage_list = []
            if pipeline:
                language_stage_list.append("MATCH (t)-[:HAS_LANGUAGE]->(l:Language)")
            else:
                language_stage_list.append("MATCH (t:Track)-[:HAS_LANGUAGE]->(l:Language)")

            language_stage_list.append("WHERE NOT l.name IN $exclude_languages")
            pipeline.append(' '.join(language_stage_list))

        if exclude_genres:
            self.params['exclude_genres'] = exclude_genres
            stage_list = []
            if pipeline:
                stage_list.append("MATCH (t)-[:HAS_GENRE]->(g:Genre)")
            else:
                stage_list.append("MATCH (t:Track)-[:HAS_GENRE]->(g:Genre)")

            stage_list.append("WHERE NOT g.name IN $exclude_genres")
            pipeline.append(' '.join(stage_list))

        if exclude_artists:
            self.params['exclude_artists'] = exclude_artists
            artist_stage_list = []
            if pipeline:
                artist_stage_list.append("MATCH (t)-[:HAS_ARTIST]->(a:Artist)")
            else:
                artist_stage_list.append("MATCH (t:Track)-[:HAS_ARTIST]->(a:Artist)")

            artist_stage_list.append("WHERE NOT a.name IN $exclude_artists")
            pipeline.append(' '.join(artist_stage_list))


        if exclude_about:
            self.params['exclude_about'] = exclude_about
            about_stage_list = []
            if pipeline:
                about_stage_list.append("MATCH (t)-[:HAS_ABOUT]->(about:About)")
            else:
                about_stage_list.append("MATCH (t:Track)-[:HAS_ABOUT]->(about:About)")

            about_stage_list.append("WHERE NOT about.name IN $exclude_about")
            pipeline.append(' '.join(about_stage_list))


        if exclude_instruments:
            self.params['exclude_instruments'] = exclude_instruments
            instrument_stage_list = []
            if pipeline:
                instrument_stage_list.append("MATCH (t)-[:HAS_INSTRUMENT]->(instrument:Instrument)")
            else:
                instrument_stage_list.append("MATCH (t:Track)-[:HAS_INSTRUMENT]->(instrument:Instrument)")

            instrument_stage_list.append("WHERE NOT instrument.name IN $exclude_instruments")
            pipeline.append(' '.join(instrument_stage_list))

        return pipeline

    def apply_positive_interests_stage(self, pipeline, k=5):

        # Adds at least one top genre to positive interests to make the algorithm stable
        top_genre = None
        max_genre_score = 0.


        keys = []
        scores = []

        for key, score in self.positive_interests.most_common(k):
            #Definitely get one top genre, this keeps it stable if the tags are wrong for language etc.
            type, value = key.split(',')
            if type == 'genres' and score > max_genre_score: top_genre = key
            keys.append(key)
            scores.append(score)

        top_sampled_interests = set(random.choices(keys, weights=scores, k=k))
        if top_genre: top_sampled_interests.add(top_genre)

        print(f"POSITIVE : {top_sampled_interests}")


        positive_genres = []
        positive_languages = []
        positive_artists = []
        positive_instruments = []
        positive_about = []
        positive_valence = []
        positive_loudness = []
        positive_deepness = []
        positive_speechiness = []

        for interest in top_sampled_interests:
            type, value = interest.split(',')
            if type == 'genres':
                positive_genres.append(value)

            elif type == 'language':
                positive_languages.append(value)

            elif type == 'artist':
                positive_artists.append(value)

            elif type == 'instrument':
                positive_instruments.append(value)

            elif type == 'about':
                positive_about.append(value)

            elif type == 'valence':
                positive_valence.append(float(value))

            elif type == 'loudness':
                positive_loudness.append(float(value))

            elif type == 'deepness':
                positive_deepness.append(float(value))

            elif type == 'speechiness':
                positive_speechiness.append(float(value))

        if positive_genres:
            self.params['positive_genres'] = positive_genres
            if pipeline:
                pipeline.append("MATCH (t)-[:HAS_GENRE]->(pg:Genre) WHERE pg.name IN $positive_genres")
            else:
                pipeline.append("MATCH (t:Track)-[:HAS_GENRE]->(pg:Genre) WHERE pg.name IN $positive_genres")


        if positive_artists:
            self.params['positive_artists'] = positive_artists
            if pipeline:
                pipeline.append("MATCH (t)-[:HAS_ARTIST]->(pa:Artist) WHERE pa.name IN $positive_artists")
            else:
                pipeline.append("MATCH (t:Track)-[:HAS_ARTIST]->(pa:Artist) WHERE pa.name IN $positive_artists")

        if positive_languages:
            self.params['positive_languages'] = positive_languages
            if pipeline:
                pipeline.append("MATCH (t)-[:HAS_LANGUAGE]->(pl:Language) WHERE pl.name IN $positive_languages")
            else:
                pipeline.append("MATCH (t:Track)-[:HAS_LANGUAGE]->(pl:Language) WHERE pl.name IN $positive_languages")

        if positive_about:
            self.params['positive_about'] = positive_about
            if pipeline:
                pipeline.append("MATCH (t)-[:HAS_ABOUT]->(pab:About) WHERE pab.name IN $positive_about")
            else:
                pipeline.append("MATCH (t:Track)-[:HAS_ABOUT]->(pab:About) WHERE pab.name IN $positive_about")

        if positive_instruments:
            self.params['positive_instruments'] = positive_instruments
            if pipeline:
                pipeline.append("MATCH (t)-[:HAS_INSTRUMENT]->(pin:Instrument) WHERE pin.name IN $positive_instruments")
            else:
                pipeline.append("MATCH (t:Track)-[:HAS_INSTRUMENT]->(pin:Instrument) WHERE pin.name IN $positive_instruments")

        if positive_valence:
            self.params['positive_valence'] = positive_valence
            if pipeline:
                pipeline.append("MATCH (t) WHERE t.valence IN $positive_valence")
            else:
                pipeline.append("MATCH (t:Track) WHERE t.valence IN $positive_valence")

        if positive_loudness:
            self.params['positive_loudness'] = positive_loudness
            if pipeline:
                pipeline.append("MATCH (t) WHERE t.loudness IN $positive_loudness")
            else:
                pipeline.append("MATCH (t:Track) WHERE t.loudness IN $positive_loudness")

        if positive_deepness:
            self.params['positive_deepness'] = positive_deepness
            if pipeline:
                pipeline.append("MATCH (t) WHERE t.deepness IN $positive_deepness")
            else:
                pipeline.append("MATCH (t:Track) WHERE t.deepness IN $positive_deepness")

        if positive_speechiness:
            self.params['positive_speechiness'] = positive_speechiness
            if pipeline:
                pipeline.append("MATCH (t) WHERE t.speechiness IN $positive_speechiness")
            else:
                pipeline.append("MATCH (t:Track) WHERE t.speechiness IN $positive_speechiness")

        return pipeline

    def apply_negative_interests_stage(self,pipeline, k=3):
        keys = []
        scores = []

        # if used directly with negative interests nothing might be found in top3
        final_ni = Counter()

        # valence etc. are not used so do not consider them while sampling
        for i,s in self.negative_interests.items():
            if i.split(',')[0] not in ['valence', 'deepness', 'speechiness', 'loudness']:
                final_ni[i] = s


        for key, score in final_ni.most_common(k):
            keys.append(key)
            scores.append(score)

        if keys:
            top_sampled_interests = set(random.choices(keys, weights=scores, k=k))
            print(f"NEGATIVE : {top_sampled_interests}")
            negative_genres = []
            negative_languages = []
            negative_artists = []
            negative_instruments = []
            negative_about = []

            for interest in top_sampled_interests:
                score = self.negative_interests[interest]
                type, value = interest.split(',')
                if type == 'genres':
                    negative_genres.append(value)

                elif type == 'language':
                    negative_genres.append(value)

                elif type == 'artist':
                    negative_genres.append(value)

                elif type == 'instrument':
                    negative_genres.append(value)

                elif type == 'about':
                    negative_genres.append(value)

            if negative_genres:
                self.params['negative_genres'] = negative_genres
                if pipeline:
                    pipeline.append("MATCH (t)-[:HAS_GENRE]->(ng:Genre) WHERE NOT ng.name IN $negative_genres")
                else:
                    pipeline.append("MATCH (t:Track)-[:HAS_GENRE]->(ng:Genre) WHERE NOT ng.name IN $negative_genres")

            if negative_artists:
                self.params['negative_artists'] = negative_artists
                if pipeline:
                    pipeline.append("MATCH (t)-[:HAS_ARTIST]->(na:Artist) WHERE NOT na.name IN $negative_artists")
                else:
                    pipeline.append("MATCH (t:Track)-[:HAS_ARTIST]->(na:Artist) WHERE NOT na.name IN $negative_artists")

            if negative_languages:
                self.params['negative_languages'] = negative_languages
                if pipeline:
                    pipeline.append("MATCH (t)-[:HAS_LANGUAGE]->(nl:Language) WHERE NOT nl.name IN $negative_languages")
                else:
                    pipeline.append(
                        "MATCH (t:Track)-[:HAS_LANGUAGE]->(nl:Language) WHERE NOT nl.name IN $negative_languages")

            if negative_instruments:
                self.params['negative_instruments'] = negative_instruments
                if pipeline:
                    pipeline.append(
                        "MATCH (t)-[:HAS_INSTRUMENT]->(nin:Instrument) WHERE NOT nin.name IN $negative_instruments")
                else:
                    pipeline.append(
                        "MATCH (t:Track)-[:HAS_INSTRUMENT]->(nin:Instrument) WHERE NOT nin.name IN $negative_instruments")

            if negative_about:
                self.params['negative_about'] = negative_about
                if pipeline:
                    pipeline.append("MATCH (t)-[:HAS_ABOUT]->(nab:About) WHERE NOT nab.name IN $negative_about")
                else:
                    pipeline.append("MATCH (t:Track)-[:HAS_ABOUT]->(nab:About) WHERE NOT nab.name IN $negative_about")

        return pipeline

    def apply_searched_tag_stage(self, searched_tag:MusicTag, pipeline):
        tag_name = searched_tag.name
        tag_type = searched_tag.tag_type

        self.params['searched_tag'] = tag_name

        if tag_type == MusicTagType.genres:
            if pipeline:
                pipeline.append("MATCH (t)-[:HAS_GENRE]->(sg:Genre) WHERE sg.name = $searched_tag")
            else:
                pipeline.append("MATCH (t:Track)-[:HAS_GENRE]->(sg:Genre) WHERE sg.name = $searched_tag")

        elif tag_type == MusicTagType.languages:
            if pipeline:
                pipeline.append("MATCH (t)-[:HAS_LANGUAGE]->(sl:Language) WHERE sl.name = $searched_tag")
            else:
                pipeline.append("MATCH (t:Track)-[:HAS_LANGUAGE]->(sl:Language) WHERE sl.name = $searched_tag")

        elif tag_type == MusicTagType.artists:
            if pipeline:
                pipeline.append("MATCH (t)-[:HAS_ARTIST]->(sa:Artist) WHERE sa.name = $searched_tag")
            else:
                pipeline.append("MATCH (t:Track)-[:HAS_ARTIST]->(sa:Artist) WHERE sa.name = $searched_tag")

        elif tag_type == MusicTagType.about:
            if pipeline:
                pipeline.append("MATCH (t)-[:HAS_ABOUT]->(sabout:About) WHERE sabout.name = $searched_tag")
            else:
                pipeline.append("MATCH (t:Track)-[:HAS_ABOUT]->(sabout:About) WHERE sabout.name = $searched_tag")


        elif tag_type == MusicTagType.instruments:
            if pipeline:
                pipeline.append("MATCH (t)-[:HAS_INSTRUMENT]->(sinstru:Instrument) WHERE sinstru.name = $searched_tag")
            else:
                pipeline.append("MATCH (t:Track)-[:HAS_INSTRUMENT]->(sinstru:Instrument) WHERE sinstru.name = $searched_tag")

        return pipeline
