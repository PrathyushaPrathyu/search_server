from recommender import *
import random
from recommender.tag.db_enums import Features, MusicTagType, FilterState
from recommender.tag.music_tag import MusicTag
from collections import Counter
import mongo_manager as mm
from pprint import pprint
import numpy as np

class NextTrackPipeline:
    def __init__(self,
                 user_id,
                 group_id,
                 group_user_ids,
                 user_history,
                 action,
                 search_seed,
                 simulating=False,
                 positive_interests=None,
                 negative_interests=None,
                 session_listened_tracks=None,
                 searched_tag=None,
                 primary_mode=None,
                 secondary_mode=None,
                 tertiary_mode=None,
                 tag_filters=None,
                 feature_filters=None,
                 ):

        self.user_id = user_id
        self.group_id = group_id
        self.group_user_ids = group_user_ids
        self.user_history = user_history
        self.primary_mode = primary_mode
        self.searched_tag = searched_tag
        self.secondary_mode = secondary_mode
        self.tertiary_mode = tertiary_mode
        self.tag_filters = tag_filters
        self.search_seed = search_seed
        self.feature_filters = feature_filters
        self.positive_interests = Counter(positive_interests)
        self.negative_interests = negative_interests
        self.session_listened_tracks = session_listened_tracks
        self.action = action
        self.is_simulating = simulating
        self.has_apple_music = False
        self.has_spotify = False

    def run(self, seq_ids = None, tag_filter_mode='and_and'):
        pipeline = []
        instructions = {}

        udoc = mm.UserInfo.from_user_id(id=self.user_id)
        self.has_apple_music = udoc.has_apple_music
        self.has_spotify = udoc.has_spotify

        # Apply All The Filters Together
        if self.searched_tag:
            pipeline = self.apply_searched_tag_stage(searched_tag=self.searched_tag, pipeline=pipeline)

        if self.search_seed and not self.searched_tag:
            pipeline = self.apply_search_seed_stage(pipeline=pipeline)

        if self.primary_mode == PrimaryMode.Favorites:
            pipeline = self.apply_favorite_tracks_stage(pipeline=pipeline)

        if self.tertiary_mode == TertiaryMode.Indie:
            pipeline = self.apply_tertiary_stage(pipeline)

        if self.tag_filters:
            if tag_filter_mode == 'and_and':
                pipeline = self.apply_tag_filters_stage(pipeline=pipeline, searched_tag=self.searched_tag)
            elif tag_filter_mode == 'and_or':
                pipeline = self.apply_or_per_category_tag_filters_stage(pipeline=pipeline, searched_tag=self.searched_tag)
            elif tag_filter_mode == 'or_or':
                pipeline = self.apply_or_tag_filters_stage(pipeline=pipeline, searched_tag=self.searched_tag)

        if self.feature_filters:
            pipeline = self.apply_feature_filters_stage(pipeline=pipeline)

        if self.primary_mode == PrimaryMode.Discover:
            pipeline = self.apply_discover_tracks_stage(pipeline=pipeline)

        if self.session_listened_tracks:
            pipeline = self.apply_session_listened_tracks_stage(pipeline=pipeline)

        # # all the steps till here are for filtering, and are set by the user
        # pre_search_filter_pipeline = []
        # pre_search_filter_pipeline.extend(pipeline)

        filter_pipeline = []
        filter_pipeline.extend(pipeline)

        if self.secondary_mode == SecondaryMode.Wild and self.tertiary_mode == TertiaryMode.All:
            if not self.tag_filters and self.primary_mode != PrimaryMode.Favorites:
                # Get random ids from the database
                pipeline = self.apply_shuffle_stage(pipeline)

        pipeline = self.apply_auth_stage(pipeline=pipeline)

        # MAGIC ALGORITHM STARTS HERE
        if (self.secondary_mode == SecondaryMode.Organic):
            # best case positive interests, filters and seq_ids, all are happy with each other
            # if seq_ids and self.positive_interests:
            #     print("Applying both tree and and pos")
            #     pipeline = self.apply_seq_ids_stage(pipeline=pipeline, seq_ids=seq_ids)
            #     pipeline = self.apply_positive_interests_stage(pipeline=pipeline)
            # elif seq_ids:
            #     print("No pos, applying only tree")
            #     pipeline = self.apply_seq_ids_stage(pipeline=pipeline, seq_ids=seq_ids)
            if self.positive_interests:
                print("Applying positive interests stage")
                pipeline = self.apply_positive_interests_stage(pipeline=pipeline)

            if self.negative_interests:
                print("Applying negative interests stage")
                pipeline = self.apply_negative_interests_stage(pipeline=pipeline)

        pipeline = self.apply_pipeline_return_stage(pipeline=pipeline)
        res = [str(o['_id']) for o in mm.Track.objects.aggregate(*pipeline)]
        # print(pipeline)
        # if res: pprint(prp)

        if seq_ids and not res:
            # if tree was applied do not apply and see if we get any results
            print("Tree with pos_neg gave no result. Applying only pos_neg")
            no_seq_ids_pipeline = []
            no_seq_ids_pipeline.extend(filter_pipeline)
            if self.positive_interests:
                no_seq_ids_pipeline = self.apply_positive_interests_stage(pipeline=no_seq_ids_pipeline)
            if self.negative_interests:
                no_seq_ids_pipeline = self.apply_negative_interests_stage(pipeline=no_seq_ids_pipeline)

            prp = no_seq_ids_pipeline
            no_seq_ids_pipeline = self.apply_pipeline_return_stage(pipeline=no_seq_ids_pipeline)
            res = [str(o['_id']) for o in mm.Track.objects.aggregate(*no_seq_ids_pipeline)]
            # if res: pprint(prp)

        if self.positive_interests and not res:
            print("Applying pos_neg without albums and artists")
            without_album_artist_pipeline = []
            without_album_artist_pipeline.extend(filter_pipeline)
            without_album_artist_pipeline = self.apply_positive_interests_stage(pipeline=without_album_artist_pipeline,
                                                                                album_enabled = False,
                                                                                artist_enabled = False)

            prp = without_album_artist_pipeline
            without_album_artist_pipeline = self.apply_pipeline_return_stage(pipeline=without_album_artist_pipeline)
            res = [str(o['_id']) for o in mm.Track.objects.aggregate(*without_album_artist_pipeline)]
            # if res: pprint(prp)

        if self.positive_interests and not res:
            print("Applying pos_neg with just genres and features")
            only_genre_feature_pipeline = []
            only_genre_feature_pipeline.extend(filter_pipeline)
            only_genre_feature_pipeline = self.apply_positive_interests_stage(pipeline=only_genre_feature_pipeline,
                                                                                album_enabled = False,
                                                                                artist_enabled = False,
                                                                                language_enabled = False,
                                                                                instrument_enabled = False,
                                                                                about_enabled = False)

            prp = only_genre_feature_pipeline
            only_genre_feature_pipeline = self.apply_pipeline_return_stage(pipeline=only_genre_feature_pipeline)
            res = [str(o['_id']) for o in mm.Track.objects.aggregate(*only_genre_feature_pipeline)]
            # if res: pprint(prp)

        if not res:
            print("Applying only tree and neg")
            # assume that our positive / negative_interests code fucked up, so use tree as backup
            seq_ids_pipeline = []
            seq_ids_pipeline.extend(filter_pipeline)
            seq_ids_pipeline = self.apply_seq_ids_stage(seq_ids=seq_ids, pipeline=seq_ids_pipeline)
            if self.negative_interests:
                seq_ids_pipeline = self.apply_negative_interests_stage(pipeline=seq_ids_pipeline)

            prp = seq_ids_pipeline
            seq_ids_pipeline = self.apply_pipeline_return_stage(pipeline=seq_ids_pipeline)
            res = [str(o['_id']) for o in mm.Track.objects.aggregate(*seq_ids_pipeline)]
            # if res: pprint(prp)

        if not res:
            print("Applying Only Filters + Search")
            only_filter_search_pipeline = self.apply_pipeline_return_stage(pipeline=filter_pipeline)
            res = [str(o['_id']) for o in mm.Track.objects.aggregate(*only_filter_search_pipeline)]
            # if res: pprint(filter_pipeline)

        if not res:
            print("Applying Only Filters.")
            only_filter_pipeline = self.apply_pipeline_return_stage(pipeline=filter_pipeline)
            res = [str(o['_id']) for o in mm.Track.objects.aggregate(*only_filter_pipeline)]
            # if res: pprint(pre_search_filter_pipeline)
        # if not res:
        #     print("Applying filter not to repeat songs in a session")

        return res, instructions


    def apply_seq_ids_stage(self, seq_ids, pipeline):
        if seq_ids: pipeline.append({"$match": {"seq_id": {"$in": seq_ids}}})
        return  pipeline

    # 2. can be easily achieved by loving and playing full, not necessarily my favorite, but let's go with it for now
    def apply_favorite_tracks_stage(self, pipeline, min_score = 2.):
        if self.group_user_ids:
            favorite_track_id_pipeline = [
                {"$match": {"user_id": {"$in": self.group_user_ids}}},
                {"$group": {
                    '_id': {'track': "$track_id", 'user': "$user_id"},
                    'score': {"$sum": "$action.score"},
                    'time': {"$max": "$time.server"}
                }},
                {"$match": {"score": {"$gte": 1.}}},
                {"$group": {
                    '_id': "$_id.track",
                    'users': {"$addToSet": "$_id.user"},
                    'score': {"$sum": "$score"},
                }},
                {"$addFields": {"nusers": {"$size": "$users"}}},
                {"$project": {"score": {"$multiply": ["$score", {"$pow": ["$nusers", 5]}]}}},
                {"$sort": {"score": -1}},
                {"$limit": 1000},
            ]

            favorite_track_ids = set([ObjectId(o['_id']) for o in mm.History.objects.aggregate(*favorite_track_id_pipeline)])


            group_favorite_track_id_pipeline = [
                {"$match": {"user_id": self.group_id}},
                {"$project": {"track_id": 1, "_id": 0, "action.score": 1}},
                {"$group": {
                    '_id': "$track_id",
                    'score': {"$sum": "$action.score"}
                }
                },
                {"$match": {"score": {"$gte": min_score}}},
            ]

            for o in mm.History.objects.aggregate(*group_favorite_track_id_pipeline):
                favorite_track_ids.add(ObjectId(o['_id']))

            favorite_track_ids = list(favorite_track_ids)

        else:
            favorite_track_id_pipeline = [
                {"$match": {"user_id": self.user_id}},
                {"$project": {"track_id": 1, "_id": 0, "action.score": 1}},
                {"$group": {
                    '_id': "$track_id",
                    'score': {"$sum": "$action.score"}
                }
                },
                {"$match": {"score": {"$gte": min_score}}},
            ]

            favorite_track_ids = [ObjectId(o['_id']) for o in mm.History.objects.aggregate(*favorite_track_id_pipeline)]

        random.shuffle(favorite_track_ids)

        if favorite_track_ids:
            pipeline.append({"$match": {"_id": {"$in": favorite_track_ids}}})

        return pipeline

    def apply_discover_tracks_stage(self, pipeline):
        all_history = [ObjectId(track_id) for track_id in self.user_history['all_history']]
        if all_history:
            pipeline.append({"$match": {"_id": {"$nin": all_history}}})

        return pipeline

    def apply_session_listened_tracks_stage(self, pipeline):
        if self.session_listened_tracks:
            session_listened_tracks = [ObjectId(tid) for tid in self.session_listened_tracks]
            pipeline.append({"$match": {"_id": {"$nin": session_listened_tracks}}})

        return pipeline

    def apply_pipeline_return_stage(self, pipeline, limit=10):
        if not pipeline:
            pipeline.append({"$sample": {"size": 100}})


        pipeline.append({"$limit": limit})
        pipeline.append({"$match":{"duration_ms":{"$exists":1}}})
        ##get names here
        pipeline.append({"$project": {"_id": 1}})

        return pipeline


    def apply_auth_stage(self, pipeline):

        # if (self.has_spotify):
        #     pipeline.append({"$match": {"preview_url": {"$ne": None, "$exists": True}}})

        # if (self.has_apple_music):
        #     pipeline.append({"$match": {"applemusic_uri": {"$gt": 0, "$exists": True}}})
        if (self.has_spotify == False):
            pipeline.append({'$match': {'uri': {"$regex": '^lishash'}}})

        return pipeline



    def apply_feature_filters_stage(self, pipeline):

        for feature_filter in self.feature_filters:

            if feature_filter.feature == Features.valence:
                min_valence = feature_filter.min
                max_valence = feature_filter.max
                if not (min_valence == 1. and max_valence == 5.):
                    if (min_valence == max_valence):
                        pipeline.append({"$match": {"valence": max_valence}})
                    elif (min_valence == 1.):
                        pipeline.append({"$match": {"valence": {"$lte": max_valence}}})
                    elif (max_valence == 5.):
                        pipeline.append({"$match": {"valence": {"$gte": min_valence}}})
                    else:
                        pipeline.append({"$match": {"valence": {"$gte": min_valence, "$lte": max_valence}}})

            if feature_filter.feature == Features.loudness:
                min_loudness = feature_filter.min
                max_loudness = feature_filter.max
                if not (min_loudness == 1. and max_loudness == 8.):
                    if (min_loudness == max_loudness):
                        pipeline.append({"$match": {"loudness": max_loudness}})
                    elif (min_loudness == 1.):
                        pipeline.append({"$match": {"loudness": {"$lte": max_loudness}}})
                    elif (max_loudness == 8.):
                        pipeline.append({"$match": {"loudness": {"$gte": min_loudness}}})
                    else:
                        pipeline.append({"$match": {"loudness": {"$gte": min_loudness, "$lte": max_loudness}}})

            if feature_filter.feature == Features.deepness:
                min_deepness = feature_filter.min
                max_deepness = feature_filter.max
                if not (min_deepness == 1. and max_deepness == 4.):
                    if (min_deepness == max_deepness):
                        pipeline.append({"$match": {"deepness": max_deepness}})
                    elif (min_deepness == 1.):
                        pipeline.append({"$match": {"deepness": {"$lte": max_deepness}}})
                    elif (max_deepness == 4.):
                        pipeline.append({"$match": {"deepness": {"$gte": min_deepness}}})
                    else:
                        pipeline.append({"$match": {"deepness": {"$gte": min_deepness, "$lte": max_deepness}}})

            if feature_filter.feature == Features.speechiness:
                min_speechiness = feature_filter.min
                max_speechiness = feature_filter.max
                if not (min_speechiness == -1. and max_speechiness == 1.):
                    if (min_speechiness == max_speechiness):
                        pipeline.append({"$match": {"speechiness": max_speechiness}})
                    elif (min_speechiness == -1.):
                        pipeline.append({"$match": {"speechiness": {"$lte": max_speechiness}}})
                    elif (max_speechiness == 1.):
                        pipeline.append({"$match": {"speechiness": {"$gte": min_speechiness}}})
                    else:
                        pipeline.append({"$match": {"speechiness": {"$gte": min_speechiness, "$lte": max_speechiness}}})

            if feature_filter.feature == Features.popularity:
                min_pop = feature_filter.min
                max_pop = feature_filter.max
                minX = 0  # define the map for what these values translate to in the db for popularity field
                midX = 5
                maxX = 30
                print ("min_pop : " + str(min_pop) + " ,max_pop: " + str(max_pop))
                if not (min_pop == 1 and max_pop == 3):
                    if (min_pop == max_pop):
                        if (min_pop == 1):
                            pipeline.append({"$match": {"popularity": {"$gte": minX, "$lte": midX}}})
                        elif (min_pop == 2):
                            pipeline.append({"$match": {"popularity": {"$gte": midX, "$lte": maxX}}})
                        elif (min_pop == 3):
                            pipeline.append({"$match": {"popularity": {"$gte": maxX}}})
                    elif ((min_pop == 1) and (max_pop == 2)):
                        pipeline.append({"$match": {"popularity": {"$lte": maxX}}})
                    elif ((min_pop == 2) and (max_pop == 3)):
                        pipeline.append({"$match": {"popularity": {"$gte": maxX}}})

            if feature_filter.feature == Features.date:
                min_date = feature_filter.min
                max_date = feature_filter.max
                # print("min_date : " + str(min_date) + " ,max_date: " + str(max_date))
                if not (min_date == 1960. and max_date == 2020.):
                    if (min_date == max_date):
                        pipeline.append({"$match": {"release_year": max_date}})
                    elif (min_date == 1960.):
                        pipeline.append({"$match": {"release_year": {"$lte": max_date}}})
                    elif (max_date == 2020.):
                        pipeline.append({"$match": {"release_year": {"$gte": min_date}}})
                    else:
                        pipeline.append({"$match": {"release_year": {"$gte": min_date, "$lte": max_date}}})

        return pipeline

    def apply_tag_filters_stage(self, pipeline, searched_tag):

        include_genres = []
        exclude_genres = []
        include_albums = []
        include_languages = []
        exclude_languages = []
        include_artists = []
        exclude_artists = []
        exclude_albums = []
        include_about = []
        exclude_about = []
        include_instruments = []
        exclude_instruments = []

        for tag_filter in self.tag_filters:
            filter_state = tag_filter.filter_state
            tag_name = tag_filter.name
            tag_type = tag_filter.tag_type
            tag_name = tag_name.lower()

            if searched_tag:
                searched_tag_type = searched_tag.tag_type
                searched_tag_name = searched_tag.name
                searched_tag_name = searched_tag_name.lower()
                if tag_type == searched_tag_type \
                        and tag_name == searched_tag_name \
                        and filter_state == FilterState.exclude: continue

            if tag_type == MusicTagType.genres:
                if filter_state == FilterState.exclude: exclude_genres.append(tag_name)
                else: include_genres.append(tag_name)
            if tag_filter.tag_type == MusicTagType.artists:
                if filter_state == FilterState.exclude: exclude_artists.append(tag_name)
                else: include_artists.append(tag_name)
            if tag_filter.tag_type == MusicTagType.languages:
                if filter_state == FilterState.exclude: exclude_languages.append(tag_name)
                else: include_languages.append(tag_name)
            if tag_filter.tag_type == MusicTagType.about:
                if filter_state == FilterState.exclude: exclude_about.append(tag_name)
                else: include_about.append(tag_name)
            if tag_filter.tag_type == MusicTagType.instruments:
                if filter_state == FilterState.exclude: exclude_instruments.append(tag_name)
                else: include_instruments.append(tag_name)
            if tag_filter.tag_type == MusicTagType.album:
                if filter_state == FilterState.exclude: exclude_albums.append(tag_name)
                else: include_albums.append(tag_name)

        if include_artists:
            for artist in include_artists:
                pipeline.append({"$match": {"artists.name" : artist}})

        if include_genres:
            for genre in include_genres:
                pipeline.append({"$match": {"genres.name": genre}})

        if include_languages:
            for language in include_languages:
                pipeline.append({"$match": {"languages.name": language}})

        if include_about:
            for about in include_about:
                pipeline.append({"$match": {"about.name": about}})

        if include_instruments:
            for instrument in include_instruments:
                pipeline.append({"$match": {"instruments.name": instrument}})

        if include_albums:
            for album in include_albums:
                pipeline.append({"$match": {"album.name": album}})

        if exclude_languages:
            pipeline.append({"$match": {"languages.name": {"$nin": exclude_languages}}})

        if exclude_genres:
            pipeline.append({"$match": {"genres.name": {"$nin": exclude_genres}}})

        if exclude_artists:
            pipeline.append({"$match": {"artists.name": {"$nin": exclude_artists}}})

        if exclude_about:
            pipeline.append({"$match": {"about.name": {"$nin": exclude_about}}})

        if exclude_instruments:
            pipeline.append({"$match": {"instruments.name": {"$nin": exclude_instruments}}})

        if exclude_albums:
            pipeline.append({"$match": {"album.name": {"$nin": exclude_albums}}})

        return pipeline

    def apply_or_per_category_tag_filters_stage(self, pipeline, searched_tag):

        include_genres = []
        exclude_genres = []
        include_albums = []
        include_languages = []
        exclude_languages = []
        include_artists = []
        exclude_artists = []
        exclude_albums = []
        include_about = []
        exclude_about = []
        include_instruments = []
        exclude_instruments = []

        for tag_filter in self.tag_filters:
            filter_state = tag_filter.filter_state
            tag_name = tag_filter.name
            tag_type = tag_filter.tag_type

            if searched_tag:
                searched_tag_type = searched_tag.tag_type
                searched_tag_name = searched_tag.name
                if tag_type == searched_tag_type \
                        and tag_name == searched_tag_name \
                        and filter_state == FilterState.exclude: continue

            if tag_type == MusicTagType.genres:
                if filter_state == FilterState.exclude: exclude_genres.append(tag_name)
                else: include_genres.append(tag_name)
            if tag_filter.tag_type == MusicTagType.artists:
                if filter_state == FilterState.exclude: exclude_artists.append(tag_name)
                else: include_artists.append(tag_name)
            if tag_filter.tag_type == MusicTagType.languages:
                if filter_state == FilterState.exclude: exclude_languages.append(tag_name)
                else: include_languages.append(tag_name)
            if tag_filter.tag_type == MusicTagType.about:
                if filter_state == FilterState.exclude: exclude_about.append(tag_name)
                else: include_about.append(tag_name)
            if tag_filter.tag_type == MusicTagType.instruments:
                if filter_state == FilterState.exclude: exclude_instruments.append(tag_name)
                else: include_instruments.append(tag_name)
            if tag_filter.tag_type == MusicTagType.album:
                if filter_state == FilterState.exclude: exclude_albums.append(tag_name)
                else: include_albums.append(tag_name)

        if include_artists:
            pipeline.append({"$match": {"artists.name": {"$in": include_artists}}})

        if include_genres:
            pipeline.append({"$match": {"genres.name": {"$in": include_genres}}})

        if include_languages:
            pipeline.append({"$match": {"languages.name": {"$in": include_languages}}})

        if include_about:
            pipeline.append({"$match": {"about.name": {"$in": include_about}}})

        if include_instruments:
            pipeline.append({"$match": {"instruments.name": {"$in": include_instruments}}})

        if include_albums:
            pipeline.append({"$match": {"album.name": {"$in": include_albums}}})

        if exclude_languages:
            pipeline.append({"$match": {"languages.name": {"$nin": exclude_languages}}})

        if exclude_genres:
            pipeline.append({"$match": {"genres.name": {"$nin": exclude_genres}}})

        if exclude_artists:
            pipeline.append({"$match": {"artists.name": {"$nin": exclude_artists}}})

        if exclude_about:
            pipeline.append({"$match": {"about.name": {"$nin": exclude_about}}})

        if exclude_instruments:
            pipeline.append({"$match": {"instruments.name": {"$nin": exclude_instruments}}})

        if exclude_albums:
            pipeline.append({"$match": {"album.name": {"$nin": exclude_albums}}})

        return pipeline

    def apply_or_tag_filters_stage(self, pipeline, searched_tag):

        include_genres = []
        exclude_genres = []
        include_albums = []
        include_languages = []
        exclude_languages = []
        include_artists = []
        exclude_artists = []
        exclude_albums = []
        include_about = []
        exclude_about = []
        include_instruments = []
        exclude_instruments = []

        for tag_filter in self.tag_filters:
            filter_state = tag_filter.filter_state
            tag_name = tag_filter.name
            tag_type = tag_filter.tag_type

            if searched_tag:
                searched_tag_type = searched_tag.tag_type
                searched_tag_name = searched_tag.name
                if tag_type == searched_tag_type \
                        and tag_name == searched_tag_name \
                        and filter_state == FilterState.exclude: continue

            if tag_type == MusicTagType.genres:
                if filter_state == FilterState.exclude: exclude_genres.append(tag_name)
                else: include_genres.append(tag_name)
            if tag_filter.tag_type == MusicTagType.artists:
                if filter_state == FilterState.exclude: exclude_artists.append(tag_name)
                else: include_artists.append(tag_name)
            if tag_filter.tag_type == MusicTagType.languages:
                if filter_state == FilterState.exclude: exclude_languages.append(tag_name)
                else: include_languages.append(tag_name)
            if tag_filter.tag_type == MusicTagType.about:
                if filter_state == FilterState.exclude: exclude_about.append(tag_name)
                else: include_about.append(tag_name)
            if tag_filter.tag_type == MusicTagType.instruments:
                if filter_state == FilterState.exclude: exclude_instruments.append(tag_name)
                else: include_instruments.append(tag_name)
            if tag_filter.tag_type == MusicTagType.album:
                if filter_state == FilterState.exclude: exclude_albums.append(tag_name)
                else: include_albums.append(tag_name)

        or_conditions = []
        if include_artists:
            or_conditions.append({"artists.name": {"$in": include_artists}})

        if include_genres:
            or_conditions.append({"genres.name": {"$in": include_genres}})

        if include_languages:
            or_conditions.append({"languages.name": {"$in": include_languages}})

        if include_about:
            or_conditions.append({"about.name": {"$in": include_about}})

        if include_instruments:
            or_conditions.append({"instruments.name": {"$in": include_instruments}})

        if include_albums:
            or_conditions.append({"album.name": {"$in": include_albums}})

        if or_conditions:
            pipeline.append({"$match": {"$or": or_conditions}})

        if exclude_languages:
            pipeline.append({"$match": {"languages.name": {"$nin": exclude_languages}}})

        if exclude_genres:
            pipeline.append({"$match": {"genres.name": {"$nin": exclude_genres}}})

        if exclude_artists:
            pipeline.append({"$match": {"artists.name": {"$nin": exclude_artists}}})

        if exclude_about:
            pipeline.append({"$match": {"about.name": {"$nin": exclude_about}}})

        if exclude_instruments:
            pipeline.append({"$match": {"instruments.name": {"$nin": exclude_instruments}}})

        if exclude_albums:
            pipeline.append({"$match": {"album.name": {"$nin": exclude_albums}}})

        return pipeline

    def apply_positive_interests_stage(self, pipeline, k=5,
                                       genre_enabled = True, artist_enabled = True,
                                       feature_enabled = True, album_enabled = True,
                                       language_enabled = True, instrument_enabled = True,
                                       about_enabled = True
                                       ):

        # Adds at least one top genre to positive interests to make the algorithm stable
        top_genre = None
        max_genre_score = 0.


        keys = []
        scores = []

        for key, score in self.positive_interests.most_common(k):
            #Definitely get one top genre, this keeps it stable if the tags are wrong for language etc.
            type = key.split(',')[0]
            if type == 'genres' and score > max_genre_score: top_genre = key
            keys.append(key)
            scores.append(score)

        top_sampled_interests = set(random.choices(keys, weights=scores, k=k))
        if top_genre: top_sampled_interests.add(top_genre)

        print(f"POSITIVE : {top_sampled_interests}")


        positive_genres = []
        positive_albums = []
        positive_languages = []
        positive_artists = []
        positive_instruments = []
        positive_about = []
        positive_valence = []
        positive_loudness = []
        positive_deepness = []
        positive_speechiness = []

        for interest in top_sampled_interests:
            interest_list = interest.split(',')
            type = interest_list[0]
            value = ''.join(interest_list[1:])
            value = value.lower()


            if type == 'genres':
                positive_genres.append(value)

            elif type == 'language':
                positive_languages.append(value)

            elif type == 'artist':

                positive_artists.append(value)

            elif type == 'instrument':
                positive_instruments.append(value)

            elif type == 'about':
                positive_about.append(value)

            elif type == 'album':
                positive_albums.append(value)

            elif type == 'valence':
                positive_valence.append(float(value))

            elif type == 'loudness':
                positive_loudness.append(float(value))

            elif type == 'deepness':
                positive_deepness.append(float(value))

            elif type == 'speechiness':
                positive_speechiness.append(float(value))

        if positive_genres and genre_enabled:
            pipeline.append({"$match": {"genres.name": {"$in": positive_genres}}})

        if positive_artists and artist_enabled:
            pipeline.append({"$match": {"artists.name": {"$in": positive_artists}}})

        if positive_albums and album_enabled:
            pipeline.append({"$match": {"album.name": {"$in": positive_albums}}})

        if positive_languages and language_enabled:
            pipeline.append({"$match": {"languages.name": {"$in": positive_languages}}})

        if positive_about and about_enabled:
            pipeline.append({"$match": {"about.name": {"$in": positive_about}}})

        if positive_instruments and instrument_enabled:
            pipeline.append({"$match": {"instruments.name": {"$in": positive_instruments}}})

        if feature_enabled:
            if positive_valence:
                pipeline.append({"$match": {"valence": {"$in": positive_valence}}})

            if positive_loudness:
                pipeline.append({"$match": {"loudness": {"$in": positive_loudness}}})

            if positive_deepness:
                pipeline.append({"$match": {"deepness": {"$in": positive_deepness}}})

            if positive_speechiness:
                pipeline.append({"$match": {"speechiness": {"$in": positive_speechiness}}})

        return pipeline

    def apply_negative_interests_stage(self,pipeline, k=3):
        keys = []
        scores = []

        # if used directly with negative interests nothing might be found in top3
        final_ni = Counter()

        # valence etc. are not used so do not consider them while sampling
        for i,s in self.negative_interests.items():
            if i.split(',')[0] not in ['valence', 'deepness', 'speechiness', 'loudness']:
                final_ni[i] = s


        for key, score in final_ni.most_common(k):
            keys.append(key)
            scores.append(score)

        if keys:
            top_sampled_interests = set(random.choices(keys, weights=scores, k=k))
            print(f"NEGATIVE : {top_sampled_interests}")
            negative_genres = []
            negative_languages = []
            negative_artists = []
            negative_instruments = []
            negative_about = []
            negative_albums = []

            for interest in top_sampled_interests:
                score = self.negative_interests[interest]
                try:
                    interest_list = interest.split(',')
                    type = interest_list[0]
                    value = ''.join(interest_list[1:])

                    if type == 'genres':
                        negative_genres.append(value)

                    elif type == 'language':
                        negative_genres.append(value)

                    elif type == 'artist':
                        negative_genres.append(value)

                    elif type == 'instrument':
                        negative_genres.append(value)

                    elif type == 'about':
                        negative_genres.append(value)

                    elif type == 'album':
                        negative_genres.append(value)

                except:
                    print("splitting fucked up in negative interests somehow")

            if negative_genres:
                pipeline.append({"$match": {"genres.name": {"$nin": negative_genres}}})

            if negative_albums:
                pipeline.append({"$match": {"album.name": {"$nin": negative_albums}}})

            if negative_artists:
                pipeline.append({"$match": {"artists.name": {"$nin": negative_artists}}})

            if negative_languages:
                pipeline.append({"$match": {"languages.name": {"$nin": negative_languages}}})

            if negative_instruments:
                pipeline.append({"$match": {"instruments.name": {"$nin": negative_instruments}}})

            if negative_about:
                pipeline.append({"$match": {"about.name": {"$nin": negative_about}}})

        return pipeline

    def apply_search_seed_stage(self, pipeline):
        search_seed_list = self.search_seed.name.split(',')
        type = search_seed_list[0]
        value = ''.join(search_seed_list[1:])
        value = value.lower()

        for tag_filter in self.tag_filters:
            is_excluded = tag_filter.filter_state == FilterState.exclude
            tag_name = tag_filter.name
            tag_type = tag_filter.tag_type
            if tag_type == type and tag_name == value and is_excluded : return pipeline

        if type == 'genres':
            pipeline.append({"$match": {"genres.name": value}})

        elif type == 'language':
            pipeline.append({"$match" : {"languages.name": value}})

        elif type == 'artist':
            pipeline.append({"$match": {"artists.name": value}})

        elif type == 'instrument':
            pipeline.append({"$match": {"instruments.name": value}})

        elif type == 'about':
            pipeline.append({"$match": {"about.name": value}})

        elif type == 'album':
            pipeline.append({"$match": {"album.name": value}})

        return pipeline

    def apply_searched_tag_stage(self, searched_tag:MusicTag, pipeline):
        tag_name = searched_tag.name
        tag_name = tag_name.lower()
        tag_type = searched_tag.tag_type

        if tag_type == MusicTagType.genres:
            pipeline.append({"$match": {"genres.name": tag_name}})

        elif tag_type == MusicTagType.languages:
            pipeline.append({"$match": {"languages.name": tag_name}})

        elif tag_type == MusicTagType.artists:
            pipeline.append({"$match": {"artists.name": tag_name}})

        elif tag_type == MusicTagType.about:
            pipeline.append({"$match": {"about.name": tag_name}})

        elif tag_type == MusicTagType.instruments:
            pipeline.append({"$match": {"instruments.name": tag_name}})

        elif tag_type == MusicTagType.album:
            pipeline.append({"$match": {"album.name": tag_name}})

        return pipeline

    def apply_shuffle_stage(self, pipeline, k=1000):
        seq_ids = np.random.randint(low=0, high=10000000, size=k).tolist()
        pipeline.append({"$match": {"seq_id": {"$in": seq_ids}}})
        return pipeline

    def apply_tertiary_stage(self, pipeline):
        pipeline.append({"$match": {"genres.name": 'indian indie'}})
        return pipeline

    def get_relevant_shared_ids(self, shared_ids_list, primary_mode):
        if not shared_ids_list: return []

        pipeline = []

        shared_tracks = [ObjectId(k) for k in shared_ids_list]
        pipeline.append({"$match": {"_id": {"$in": shared_tracks}}})

        if self.session_listened_tracks:
            pipeline = self.apply_session_listened_tracks_stage(pipeline=pipeline)

        if self.tag_filters:
            pipeline = self.apply_tag_filters_stage(pipeline=pipeline, searched_tag=self.searched_tag)

        if self.feature_filters:
            pipeline = self.apply_feature_filters_stage(pipeline=pipeline)

        if primary_mode == PrimaryMode.Favorites:
            pipeline = self.apply_favorite_tracks_stage(pipeline=pipeline)

        if self.tertiary_mode == TertiaryMode.Indie:
            pipeline = self.apply_tertiary_stage(pipeline)

        if primary_mode == PrimaryMode.Discover:
            pipeline = self.apply_discover_tracks_stage(pipeline=pipeline)

        if self.searched_tag:
            pipeline = self.apply_searched_tag_stage(searched_tag=self.searched_tag, pipeline=pipeline)

        keys = []
        scores = []

        for key, score in self.positive_interests.most_common(20):
            keys.append(key)
            scores.append(score)

        top_sampled_interests = set(random.choices(keys, weights=scores, k=20))

        positive_feats = []

        for interest in top_sampled_interests:
            interest_list = interest.split(',')
            type = interest_list[0]
            value = ''.join(interest_list[1:])

            if type == 'genres':
                positive_feats.append({'genres.name': value})

            elif type == 'artist':
                positive_feats.append({'artists.name': value})

            elif type == 'about':
                positive_feats.append({'about.name': value})

            elif type == 'album':
                positive_feats.append({'album.name': value})

            # elif type == 'language':
            #     positive_feats.append({'languages.name': value})
            # elif type == 'speechiness':
            #     positive_feats.append({'speechiness': float(value)})
            #
            # elif type == 'loudness':
            #     positive_feats.append({'loudness': float(value)})

        pipeline.append({"$match": {"$or": positive_feats}})

        pipeline = self.apply_pipeline_return_stage(pipeline=pipeline)
        res = [str(o['_id']) for o in mm.Track.objects.aggregate(*pipeline)]

        return res






