import mongo_manager as mm
from mongoengine import *
from .next_tracks_pipeline import *
from .neo_next_track_pipeline import *
from .user_history_pipeline import *
from datetime import datetime
import db_manager as dm
import neo_db as nd
import random
from bson import ObjectId
from recommender.tag.db_enums import Features, MusicTagType
from recommender.tag.music_tag import MusicTag
from recommender.api import add_track
import re
import requests
from tekore.auth import Credentials


# TODO: Add favorites of a group to the normal favorites
# TODO: Give less weight to artist and album on a right swipe / love

class AlphaPlayback:
    # these values are used to update the seed track and to calculate short and long term interests
    max_seed_score = 2.
    search_seed_score = 2.
    love_seed_mul = 2.
    right_seed_score = 0.5
    complete_seed_score = 1.0
    left_seed_pen = 0.75
    right_time_threshold = 30000  # If a user listens less than this milis, we just consider it to be a skip

    def __init__(self, user_id, group_user_ids, group_id, context: MeTag, time,
                 tag_filters, feature_filters, primary_mode: PrimaryMode = None,
                 secondary_mode: SecondaryMode = None, tertiary_mode: TertiaryMode = None,
                 track_id=None, session_id=None):
        self.user_id = user_id
        self.track_id = track_id
        self.session_id = session_id
        self.context = context
        self.time = time
        self.tag_filters = tag_filters
        self.feature_filters = feature_filters
        self.primary_mode = primary_mode
        self.secondary_mode = secondary_mode
        self.tertiary_mode = tertiary_mode
        self.group_id = group_id
        self.group_user_ids = group_user_ids

    def start(self, reset: bool = False):
        # get all the history from the history_collection
        if self.session_id:
            # load the session document
            session_doc = mm.MusicSession.objects().get(id=ObjectId(self.session_id))

            if reset: session_doc.search_seed = None
            positive_interests = self.interest_list_to_dict_adapter(session_doc.positive_interests)
            negative_interests = self.interest_list_to_dict_adapter(session_doc.negative_interests)

        else:
            # Think of a group as a user as well, and everything will be simplified conceptually. Don't take the names
            # too seriously here.
            user_history_pipeline = UserHistoryPipeline(user_id=self.user_id,
                                                        group_id=self.group_id,
                                                        group_user_ids=self.group_user_ids,
                                                        user_minute_hour=self.time['user']['minute_hour'], time=self.time)
            # 1. is the max_love count, .5 is the right seed score
            user_history = user_history_pipeline.get_user_history()

            # load all the tags from the track collection
            # if self.group_id:
            #     positive_interests = user_history_pipeline.get_group_positive_interests()
            # else:
            positive_interests = user_history_pipeline.get_positive_interests(user_history=user_history)
            negative_interests = {}

            session_doc = mm.MusicSession(user_history=user_history)

        sampled_track_id = None
        shared_by = None
        instructions = {}
        # check if any of the filters is a track
        for i, music_tag in enumerate(self.tag_filters):

            # TO DO: add condition of apple_music to write to database
            if music_tag.tag_type == MusicTagType.track:
                sampled_track_id = music_tag.id

                # check if id is not in db, add it to mongodb,neodb and get the id and assign to the sample id
                if '+' in music_tag.id:

                    sampled_track_id = add_track.check_and_add_new_track_to_db(trackId=music_tag.id)

                    #sampled_track_id, name_lower, popularity, genre, cover_image_url = self.get_id_from_am_api(music_tag.id)
                    #self.load_track_to_neo4j(str(sampled_track_id), name_lower, popularity, genre, cover_image_url)
                    music_tag.id = sampled_track_id
                    self.tag_filters[i] = music_tag
                # write func for get_sample_id using isrc
                break

        # change seed_track if a track is searched
        # give max reward possible
        if sampled_track_id:
            session_doc.seed_track = self._get_base_track_from_id(tid=sampled_track_id)
            session_doc.seed_track.score = self.search_seed_score

        # we only care for the positive_interests here because the at start negative interest does not matter
        positive_interests, negative_interests = self.update_interests(action='start',
                                                                       positive_interests_dict=positive_interests,
                                                                       negative_interests_dict=negative_interests,
                                                                       search_seed=session_doc.search_seed,
                                                                       seed_track=session_doc.seed_track,
                                                                       thorn_track=session_doc.thorn_track)

        print('START')
        start_primary_mode = self.get_primary_mode(action='start')
        # if no track was started with, sample a track using the positive interests
        if not sampled_track_id: sampled_track_id, instructions, shared_by = self._sample_track_id(
            session_doc=session_doc,
            action='start',
            primary_mode=start_primary_mode,
            positive_interests=positive_interests,
            negative_interests=negative_interests)

        session_doc.positive_interests = self.interest_dict_to_list_adapter(positive_interests)
        session_doc.negative_interests = self.interest_dict_to_list_adapter(negative_interests)
        session_doc = session_doc.save()

        # on start we are returning the session id as well, so the ui can save it for further requests
        return sampled_track_id, str(session_doc.id), instructions, shared_by

    def get_spotify_info_using_isrc(self, term):
        SPOTIPY_CLIENT_SECRET = "8a375348c3d341cd918ad0ccc02ef658"
        SPOTIPY_CLIENT_ID = "985e8f22ccfd487cb4fb07eb22b32fe6"
        URL = "https://api.spotify.com/v1/search?type=track&q=isrc:" + term
        cred = Credentials(SPOTIPY_CLIENT_ID, SPOTIPY_CLIENT_SECRET, 'lishash.com/about')
        token = cred.request_client_token()
        hed = {'Authorization': 'Bearer ' + str(token)}
        PARAMS = {'id': term}
        res = json.loads(requests.get(url=URL, headers=hed).text)
        uri = ""
        popularity = 0
        for each in res['tracks']['items']:
            uri = each['uri']
            popularity = each['popularity']
        return uri, popularity


    def get_id_from_am_api(self, isrc):
        cover_size = str(600)
        seq_id_pipeline = [
            {"$sort": {"seq_id": -1}},
            {"$limit": 1},
            {"$project": {"seq_id": 1, "_id": 0}}]
        seq_id = list(mm.Track.objects().aggregate(*seq_id_pipeline))[0]['seq_id']
        # seq_id = mm.Track.objects().order_by('-seq_id')[:1]
        seq_id += 1
        playing_track_apple_id = isrc[:12]
        res = self.search_isrc_with_applemusic(playing_track_apple_id)
        d = {}
        for item in res['data']:
            pprint(f"item returned from apple music : {item}")
            artists_AM = item['attributes']['artistName']
            artists_AM = re.split(", | & ", artists_AM)

            Languages_list = ['Hindi', 'English', 'Telugu', 'Punjabi', 'Tamil', 'Gujarati', 'Kannada', 'Urdu',
                              'Malayalam', 'Bengali', 'Odia', 'Marathi', 'Spanish', 'French']
            genres_AM = item['attributes']['genreNames']

            if 'Indian' in genres_AM:
                genres_AM.remove('Indian')
            if 'Music' in genres_AM:
                genres_AM.remove('Music')
            genn = []
            for g in genres_AM:
                genn.append(re.split("/", g))
            genres_AM = sum(genn, [])

            languages = []

            for i in Languages_list:
                if i in genres_AM:
                    languages.append(i)
                    genres_AM.remove(i)

            gen_list = []
            art_list = []
            lan_list =[]
            i = 0
            for each_art in item['relationships']['artists']['data']:
                art_list.append({'name': artists_AM[i].lower(), 'id': each_art['id']})
                i += 1
            for each_gen in genres_AM:
                gen_list.append({'name': each_gen.lower(),'score':-1})

            for each_lan in languages:
                lan_list.append({'name': each_lan.lower(),'score':-1})

            name = item['attributes']['name']
            name_lower = item['attributes']['name'].lower()
            album = item['attributes']['albumName'].lower()
            album = {'id': -1, 'name': album}


            duration_ms = item['attributes']['durationInMillis']
            cover_image_url = item['attributes']['artwork']['url'].replace("{w}", cover_size).replace("{h}", cover_size)
            release_date = item['attributes']['releaseDate']
            release_year = release_date.split("-")[0]
            applemusic_uri = item['id']
            isrc = item['attributes']['isrc']

            for each_url in item['attributes']['previews']:
                preview_url = each_url['url']

        valence = 3
        loudness = 5
        deepness = 2
        speechiness = 0
        embed = [0.0, 0.0, 0.0, 0.0, 0.0]
        spotify_uri, popularity = self.get_spotify_info_using_isrc(isrc)

        new_track_ses_doc = mm.Track(uri=spotify_uri,
                                     name=name,
                                     name_lower=name_lower,
                                     popularity=popularity,
                                     valence=valence,
                                     loudness=loudness,
                                     deepness=deepness,
                                     embed=embed,
                                     artists=art_list,
                                     genres=gen_list,
                                     languages=lan_list,
                                     about=[],
                                     instruments=[],
                                     speechiness=speechiness,
                                     seq_id=seq_id,
                                     album=album,
                                     duration_ms=duration_ms,
                                     release_date=release_date,
                                     preview_url=preview_url,
                                     cover_image_url=cover_image_url,
                                     art_gens=[],
                                     release_year=release_year,
                                     applemusic_uri=applemusic_uri,
                                     isrc=isrc)
        new_track_ses_doc.save()

        return str(new_track_ses_doc.id), name_lower, popularity, gen_list, cover_image_url

    def make_ngrams(self, word, min_size=3, edge_ngram=False):
        # concatenate the edge ngrams again into a string
        # first split all words
        word = re.sub(r" ?\([^)]+\)", " ", word)
        word = re.sub(r'\[[^()]*\]', ' ', word)
        word = word.replace('"', ' ')
        word = word.replace(",", ' ')
        word = word.replace(":", " ")
        word = word.replace("!", " ")
        word = word.replace("-", " ")
        word = word.replace("'", '')

        word_list = word.split(" ")

        ngrams_set = set()
        # for each word calculate the edge ngrams
        for word in word_list:
            length = len(word)
            size_range = range(min_size, max(length, min_size) + 1)
            for size in size_range:
                for i in range(0, max(0, length - size) + 1):
                    if edge_ngram:
                        ngrams_set.add(word[0:i + size])
                    else:
                        ngrams_set.add(word[i:i + size])
                        # if edge

        return ngrams_set

    def load_track_to_neo4j(self,_id,name_lower,popularity,genre):
        # import pdb; pdb.set_trace()
        title_ngrams_set = self.make_ngrams(name_lower)
        title_ngrams = ' '.join(list(title_ngrams_set))

        name_lower = re.sub(r" ?\([^)]+\)", " ", name_lower)
        name_lower = re.sub(r'\[[^()]*\]', ' ', name_lower)
        name_lower = name_lower.replace('"', ' ')

        code = """
        MERGE (t:Track {id:$_id})   
        ON CREATE SET t.name = $name_lower, t.cover_image_url = $cover_image_url  
                
        WITH t
        UNWIND $genre AS genre_dict
        MERGE (genre:TrackFeature:Genre {name: genre_dict.name})        
        MERGE (t)-[r:HAS_GENRE]->(genre)        
        ON CREATE SET r.score = genre_dict.score

        MERGE (s:TitleNgrams{mongoId:$_id})
        SET s.popularity = $popularity,
            s.title_ngrams=$title_ngrams
         """
        res = nd.api_alpha.write(code, _id=_id, name_lower=name_lower, popularity=popularity, genre=genre,
                                 title_ngrams=title_ngrams).data()

    def search_isrc_with_applemusic(self, term):

        URL = "https://api.music.apple.com/v1/catalog/in/songs?filter[isrc]=" + term
        #     print(URL)
        token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IjNIRzk3UUs4WDQifQ.eyJpc3MiOiIyOTU1U0o3UDJSIiwiaWF0IjoxNTg2NDIxNTU4LCJleHAiOjE2MDIxODk1NTh9.QciZoAPw_pf5vB2Kk6GvkLgtYJVuM2xYCbqmHYnGP6sE79qO3zB183v_c8uJb4lWCiIqFYfJOYuh2FULPWi_Yg'
        hed = {'Authorization': 'Bearer ' + token}
        PARAMS = {'filter[isrc]': term}
        res = json.loads(requests.get(url=URL, params=PARAMS, headers=hed).text)
        return res

    def step(self, action: Action, simulating=False, seq_ids=None, seq_dists=None):
        print(f"SIMULATING {action.name}")
        scored_action_dict = self._get_scored_action_dict(action=action)
        score = scored_action_dict['score']

        # load the session document
        session_doc = mm.MusicSession.objects().get(id=ObjectId(self.session_id))

        positive_interests = self.interest_list_to_dict_adapter(session_doc.positive_interests)
        negative_interests = self.interest_list_to_dict_adapter(session_doc.negative_interests)

        # change session_listened_tracks
        session_doc.session_listened_tracks.append(self.track_id)

        # reduce seed_track value each step
        if session_doc.seed_track:
            session_doc.seed_track.score *= 0.75
            if session_doc.seed_track.score <= score:
                session_doc.seed_track = self._get_base_track_from_id(self.track_id)
                session_doc.seed_track.score = score
                session_doc.thorn_track = None

        # if there is no seed track
        elif score > 0.0:
            session_doc.seed_track = self._get_base_track_from_id(self.track_id)
            session_doc.seed_track.score = score

        # if a left swipe happened, set a thorn track
        if score < 0.0: session_doc.thorn_track = self._get_base_track_from_id(self.track_id)

        step_action = scored_action_dict['action']
        if scored_action_dict['love_count'] > 0.75 and step_action != 'left': step_action = 'love'

        session_doc = self.update_search_seed(session_doc=session_doc,
                                              action=step_action,
                                              play_ratio=scored_action_dict['play_ratio'])

        positive_interests, negative_interests = self.update_interests(action=step_action,
                                                                       positive_interests_dict=positive_interests,
                                                                       negative_interests_dict=negative_interests,
                                                                       seed_track=session_doc.seed_track,
                                                                       thorn_track=session_doc.thorn_track,
                                                                       search_seed=session_doc.search_seed,
                                                                       love_count=scored_action_dict['love_count'],
                                                                       play_ratio=scored_action_dict['play_ratio'],
                                                                       track_id=self.track_id)

        session_doc.positive_interests = self.interest_dict_to_list_adapter(positive_interests)
        session_doc.negative_interests = self.interest_dict_to_list_adapter(negative_interests)

        if not simulating:
            # save music_session_doc on mongodb
            session_doc.save()

            db_context = self.context.to_json() if self.context else {'name': "UNSPECIFIED", 'type': "UNSPECIFIED"}
            # save event to history on mongodb
            save_user_id = self.group_id if self.group_id else self.user_id
            mm.History(user_id=save_user_id, session_id=self.session_id, track_id=self.track_id,
                       context=db_context, action=scored_action_dict, time=self.time).save()

            if save_user_id == self.user_id:
                # register listen on neodb to be used for expertise and social stuff
                user = nd.User.from_mongo_id(mongo_id=self.user_id)
                user.register_listen(track_id=self.track_id, score=scored_action_dict['score'],
                                     time=self.time['server'])

                # give a currency reward for each action
                fire_user = dm.FirebaseUser.from_id(id=self.user_id)

                # Added by aditya
                print("shared_tracks notifier check")
                self.shared_tracks_notify_friends(user_id=self.user_id, track_id=self.track_id)
                print("shared_tracks notifier check done")
                fire_user.modify_currency(v=1)
                # fire_user.add_shared_track_to_friends_fireuser_snapshot(id,)

        step_primary_mode = self.get_primary_mode(action=step_action)

        sampled_track_id, instructions, shared_by = self._sample_track_id(session_doc=session_doc,
                                                                          action=step_action,
                                                                          simulating=simulating,
                                                                          primary_mode=step_primary_mode,
                                                                          positive_interests=positive_interests,
                                                                          negative_interests=negative_interests,
                                                                          seq_ids=seq_ids, seq_dists=seq_dists)

        return sampled_track_id, instructions, shared_by

    def search(self, searched_tag: MusicTag, love_count: float, play_ratio: float):
        session_doc = mm.MusicSession.objects().get(id=ObjectId(self.session_id))
        positive_interests = self.interest_list_to_dict_adapter(session_doc.positive_interests)
        negative_interests = self.interest_list_to_dict_adapter(session_doc.negative_interests)

        # check if a track was searched
        if searched_tag.tag_type == MusicTagType.track:
            # update seed track, no matter what you loved, this is going to be your new seed_track now
            sampled_track_id = searched_tag.id
            if '+' in searched_tag.id:
                sampled_track_id = add_track.check_and_add_new_track_to_db(trackId=searched_tag.id)
                # sampled_track_id, name, popularity, genre, cover_image_url = self.get_id_from_am_api(searched_tag.id)
                # self.load_track_to_neo4j(sampled_track_id, name, popularity, genre, cover_image_url)
                searched_tag.id = sampled_track_id
            instructions = {}
            shared_by = None
            session_doc.seed_track = self._get_base_track_from_id(self.track_id)
            session_doc.seed_track.score = self.search_seed_score
            session_doc.thorn_track = None
            session_doc.search_seed = None

            positive_interests, negative_interests = self.update_interests(action='search',
                                                                           positive_interests_dict=positive_interests,
                                                                           negative_interests_dict=negative_interests,
                                                                           seed_track=session_doc.seed_track,
                                                                           thorn_track=session_doc.thorn_track,
                                                                           searched_tag=searched_tag,
                                                                           search_seed=session_doc.search_seed,
                                                                           track_id=sampled_track_id,
                                                                           love_count=love_count,
                                                                           play_ratio=play_ratio)

            # save the track in history that was just searched for, with the search action
            search_action_dict = {'action': 'search', 'love_count': 0.0, 'play_ratio': 0.0,
                                  'score': self.search_seed_score}
            db_context = self.context.to_json() if self.context else {'name': "UNSPECIFIED", 'type': "UNSPECIFIED"}
            save_user_id = self.group_id if self.group_id else self.user_id
            mm.History(user_id=save_user_id, track_id=sampled_track_id, session_id=self.session_id,
                       time=self.time, context=db_context, action=search_action_dict).save()


        else:
            session_doc = self.update_search_seed(session_doc=session_doc, action='search', searched_tag=searched_tag)

            positive_interests, negative_interests = self.update_interests(action='search',
                                                                           positive_interests_dict=positive_interests,
                                                                           negative_interests_dict=negative_interests,
                                                                           seed_track=session_doc.seed_track,
                                                                           thorn_track=session_doc.thorn_track,
                                                                           search_seed=session_doc.search_seed,
                                                                           searched_tag=searched_tag)

            # sample a track_id with this tag as the base
            sampled_track_id, instructions, shared_by = self._sample_track_id(session_doc=session_doc,
                                                                              action='search',
                                                                              searched_tag=searched_tag,
                                                                              primary_mode=self.primary_mode,
                                                                              positive_interests=positive_interests,
                                                                              negative_interests=negative_interests)

        session_doc.positive_interests = self.interest_dict_to_list_adapter(positive_interests)
        session_doc.negative_interests = self.interest_dict_to_list_adapter(negative_interests)

        # save session_doc
        session_doc.save()

        # save the currently playing track in history with action UNSPECIFIED
        action_dict = self._get_no_feedback_action_dict(play_ratio=play_ratio, love_count=love_count)
        db_context = self.context.to_json() if self.context else {'name': "UNSPECIFIED", 'type': "UNSPECIFIED"}
        save_user_id = self.group_id if self.group_id else self.user_id
        mm.History(user_id=save_user_id, track_id=self.track_id, session_id=self.session_id,
                   time=self.time, context=db_context, action=action_dict).save()

        return sampled_track_id, instructions, shared_by

    def _sample_track_id(self, session_doc, action, primary_mode: PrimaryMode, positive_interests: dict,
                         negative_interests: dict,
                         seq_ids=None, simulating=False, seq_dists=None, searched_tag=None):

        code = """
        MATCH (friend:User)-[r:SHARES_TRACK]->(u:User {id: $id})
        WHERE NOT (u)-[:LISTENED_TO]->(:Track {id: r.track_mongo_id})
        RETURN r.track_mongo_id as tid, friend.username as friend_name, friend.id as friend_id
        """
        res = nd.api_alpha.read(code, id=self.user_id).data()
        shared_id_list = []
        #shared_id_dict_list = []
        shared_id_dict = {}

        for o in res:
            shared_id_list.append(o['tid'])
            #shared_id_dict_list.append({'track_id':o['tid'] , 'friend': o['friend_name'],'friendId': o['friend_id']})
            shared_id_dict[o['tid']] = {'friend': o['friend_name'],'friendId': o['friend_id']}
        # for o in res: shared_id_dict[o['tid']] = o['friends']

        print(f"Actual Mode: {self.primary_mode}    | Sampled Mode: {primary_mode}")

        pipeline = NextTrackPipeline(user_history=session_doc.user_history,
                                     user_id=self.user_id,
                                     group_user_ids=self.group_user_ids,
                                     group_id=self.group_id,
                                     action=action,
                                     simulating=simulating,
                                     searched_tag=searched_tag,
                                     positive_interests=positive_interests,
                                     negative_interests=negative_interests,
                                     session_listened_tracks=session_doc.session_listened_tracks,
                                     primary_mode=primary_mode,
                                     secondary_mode=self.secondary_mode,
                                     tertiary_mode=self.tertiary_mode,
                                     tag_filters=self.tag_filters,
                                     search_seed=session_doc.search_seed,
                                     feature_filters=self.feature_filters)

        relevant_shared_ids = pipeline.get_relevant_shared_ids(shared_ids_list=shared_id_list,
                                                               primary_mode=self.primary_mode)

        if relevant_shared_ids and not self.group_id:
            instructions = {}
            track_id = random.choice(relevant_shared_ids)
            shared_by = [shared_id_dict[track_id]]
            return track_id, instructions, shared_by

        else:
            track_ids, instructions = pipeline.run(seq_ids=seq_ids)

            # this can happen in magic magic mode, suppose favorite gets sampled for a user who just started out
            if not track_ids:
                if primary_mode != self.primary_mode:
                    print(f"Found nothing for {primary_mode}. Switching to {self.primary_mode}")
                    pipeline = NextTrackPipeline(user_history=session_doc.user_history,
                                                 user_id=self.user_id,
                                                 group_id=self.group_id,
                                                 group_user_ids=self.group_user_ids,
                                                 action=action,
                                                 simulating=simulating,
                                                 searched_tag=searched_tag,
                                                 positive_interests=positive_interests,
                                                 negative_interests=negative_interests,
                                                 session_listened_tracks=session_doc.session_listened_tracks,
                                                 primary_mode=self.primary_mode,
                                                 secondary_mode=self.secondary_mode,
                                                 tertiary_mode=self.tertiary_mode,
                                                 tag_filters=self.tag_filters,
                                                 search_seed=session_doc.search_seed,
                                                 feature_filters=self.feature_filters)
                    track_ids, instructions = pipeline.run(seq_ids=seq_ids)

                if not track_ids and session_doc.search_seed:
                    print("Search Seed collided with active filters, not considering search_seed")
                    pipeline = NextTrackPipeline(user_history=session_doc.user_history,
                                                 user_id=self.user_id,
                                                 group_id=self.group_id,
                                                 group_user_ids=self.group_user_ids,
                                                 action=action,
                                                 simulating=simulating,
                                                 searched_tag=searched_tag,
                                                 positive_interests=positive_interests,
                                                 negative_interests=negative_interests,
                                                 session_listened_tracks=session_doc.session_listened_tracks,
                                                 primary_mode=self.primary_mode,
                                                 secondary_mode=self.secondary_mode,
                                                 tertiary_mode=self.tertiary_mode,
                                                 tag_filters=self.tag_filters,
                                                 search_seed=None,
                                                 feature_filters=self.feature_filters)

                    track_ids, instructions = pipeline.run(seq_ids=seq_ids)

                if not track_ids and self.tag_filters:
                    print("Found nothing for and_and tags, switching to and_or tags")
                    track_ids, instructions = pipeline.run(seq_ids=seq_ids, tag_filter_mode='and_or')

                    if not track_ids:
                        print("Found nothing for and_or tags, switching to or_or tags")
                        track_ids, instructions = pipeline.run(seq_ids=seq_ids, tag_filter_mode='or_or')

                if not track_ids and searched_tag:
                    print("Searched Tag collided with active filters, not considering filters.")
                    pipeline = NextTrackPipeline(user_history=session_doc.user_history,
                                                 user_id=self.user_id,
                                                 group_id=self.group_id,
                                                 group_user_ids=self.group_user_ids,
                                                 action=action,
                                                 simulating=simulating,
                                                 searched_tag=searched_tag,
                                                 positive_interests=positive_interests,
                                                 negative_interests=negative_interests,
                                                 session_listened_tracks=[],
                                                 primary_mode=self.primary_mode,
                                                 secondary_mode=self.secondary_mode,
                                                 tertiary_mode=self.tertiary_mode,
                                                 tag_filters=None,
                                                 search_seed=None,
                                                 feature_filters=self.feature_filters)

                    track_ids, instructions = pipeline.run(seq_ids=seq_ids)

                if not track_ids:
                    print("No Tracks Available")
                    raise NoTracksAvailable

            shared_by = None
            return random.choice(track_ids), instructions, shared_by

    def _get_scored_action_dict(self, action: Action):

        if action == RightSwipe and action.play_ratio < self.right_time_threshold:
            score = (action.love * self.love_seed_mul)
            if score > self.max_seed_score: score = self.max_seed_score

            return {'action': 'right',
                    'love_count': action.love,
                    'play_ratio': action.play_ratio,
                    'score': score
                    }


        elif action == RightSwipe:
            score = (action.love * self.love_seed_mul) + self.right_seed_score
            if score > self.max_seed_score: score = self.max_seed_score

            return {'action': 'right',
                    'love_count': action.love,
                    'play_ratio': action.play_ratio,
                    'score': score
                    }

        elif action == LeftSwipe:
            score = (action.love * self.love_seed_mul) - self.left_seed_pen
            if score > self.max_seed_score: score = self.max_seed_score

            return {'action': 'left',
                    'love_count': action.love,
                    'play_ratio': action.play_ratio,
                    'score': score
                    }

        elif action == Complete:
            score = (action.love * self.love_seed_mul) + self.complete_seed_score

            if score > self.max_seed_score: score = self.max_seed_score

            return {'action': 'complete',
                    'love_count': action.love,
                    'play_ratio': 1.0,
                    'score': score
                    }

        raise Exception("Got Unknown action in step")

    def _get_no_feedback_action_dict(self, play_ratio, love_count):
        score = love_count
        if play_ratio >= 3 * self.right_time_threshold:
            score += 0.25
        return {'action': "UNSPECIFIED", "love_count": love_count, "play_ratio": play_ratio, "score": score}

    # def _sample_track_from_tree(self, cursor, session_doc:mm.MusicSession):
    #     embedder = TrackEmbedder.from_file('/home/admin/embed_models/')
    #     scaler = EmbedScaler.from_file('/home/admin/embed_models/')
    #
    #     tdoc = mm.Track.objects().get(id=ObjectId(session_doc.seed_track['id']))
    #     # tdoc = mm.Track.from_id(id=session_doc.seed_track['id']).to_dict()
    #     feats = embedder.tdoc2feats(tdoc)
    #     seed_v = embedder.feats2v(feats)
    #
    #     feat2w = {
    #         MusicTagType.genres: session_doc.feature_importance['genre'],
    #         Features.valence: session_doc.feature_importance['valence'],
    #         Features.loudness: session_doc.feature_importance['activation'],
    #         Features.deepness: session_doc.feature_importance['deepness'],
    #     }
    #
    #     tspace = self._get_tspace(embedder=embedder, scaler=scaler,
    #                               feat2w=feat2w, cursor=cursor)
    #     ids, dists = tspace.get_nns_by_vector(v=seed_v, n=10)
    #     weights = [1 / dist if dist != 0 else 0 for dist in dists]
    #     track_id = random.choices(ids, weights=weights, k=1)[0]
    #     return track_id
    #
    # def _get_tspace(self,embedder:TrackEmbedder, scaler:EmbedScaler,
    #                 feat2w: Dict[TrackFeaturesTag, float], cursor: None) -> TSpace:
    #     index = FaissIndex(dim=embedder.dim, metric='euclidean')
    #     index = IndexMap(index)
    #     tspace = TSpace(
    #         index=index,
    #         embedder=embedder,
    #         scaler=scaler,
    #         feat2w=feat2w,
    #     )
    #     cursor = cursor or mm.Track.objects().aggregate({"$match": {}})
    #     tspace.populate_index(cursor=cursor)
    #     return tspace

    def update_search_seed(self, session_doc, action, play_ratio=None, searched_tag: MusicTag = None):
        love_threshold = 2
        complete_threshold = 3
        right_threshold = 5

        album_love_threshold = 1
        album_right_threshold = 2
        album_complete_threshold = 2

        artist_love_threshold = 1
        artist_complete_threshold = 2
        artist_right_threshold = 2

        if action == 'search':
            if searched_tag.tag_type == MusicTagType.artists:
                session_doc.search_seed = mm.SearchSeed(name=self._get_key_from_music_tag(searched_tag),
                                                        loves=artist_love_threshold,
                                                        completes=artist_complete_threshold,
                                                        rights=artist_right_threshold)

            elif searched_tag.tag_type == MusicTagType.album:
                session_doc.search_seed = mm.SearchSeed(name=self._get_key_from_music_tag(searched_tag),
                                                        loves=album_love_threshold,
                                                        completes=album_complete_threshold,
                                                        rights=album_right_threshold)

            else:
                session_doc.search_seed = mm.SearchSeed(name=self._get_key_from_music_tag(searched_tag),
                                                        loves=love_threshold,
                                                        completes=complete_threshold,
                                                        rights=right_threshold)


        elif action == 'love':
            if session_doc.search_seed:
                session_doc.search_seed.loves -= 1
                session_doc.search_seed.completes -= 0.5
                session_doc.search_seed.rights -= 0.5
                if session_doc.search_seed.loves <= 0:
                    session_doc.search_seed = None

        elif action == 'complete':
            if session_doc.search_seed:
                session_doc.search_seed.completes -= 1
                session_doc.search_seed.rights -= 0.5
                if session_doc.search_seed.completes <= 0:
                    session_doc.search_seed = None

        elif action == 'right' and play_ratio >= self.right_time_threshold:
            if session_doc.search_seed:
                session_doc.search_seed.rights -= 1
                if session_doc.search_seed.rights <= 0:
                    session_doc.search_seed = None

        return session_doc

    def update_interests(self, action: str, positive_interests_dict, negative_interests_dict,
                         seed_track, thorn_track, searched_tag=None, search_seed=None,
                         track_id=None, play_ratio=None, love_count=None):

        positive_interests = defaultdict(float, self._redistribute(interests=positive_interests_dict, red_factor=0.5))
        negative_interests = defaultdict(float, self._redistribute(interests=negative_interests_dict, red_factor=0.5))

        seed_feats = seed_track.feats if seed_track else []
        thorn_feats = thorn_track.feats if thorn_track else []
        seed_track_score = seed_track.score if seed_track else 0.0
        search_seed_name = search_seed.name if search_seed else None

        # ints as concat pos and neg dict, with neg values with appended -
        ints = defaultdict(float)
        max_int = 0.
        min_int = 0.

        # calculate max(ints), min(ints) or base_neg
        for i, value in negative_interests.items():
            value = -value
            if value < min_int: min_int = value
            ints[i] = value

        for i, value in positive_interests.items():
            if value > max_int: max_int = value
            ints[i] = value

        # if positive or negative interests are empty [1 and -1 don't imply directly that but should be ol]
        max_int = max(1., max_int)

        if action == 'start':
            tag_search_mul = 4.
            track_search_mul = 2.

            # filters passed in the beginning
            for tag_filter in self.tag_filters:

                # a track was passed
                if tag_filter.tag_type == MusicTagType.track:
                    interests = self._get_track_interests(track_id=tag_filter.id)
                    for interest in interests:
                        ints[interest] += track_search_mul * max_int

                # for all other generic tags
                else:
                    interest = self._get_key_from_music_tag(tag_filter)
                    ints[interest] += tag_search_mul * max_int

        elif action == 'search':
            tag_search_mul = 4.
            track_search_mul = 2.

            # If a track is searched, the seed track is already the searched track
            if track_id:
                for interest in seed_feats:
                    ints[interest] += track_search_mul * max_int

            else:
                interest = self._get_key_from_music_tag(searched_tag)
                ints[interest] += tag_search_mul * max_int

        elif action == 'right' or action == 'complete' or action == 'love':
            love_seed_mul = 3.
            love_mul = 1.5
            complete_seed_mul = 1.
            right_seed_mul = 0.75
            complete_mul = 0.5
            love_thorn_mul = 0.5
            # quick_right_seed_mul  = 0.5
            complete_thorn_mul = 0.25
            right_mul = 0.25
            # quick_right_mul       = 0.1
            right_thorn_mul = 0.01

            interests = self._get_track_interests(track_id=track_id)
            # the song was loved and not left swiped, as good as it gets
            if love_count > 0.:
                love_count_mult = max(0.2, love_count)
                for interest in interests:
                    if interest in seed_feats and interest in thorn_feats:
                        continue
                    elif interest == search_seed_name:
                        mult = 1.5
                        if interest in seed_feats:
                            ints[interest] += love_count_mult * mult * love_seed_mul * max_int
                        elif interest in thorn_feats:
                            ints[interest] += love_count_mult * mult * love_thorn_mul * max_int
                        else:
                            ints[interest] += love_count_mult * mult * love_mul * max_int
                    else:
                        if interest in seed_feats:
                            ints[interest] += love_count_mult * love_seed_mul * max_int
                        elif interest in thorn_feats:
                            ints[interest] += love_count_mult * love_thorn_mul * max_int
                        else:
                            ints[interest] += love_count_mult * love_mul * max_int

            # the song was completed but not loved
            elif action == 'complete':
                for interest in interests:
                    interest_name_list = interest.split(',')
                    interest_type = interest_name_list[0]
                    interest_value = ''.join(interest_name_list[1:])
                    if interest_type not in ['album', 'artist']:
                        if interest in seed_feats and interest in thorn_feats:
                            continue
                        elif interest == search_seed_name:
                            if interest in seed_feats:
                                ints[interest] += 1.5 * complete_seed_mul * max_int
                            elif interest in thorn_feats:
                                ints[interest] += 1.5 * complete_thorn_mul * max_int
                            else:
                                ints[interest] += 1.5 * complete_mul * max_int
                        else:
                            if interest in seed_feats:
                                ints[interest] += complete_seed_mul * max_int
                            elif interest in thorn_feats:
                                ints[interest] += complete_thorn_mul * max_int
                            else:
                                ints[interest] += complete_mul * max_int

            # the song was neither completed nor loved
            elif play_ratio >= self.right_time_threshold:
                for interest in interests:
                    interest_name_list = interest.split(',')
                    interest_type = interest_name_list[0]
                    interest_value = ''.join(interest_name_list[1:])
                    if interest_type not in ['album', 'artist']:
                        if interest in seed_feats and interest in thorn_feats:
                            continue
                        elif interest == search_seed_name:
                            if interest in seed_feats:
                                ints[interest] += 1.5 * right_seed_mul * max_int
                            elif interest in thorn_feats:
                                ints[interest] += 1.5 * right_thorn_mul * max_int
                            else:
                                ints[interest] += 1.5 * right_mul * max_int
                        else:
                            if interest in seed_feats:
                                ints[interest] += right_seed_mul * max_int
                            elif interest in thorn_feats:
                                ints[interest] += right_thorn_mul * max_int
                            else:
                                ints[interest] += right_mul * max_int

            # the song was very quickly right swiped
            # elif play_ratio > 0.0:
            #     for interest in interests:
            #         interest_name_list = interest.split(',')
            #         interest_type = interest_name_list[0]
            #         interest_value = ''.join(interest_name_list[1:])
            #
            #         if interest_type not in ['album', 'artist']:
            #             if interest in seed_feats and interest in thorn_feats:
            #                 continue
            #             elif interest == search_seed_name:
            #                 if interest in seed_feats:
            #                     ints[interest] += 1.5 * right_seed_mul * max_int
            #             else:
            #                 if interest in seed_feats:
            #                     ints[interest] += right_seed_mul * max_int
            #                 else:
            #                     ints[interest] += right_mul * max_int

        # the thorn is basically the track_id, all the interests are going to be in thorn
        elif action == 'left':
            left_mul_artist = -1.0  # penalize artists more heavily on left swipe than anything else
            left_mul_album = -1.0  # penalize albums more heavily than everything as well, regardless if it is in seed
            left_mul_seed = -0.5  # all features in seed track
            left_mul = -0.75  # for features not in seed track
            seed_mul = 0.5  # if feature in seed but not in thorn, a very positive signal
            search_mul = 1.  # user searched for a tag, and a song without the search seed played

            is_seed_track_relevant = seed_track_score >= 0.25

            for interest in thorn_feats:
                interest_name_list = interest.split(',')
                interest_type = interest_name_list[0]
                interest_value = ''.join(interest_name_list[1:])

                if search_seed_name == interest:
                    continue

                if interest_type == 'artist':
                    ints[interest] += left_mul_artist * max_int

                if interest_type == 'album':
                    ints[interest] += left_mul_album * max_int

                elif interest in seed_feats and is_seed_track_relevant:
                    # if something is in search and in seed_track as well, do not penalize at all
                    if search_seed_name == interest:
                        print(f"Found {search_seed_name} in both search and seed")
                        continue
                    else:
                        ints[interest] += left_mul_seed * max_int

                elif interest == search_seed_name:
                    ints[interest] += left_mul_seed * max_int

                else:
                    ints[interest] += left_mul * max_int

            for seed_interest in seed_feats:
                if seed_interest not in thorn_feats:
                    ints[seed_interest] += seed_mul * max_int

            if search_seed_name not in thorn_feats:
                if is_seed_track_relevant:
                    ints[search_seed_name] += search_mul * max_int
                else:
                    ints[search_seed_name] += 4. * search_mul * max_int

        # divide interests into negative interests and positive interests
        pis = {}
        nis = {}
        for i, v in ints.items():
            # only add if positive_interest not in negative_filters and vice versa
            if self._complies_with_filters(i, v):
                if v > 0.:
                    pis[i] = v
                elif v < 0.:
                    nis[i] = -v

        return pis, nis

    def _redistribute(self, interests, red_factor):
        sum_values = sum(interests.values())
        interests_len = len(interests)
        for key, value in interests.items():
            interests[key] = value * (1 - red_factor) + ((red_factor * sum_values) / interests_len)
        return interests

    def _get_key_from_music_tag(self, music_tag):
        if music_tag.tag_type == MusicTagType.genres:
            return f"genres,{music_tag.name}"
        elif music_tag.tag_type == MusicTagType.artists:
            return f"artist,{music_tag.name}"
        elif music_tag.tag_type == MusicTagType.languages:
            return f"language,{music_tag.name}"
        elif music_tag.tag_type == MusicTagType.about:
            return f"about,{music_tag.name}"
        elif music_tag.tag_type == MusicTagType.instruments:
            return f"instrument,{music_tag.name}"
        elif music_tag.tag_type == MusicTagType.album:
            return f"album,{music_tag.name}"

    # these hacks required because mongodb complains with keys sometimes
    def interest_dict_to_list_adapter(self, interests_dict: dict):
        interests_list = []
        for k, v in interests_dict.items():
            klist = k.split(',')
            type = klist[0]
            name = ''.join(klist[1:])
            interests_list.append({'type': type, 'value': name, 'score': v})
        return interests_list

    def interest_list_to_dict_adapter(self, interests_list: list):
        interests_dict = {}
        for interest in interests_list:
            key = interest['type'] + ',' + interest['value']
            interests_dict[key] = interest['score']
        return interests_dict

    def _get_track_interests(self, track_id):
        track = mm.Track.objects().get(id=ObjectId(track_id))
        interests = []
        for interest in track.genres: interests.append("genres,{}".format(interest['name']))
        for interest in track.languages: interests.append("language,{}".format(interest['name']))
        for interest in track.artists: interests.append("artist,{}".format(interest['name']))
        for interest in track.about: interests.append("about,{}".format(interest['name']))
        for interest in track.instruments: interests.append("instrument,{}".format(interest['name']))
        if track.album: interests.append(f"album,{track.album['name']}")
        interests.append(f"valence,{track.valence}")
        interests.append(f"loudness,{track.loudness}")
        interests.append(f"deepness,{track.deepness}")
        interests.append(f"speechiness,{track.speechiness}")
        return interests

    def _get_base_track_from_id(self, tid: str):
        seed_tdoc = mm.Track.objects().get(id=ObjectId(tid))
        feats = []
        feats += [f'valence,{seed_tdoc.valence}']
        feats += [f'loudness,{seed_tdoc.loudness}']
        feats += [f'deepness,{seed_tdoc.deepness}']
        feats += [f'speechiness,{seed_tdoc.speechiness}']
        feats += ['genres,{}'.format(o['name']) for o in seed_tdoc.genres]
        if seed_tdoc.album: feats += ['album,{}'.format(seed_tdoc.album['name'])]
        feats += ['artist,{}'.format(o['name']) for o in seed_tdoc.artists]
        feats += ['language,{}'.format(o['name']) for o in seed_tdoc.languages]
        feats += ['about,{}'.format(o['name']) for o in seed_tdoc.about]
        feats += ['instrument,{}'.format(o['name']) for o in seed_tdoc.instruments]

        return mm.BaseTrack(feats=feats, embed=seed_tdoc.embed)

    def _map_string_to_old_types(self, i_type: str):
        if i_type == 'valence':
            return Features.valence
        elif i_type == 'loudness':
            return Features.loudness
        elif i_type == 'deepness':
            return Features.deepness
        elif i_type == 'speechiness':
            return Features.speechiness
        elif i_type == 'genres':
            return MusicTagType.genres
        elif i_type == 'language':
            return MusicTagType.languages
        elif i_type == 'about':
            return MusicTagType.about
        elif i_type == 'instrument':
            return MusicTagType.instruments
        elif i_type == 'artist':
            return MusicTagType.artists
        elif i_type == 'album':
            return MusicTagType.album

    def _complies_with_filters(self, interest, score):
        # the basic utility of this function is to make sure that interests don't collide with filters
        if not interest: return False

        i_list = interest.split(',')
        i_type = i_list[0]
        i_val = ''.join(i_list[1:])

        i_type_old = self._map_string_to_old_types(i_type)

        # if interest type is a feature
        if i_type in ['valence', 'loudness', 'deepness', 'speechiness']:
            i_val = float(i_val)
            for feature_filter in self.feature_filters:

                # we can directly return values because at once one kind of feature is passed to this function
                if feature_filter.feature == i_type_old:

                    if self._is_feature_filter_applied(feature_filter):
                        # If positive interest, Make sure that the current value lies in the range set by user
                        # if the interest is negative and lies in range set by user, we don't want to fuck with it
                        # if the interest is negative and lies in range outside, filter automatically takes care of it
                        if i_val >= feature_filter.min and i_val <= feature_filter.max and score > 0.: return True

                    # if no filter is applied any value should be allowed to be in interest
                    else:
                        return True

            # cases where pin not in user applied range, nin in user applied range
            return False

        # if interest type is a tag
        else:
            for tag_filter in self.tag_filters:
                filter_state = tag_filter.filter_state
                tag_name = tag_filter.name
                tag_type = tag_filter.tag_type

                if tag_type == i_type_old and tag_name == i_val:
                    if filter_state == FilterState.exclude and score > 0.: return False
                    if filter_state == FilterState.include and score < 0.: return False

            # if no applied tag filters match any interest return true for that interest
            return True

    def _is_feature_filter_applied(self, feature_filter):
        if (feature_filter.feature == Features.valence and not (feature_filter.min == 1. and feature_filter.max == 5.)):
            return True
        elif (feature_filter.feature == Features.loudness and not (
                feature_filter.min == 1. and feature_filter.max == 8.)):
            return True
        elif (feature_filter.feature == Features.deepness and not (
                feature_filter.min == 1. and feature_filter.max == 4.)):
            return True
        elif (feature_filter.feature == Features.speechiness and not (
                feature_filter.min == -1. and feature_filter.max == 1.)):
            return True
        else:
            return False

    def get_primary_mode(self, action):
        if self.primary_mode == PrimaryMode.Magic:
            modes = [PrimaryMode.Magic, PrimaryMode.Discover, PrimaryMode.Favorites]
            if self.group_id:
                if action == 'start':
                    prs = [0.1, 0.7, 0.2]
                elif action == 'left':
                    prs = [0.2, 0.4, 0.4]
                else:
                    prs = [0.1, 0.7, 0.2]
            else:
                if action == 'start':
                    prs = [0., 0.9, 0.1]
                elif action == 'left':
                    prs = [0.1, 0.45, 0.45]
                else:
                    prs = [0.1, 0.7, 0.2]

            return random.choices(modes, prs)[0]

        else:
            return self.primary_mode

    @staticmethod
    def get_group_id(group_user_ids):
        ids = sorted(group_user_ids)
        try:
            gdoc = mm.GroupInfo.objects.get(users=ids)
            gid = gdoc.id
        except DoesNotExist:
            gid = mm.GroupInfo(users=ids).save()

        return str(gid)

    @classmethod
    def from_body(cls, user_id, body, track_id=None, session_id=None):
        _me_tags = MeTag.from_list_json(body['meTags'])
        context = _me_tags[0] if _me_tags else None
        tag_filters = MusicTag.from_list_json(body['musicTags'])
        group_user_ids = set(body['groupUsers']) if ('groupUsers' in body) else []
        if group_user_ids:
            group_user_ids.add(user_id)
            group_user_ids = list(group_user_ids)
        group_id = AlphaPlayback.get_group_id(group_user_ids=group_user_ids) if group_user_ids else None
        feature_filters = UserFeature.from_dict_json(body['userFeatures'])
        primary_mode = PrimaryMode[body['primaryMode']] if ('primaryMode' in body) else None
        secondary_mode = SecondaryMode[body['secondaryMode']] if ('secondaryMode' in body) else None
        tertiary_mode = TertiaryMode[body['tertiaryMode']] if ('tertiaryMode' in body) else None
        user_datetime = datetime.fromisoformat(body['time']) if ('time' in body) else None
        user_time = {}

        if user_datetime:
            user_time['year'] = user_datetime.year
            user_time['month'] = user_datetime.month
            user_time['day'] = user_datetime.day
            user_time['hour'] = user_datetime.hour
            user_time['minute'] = user_datetime.minute
            user_time['minute_hour'] = user_datetime.hour + (user_datetime.minute / 60.)

        time = {'server': datetime.utcnow(),
                'user': user_time}

        return cls(
            user_id=user_id,
            group_user_ids=group_user_ids,
            group_id=group_id,
            track_id=track_id,
            session_id=session_id,
            time=time,
            context=context,
            tag_filters=tag_filters,
            feature_filters=feature_filters,
            primary_mode=primary_mode,
            secondary_mode=secondary_mode,
            tertiary_mode=tertiary_mode
        )

    def shared_tracks_notify_friends(self, user_id, track_id):
        dm_user = dm.UserManager.from_id(user_id=user_id)
        neo_user = dm_user.neo_user
        track_name = mm.Track.from_id(track_id).name

        notify_friends = nd.api_alpha.notify_friends_share_track(user=neo_user,
                                                                 shared_track_id=track_id)  # friendname, trackname, notified, visualized returned
        #ADDED BY PRATHYUSHA
        for friend in notify_friends:
            if friend['track_name'] == None:
                nd.api_alpha.set_track_name_from_id(track_id=track_id,track_name=track_name.lower())
                # track_name = track_name.lower()
                # code = """
                #         MERGE (t:Track{id:$track_id})
                #         set t.name = $track_name
                #         """
                # write(code, track_name=track_name, track_id = track_id).data()
        ###ADD COMPLETED BY PRATHYUSHA

        for friend in notify_friends:
            if (friend['notified'] != True):
                message_title = dm_user.mongo_user.username + " loved your recommendation!"
                message_body = str(track_name).capitalize()
                data = {'page': '/chatPage',
                        'username': str(neo_user.name),
                        'trackId': str(track_id),
                        'trackName': track_name,
                        'user_id': str(neo_user.mongo_id)}
                fire_user_friend = friend['friend_id'].fire_user
                neo_user_friend = friend['friend_id'].neo_user
                fire_user_friend.create_notification(message_title=message_title, message_body=message_body, data=data)
                # friend['notified']=True  ##add in shared_tracks todo(attribute for shared.)...#write notified as true for this shared track
                nd.api_alpha.set_friend_notified(user=neo_user, friend=neo_user_friend, shared_track_id=track_id)