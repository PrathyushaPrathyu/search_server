from recommender import *
from .context_controller import ContextController
from .music_tag_controller import MusicTagController
from .user_features_controller import UserFeaturesController
from .search_filters import SearchFilters


class PipelineController:
  def __init__(
      self,
      context_control:ContextController,
      music_tag_control:MusicTagController,
      feats_control:UserFeaturesController,
      search_filters:SearchFilters,
      instructions_recorder:InstructionsRecorder,
  ):
    self.context_control = context_control
    self.music_tag_control = music_tag_control
    self.feats_control = feats_control
    self.search_filters = search_filters
    self.instructions_recorder = instructions_recorder

  def get_pipeline(self, seeding: bool, pipeline:db.Pipeline=None):
    # Define the order of operations
    ops = [
      lambda: True,
      lambda: self._remove_tag_type(MusicTagType.track),
      lambda: self._increase_feat_ranges(),
      lambda: self._remove_tag_type(MusicTagType.instrument),
      lambda: self._remove_tag_type(MusicTagType.artist),
      lambda: self._remove_tag_type(MusicTagType.about),
      lambda: self._remove_tag_type(MusicTagType.emotion),
      lambda: self._increase_feat_ranges(),
      lambda: self._remove_tag_type(MusicTagType.genres),
      lambda: self._increase_feat_ranges(),
      lambda: self._remove_tag_type(MusicTagType.language),
    ]
    for op in ops:
      changed = op()
      if changed:
        available, new_pipeline = self._has_available_tracks(seeding=seeding, starting_pipeline=pipeline)
        if available: return new_pipeline

    raise NoTracksAvailable

  def _remove_tag_type(self, music_tag_type:MusicTagType):
    removed = self.music_tag_control.remove_tag_type(music_tag_type)
    search_removed = self.search_filters.remove_tag_type(music_tag_type)
    if removed and music_tag_type != MusicTagType.track: self.instructions_recorder.add(f'tag remove {music_tag_type}')
    return removed or search_removed

  def _increase_feat_ranges(self):
    removed = self.feats_control.increase_ranges()
    if removed:
      self.instructions_recorder.add(f'feature increaseRange valence')
      self.instructions_recorder.add(f'feature increaseRange deepness')
      self.instructions_recorder.add(f'feature increaseRange loudness')
    return removed

  def _has_available_tracks(self, seeding:bool, starting_pipeline:db.Pipeline=None) -> [bool, db.Pipeline]:
    #TODO: Add searched tag logic here. Should take care of active filters and features already.
    for inner_match in ['$and', '$or']:
      pipeline = starting_pipeline.copy() if starting_pipeline else db.Pipeline()
      pipeline.add_stages(self.search_filters.get_pipeline_stages(seeding=seeding, inner_match=inner_match))
      pipeline.add_stage(self.context_control.get_played_tracks_pipeline_stage())
      pipeline.add_stages(self.feats_control.get_pipeline_stages())
      pipeline.add_stages(self.music_tag_control.get_pipeline_stages(seeding=seeding, inner_match=inner_match))
      # logger.debug(f'Pipeline: {pipeline}')

      # OPTIMIZE: Running filters only to get document count
      n_tracks = len(list(pipeline.run(projection=['_id'])))
      # logger.debug(f'AvailableTracks({inner_match}): {n_tracks}')
      if n_tracks > 0: break

    return n_tracks > 0, pipeline
