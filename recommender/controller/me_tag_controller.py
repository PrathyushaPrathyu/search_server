from recommender import *


class MeTagController:
  def __init__(self, user_id:str, tags:List[MeTag]):
    self.user_id, self.tags = user_id, tags
    self.contexts = self._get_contexts(tags=tags)

  def get_pipeline_stages(self, interest:InterestTag, reduce):
    ids = [set([ObjectId(_id) for _id in o]) for o in self.get_history(interest=interest)]
    if ids: return [{'$match': {'_id': {'$in': list(reduce(*ids))}}}]
    else:   return [{'$match': {'_id': 'nan'}}]

  def get_history(self, interest:InterestTag):
    return [o.get_history(interest=interest) for o in self.contexts]

  def get_scores(self, interest:InterestTag, track_ids:List[str]):
    return [cont.get_scores(interest=interest, track_ids=track_ids) for cont in self.contexts]

  def _get_contexts(self, tags:List[MeTag]):
    return [factory.get_interest_controller(user_id=self.user_id, context=o.name.lower()) for o in tags]

  @classmethod
  def from_list_json(cls, user_id:str, x):
    return cls(tags=[MeTag.from_json(o) for o in x], user_id=user_id)
