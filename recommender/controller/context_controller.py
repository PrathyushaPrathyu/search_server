from boltons.cacheutils import cachedproperty

from recommender import *
from recommender import factory
from recommender.tag import MeTag
from recommender.models import Action
from recommender.enums import InterestTag
from recommender.history import History, CombinedHistory
from typing import *


class ContextController:
  def __init__(self, global_hist:History, hists:List[History]):
    self.global_hist = global_hist
    self.hists = hists
    self.tag2items = {
      InterestTag.glt: self.global_hist.long,
      InterestTag.gst: self.global_hist.short,
      InterestTag.favorites: self.global_hist.favorites,
      InterestTag.slt: self.hists[0].long if hists else [],
      InterestTag.sst: self.hists[0].short if hists else [],
      # InterestTag.slt: self.combined_hist.long,
      # InterestTag.sst: self.combined_hist.short,
    }
    self.tag2history = {
      InterestTag.glt: self.global_hist,
      InterestTag.gst: self.global_hist,
      InterestTag.favorites: self.global_hist,
      InterestTag.slt: self.hists[0] if hists else [],
      InterestTag.sst: self.hists[0] if hists else [],
      # InterestTag.slt: self.combined_hist,
      # InterestTag.sst: self.combined_hist,
    }

  @property
  def seed_track(self):
    # Be careful to change how the seed_track is get. Update_seed_track only updates global
    return self.global_hist.seed_track

  @property
  def contexts(self) -> List[str]:
    return [o.context for o in self.hists]

  def set_seed_track(self, track_id:str):
    reward = reward_from_action(Complete(love=0.0))
    self.global_hist.set_seed_track(track_id=track_id, reward=reward)

  def register_action(self, action:Action, track_id:str):
    self.global_hist.register_action(action=action, track_id=track_id)
    # self.combined_hist.register_action(action=action, track_id=track_id)
    for hist in self.hists: hist.register_action(action=action, track_id=track_id)

  def get_interest_items(self, interest:InterestTag):
    return self.tag2items[interest]

  def get_history(self, interest:InterestTag):
    return self.tag2history[interest]

  def reset_session(self):
    self.global_hist.reset_session()
    # self.combined_hist.reset_session()
    for hist in self.hists: hist.reset_session()

  def get_played_tracks_pipeline_stage(self):
    return self.global_hist.get_played_tracks_pipeline_stage()

  def get_new_tracks_pipeline_stage(self):
    return self.global_hist.get_new_tracks_pipeline_stage()

  def save(self, user_id:str):
    self.global_hist.save_as_global(user_id=user_id)
    for hist in self.hists: hist.save_as_context(user_id=user_id)

  # @cachedproperty
  # def combined_hist(self):
  #   hist = CombinedHistory(hists=self.hists, keys_reduce=set.intersection)
  #   if len(hist) == 0:
  #     hist = CombinedHistory(hists=self.hists, keys_reduce=set.union)
  #   return hist

  @classmethod
  def from_tags(cls, user_id:str, tags:List[MeTag]):
    global_hist = factory.get_global_history(user_id=user_id)
    hists = [factory.get_context_history(user_id=user_id, context=o.name) for o in tags]
    return cls(global_hist=global_hist, hists=hists)

  @classmethod
  def from_list_json(cls, user_id, list_json):
    tags = [MeTag.from_json(o) for o in list_json]
    return cls.from_tags(user_id=user_id, tags=tags)
