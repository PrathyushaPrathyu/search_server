from recommender import *
from typing import *
from collections import namedtuple
from copy import deepcopy

FeatureConstraints = namedtuple('FeatureConstraints', 'min max')


class UserFeaturesController:
  feature2constraint = {
    Features.valence: FeatureConstraints(min=1, max=5),
    Features.loudness: FeatureConstraints(min=1, max=8),
    Features.deepness: FeatureConstraints(min=1, max=4)
  }

  def __init__(self, user_features: List[UserFeature]):
    self.user_feats = user_features
    self._feats2prefs = {o.feature: o for o in self.user_feats}

  def increase_ranges(self):
    # logger.debug('increase_ranges')
    # TODO: Modify feature one by one
    changed = False
    for feat, c in self.feature2constraint.items():
      user_feat = self._feats2prefs[feat]
      before = deepcopy(user_feat)
      user_feat.min = int(np.clip(user_feat.min - 1, c.min, c.max))
      user_feat.max = int(np.clip(user_feat.max + 1, c.min, c.max))
      if before != user_feat: changed = True
    return changed

  def get_pipeline_stages(self):
    return [{'$match': o.to_filter()} for o in self.user_feats]

  @classmethod
  def from_dict_json(cls, x):
    return cls(user_features=UserFeature.from_dict_json(x))
