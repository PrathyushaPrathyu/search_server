from itertools import groupby
from recommender import *
from recommender import db


class MusicTagController:
  def __init__(self, tags:List[MusicTag], searched_tag:MusicTag=None):
    self.searched_tag = searched_tag
    self.tags = tags


  def remove_tag_type(self, tag_type: MusicTagType) -> bool:
    # logger.debug(f'remove_tag_type | tag_type: {tag_type}')
    n_tags_before = len(self.tags)
    self.tags = [t for t in self.tags if not
    (t.tag_type == tag_type and t.filter_state == FilterState.include)]
    return n_tags_before != len(self.tags)

  def get_searched_tracks_ids(self, seeding: bool):
    if seeding:
      if self.searched_tag:
        if self.searched_tag.tag_type == MusicTagType.track:
          return [self.searched_tag.id]
      return [t.id for t in self.tags if t.tag_type == MusicTagType.track]
    return []

  def get_pipeline(self, seeding: bool):
    return self.get_pipeline_stages(seeding=seeding)

  def get_pipeline_stages(self, seeding: bool, inner_match:str):
    if not self.tags:
      return [{'$match': {}}]

    groups = []
    for k, g in groupby(self.tags, lambda x: x.tag_type):
      groups.append({inner_match: [t.to_filter(seeding=seeding) for t in g]})
    return [{'$match': {'$and': groups}}]

  # def get_tdocs(self, seeding: bool):
  #   return db.get_tcol().aggregate(self.get_pipeline(seeding=seeding))

  def get_search_pipeline_stages(self):
    if not self.searched_tag:
      return [{'$match': {}}]

    return [{'$match': self.searched_tag.to_filter(seeding=False)}]

  def get_track_tags(self) -> List[MusicTag]:
    return [t for t in self.tags if t.tag_type == MusicTagType.track]

  def tags_to_json(self):
    return [t.to_json() for t in self.tags]

  def register_searched_tag(self, searched_tag:MusicTag):
    searched_tag.filter_state = FilterState.include
    self.searched_tag = searched_tag

  @classmethod
  def from_list_json(cls, x):
    return cls(tags=[MusicTag.from_json(o) for o in x])

  @classmethod
  def from_tdoc(cls, tdoc):
    tags = []
    for o in MusicTagType:
      if o == MusicTagType.emotion: continue
      xs = tdoc.get(o.db_field, [])
      # TODO:HACK MusicTagType name hack
      tags.extend([MusicTag.from_json({'tagType': o.name, 'name': x['name']}) for x in xs])
    return cls(tags=tags)
