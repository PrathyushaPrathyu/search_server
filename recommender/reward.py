from recommender import *


def reward_from_action(action:Action):
  if action == RightSwipe:
    reward = action.love
    # Implement linear schedule
    if action.play_ratio > 0.25: reward += .1
    # reward = action.love + 0.2

  if action == LeftSwipe: reward = action.love
  if action == Complete:  reward = action.love + 0.4
  if action == Search:    reward = 0.8

  return reward
