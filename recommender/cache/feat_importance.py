import recommender as rd

_CACHE = {}

def get_feat_importance(key:str, reset:bool=False) -> rd.feature_importance.FeatureImportance:
  if reset:
    x = rd.factory.get_feat_importance()
    _CACHE[key] = x
    return x
  try: return _CACHE[key]
  except KeyError:
    x = rd.factory.get_feat_importance()
    _CACHE[key] = x
    return x
