from recommender import *

_CACHE = None

def get_tcol():
  global _CACHE
  if _CACHE is None:
    _CACHE = db.Cache.from_col(col=db.get_tcol())
  return _CACHE
