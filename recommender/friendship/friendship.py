# from recommender import *
# from recommender import db
#
# class Friendship(SerializableClass):
#   def __init__(self, _id:str, users:List[UserSimplifiedModel], shares=None):
#     self._writer = db.FriendsDBWriter(_id=_id)
#     self._id = _id
#     self.users = SerializableList(users)
#     self.shares = shares or {o._id: [] for o in self.users}
#
#   def has_user(self, user_id): return any([user_id == o._id for o in self.users])
#
#   def share_track(self, from_user_id, to_user_id, track_id):
#     assert self.has_user(from_user_id)
#     assert self.has_user(to_user_id)
#     self.shares[from_user_id].append(track_id)
#     up = {'$addToSet': {f'shares.{from_user_id}': track_id}}
#     self._writer.register_update(db.DBOperation(up))
#
#   def get_friend_of(self, user_id):
#     for user in self.users:
#       if user_id != user._id:
#         return user
#
#   def save(self):
#     return self._writer.write()
#
#   @classmethod
#   def from_users(cls, user_id1, user_id2):
#     doc = db.get_friend_doc(user_id1=user_id1, user_id2=user_id2)
#     users = [UserSimplifiedModel.from_dict(d) for d in doc['users']]
#     return cls(
#       _id=str(doc['_id']),
#       users=users,
#       shares=doc['shares'],
#     )
#
#   @classmethod
#   def from_dict(cls, d:dict):
#     return cls(
#       _id=str(d['_id']),
#       users=[UserSimplifiedModel.from_dict(o) for o in d['users']],
#       shares=d['shares'],
#     )
#
#   @classmethod
#   def new(cls, users):
#     doc = cls(_id=None, users=users).to_dict(apply_ObjectId=True)
#     doc.pop('_id')
#     res = db.FriendsDBWriter.insert_new(doc)
#     return cls(
#       _id=str(res.inserted_id),
#       users=users,
#     )
