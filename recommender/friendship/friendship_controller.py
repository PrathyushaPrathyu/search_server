# from recommender import *
# from .friendship import Friendship
#
# class FriendshipController:
#   def __init__(self, user_id, friendships:List[Friendship]):
#     self.user_id = user_id
#     self.friendships = friendships
#
#   def get_friends(self) -> List[UserSimplifiedModel]:
#     return [o.get_friend_of(user_id=self.user_id) for o in self.friendships]
#
#   @classmethod
#   def from_user_id(cls, user_id):
#     friendships = db.get_friends_col().find({db.FriendsKeys.user_ids: ObjectId(user_id)})
#     friendships = [Friendship.from_dict(d) for d in friendships]
#     return cls(user_id=user_id, friendships=friendships)
