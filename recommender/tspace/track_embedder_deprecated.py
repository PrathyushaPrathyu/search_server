# import numpy as np, pandas as pd
# import lishutils as U
# from pathlib import Path
# from boltons.cacheutils import cachedproperty
# from lishutils.text import tfms
# from typing import *
#
# from recommender.errors import EmptyTagsError
# from recommender import *
# from recommender.enums import *
#
# class TrackSlices:
#   def __init__(self, genre:slice, emotion:slice, emotion_detailed:Dict[EmotionTag, slice]):
#     self.genre, self.emotion, self.emotion_detailed = genre, emotion, emotion_detailed
#
#   def __iter__(self):
#     yield from {
#       'genre': self.genre,
#       # 'emotion': self.emotion,
#       'valence': self.emotion_detailed[EmotionTag.valence],
#       'activation': self.emotion_detailed[EmotionTag.activation],
#       # 'deepness': self.emotion_detailed[EmotionTag.deepness],
#     }.items()
#
# class TrackEmbedder(Serializable2):
#   def __init__(self):
#     super().__init__()
#     self.dim = 23
#     self.feats2slices = {
#       TrackFeaturesTag.genre: slice(0, 20),
#       TrackFeaturesTag.valence: slice(20, 21),
#       TrackFeaturesTag.activation: slice(21, 22),
#       TrackFeaturesTag.deepness: slice(22, 23),
#     }
#     # self.slices = TrackSlices(
#     #   genre=slice(3, 23),
#     #   emotion=slice(0, 3),
#     #   emotion_detailed={
#     #     EmotionTag.valence: slice(0, 1),
#     #     EmotionTag.activation: slice(1, 2),
#     #     EmotionTag.deepness: slice(2, 3),
#     #   }
#     # )
#
#   def embed(self, tdoc) -> np.ndarray:
#     # tgenres = get_genres_intersect(tdoc=tdoc, top_n=5, intersect_w=1.)
#     tgenres = get_genres_intersect(tdoc=tdoc,
#                                    additional_genres=self._additional_genres,
#                                    languages=self._languages)
#     # tgenres = tdoc['tags']['arts_genres']
#     genre = self.genre_embed(tgenres=tgenres)
#     try: spot_feats = tdoc['spot_feats']
#     except KeyError: spot_feats = {}
#     emotion = self.emot_embed(temots=tdoc['tags']['emotions'], spot_feats=spot_feats)
#     return self._concat_embed(emotion=emotion, genre=genre)
#
#   def v2feats(self, v:np.ndarray):
#     if len(v) != self.dim: raise ValueError(f'v should have a len of {self.dim} but has {len(v)}')
#     return {k: v[slc] for k, slc in self.feats2slices.items()}
#
#   def feats2v(self, feats2v:Dict[TrackFeaturesTag, Union[float, np.ndarray]]):
#     if set(self.feats2slices) != set(feats2v):
#       raise ValueError(f'feats2v should contain exactly this keys: {list(self.feats2slices.keys())}')
#     res = np.zeros(self.dim)
#     for tag, v in feats2v.items():
#       slc = self.feats2slices[tag]
#       res[slc] = v
#     return res
#
#   def _concat_embed(self, emotion:np.ndarray, genre:np.ndarray) -> np.ndarray:
#     return np.concatenate((emotion, genre))
#
#   def genre_embed(self, tgenres):
#     if not tgenres: raise EmptyTagsError('Tdoc has no genres tags')
#     genres, counts = zip(*[(o['tag'], o['count']) for o in tgenres])
#     genre_embeds = self.genres2embeds(genres)
#     genre_count = np.array(counts)
#     # TODO: genres can be dropped silently if they're not in the dataframe
#     # TODO: Hack for giving equal weights to genres
#     if len(genre_embeds) == 0: raise EmptyTagsError('Hacky stuff')
#     # final_genre_embeds = genre_embeds.mean(axis=0)
#     genre_norm = genre_count / genre_count.sum()
#     final_genre_embeds = (genre_embeds * genre_norm[:, None]).sum(axis=0)
#     return final_genre_embeds
#
#   def emot_embed(self, temots, spot_feats):
#     # if not temots: raise EmptyTagsError('Tdoc has no emotions tags')
#     if temots:
#       emotions, counts = zip(*[(o['tag'], o['count']) for o in temots])
#       song_emot = self.emotions2embeds(emotions=emotions)
#       song_emot_count = ~np.isnan(song_emot) * np.asarray(counts)[:, None]
#       top_emot = song_emot_count.idxmax(axis=0)
#       final_emot_embeds = np.array(song_emot.loc[list(top_emot)]).diagonal().copy()
#     else:
#       raise EmptyTagsError
#       # total_count = 0
#       # final_emot_embeds = np.array([np.nan, np.nan, np.nan])
#     # TODO: zero divide replaces nans with zeros and the afterward we substitute zeros with nans again
#     # Take the mean of our embed and spotify value, nans are ignored
#     final_emot_embeds[final_emot_embeds == 0] = np.nan
#     spot_emot_embed = np.array([spot_feats.get('valence', np.nan), spot_feats.get('energy', np.nan), np.nan])
#     # TODO: Hackiest way possible
#     # Only for valence
#     tags_w, spot_w = self.get_tags_w_balance(count=sum(counts), max_count=50, max_w=0.9)
#     final_emot_embeds_valence = utils.math.weighted_nanmean([final_emot_embeds, spot_emot_embed], ws=[tags_w, spot_w])
#     valence = final_emot_embeds_valence[0]
#     # Only for activation
#     tags_w, spot_w = self.get_tags_w_balance(count=sum(counts), max_count=100, max_w=0.2)
#     final_emot_embeds_activation = utils.math.weighted_nanmean([final_emot_embeds, spot_emot_embed], ws=[tags_w, spot_w])
#     activation = final_emot_embeds_activation[1]
#     # Only for deepness
#     deepness = final_emot_embeds[2]
#     if deepness > 0.5: deepness = 1.
#     elif deepness < 0.5 and deepness > 0: deepness = 0.1
#     else: deepness = 0.
#
#     final_emot_embeds = np.array([valence, activation, deepness])
#     # nan means that the value was not modified, so it should be 0.5 (neutral)
#     final_emot_embeds[np.isnan(final_emot_embeds)] = 0.5
#     final_emot_embeds[final_emot_embeds == 0] = 0.5
#     return final_emot_embeds
#
#   def get_tags_w_balance(self, count, max_count=100, max_w=0.98):
#     tags_w = (count/max_count) * max_w
#     tags_w = np.clip(tags_w, 0., max_w)
#     spot_w = 1 - tags_w
#     return tags_w, spot_w
#
#   def genres2embeds(self, genres:List[str]) -> np.ndarray:
#     # If anything is dropped an error will be throw on genre_embed
#     return np.array(self._df_genres.reindex(list(genres)).dropna())
#
#   def emotions2embeds(self, emotions:List[str]) -> np.ndarray: return self._df_emotions.loc[list(emotions)]
#     # return np.array(self._df_emotions.reindex(list(emotions)).dropna())
#
#   @cachedproperty
#   def _df_genres(self):
#     # PATH = Path('/mnt/disks/dev/sketch/genre_similarity/from_tracks/data/50/tfmed/30_isomap_67')
#     # PATH = Path('/mnt/disks/dev/sketch/genre_similarity/from_tracks/data/50/intersect_genres/tfmed/20_isomap_76')
#     # PATH = Path('/mnt/disks/dev/sketch/genre_similarity/from_tracks/data/50/intersect_genres/tfmed_v1_2/additional_20_isomap_69')
#     PATH = Path('/mnt/disks/dev/sketch/genre_similarity/from_tracks/data/50/intersect_genres/tfmed_v2_0/20_isomap_70')
#     df_genres = pd.read_csv(PATH/'labels_embeds.tsv', header=None, sep='\t', index_col=0)
#     return df_genres
#
#   @cachedproperty
#   def _df_emotions(self):
#     df_emotions = U.df_from_gsheet('1XOzTntu_v0jAuwMPgxpvTa-yFE_mV5_HYU4a1MvnAnk')
#     df_emotions = df_emotions.drop(index=0).reset_index(drop=True)
#     df_emotions['emotion'] = df_emotions['emotion'].apply(self._preprocess_genre)
#     df_emotions.set_index(df_emotions.pop('emotion'), inplace=True)
#     df_emotions = df_emotions.astype(np.float32)
#     df_emotions = df_emotions.replace(0, np.NaN)
#     return df_emotions
#
#   @cachedproperty
#   def _additional_genres(self):
#     df = U.df_from_gsheet('1bmn8cSWalh6S55bQQOrBe_YTemS_Sopup9my3uxynwQ', header=None, index_col=0)
#     return list(df.index)
#
#   @cachedproperty
#   def _languages(self):
#     df = U.df_from_gsheet('1W2HAcPXJ7ww9sznuqCPPQc3j0LwerURRNN6yMkhFd9g', header=None, index_col=0)
#     return list(df.index)
#
#   def _preprocess_genre(self, s): return tfms.apply_tfms(s, tfms=tfms.COMMON)
#
# def get_genres_intersect(tdoc, additional_genres:List[str], languages:List[str], top_languages:int=2):
#   genres_arts = tdoc['tags']['arts_genres']
#   genres_arts = list({v['tag']:v for v in genres_arts}.values())
#   genres_arts_tags = [o['tag'] for o in genres_arts]
#   genres_all = tdoc['tags']['genres_all']
#   genres_all = sorted(genres_all, key=lambda x: x['count'], reverse=True)[:25]
#   genres_all_tags = [o['tag'] for o in genres_all]
#   genres_all_final = []
#   genres_arts_final = []
#   additional_genres_final = []
#   languages_final = []
#   for i, genre in enumerate(genres_all):
#     if genre['tag'] in languages and i < 3: languages_final.append(genre)
#     elif genre['tag'] in additional_genres: additional_genres_final.append(genre)
#     elif genre['tag'] in genres_arts_tags: genres_arts_final.append(genre)
#     else:                         genres_all_final.append(genre)
#   genres_arts_final = genres_arts_final or genres_arts
#   final_genres = additional_genres_final + genres_arts_final
#   # Modify genres to have same weight
#   final_genres = [{'tag': o['tag'], 'count': 1} for o in final_genres]
#   # Calculate languages weights
#   if languages_final:
#     languages_final = languages_final[:top_languages]
#     languages_tags, languages_weights = zip(*[(o['tag'], o['count']) for o in languages_final])
#     # languages_weights = (np.array(languages_weights) / np.sum(languages_weights)) * max(len(final_genres), 1)
#     languages_weights = np.array(languages_weights) / np.sum(languages_weights)
#     languages_final = [{'tag': k, 'count': w} for k, w in zip(languages_tags, languages_weights)]
#   else:
#     languages_final = []
#   # final_genres += languages_final_tags[:1] * len(final_genres)
#   # final_genres += languages_final_tags[:1] * 10
#   # final_genres = list(set(final_genres))
#   final_genres = languages_final + final_genres
#   return final_genres
#
# # def get_genres_intersect(tdoc, top_n:int, intersect_w:float=1.):
# #   genres_arts = tdoc['tags']['arts_genres']
# #   genres_arts_tags = [o['tag'] for o in genres_arts]
# #   genres_all = tdoc['tags']['genres_all']
# #   genres_all = sorted(genres_all, key=lambda x: x['count'], reverse=True)
# #   genres_all_tags = [o['tag'] for o in genres_all]
# #   genres_all_final_tags = []
# #   genres_arts_final_tags = []
# #   for genre in genres_all_tags:
# #     if genre in genres_arts_tags: genres_arts_final_tags.append(genre)
# #     else:                         genres_all_final_tags.append(genre)
# #   genres_final = genres_arts_final_tags + genres_all_final_tags
# #   # Change the schedule here
# #   top_n = len(genres_final[:top_n])
# #   ws = np.linspace(2, 1, top_n)
# #   ws[:len(genres_arts_final_tags)] *= intersect_w # New
# #   ws /= ws.sum()
# #   genres_intersect = [{'tag': g, 'count': w} for g, w in zip(genres_final[:top_n], ws)]
# #   #     import pdb; pdb.set_trace()
# #   return genres_intersect
#
# # def get_genres_intersect(tdoc, additional_genres:List[str], languages:List[str]):
# #   genres_arts = tdoc['tags']['arts_genres']
# #   genres_arts_tags = [o['tag'] for o in genres_arts]
# #   genres_all = tdoc['tags']['genres_all']
# #   genres_all = sorted(genres_all, key=lambda x: x['count'], reverse=True)[:25]
# #   genres_all_tags = [o['tag'] for o in genres_all]
# #   genres_all_final = []
# #   genres_arts_final = []
# #   additional_genres_final = []
# #   languages_final = []
# #   genres_all_final_tags = []
# #   genres_arts_final_tags = []
# #   additional_genres_final_tags = []
# #   languages_final_tags = []
# #   import pdb; pdb.set_trace()
# #   for genre in genres_all:
# #     if genre in languages: languages_final_tags.append(genre)
# #     if genre in additional_genres: additional_genres_final_tags.append(genre)
# #     if genre in genres_arts_tags: genres_arts_final_tags.append(genre)
# #     else:                         genres_all_final_tags.append(genre)
# #   genres_arts_final_tags = genres_arts_final_tags or genres_arts_tags
# #   final_genres = additional_genres_final_tags + genres_arts_final_tags
# #   final_genres = list(set(final_genres))
# #   # final_genres += languages_final_tags[:1] * len(final_genres)
# #   # final_genres += languages_final_tags[:1] * 10
# #   return [{'tag': g, 'count': 1} for g in final_genres]