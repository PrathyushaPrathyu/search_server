from recommender import *
from .track_embedder import TrackEmbedder
from .embed_scaler import EmbedScaler
from recommender.db import IterAll


class TSpace(Serializable2):
  def __init__(
      self,
      *,
      index:IndexMap,
      embedder:TrackEmbedder,
      scaler:EmbedScaler,
      feat2w:Dict[TrackFeaturesTag, float]=None,
  ):
    super().__init__()
    self.index,self.embedder,self.scaler = index,embedder,scaler
    self.feat2w = feat2w or {}
    self._skipped = 0

  def __len__(self): return len(self.index)

  @property
  def skipped_ratio(self): return self._skipped / len(self)

  def get_distance_by_vector(self, v1, v2):
    return self.index.get_distance_by_vector(v1=self.scale(v1), v2=self.scale(v2))

  def get_nns_by_vector(self, v, n:int):
    v = self.scaler.scale_embed(v=v, feat2w=self.feat2w)
    return self.index.get_nns_by_vector(v=v, n=n)

  def get_nns_by_name(self, name, n:int): return self.index.get_nns_by_name(name=name, n=n)

  def get_item_vector(self, name): return self.index.get_item_vector(name=name)

  # def v_from_id(self, _id, unscaled:bool=False) -> np.array:
  #   v = self.index.get_item_vector(_id)
  #   if unscaled: v = self.unscale(v)
  #   return v

  def scale(self, v): return self.scaler.scale_embed(v=v, feat2w=self.feat2w)

  def unscale(self, v): return self.scaler.unscale_embed(v=v, feat2w=self.feat2w)

  def populate_index(self, *, tcol=None, cursor=None):
    assert bool(tcol) != bool(cursor)
    if tcol:
      for tdoc in IterAll(tcol):
        try: self._add_tdoc(tdoc)
        except EmptyTagsError:
          self._skipped += 1
          continue
    elif cursor:
      for tdoc in cursor:
        try: self._add_tdoc(tdoc)
        except EmptyTagsError:
          self._skipped += 1
          continue

  def _add_tdoc(self, tdoc):
    # embed = self.embedder.tdoc2v(tdoc)
    # embed = self.scaler.scale_embed(v=embed, feat2w=self.feat2w)
    embed = self.scale(tdoc['embed'])
    self.index.add_one(name=str(tdoc[TSPACE_ID_FIELD]), v=embed)
