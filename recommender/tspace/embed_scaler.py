import numpy as np
from pymongo.collection import Collection

from recommender import *
from recommender.utils.math import VarianceScaler
from .track_embedder import TrackEmbedder


class EmbedScaler(VarianceScaler):
  def __init__(self, embedder:TrackEmbedder, feats2var:Dict[TrackFeaturesTag, Union[float, np.ndarray]]):
    self._embedder, self._feats2var = embedder, feats2var
    variances = self._embedder.feats2v(feats2var)
    super().__init__(variances=variances)

  def scale_embed(self, v, feat2w:Dict[TrackFeaturesTag, float]) -> np.ndarray:
    if feat2w:
      feats, ws = zip(*list(feat2w.items()))
      slices = [self._embedder.feats2slices[k] for k in feats]
      return super().scale(v=v, slices=slices, ws=ws)
    else:
      return v

  def unscale_embed(self, v, feat2w:Dict[TrackFeaturesTag, float]) -> np.ndarray:
    if feat2w:
      feats, ws = zip(*list(feat2w.items()))
      slices = [self._embedder.feats2slices[k] for k in feats]
      return super().unscale(v=v, slices=slices, ws=ws)
    else:
      return v

  @classmethod
  def from_col(cls,
               tcol:Collection,
               embedder:TrackEmbedder,
               precomputed:Dict[TrackFeaturesTag, Union[float, np.ndarray]]=None,
               save_freq:int=None,
               save_path:str=None):
    if save_freq: assert save_path, 'You must provide a save path if save_freq is not None'
    embeds = []
    for tdoc in db.IterAll(tcol):
      try: embeds.append(embedder.tdoc2v(tdoc))
      except errors.EmptyTagsError:
        continue
      if save_freq and len(embeds) % save_freq == 0:
        obj = cls.from_data(data=embeds, embedder=embedder, precomputed=precomputed)
        obj.save(path=save_path)
        # print(f'Saved EmbedScaler to {save_path}')
    return cls.from_data(data=embeds, embedder=embedder, precomputed=precomputed)

  @classmethod
  def from_data(cls,
                data:np.ndarray,
                embedder:TrackEmbedder,
                precomputed:Dict[TrackFeaturesTag, Union[float, np.ndarray]]=None,
                ):
    data = np.asarray(data)
    feats2var = embedder.v2feats(data.var(axis=0))
    feats2var.update(precomputed or {})
    return cls(embedder=embedder, feats2var=feats2var)

