from recommender import *


class TrackEmbedder(Serializable2):
  def __init__(self):
    super().__init__()
    # TODO: This configurations should be defined globally
    self.dim = 8
    self.feats2slices = {
      MusicTagType.genres: slice(0, 5),
      Features.valence: slice(5, 6),
      Features.loudness: slice(6, 7),
      Features.deepness: slice(7, 8),
    }

  def embed_genre(self, genres):
    genres = [g['name'].lower() for g in genres]
    embeds = self.genres2embeds(genres)
    if len(embeds) == 0:
      # print(f'Skipped: {genres}')
      raise EmptyTagsError
    if len(genres) != len(embeds):
      ignored = set(genres) - set(self._df_genres.index)
      # print(f'{ignored} is not in df_genres, ignored')
    return embeds.mean(axis=0)

  def tdoc2v(self, tdoc) -> np.ndarray:
    feats = self.tdoc2feats(tdoc=tdoc)
    return self.feats2v(feats)

  def tdoc2feats(self, tdoc) -> Dict[DBEnum, Union[float, np.ndarray]]:
    feats = {k: tdoc[k.db_field] for k in self.feats2slices}
    # feats = {TrackFeaturesTag[k]: v for k, v in tdoc['feats'].items()}
    feats[MusicTagType.genres] = self.embed_genre(genres=tdoc['genres'])
    return feats

  def v2feats(self, v:np.ndarray):
    if len(v) != self.dim: raise ValueError(f'v should have a len of {self.dim} but has {len(v)}')
    return {k: v[slc] for k, slc in self.feats2slices.items()}

  def feats2v(self, feats2v:Dict[DBEnum, Union[float, np.ndarray]]):
    if set(self.feats2slices) != set(feats2v):
      raise ValueError(f'feats2v should contain exactly this keys: {list(self.feats2slices.keys())}')
    res = np.zeros(self.dim)
    for tag, v in feats2v.items():
      slc = self.feats2slices[tag]
      res[slc] = v
    return res

  def genres2embeds(self, genres:List[str]) -> np.ndarray:
    # TODO: Use dict instead of reindex should be much faster
    genres = [s.lower() for s in genres]
    # If anything is dropped an error will be throw on genre_embed
    return np.array(self._df_genres.reindex(genres).dropna())

  @cachedproperty
  def _df_genres(self):
    # PATH = Path('/mnt/disks/dev/sketch/genre_similarity/from_tracks/data/50/tfmed/30_isomap_67')
    # PATH = Path('/mnt/disks/dev/sketch/genre_similarity/from_tracks/data/50/intersect_genres/tfmed/20_isomap_76')
    # PATH = Path('/mnt/disks/dev/sketch/genre_similarity/from_tracks/data/50/intersect_genres/tfmed_v1_2/additional_20_isomap_69')
    # PATH = Path('/mnt/disks/dev/sketch/genre_similarity/from_tracks/data/50/intersect_genres/tfmed_v2_0/20_isomap_70')
    # PATH = Path('/mnt/disks/dev/sketch/genre_similarity/from_tracks/data/50/intersect_genres/tfmed_v3_3/5_isomap_73')
    PATH = Path('/home/admin/embed_models')
    df_genres = pd.read_csv(PATH/'labels_embeds.tsv', header=None, sep='\t', index_col=0)
    return df_genres
