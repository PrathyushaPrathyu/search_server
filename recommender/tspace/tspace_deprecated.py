from pathlib import Path
from typing import *

from recommender import *
from recommender.errors import *
from .track_embedder import TrackEmbedder


class TSpace(Serializable2):
  def __init__(
      self,
      *,
      index:Index,
      embedder:TrackEmbedder,
      var_scaler:math.VarianceScaler,
      slices:List[slice],
      ws=List[float],
  ):
    super().__init__()
    self.index,self.embedder,self.var_scaler,self.slices,self.ws = index,embedder,var_scaler,slices,ws
    self._len, self._skipped = 0, 0

  def __len__(self): return self._len

  @property
  def skipped_ratio(self): return self._skipped / (len(self) + self._skipped)

  def v_from_id(self, id, unscaled:bool=False) -> np.array:
    v = self.index.get_item_vector(id)
    if unscaled: v = self.unscale(v)
    return v

  def unscale(self, v): return self.var_scaler.unscale(v, slices=self.slices, ws=self.ws)

  def change_from_ids(self, id1, id2):
    v1, v2 = self.index.get_item_vector(id1), self.index.get_item_vector(id2)
    return self.change_from_vectors(v1=v1, v2=v2)

  def change_from_vectors(self, v1, v2):
    v1, v2 = self.unscale(v1), self.unscale(v2)
    diffs = abs(v1 - v2) / self.var_scaler.variances
    changes = {k: diffs[slc].sum() for k, slc in self.embedder.slices}
    return changes

  def populate_index(self, tcol):
    for tdoc in db.IterAll(tcol):
      seq_id = tdoc['seq_id']
      try:
        comb_embed = self.embedder.embed(tdoc=tdoc)
        self._len += 1
      except errors.EmptyTagsError:
        self._skipped += 1
        continue
      comb_embed = self.var_scaler.scale(comb_embed, slices=self.slices, ws=self.ws)
      self.index.add_one(name=seq_id, v=comb_embed)


