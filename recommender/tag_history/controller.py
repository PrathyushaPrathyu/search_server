from typing import *
import mongo_manager as mm

class TagsHistoryController:
  def __init__(self, model:mm.TagsHistoryController):
    self.model = model

  def reset_session(self):
    return self.model.reset_session()

  def get_music_items(self) -> List[mm.TagHistoryItem]:
    return self.model.get_music_items()

  def get_music_top_recent(self, max_top:int, max_recent:int):
    return self.model.get_music_top_recent(max_top=max_top, max_recent=max_recent)

  def get_me_top_recent(self, max_top:int, max_recent:int):
    return self.model.get_me_top_recent(max_top=max_top, max_recent=max_recent)

  def get_recent(self, n=None):
    return self.model.get_recent(n=n)

  def get_favorites(self, n=None):
    return self.model.get_favorites(n=n)

  def register_tags(self, tags, increment:int=1):
    return self.model.register_tags(tags=tags, increment=increment)

  def save(self, user_id):
    return self.model.save()

  @classmethod
  def load(cls, user_id):
    model = mm.TagsHistoryController.load(user_id)
    return cls(model=model)

# from recommender import *
# from .history_item import TagHistoryItem
#
# class TagsHistoryController(Serializable2):
#   base_path = Path('/mnt/disks/dev/tmp/tag_history')
#
#   def __init__(self):
#     super().__init__()
#     self.tags_session = []
#     self.tags_history = {}
#
#   # TODO: Do we need to call reset on login?
#   def reset_session(self):
#     self.tags_session = []
#
#   def get_music_items(self) -> List[TagHistoryItem]:
#     return [t for t in self.tags_history.values() if isinstance(t.tag, MusicTag)]
#
#   def get_music_top_recent(self, max_top:int, max_recent:int):
#     items = self.get_music_items()
#     if items:
#       data = [{'score': t.score, 'last_used': t.last_used} for t in items]
#       df = pd.DataFrame(data)
#       df_top = df[df['score'] > 1].sort_values(['score', 'last_used'], ascending=[False, False])
#       top_idxs = list(df_top.index)[:max_top]
#       df_recent = df.drop(index=top_idxs).sort_values(['last_used'], ascending=False)
#       recent_idxs = list(df_recent.index)[:max_recent]
#     else: top_idxs, recent_idxs = [], []
#     return {
#       'favoritesMusicTags': [items[i].tag.to_json() for i in top_idxs],
#       'recentMusicTags': [items[i].tag.to_json() for i in recent_idxs],
#     }
#
#   def get_recent(self, n=None):
#     items = {'recentMeTags': [], 'recentMusicTags': []}
#     for o in sorted(self.tags_history.values(), key=lambda o: o.last_used, reverse=True)[:n]:
#       tag = o.tag
#       if isinstance(tag, MeTag): items['recentMeTags'].append(tag.to_json())
#       elif isinstance(tag, MusicTag): items['recentMusicTags'].append(tag.to_json())
#       else: raise ValueError(f'Tag type {type(tag)} not supported')
#     return items
#
#   def get_favorites(self, n=None):
#     items = {'favoritesMeTags': [], 'favoritesMusicTags': []}
#     for o in sorted(self.tags_history.values(), key=lambda o: o.score, reverse=True)[:n]:
#       tag = o.tag
#       if isinstance(tag, MeTag): items['favoritesMeTags'].append(tag.to_json())
#       elif isinstance(tag, MusicTag): items['favoritesMusicTags'].append(tag.to_json())
#       else: raise ValueError(f'Tag type {type(tag)} not supported')
#     return items
#
#   def register_tags(self, tags, increment:int=1):
#     new_tags = []
#     for tag in tags:
#       if tag in self.tags_session: continue
#       new_tags.append(tag)
#       self.tags_session.append(tag)
#     # Add new tags to history
#     for tag in new_tags:
#       try: item = self.tags_history[tag]
#       except KeyError:
#         item = TagHistoryItem(tag=tag)
#         self.tags_history[tag] = item
#       if isinstance(tag, MusicTag):
#         item.register_use(filter_state=tag.filter_state, increment=increment)
#       else:
#         item.register_use(filter_state=FilterState.seed, increment=increment)
#
#   def save(self, user_id):
#     return super().save(self.base_path/user_id)
#
#   @classmethod
#   def load(cls, user_id):
#     try:
#       return super(TagsHistoryController, cls).from_file(cls.base_path/user_id)
#     except FileNotFoundError:
#       return cls()
