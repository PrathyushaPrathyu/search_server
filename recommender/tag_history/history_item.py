from datetime import datetime
from recommender import *

class TagHistoryItem:
  def __init__(self, tag):
    self.tag = tag
    self.count = {k.name: 0 for k in FilterState}
    self.last_used = datetime.utcnow()

  @property
  def score(self):
    return self.count['seed'] + self.count['include']

  def register_use(self, filter_state, increment:int=1):
    self.count[filter_state] += increment
    self.last_used = datetime.utcnow()

  def to_json(self):
    return self.tag.to_json()

  def __repr__(self):
    return f'<{self.__class__.__name__}: (name: {self.tag.name})>'