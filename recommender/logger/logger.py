import logging
from collections import defaultdict

logging.basicConfig()
logger = logging.getLogger('recommender')
logger.setLevel(logging.INFO)


class LoggerTags: pass

class LishashLogger:
    def __init__(self, include=None, exclude=None):
        self.include, self.exclude = include or [], exclude or []

    def log(self, msg, tag=None):
        if tag is not None:
            if self.exclude and tag in self.exclude: return
            if self.include and tag not in self.include: return
        print(msg)

LTAGS = LoggerTags()
LTAGS.context = 'Context'
LTAGS.search_while_playing = 'search_while_playing'
LTAGS.tag_filters = 'tag_filtres'
lishash_logger = LishashLogger()