from .primary_playback_pipeline import *
from .secondary_playback_pipeline import *

class FavoriteOrganicPlayback(OrganicPlayback, FavoritePlaybackPipeline): pass
class FavoriteWildPlayback(WildPlayback, FavoritePlaybackPipeline): pass

class MagicalOrganicPlayback(OrganicPlayback, MagicalPlaybackPipeline): pass
class MagicalWildPlayback(WildPlayback, MagicalPlaybackPipeline): pass

class DiscoverOrganicPlayback(OrganicPlayback, DiscoverPlaybackPipeline): pass
class DiscoverWildPlayback(WildPlayback, DiscoverPlaybackPipeline): pass
