import neo_db as nd
from typing import *

from recommender import *
from recommender.tag import MeTag, MusicTag
from recommender.feature_importance import FeatureImportance
from recommender.controller import MusicTagController, ContextController, UserFeaturesController
from recommender.negative_interest import NegativeInterestController
from recommender.enums import *
from .concrete_playback_pipeline import *
from recommender.friends_recom import FriendsRecom


def get_playback(
    *,
    primary_mode:PrimaryMode,
    secondary_mode:SecondaryMode,
    feat_importance:FeatureImportance,
    music_tag_control:MusicTagController,
    user_feats_control:UserFeaturesController,
    context_control:ContextController,
    negative_interest_control:NegativeInterestController,
    friends_recom:FriendsRecom,
    shared_tracks_manager:nd.SharedTracksManager,
    search_filters:SearchFilters,
    instructions_recorder: InstructionsRecorder,
):
  _mode2playback_fn = {
    PrimaryMode.Favorites: {
      SecondaryMode.Wild: FavoriteWildPlayback,
      SecondaryMode.Organic: FavoriteOrganicPlayback,
    },
    PrimaryMode.Magic: {
      SecondaryMode.Wild: MagicalWildPlayback,
      SecondaryMode.Organic: MagicalOrganicPlayback,
    },
    PrimaryMode.Discover: {
      SecondaryMode.Wild: DiscoverWildPlayback,
      SecondaryMode.Organic: DiscoverOrganicPlayback,
    }
  }

  playback_fn = _mode2playback_fn[primary_mode][secondary_mode]
  return playback_fn(
    feat_importance=feat_importance,
    music_tag_control=music_tag_control,
    context_control=context_control,
    user_feats_control=user_feats_control,
    negative_interest_control=negative_interest_control,
    friends_recom=friends_recom,
    shared_tracks_manager=shared_tracks_manager,
    search_filters=search_filters,
    instructions_recorder=instructions_recorder,
  )

def get_playback_controller(*, user_id:str, me_tags:List[MeTag], music_tags:List[MusicTag]):
  from .playback_controller import PlaybackController
  music_tag_control = MusicTagController(tags=music_tags)
  context_control = ContextController.from_tags(user_id=user_id, tags=me_tags)
  return PlaybackController(user_id=user_id, music_tag_control=music_tag_control, context_control=context_control)
