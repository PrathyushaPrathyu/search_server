import logging
import numpy as np
import neo_db as nd

from recommender import *
from recommender.controller import MusicTagController, ContextController, UserFeaturesController
from recommender.feature_importance import FeatureImportance
from recommender import db, factory, SeedTrackNotAvailable
from recommender.tag import MusicTag
from recommender.negative_interest import NegativeInterestController
from recommender.friends_recom import FriendsRecom


class Playback:
  def __init__(
      self,
      feat_importance:FeatureImportance,
      music_tag_control:MusicTagController,
      context_control:ContextController,
      user_feats_control:UserFeaturesController,
      negative_interest_control:NegativeInterestController,
      friends_recom:FriendsRecom,
      shared_tracks_manager:nd.SharedTracksManager,
      search_filters:SearchFilters,
      instructions_recorder:InstructionsRecorder,
  ):
    self.feat_importance = feat_importance
    self.music_tag_control = music_tag_control
    self.context_control = context_control
    self.user_feats_control = user_feats_control
    self.negative_interest_control = negative_interest_control
    self.friends_recom = friends_recom
    self.shared_tracks_manager = shared_tracks_manager
    self.search_filters = search_filters
    self.instructions_recorder = instructions_recorder

  # @property
  # def seed_tdoc(self):
  #   seed_track = self.context_control.seed_track
  #   if seed_track is None: raise SeedTrackNotAvailable
  #   tdoc = db.get_tdoc_from_tspace_key(seed_track._id)
  #   logging.debug('Seed track: %s', tdoc['name'])
  #   return tdoc

  def get_track_nn(self, cursor, v=None) -> MusicTag:
    tspace = factory.get_tspace3(feat_importance=self.feat_importance, cursor=cursor)
    ids, dists = tspace.get_nns_by_vector(v=v, n=10)
    ## TODO: Weighted sampling
    track_id = np.random.choice(ids)
    return MusicTag.from_tdoc_id(track_id=track_id)

  # if track_id: seed_embed = db.get_tdoc_from_tspace_key(track_id)['embed']
    # else: seed_embed = self.seed_tdoc['embed']
    # tspace = factory.get_tspace3(feat_importance=self.feat_importance, cursor=cursor)
    # ids, dists = tspace.get_nns_by_vector(v=seed_embed, n=10)
    # ## TODO: Weighted sampling
    # track_id = np.random.choice(ids)
    # return MusicTag.from_tdoc_id(track_id=track_id)

  def next_track(self, track_id:str, seeding:bool=False): raise NotImplementedError