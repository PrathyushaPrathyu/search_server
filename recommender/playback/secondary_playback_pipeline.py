import neo_db as nd
import mongo_manager as mm
import numpy as np
from recommender import *
from recommender import factory
from recommender.tag import MusicTag
from .playback_pipeline import PlaybackPipeline


# class OrganicPlayback(PlaybackPipeline):
#   def next_track(self, v:str=None, seeding:bool=False):
#     pipeline = self.get_pipeline(seeding=seeding)
#     return self.get_track_nn(cursor=pipeline.run(), v=v)

# With the list of friends recommendations, decide if we should sample a nn or
# play a friend recommendation

class WildPlayback(PlaybackPipeline):
  def next_track(self, v: str, seeding: bool = False):
    while True:
      tracks_ids = list(self.get_pipeline(seeding=seeding).run(projection=['_id']))
      # logger.debug(f'Available after interest control: {len(tracks_ids)}')
      if tracks_ids: break
    # TODO: Weighted sampling
    track_id = np.random.choice(tracks_ids)['_id']
    return MusicTag.from_tdoc_id(track_id=track_id)


class OrganicPlayback(PlaybackPipeline):
  def _get_shared_tracks_pipeline_stage(self):
    shared_tracks = self.shared_tracks_manager.get_received_shared_tracks()
    shared_tracks_ids = [ObjectId(o['track_mongo_id']) for o in shared_tracks]
    return {'$match': {'_id': {'$in': shared_tracks_ids}}}

  # TODO: Friends recom should never be None
  def next_track(self, v: str = None, seeding: bool = False):
    # DEADLINE
    while True:
      # logger.debug('Building pipeline for next_tracks...')
      pipeline = self.get_pipeline(seeding=seeding)
      # logger.debug(f'Pipeline: {pipeline}')
      tspace = factory.get_tspace3(feat_importance=self.feat_importance, cursor=pipeline.run())
      ids, dists = tspace.get_nns_by_vector(v=v, n=10)
      # logger.debug(f'Available after interest control: {len(ids)}')
      if ids: break
    ids2, dists2 = ids, dists
    names = []
    for id in ids2:
      tdoc = mm.Track.from_id(id=id).to_dict()
      names.append(tdoc['name'])

    # Get the nns until we have a spike in the distance
    min_dist = dists[(dists != 0).argmax()]
    last_i = np.where(dists <= 5 * min_dist)[0][-1] + 1
    ids, dists = ids[:last_i], dists[:last_i]

    # Check for friends recommendations
    # pipeline.add_stages(self.friends_recom.get_pipeline_stages())
    pipeline.add_stage(self._get_shared_tracks_pipeline_stage())
    friends_dists = {tspace.get_distance_by_vector(v1=v, v2=o['embed']): o for o in pipeline.run()}
    if friends_dists:  # If there is songs to recommend after filtering
      friends_min_dist, friend_tdoc = min(friends_dists.items())
      # logger.debug('NN dist: %s, Friends dist: %s', dists[-1], friends_min_dist)
      if friends_min_dist < 1.22:  # 1.22 is the maximum threshold allowed
        friends_prob = max(0.1, dists[-1]) / max(friends_min_dist, 1e-6)  # Probability of choosing friend tdoc
        if friends_min_dist < dists[-1] or friends_prob > np.random.uniform():
          # TODO: Mark this guy as a friend recom
          # logger.debug('Sampled a friends track: %s', friend_tdoc['name'])
          neo_track = nd.Track.from_mongodb_doc(friend_tdoc)
          shared_by = self.shared_tracks_manager.track_shared_by(track=neo_track)
          self.shared_tracks_manager.mark_as_listened(track=neo_track)
          # self.friends_recom.remove(str(friend_tdoc['_id']))
          return MusicTag.from_tdoc(friend_tdoc, recommended_by=', '.join(shared_by))

    track_id = np.random.choice(ids)
    return MusicTag.from_tdoc_id(track_id=track_id)
