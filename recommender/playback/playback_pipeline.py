from recommender import *

from .base_playback import Playback

class PlaybackPipeline(Playback):
  @property
  def pipe_control(self):
    return PipelineController(
      context_control=self.context_control,
      feats_control=self.user_feats_control,
      music_tag_control=self.music_tag_control,
      search_filters=self.search_filters,
      instructions_recorder=self.instructions_recorder,
    )

  def get_pipeline(self, seeding:bool): raise NotImplementedError

  def get_starting_pipeline(self):
    pipeline = db.Pipeline()
    pipeline.add_stages(self.music_tag_control.get_search_pipeline_stages())
    return pipeline
