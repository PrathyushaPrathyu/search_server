from .playback_pipeline import PlaybackPipeline

from recommender.controller import PipelineController
from recommender import db
from recommender.enums import *


class FavoritePlaybackPipeline(PlaybackPipeline):
  def get_pipeline(self, seeding:bool):
    pipeline = self.get_starting_pipeline()
    items_control = self.context_control.get_interest_items(InterestTag.favorites)
    pipeline.add_stages(items_control.get_pipeline_stages())
    pipeline = self.pipe_control.get_pipeline(seeding=seeding, pipeline=pipeline)
    # OPTIMIZE: This is the only pipeline step that needs the while loop, above only needs to be figured out once
    pipeline.add_stages(self.negative_interest_control.get_pipeline_stages())
    return pipeline

class MagicalPlaybackPipeline(PlaybackPipeline):
  def get_pipeline(self, seeding:bool):
    pipeline = self.get_starting_pipeline()
    pipeline = self.pipe_control.get_pipeline(seeding=seeding, pipeline=pipeline)
    # OPTIMIZE: This is the only pipeline step that needs the while loop, above only needs to be figured out once
    pipeline.add_stages(self.negative_interest_control.get_pipeline_stages())
    return pipeline

class DiscoverPlaybackPipeline(PlaybackPipeline):
  def get_pipeline(self, seeding:bool):
    pipeline = self.get_starting_pipeline()
    pipeline.add_stage(self.context_control.get_new_tracks_pipeline_stage())
    pipeline = self.pipe_control.get_pipeline(seeding=seeding, pipeline=pipeline)
    # OPTIMIZE: This is the only pipeline step that needs the while loop, above only needs to be figured out once
    pipeline.add_stages(self.negative_interest_control.get_pipeline_stages())
    return pipeline
