import db_manager as dm
import neo_db as nd
import mongo_manager as mm
from recommender import *
from recommender.controller.context_controller import ContextController
from recommender.controller.music_tag_controller import MusicTagController
from recommender.controller.user_features_controller import UserFeaturesController
from recommender.feature_importance import FeatureImportanceController
from recommender.feature_preference import FeaturePreferenceController
from recommender.seed_track import SeedTrackController
from .factory import get_playback


class PlaybackController:
  def __init__(
      self,
      *,
      user: dm.UserManager,
      user_id: str,
      music_tag_control: MusicTagController,
      context_control: ContextController,
      user_feats_control: UserFeaturesController,
      feat_preference_control: FeaturePreferenceController,
      feat_importance_control: FeatureImportanceController,
      seed_track_control: SeedTrackController,
      negative_interest_control: InterestControllerInterface,
      shared_tracks_manager:nd.SharedTracksManager,
      context_manager:nd.ContextManager,
      search_filters:SearchFilters,
      instructions_recorder:InstructionsRecorder=None,
  ):
    self.user = user
    self.user_id = user_id
    self.music_tag_control = music_tag_control
    self.context_control = context_control
    self.user_feats_control = user_feats_control
    self.feat_importance_control = feat_importance_control
    self.feat_preference_control = feat_preference_control
    self.seed_track_control = seed_track_control
    self.negative_interest_control = negative_interest_control
    self.friends_recom = friends_recom
    self.shared_tracks_manager = shared_tracks_manager
    self.embedder = TrackEmbedder()
    self.context_manager = context_manager
    self.search_filters = search_filters
    self.instructions_recorder = instructions_recorder or InstructionsRecorder()

  def reset_session(self):
    self.context_control.reset_session()
    self.seed_track_control.reset_session()
    # self.feat_importance = cache.get_feat_importance(key=self.user_id, reset=True)

  @log_time
  def register_action(self, action: Action, track_id: str, simulating:bool=False):
    tdoc = mm.Track.from_id(id=track_id).to_dict()
    seed_tdoc = self.seed_track_control.seed_tdoc
    self.context_control.register_action(action=action, track_id=track_id)

    #TODO: BUG this might be an error? Not using seed track
    self.feat_importance_control.register_action(action=action, track_v=tdoc['embed'], name=tdoc['name'])
    # self.feat_preference_control.register_action(action=action, tdoc=tdoc)

    #TODO: BUG Are we checking for which had higher reward? Currently, we are chaning the seed track each song
    self.seed_track_control.register_action(action=action,
                                            context_control=self.context_control,
                                            feat_preference_control=self.feat_preference_control,
                                            embedder=self.embedder)

    self.negative_interest_control.register_action(action=action, seed_tdoc=seed_tdoc, tdoc=tdoc)
    ### New for neo4j
    if not simulating:
      track = nd.Track.from_mongodb_doc(tdoc)
      reward = reward_from_action(action)
      self.context_manager.register_action(track=track, action=action.name, reward=reward)



  @log_time
  def next_track(
      self,
      primary_mode: PrimaryMode,
      secondary_mode: SecondaryMode,
      track_id: str = None,
      seeding: bool = False,
      new_seed_track_id: str = None
  ):
    if new_seed_track_id: self.context_control.set_seed_track(track_id=new_seed_track_id)
    playback = get_playback(
      primary_mode=primary_mode,
      secondary_mode=secondary_mode,
      feat_importance=self.feat_importance_control,
      music_tag_control=self.music_tag_control,
      context_control=self.context_control,
      user_feats_control=self.user_feats_control,
      negative_interest_control=self.negative_interest_control,
      friends_recom=self.friends_recom,
      shared_tracks_manager=self.shared_tracks_manager,
      search_filters=self.search_filters,
      instructions_recorder=self.instructions_recorder,
    )
    try:
      seed_v = self.seed_track_control.get_seed_v()
      return playback.next_track(v=seed_v, seeding=seeding)
    except SeedTrackNotAvailable:
      logging.debug('Seed track not available, sampling new track..')
      return self.sample_track(seeding=True, consider_nn=True, simulating=False)

  def _get_music_tag(self, track_id) -> MusicTag:
    tdoc = mm.Track.from_id(id=track_id).to_dict()
    neo_track = nd.Track.from_mongodb_doc(tdoc)
    shared_by = self.shared_tracks_manager.track_shared_by(track=neo_track)
    self.shared_tracks_manager.mark_as_listened(track=neo_track)
    return MusicTag.from_tdoc(tdoc, recommended_by=', '.join(shared_by) or None)

  # seeding = Ture is deprecated, seeding is deprecated in general
  def sample_track(self, seeding: bool = True, consider_nn: bool = True, simulating: bool = False) -> MusicTag:
    searched_tracks_ids = self.music_tag_control.get_searched_tracks_ids(seeding=seeding)
    if searched_tracks_ids:
      # This too is not allowed not most probably on the ui
      track_id = np.random.choice(searched_tracks_ids)
      if not simulating: self.register_action(action=Search(tag=None), track_id=track_id)
      # Get users that shared this track (if any)
      return self._get_music_tag(track_id=track_id)
    track_id = self.sample_track_id(seeding=seeding)
    tdoc = mm.Track.from_id(id=track_id).to_dict()
    feats = self.embedder.tdoc2feats(tdoc)
    seed_v = self.embedder.feats2v(feats)
    if consider_nn:
      playback = get_playback(
        primary_mode=PrimaryMode.Magic,
        secondary_mode=SecondaryMode.Organic,
        feat_importance=self.feat_importance_control,
        music_tag_control=self.music_tag_control,
        context_control=self.context_control,
        user_feats_control=self.user_feats_control,
        negative_interest_control=self.negative_interest_control,
        friends_recom=self.friends_recom,
        shared_tracks_manager=self.shared_tracks_manager,
        search_filters=self.search_filters,
        instructions_recorder=self.instructions_recorder,
      )
      return playback.next_track(v=seed_v, seeding=seeding)
    return self._get_music_tag(track_id=track_id)

  def sample_track_id(self, seeding=True):
    interest_order = self.sample_interest_order()
    for interest in interest_order:
      logging.debug('Trying interest %s', interest.name)
      item_control = self.context_control.get_interest_items(interest=interest)
      if len(item_control) == 0: continue  # If there's no song in this interest, skip
      pipeline = db.Pipeline()
      pipeline.add_stages(item_control.get_pipeline_stages())
      pipeline.add_stages(self.negative_interest_control.get_pipeline_stages())
      pipeline = self.get_pipeline(starting_pipeline=pipeline, seeding=seeding)
      tracks = list(pipeline.run(projection=['_id']))
      if len(tracks) > 0: #If the applied filters still resolve to a track in sst let's say
        track_ids = [str(o['_id']) for o in tracks]
        item_control = self.context_control.get_history(interest=interest).get_items(ids=track_ids)
        track_ids, ws = zip(*[(o.id, o.score) for o in item_control.items])
        ws = np.array(ws) / np.sum(ws)
        if any(np.isnan(ws)): continue
        break
    else:  # If everything fails, consider all collection
      logging.debug('All interests are exausted, using full collection')
      pipeline = self.get_pipeline(seeding=seeding)
      tracks = list(pipeline.run(projection=['_id']))
      track_ids = [str(o['_id']) for o in tracks]
      ws = None
    if len(track_ids) == 0: raise NoTracksAvailable
    return np.random.choice(track_ids, p=ws)

  def get_pipeline(self, seeding: bool, starting_pipeline: db.Pipeline = None) -> db.Pipeline:
    for inner_match in ['$and', '$or']:
      pipeline = starting_pipeline.copy() if starting_pipeline else db.Pipeline()
      pipeline.add_stage(self.context_control.get_played_tracks_pipeline_stage())
      pipeline.add_stages(self.user_feats_control.get_pipeline_stages())
      pipeline.add_stages(self.music_tag_control.get_pipeline_stages(seeding=seeding, inner_match=inner_match))
      # OPTIMIZE: Running filters only to get document count
      n_tracks = len(list(pipeline.run(projection=['_id'])))
      # logger.debug(f'Start | AvailableTracks({inner_match}): {n_tracks}')
      if n_tracks > 0: break
    return pipeline

  def sample_interest_order(self) -> List[InterestTag]:
    #Returns a permutation of the interests in a single list of shape 5*1
    return list(np.random.choice(
      [InterestTag.sst, InterestTag.gst, InterestTag.slt, InterestTag.glt, InterestTag.favorites],
      p=[0.35, 0.25, 0.15, 0.1, 0.15],
      size=5,
      replace=False,
    ))

  def save(self):
    self.context_control.save(user_id=self.user_id) # Done
    # self.feat_preference_control.save() # Deprecated
    self.feat_importance_control.save() # Done Features
    self.seed_track_control.save(user_id=self.user_id) # Done SeedTrack
    self.negative_interest_control.save() # Done Positive and Negative Interest
    # print("#####################SAVE#####################")
    # if(self.seed_track_control.seed_tdoc):
    #   print("SEED TRACK : {}".format(self.seed_track_control.seed_tdoc['name_lower']))
    # else:
    #   print("No Seed Track Set Yet.")
    # print("#############################")
    # print('')
    # print("POSITIVE INTEREST : ")
    # if(len(self.negative_interest_control.positive_locals) > 0):
    #   print('LOCAL: %s', [(k.name, v.value) for k, v in self.negative_interest_control.positive_locals[0].model.tag2sched.items()])
    # print('')
    # print('GLOBAL: %s', [(k.name, v.value) for k, v in self.negative_interest_control.positive_global.model.tag2sched.items()])
    # print("#############################")
    # print('')
    # print("NEGATIVE INTEREST : ")
    # if (len(self.negative_interest_control.locals_) > 0):
    #   print('LOCAL: %s', [(k.name, v.value) for k, v in self.negative_interest_control.locals_[0].model.tag2sched.items()])
    # print('')
    # print('GLOBAL: %s', [(k.name, v.value) for k, v in self.negative_interest_control.global_.model.tag2sched.items()])
    # print("#############################")
    # print('')
    # import pdb; pdb.set_trace()
    # self.friends_recom.save(user=self.user)

  @classmethod
  def from_body(cls, user_id: str, body: dict):
    # user = UserModel.from_token(token=user_id)
    user_manager = dm.UserManager.from_id(user_id=user_id)
    # neo_user = nd.User.from_name(user.username)
    me_tags = MeTag.from_list_json(body['meTags'])
    # HACK: Global context
    context = nd.Context(me_tags[0].name) if me_tags else nd.Context('#<-GLOBAL->#')
    group_users = body['groupUsers']
    # Controllers
    music_tag_control = MusicTagController.from_list_json(body['musicTags'])
    if('searchedTag' in body): music_tag_control.register_searched_tag(MusicTag.from_json(body['searchedTag']))
    search_filters = SearchFilters.from_list_json(body['searchFilters'])
    context_control = ContextController.from_list_json(user_id=user_id, list_json=body['meTags'])
    user_feats_control = UserFeaturesController.from_dict_json(body['userFeatures'])
    feat_preference_control = FeaturePreferenceController.load(user_id=user_id, contexts=context_control.contexts)
    #TODO: If the searched tag has very differnent features, change current importance
    feat_importance_control = FeatureImportanceController.load(user_id=user_id, contexts=context_control.contexts)
    #TODO: Make the searchedTrack the seedTrack, IF a good action happens on it
    seed_track_controller = SeedTrackController.load(user_id=user_id)
    # friends_recom = FriendsRecom.from_user(user=user)
    if group_users: negative_interest_control = GroupInterestController.from_users(users_ids=[user_id]+group_users)
    #TODO: If the searched tag is in negativeInterests, change the negative interests
    #TODO: All the tags of the searched track, should be put in positive interest
    else: negative_interest_control = NegativeInterestController.load(user_id=user_id, contexts=context_control.contexts)
    # TODO: If other in group session ignores context
    # Neo_db stuff
    shared_tracks_manager = nd.SharedTracksManager(user=user_manager.neo_user)
    context_manager = nd.ContextManager(user=user_manager.neo_user, context=context)
    return cls(
      user=user_manager,
      user_id=user_id,
      music_tag_control=music_tag_control,
      context_control=context_control,
      user_feats_control=user_feats_control,
      feat_preference_control=feat_preference_control,
      feat_importance_control=feat_importance_control,
      seed_track_control=seed_track_controller,
      negative_interest_control=negative_interest_control,
      shared_tracks_manager=shared_tracks_manager,
      context_manager=context_manager,
      search_filters=search_filters
    )

  @classmethod
  def from_no_context(cls, user_id: str):
    # user = UserModel.from_token(token=user_id)
    user_manager = dm.UserManager.from_id(user_id=user_id)
    # neo_user = nd.User.from_name(user.username)
    music_tag_control = MusicTagController(tags=[])
    search_filters = SearchFilters(tags=[])
    context_control = ContextController.from_tags(user_id=user_id, tags=[])
    user_feats_control = UserFeaturesController(user_features=[])
    feat_preference_control = FeaturePreferenceController.load(user_id=user_id, contexts=context_control.contexts)
    feat_importance_control = FeatureImportanceController.load(user_id=user_id, contexts=context_control.contexts)
    seed_track_controller = SeedTrackController.load(user_id=user_id)
    negative_interest_control = NegativeInterestController.load(user_id=user_id, contexts=context_control.contexts)
    # friends_recom = FriendsRecom.from_user(user=user)
    shared_tracks_manager = nd.SharedTracksManager(user=user_manager.neo_user)
    # HACK: Global context
    context_manager = nd.ContextManager(user=user_manager.neo_user, context=nd.Context('#<-GLOBAL->#'))
    return cls(
      user=user_manager,
      user_id=user_id,
      music_tag_control=music_tag_control,
      context_control=context_control,
      user_feats_control=user_feats_control,
      feat_preference_control=feat_preference_control,
      feat_importance_control=feat_importance_control,
      seed_track_control=seed_track_controller,
      negative_interest_control=negative_interest_control,
      shared_tracks_manager=shared_tracks_manager,
      context_manager=context_manager,
      search_filters=search_filters,
    )
