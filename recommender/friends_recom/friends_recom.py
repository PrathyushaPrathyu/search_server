from recommender import *


class FriendsRecom:
  def __init__(self, tracks):
    self.track_ids = tracks
    # self.track_ids = [
    #   '84f8024a8399c6e9c3eb26db',
    #   '5a3f28fa2d4e813e2bff6d54',
    #   '19df16b6e1ea454c955ffe8c',
    #   '394150620ac54d9eb084a1a6',
    #   '70e2594fcd279a7e81bac63f',
    # ]

  def add(self, track_id):
    self.track_ids.append(track_id)

  def remove(self, track_id):
    self.track_ids.remove(track_id)

  def get_pipeline_stages(self):
    return [{'$match': {'_id': {'$in': [ObjectId(o) for o in self.track_ids]}}}]

  def save(self, user: UserModel):
    return user.register_update(db.DBOperation({'$set': {UserKeys.friends_recom: self.track_ids}}))

  @classmethod
  def from_user(cls, user: UserModel):
    return cls(tracks=user.friends_recom)
