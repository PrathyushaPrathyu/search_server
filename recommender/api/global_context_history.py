import mongo_manager as mm


def top_contexts():
  pipeline = [
    {"$match": {"context.name": {"$ne": 'UNSPECIFIED'}}},
    {"$sortByCount": "$context"},
  ]

  res = [{'name': hdoc['_id']['name'], 'type': hdoc['_id']['type']} for hdoc in mm.History.objects.aggregate(*pipeline)]

  return {'items': res}
