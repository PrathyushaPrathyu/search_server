from recommender import api
import mongo_manager as mm
from pprint import pprint
import json

import re
import requests


def search_isrc_with_applemusic(term):
    URL = "https://api.music.apple.com/v1/catalog/in/songs?filter[isrc]=" + term
    #     print(URL)
    token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IjNIRzk3UUs4WDQifQ.eyJpc3MiOiIyOTU1U0o3UDJSIiwiaWF0IjoxNTg2NDIxNTU4LCJleHAiOjE2MDIxODk1NTh9.QciZoAPw_pf5vB2Kk6GvkLgtYJVuM2xYCbqmHYnGP6sE79qO3zB183v_c8uJb4lWCiIqFYfJOYuh2FULPWi_Yg'
    hed = {'Authorization': 'Bearer ' + token}
    PARAMS = {'filter[isrc]': term}
    res = json.loads(requests.get(url=URL, params=PARAMS, headers=hed).text)
    return res


def get_id_from_am_api(isrc):
    cover_size = str(600)
    seq_id_pipeline = [
        {"$sort": {"seq_id": -1}},
        {"$limit": 1},
        {"$project": {"seq_id": 1, "_id": 0}}]
    seq_id = list(mm.Track.objects().aggregate(*seq_id_pipeline))[0]['seq_id']
    seq_id += 1

    playing_track_apple_id = isrc
    res = search_isrc_with_applemusic(term=playing_track_apple_id)
    if res['data']:

        for item in res['data']:
            artists_AM = item['attributes']['artistName']
            artists_AM = re.split(", | & ", artists_AM)

            Languages_list = ['Hindi', 'English', 'Telugu', 'Punjabi', 'Tamil', 'Gujarati', 'Kannada', 'Urdu',
                              'Malayalam', 'Bengali', 'Odia', 'Marathi', 'Spanish', 'French']
            genres_AM = item['attributes']['genreNames']

            if 'Indian' in genres_AM:
                genres_AM.remove('Indian')
            if 'Music' in genres_AM:
                genres_AM.remove('Music')
            genn = []
            for g in genres_AM:
                genn.append(re.split("/", g))
            genres_AM = sum(genn, [])

            languages = []

            for i in Languages_list:
                if i in genres_AM:
                    languages.append(i)
                    genres_AM.remove(i)

            gen_list = []
            art_list = []
            lan_list = []
            i = 0
            for each_art in item['relationships']['artists']['data']:
                art_list.append({'name': artists_AM[i].lower(), 'id': each_art['id']})
                i += 1
            for each_gen in genres_AM:
                gen_list.append({'name': each_gen.lower(), 'score': -1})

            for each_lan in languages:
                lan_list.append({'name': each_lan.lower(), 'score': -1})

            name = item['attributes']['name']
            name_lower = item['attributes']['name'].lower()
            album = item['attributes']['albumName'].lower()
            album = {'id': -1, 'name': album}

            duration_ms = item['attributes']['durationInMillis']
            cover_image_url = item['attributes']['artwork']['url'].replace("{w}", cover_size).replace("{h}", cover_size)
            if "releaseDate" in item['attributes']:
                release_date = item['attributes']['releaseDate']
                release_year = release_date.split("-")[0]
            else:
                release_date = None
                release_year = None

            applemusic_uri = item['id']
            isrc = item['attributes']['isrc']

            for each_url in item['attributes']['previews']:
                preview_url = each_url['url']

        valence = 3
        loudness = 5
        deepness = 2
        speechiness = 0
        embed = [0.0, 0.0, 0.0, 0.0, 0.0]
        spotify_uri, popularity = api.get_spotify_info_using_isrc(term=isrc)

        new_track_ses_doc = mm.Track(uri=spotify_uri,
                                     name=name,
                                     name_lower=name_lower,
                                     popularity=popularity,
                                     valence=valence,
                                     loudness=loudness,
                                     deepness=deepness,
                                     embed=embed,
                                     artists=art_list,
                                     genres=gen_list,
                                     languages=lan_list,
                                     about=[],
                                     instruments=[],
                                     speechiness=speechiness,
                                     seq_id=seq_id,
                                     album=album,
                                     duration_ms=duration_ms,
                                     release_date=release_date,
                                     preview_url=preview_url,
                                     cover_image_url=cover_image_url,
                                     art_gens=[],
                                     release_year=release_year,
                                     applemusic_uri=applemusic_uri,
                                     isrc=isrc)
        new_track_ses_doc.save()

    return str(new_track_ses_doc.id), name_lower, popularity, gen_list,cover_image_url