import neo_db as nd


def search_user(token: str, query: str):
  users = nd.search_user(query=query)
  return [o.to_dict(simplified=True) for o in users]

def get_users_with_query_openbeta(token:str,query:str):
  user = nd.User.from_mongo_id(mongo_id=token)
  friends = user.get_users_with_query_openbeta(query=query)
  return {'users': [o.to_dict(simplified=True) for o in friends]}

