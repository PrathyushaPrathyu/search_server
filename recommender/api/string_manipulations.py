import re

def make_ngrams(word, min_size=3, edge_ngram=False):
    # concatenate the edge ngrams again into a string
    # first split all words
    word = re.sub(r" ?\([^)]+\)", " ", word)
    word = re.sub(r'\[[^()]*\]', ' ', word)
    word = word.replace('"', ' ')
    word = word.replace(",", ' ')
    word = word.replace(":", " ")
    word = word.replace("!", " ")
    word = word.replace("-", " ")
    word = word.replace("'", '')

    word_list = word.split(" ")

    ngrams_set = set()
    # for each word calculate the edge ngrams
    for word in word_list:
        length = len(word)
        size_range = range(min_size, max(length, min_size) + 1)
        for size in size_range:
            for i in range(0, max(0, length - size) + 1):
                if edge_ngram:
                    ngrams_set.add(word[0:i + size])
                else:
                    ngrams_set.add(word[i:i + size])
                    # if edge

    return ngrams_set