from typing import *

from recommender import *
from .db_ops import *
from .utils import *


# def register_context(token:str, context:str, info:dict) -> dict:
#   writer = db.UserDBWriter(_id=token)
#   udoc = db.get_udoc(token=token)
#   is_new = context not in db.get_past_contexts(udoc)
#   writer.register_update(register_context_op(context=context))
#   writer.write()
#   return {'isNew': is_new}

# def previous_contexts(token: str, info:dict) -> List[str]:
#   udoc = db.get_udoc(token=token)
#   return db.get_past_contexts(udoc)

# def start_from_context(token:str, info:dict) -> dict:
#   track = get_next_track(token=token, last_track_id=None)
#   return track.to_dict()
