from recommender import *
from .get_track_metadata import get_track_metadata
from .track2 import Track2
import mongo_manager as mm
from bson import ObjectId
from recommender import api_alpha


def search_while_playing(token:str, track_id:str, love_count:float, play_ratio:float, body:dict):
  try:
    searched_tag = MusicTag.from_json(body['searchedTag'])
    session_id = body['sessionId']
    alpha_playback = api_alpha.AlphaPlayback.from_body(user_id=token, body=body, track_id=track_id, session_id=session_id)
    track_id, instructions, shared_by = alpha_playback.search(searched_tag=searched_tag, love_count=love_count, play_ratio=play_ratio)
    track_music_tag = MusicTag.from_tdoc_id(track_id=track_id)

    track = Track2.from_music_tag(token=token, music_tag=track_music_tag, shared_by=shared_by)
    res = {
      'track': track.to_dict(),
      'success': True,
      'instructions': instructions,
      'sessionId': session_id
    }
    return res

  except NoTracksAvailable:
    return {'success': False}

