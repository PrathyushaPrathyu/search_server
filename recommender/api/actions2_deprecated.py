from recommender import *
from recommender.db import *
from .db_ops import *

def register_right_swipe2(*, token:str, track_id:str, love_count:float, play_ratio:float, info:dict) -> None:
  _register_action(action=ActionTag.right, token=token, track_id=track_id, love_count=love_count, play_ratio=play_ratio)

def register_left_swipe2(*, token:str, track_id:str, love_count:float, play_ratio:float, info:dict) -> None:
  _register_action(action=ActionTag.left, token=token, track_id=track_id, love_count=love_count, play_ratio=play_ratio)

def register_complete2(*, token:str, track_id:str, love_count:float, info:dict) -> None:
  _register_action(action=ActionTag.complete, token=token, track_id=track_id, love_count=love_count, play_ratio=None)

def _register_action(*, action:ActionTag, token:str, track_id:str, love_count:float, play_ratio:float):
  # writer = UserDBWriter(_id=token)
  # udoc = get_udoc(token=token)
  # # Get track embed
  # tdoc = db.get_tdoc_from_tspace_key(track_id)
  # v = np.asarray(tdoc['embed'])
  # # Update feature importance
  # raise DeprecationWarning
  # feat_importance = cache.get_feat_importance(key=token)
  # feat_importance.register_action(track_v=v, action=action, name=tdoc['name'])
  # if action == ActionTag.right:
  #   writer.register_update(update_seed_track(udoc=udoc, tdoc=tdoc, score=love_count))
  # elif action == ActionTag.complete:
  #   writer.register_update(update_seed_track(udoc=udoc, tdoc=tdoc, score=love_count))
  # # Update interests
  # context = db.get_current_context(udoc)
  # interests = factory.get_interests(user_id=token, context=context)
  # for interest in interests.values():
  #   if action in [ActionTag.right, ActionTag.complete]:
  #     interest.register_right(id=track_id, reward=love_count)
  #   elif action == ActionTag.left:
  #     interest.register_left(id=track_id, reward=love_count)
  # for o in interests.values(): o.context_save(token, context)


  # writer.register_update(mark_as_played2(tdoc))
  writer.write()
  # if action == ActionTag.left or action == ActionTag.quick_right: pass
  # else: name = selected_name
