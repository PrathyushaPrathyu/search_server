from recommender import *

def update_tfeats(feats: Dict[TrackFeaturesTag, any]):
  return db.DBOperation({'$set': {f'feats.{k.name}': v for k, v in feats.items()}})
