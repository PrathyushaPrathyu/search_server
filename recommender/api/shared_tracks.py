import neo_db as nd
from recommender import *
from .track2 import Track2
import db_manager as dm
from pprint import pprint


def received_shared_tracks(token:str):
  user_manager = dm.UserManager.from_id(user_id=token)
  # print()
  neo_user = user_manager.neo_user
  shared_tracks_manager = nd.SharedTracksManager(user=neo_user)
  shared_tracks = shared_tracks_manager.get_received_shared_tracks()

  res=[]
  for shared_track in shared_tracks:
    track_id = shared_track.pop('track_mongo_id')
    music_tag = MusicTag.from_tdoc_id(track_id=track_id)
    track = Track2.from_music_tag(token=token, music_tag=music_tag)
    shared_track['track'] = track.to_dict()
    res.append(shared_track)
  #pprint(shared_tracks[0])
  return res

#Added by Aditya
def sent_shared_tracks(token:str):
  # #todo:remove...only for testing
  # udoc = dm.UserManager.username_exists_check(username="adityaram")
  # #todo:remove


  user_manager = dm.UserManager.from_id(user_id=token)
  neo_user = user_manager.neo_user

  shared_tracks = nd.api_alpha.get_tracks_user_shared(user=neo_user)

  res=[]
  for shared_track in shared_tracks:
    #track_id = shared_track.pop('track_mongo_id')
    #music_tag = MusicTag.from_tdoc_id(track_id=track_id)
    #track = Track2.from_music_tag(token=token, music_tag=music_tag)
    #shared_track['track'] = track.to_dict()
    res.append(shared_track)
  return res
#ENDED BY ADITYA

def visualized_shared_tracks(token:str):
  neo_user = nd.User.from_mongo_id(mongo_id=token)
  shared_tracks_manager = nd.SharedTracksManager(user=neo_user)
  shared_tracks_manager.visualized()

