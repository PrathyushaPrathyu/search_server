from sortedcontainers import *

from recommender import *

# # TODO: Return containers track is in
# def track_info(token:str, track_id:str):
#   udoc = db.get_udoc(token=token)
#   tdoc = U.v2.db.get_tdoc(track_id)
#   recom = factory.get_recommender(user_id=token, context=db.get_current_context(udoc))
#   info = dict()
#   info['tags'] = _get_track_tags(tdoc)
#   info['interests'] = _track_interests(track_id=tdoc['seq_id'], recom=recom)
#   info['arm'] = db.get_last_arm(udoc).name
#   info['immediateTrack'] = _immediate_track_name(recom)
#   return info

# def _immediate_track_name(recom):
#   seq_id = recom.interests[enums.InterestTag.immediate].sample()
#   tdoc = U.v2.db.get_tdoc_by_seq_id(seq_id=seq_id)
#   name = tdoc['most_popular_track']['name_lower']
#   return name

def _track_interests(track_id, recom: framework.Recommender):
  return [k.name for k, v in recom.interests.items() if track_id in v]

def _spot_feats(tdoc):
  spot_feats = tdoc['spot_feats']
  return [
    {'count': spot_feats['valence'], 'tag': 'valence'},
    {'count': spot_feats['energy'], 'tag': 'energy'},
  ]

def _get_track_tags(tdoc):
  tdoc_tags = tdoc['tags']
  tags = dict()
  tags['emotions'] = _get_top_tags(tdoc_tags['emotions'])
  tags['genres'] = _get_top_tags(tdoc_tags['genres_intersect'])
  tags['spotFeats'] = _spot_feats(tdoc)
  return tags

def _get_top_tags(tags, n:int=5):
  top_tags = SortedList(tags, key=lambda x: x['count'])[::-1][:10]
  return [{'tag': o['tag'], 'count': f'{o["count"]:.2f}'} for o in top_tags]