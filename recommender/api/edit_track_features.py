from bson import ObjectId
import neo_db as nd
import mongo_manager as mm
from recommender import *
import db_manager as dm
from .track2 import Track2


def edit_track_features(token:str, tid:str, action_type:str, feat_type:str, feat_value:str):
    tdoc = mm.Track.objects().get(id=ObjectId(tid))
    fire_user = dm.FirebaseUser.from_id(id=token)
    fire_user.modify_currency(v=5)

    if feat_type == 'valence':
        feat_value = float(feat_value)
        if action_type == 'update':
            tdoc.valence = feat_value

            # neo4j update
            code = """
            MATCH (t:Track{id: $tid})
            SET t.valence = $target_value
            """
            nd.api_alpha.write(code, tid=tid, target_value=feat_value, token=token)

    elif feat_type == 'loudness':
        feat_value = float(feat_value)
        if action_type == 'update':
            tdoc.loudness = feat_value

            # neo4j update
            code = """
            MATCH (t:Track{id: $tid})
            SET t.loudness = $target_value
            """
            nd.api_alpha.write(code, tid=tid, target_value=feat_value, token=token)

    elif feat_type == 'deepness':
        feat_value = float(feat_value)
        if action_type == 'update':
            tdoc.deepness = feat_value

            # neo4j update
            code = """
            MATCH (t:Track{id: $tid})
            SET t.deepness = $target_value
            """
            nd.api_alpha.write(code, tid=tid, target_value=feat_value, token=token)

    elif feat_type == 'speechiness':
        feat_value = float(feat_value)
        if action_type == 'update':
            tdoc.speechiness = feat_value

            # neo4j update
            code = """
            MATCH (t:Track{id: $tid})
            SET t.speechiness = $target_value
            """
            nd.api_alpha.write(code, tid=tid, target_value=feat_value, token=token)

    #TODO:SET re.expertise for each of them as r.last_count from listened_To
    elif feat_type == 'genre':
        if action_type == 'create':
            tdoc.genres.append({'name': feat_value, 'score': 0.})

            code = """
            MATCH (t:Track{id: $tid})
            MERGE (g:Genre:TrackFeature{name: $name})
            MERGE (t)-[:HAS_GENRE]->(g)
            WITH t,g
            MATCH (u:User{id: $token})-[r:LISTENED_TO]->(g)
            CREATE (u)-[re:HAS_EDITED]->(g)
            SET re.action = 'add', re.tid = $tid, re.expertise = r.count
            """

            nd.api_alpha.write(code, tid=tid, name=feat_value, token=token)

        elif action_type == 'delete':
            # mongo delete
            for i, v in enumerate(tdoc.genres):
                if v['name'] == feat_value:
                    tdoc.genres.pop(i)
                    break

            # neo delete
            code = """
            MATCH (t:Track{id: $tid})-[r:HAS_GENRE]->(g:Genre{name: $name}) DELETE r
            WITH t,g
            MATCH (u:User{id: $token})-[r:LISTENED_TO]->(g)
            CREATE (u)-[re:HAS_EDITED]->(g)
            SET re.action = 'delete', re.tid = $tid, re.expertise = r.count
            """
            nd.api_alpha.write(code, tid=tid, name=feat_value, token=token)

    elif feat_type == 'about':
        if action_type == 'create':

            tdoc.about.append({'name': feat_value, 'score': 0.})

            code = """
            MATCH (t:Track{id: $tid})
            MERGE (a:About:TrackFeature{name: $name})
            MERGE (t)-[:HAS_ABOUT]->(a)
            WITH t,a
            MATCH (u:User{id: $token})-[r:LISTENED_TO]->(a) 
            CREATE (u)-[re:HAS_EDITED]->(a)
            SET re.action = 'add', re.tid = $tid, re.expertise = r.count
            """
            nd.api_alpha.write(code, tid=tid, name=feat_value, token=token)

        elif action_type == 'delete':
            # mongo delete
            for i, v in enumerate(tdoc.about):
                if v['name'] == feat_value:
                    tdoc.about.pop(i)
                    break

            code = """
            MATCH (t:Track{id: $tid})-[r:HAS_ABOUT]->(a:About{name: $name}) DELETE r
            WITH t,a
            MATCH (u:User{id: $token})-[r:LISTENED_TO]->(a)
            CREATE (u)-[re:HAS_EDITED]->(a)
            SET re.action = 'delete', re.tid = $tid, re.expertise = r.count
            """
            nd.api_alpha.write(code, tid=tid, name=feat_value, token=token)

    elif feat_type == 'instrument':
        if action_type == 'create':
            tdoc.instruments.append({'name': feat_value, 'score': 0.})

            code = """
            MATCH (t:Track{id: $tid})
            MERGE (i:Instrument:TrackFeature{name: $name})
            MERGE (t)-[:HAS_INSTRUMENT]->(i)
            WITH t,i
            MATCH (u:User{id: $token})-[r:LISTENED_TO]->(i)
            CREATE (u)-[re:HAS_EDITED]->(i)
            SET re.action = 'add', re.tid = $tid, re.expertise = r.count
            """
            nd.api_alpha.write(code, tid=tid, name=feat_value, token=token)

        elif action_type == 'delete':
            # mongo delete
            for i, v in enumerate(tdoc.instruments):
                if v['name'] == feat_value:
                    tdoc.instruments.pop(i)
                    break

            code = """
            MATCH (t:Track{id: $tid})-[r:HAS_INSTRUMENT]->(i:Instrument{name: $name}) DELETE r
            WITH t,i
            MATCH (u:User{id: $token})-[r:LISTENED_TO]->(i)
            CREATE (u)-[re:HAS_EDITED]->(i)
            SET re.action = 'delete', re.tid = $tid, re.expertise = r.count
            """
            nd.api_alpha.write(code, tid=tid, name=feat_value, token=token)

    elif feat_type == 'language':
        if action_type == 'create':
            tdoc.languages.append({'name': feat_value, 'score': 0.})

            code = """
            MATCH (t:Track{id: $tid})
            MERGE (l:Language:TrackFeature{name: $name})
            MERGE (t)-[:HAS_LANGUAGE]->(l)
            WITH t,l
            MATCH (u:User{id: $token})-[r:LISTENED_TO]->(l)
            CREATE (u)-[re:HAS_EDITED]->(l)
            SET re.action = 'add', re.tid = $tid, re.expertise = r.count
            """
            nd.api_alpha.write(code, tid=tid, name=feat_value, token=token)

        elif action_type == 'delete':
            # mongo delete
            for i, v in enumerate(tdoc.languages):
                if v['name'] == feat_value:
                    tdoc.languages.pop(i)
                    break

            code = """
            MATCH (t:Track{id: $tid})-[r:HAS_LANGUAGE]->(l:Language{name: $name}) DELETE r
            WITH t,l
            MATCH (u:User{id: $token})-[r:LISTENED_TO]->(l)
            CREATE (u)-[re:HAS_EDITED]->(l)
            SET re.action = 'delete', re.tid = $tid, re.expertise = r.count
            """
            nd.api_alpha.write(code, tid=tid, name=feat_value, token=token)


    # give a currency reward for each action
    # self.write_to_mongodb(tdoc,feat_type)
    #to save the features into mongodb
    tdoc.save()
    # this will be saved by api
    return tdoc
