from recommender import *
import db_manager as dm
from recommender import api_alpha
from datetime import datetime

def register_right_swipe2(*, token:str, track_id:str, session_id:str, love_count:float, play_ratio:float, body:dict) -> None:

  try:
    action = RightSwipe(love=love_count, play_ratio=play_ratio)

    alpha_playback = api_alpha.AlphaPlayback.from_body(user_id=token, body=body,
                                                       track_id=track_id, session_id=session_id)

    alpha_playback.step(action=action)

  except NoTracksAvailable:
    pass


def register_left_swipe2(*, token:str, track_id:str, session_id:str, love_count:float, play_ratio:float, body:dict) -> None:

  try:
    action = LeftSwipe(love=love_count, play_ratio=play_ratio)

    alpha_playback = api_alpha.AlphaPlayback.from_body(user_id=token, body=body,
                                                       track_id=track_id, session_id=session_id)

    alpha_playback.step(action=action)

  except NoTracksAvailable:
    pass

def register_complete2(*, token:str, track_id:str, session_id:str, love_count:float, body:dict) -> None:

  try:
    action = Complete(love=love_count)

    alpha_playback = api_alpha.AlphaPlayback.from_body(user_id=token, body=body,
                                                       track_id=track_id, session_id=session_id)

    alpha_playback.step(action=action)

  except NoTracksAvailable:
    pass

def register_track_search2(*, token:str, track_id:str, body:dict) -> None:
  action = Search(tag=None)
  _register_action(action=action, token=token, track_id=track_id, body=body)


@log_time
def _register_action(*, action:Action, token:str, track_id:str, body):
  # print('############## START REGISTER ACTIONS ####################')
  # logger.debug('Registered action: %s', action.name)
  # playback_control = PlaybackController.from_body(user_id=token, body=body)
  # playback_control.register_action(action=action, track_id=track_id)
  # playback_control.save()
  # print('############## END REGISTER ACTIONS ####################')
  user_manager = dm.UserManager.from_id(user_id=token)
  user_manager.modify_currency(v=2)
