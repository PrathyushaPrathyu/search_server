import io
from flask import send_file

from recommender import *
import matplotlib.pyplot as plt


# def bandit_plot(token:str):
#   udoc = db.get_udoc(token=token)
#   context = db.get_current_context(doc=udoc)
#   recom = factory.get_recommender(user_id=token, context=context)
#   fig, ax = recom.bandit.plot()
#   bytes_image = io.BytesIO()
#   fig.savefig(bytes_image, format='png')
#   bytes_image.seek(0)
#   fig.close()
#   return send_file(bytes_image, attachment_filename='plot.png', mimetype='image/png')

def bandit_plot(token:str):
  fig, ax = plt.subplots()
  bytes_image = io.BytesIO()
  fig.savefig(bytes_image, format='png', bbox_inches='tight')
  bytes_image.seek(0)
  plt.close()
  return send_file(bytes_image, attachment_filename='plot.png', mimetype='image/png')

