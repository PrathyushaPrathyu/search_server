import mongo_manager as mm
import neo_db as nd
from recommender import *

def get_tags_of_type(token:str, type:str):
  tag_type = MusicTagType[type]
  tags = []
  code = ""

  if tag_type == MusicTagType.genres:
    code = """
    MATCH(g:Genre)<-[r:LISTENED_TO]-(:User) 
    RETURN g.name as name, count(r) AS score ORDER BY score DESC LIMIT 100
    """

  elif tag_type == MusicTagType.artists:
    code = """ MATCH (g:Artist) RETURN g.name as name"""

  elif tag_type == MusicTagType.about:
    code = """ MATCH (g:About) RETURN g.name as name"""

  elif tag_type == MusicTagType.languages:
    code = """ MATCH (g:Language) RETURN g.name as name"""

  elif tag_type == MusicTagType.instruments:
    code = """ MATCH (g:Instrument) RETURN g.name as name"""

  res = nd.api_alpha.read(code).data()
  tags += [mm.MusicTagSimplified(name=o['name'], tag_type=tag_type) for o in res]

  return {'items': [{'name': o.name, 'tagType': o.tag_type} for o in tags]}
