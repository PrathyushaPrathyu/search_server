from recommender import *
from recommender import api_alpha
from .track2 import Track2
from .get_track_metadata import get_track_metadata

def _simulate_action(*, token:str, action:Action, track_id:str, seq_ids, seq_dists, session_doc, body):
  # Register action
  # if body['forceSeedTrackUpdate']: new_seed_track_id = track_id
  # else: new_seed_track_id = None
  try:
    alpha_playback = api_alpha.AlphaPlayback.from_body(user_id=token, body=body, track_id=track_id,
                                                       session_id=str(session_doc.id))
    sampled_track_id, instructions, shared_by = alpha_playback.step(action=action, seq_ids=seq_ids,
                                                         seq_dists=seq_dists, simulating=True)

    track_music_tag = MusicTag.from_tdoc_id(track_id=sampled_track_id)
    track = Track2.from_music_tag(token=token, music_tag=track_music_tag, shared_by=shared_by)

    return {
      'track': track.to_dict(),
      'instructions': instructions,
      'success': True
    }

  except:
    return {'success': False}
  # return track.to_json()


@log_time
def simulate_actions(*, token: str, track_id: str, seq_ids, seq_dists, session_doc,  body):
  actions = {
    'rightSwipe': RightSwipe(love=0.0, play_ratio=10000),
    'leftSwipe': LeftSwipe(love=0.0, play_ratio=0.1),
    'complete': Complete(love=0.0),
    'love': Complete(love=1.0),
  }
  tracks = {k: _simulate_action(token=token, action=v, track_id=track_id, seq_ids=seq_ids,
                                seq_dists=seq_dists, session_doc=session_doc, body=body) for k, v in actions.items()}

  return tracks




