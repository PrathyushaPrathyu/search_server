from neo_db import *
import neo_db as nd

def recommend_people(token:str, recommend_by:str) -> List[User]:
  if recommend_by == 'expertise':
    res = nd.api_alpha.get_top_expert_people(user_id=token, tag_type='genres', name='jazz')
  elif recommend_by == 'badge':
    res = nd.api_alpha.get_top_badge_people(user_id=token, badge_name='Founder')
  else:
    res = nd.api_alpha.get_top_people_recommendations(user_id=token)

  return {'items' : res}
