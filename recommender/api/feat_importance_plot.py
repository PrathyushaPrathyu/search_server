import io
from flask import send_file

from recommender import *
import matplotlib.pyplot as plt


# def feat_importance_plot(token:str, body):
#   context_control = ContextController.from_list_json(user_id=token, list_json=body['meTags'])
#   feat_importance_control = FeatureImportanceController.load(user_id=token, contexts=context_control.contexts)
#   fig, ax = feat_importance_control.plot()
#   bytes_image = io.BytesIO()
#   fig.savefig(bytes_image, format='png', bbox_inches='tight')
#   bytes_image.seek(0)
#   fig.close()
#   return send_file(bytes_image, attachment_filename='plot.png', mimetype='image/png')


def feat_importance_plot(token:str, body):
  fig, ax = plt.subplots()
  bytes_image = io.BytesIO()
  fig.savefig(bytes_image, format='png', bbox_inches='tight')
  bytes_image.seek(0)
  plt.close()
  return send_file(bytes_image, attachment_filename='plot.png', mimetype='image/png')

