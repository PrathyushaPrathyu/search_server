import random
from bson import ObjectId

from recommender import *
from recommender.enums import *
from recommender.factory import *
from recommender.models import *
from .db_ops import *
from recommender import cache


# def get_next_track(token:str, last_track_id:str) -> TrackModel:
#   # TODO: Dont need to use seq_id
#   if last_track_id:
#     tdoc = U.db.get_track_col().find_one({'_id': ObjectId(last_track_id)})
#     last_track_id = tdoc['seq_id']
#   else:
#     last_track_id = None
#   writer = db.UserDBWriter(_id=token)
#   udoc = db.get_udoc(token=token)
#   recom = get_recommender(user_id=token, context=db.get_current_context(udoc))
#   track_id, arm = recom.next_track(last_track_id=last_track_id, played=db.get_played_tracks(udoc))
#   print(f'Sampled arm: {arm.name}')
#   # writer.register_update(register_last_arm(arm))
#   writer.write()
#   return TrackModel.from_seq_id(seq_id=track_id)

def get_next_track2(token:str, track_id:str, languages:List[str]) -> TrackModel2:
  raise DeprecationWarning
  feat_importance = cache.get_feat_importance(key=token)
  tspace = factory.get_tspace(embedder=feat_importance.embedder,
                              scaler=feat_importance.scaler,
                              feat2w=feat_importance.feat2w,
                              languages=languages,
                              must_have=[],
                              exclude=[])

  udoc = db.get_udoc(token=token)
  played = db.get_played_tracks(udoc)
  # TODO: Bug if seed_track is not in tspace (can happen because of language filtering)
  names, dists = tspace.index.get_nns_by_name(name=track_id, n=3+len(played))
  names = [n for n in names if n not in played]
  selected_name = random.choice(names)
  # print(f'Next track is {selected_name}')
  # TODO: If last_track_id is _id we have to encode ObjectId
  tdoc = db.get_tdoc_from_tspace_key(selected_name)
  return TrackModel2.from_tdoc(tdoc=tdoc)

def get_next_track3(
    udoc:dict,
    primary_mode:PrimaryMode,
    secondary_mode:SecondaryMode,
    interest_control:InterestController,
    tag_control:MusicTagController,
    seeding:bool=False,
) -> MusicTag:
  token = str(udoc['_id'])
  played = db.get_played_tracks(udoc)
  seed_track = interest_control.seed_track
  pipeline = tag_control.get_pipeline(seeding=seeding)

  if primary_mode == PrimaryMode.Favorites:
    favs = [fav for k, v in interest_control.interests.items() for fav in v.favorites.items()]
    favs_nplayed = [o for o in favs if o[0] not in played]
    ids, ws = zip(*favs_nplayed)
    if secondary_mode == SecondaryMode.Wild:
      track_id = random.choices(ids, weights=ws)[0]
    if secondary_mode == SecondaryMode.Organic:
      _ids = [ObjectId(o) for o in set(ids+(seed_track,))]
      pipeline.insert(0, {'$match': {'_id': {'$in': _ids}}})
      cursor = db.get_tcol().aggregate(pipeline)
      tspace = factory.get_tspace2(token=token, cursor=cursor)
      nns, dists = tspace.index.get_nns_by_name(name=seed_track, n=4)
      try: nns.remove(seed_track)
      except ValueError: pass
      track_id = random.choices(nns)[0]

  if primary_mode == PrimaryMode.Magic:
    if secondary_mode == SecondaryMode.Organic:
      # TODO: Hardcoded filter
      pipeline.insert(0, {'$match': {'_id': {'$nin': [ObjectId(o) for o in played if o != seed_track]}}})
      cursor = db.get_tcol().aggregate(pipeline)
      tspace = factory.get_tspace2(token=token, cursor=cursor)
      nns, dists = tspace.index.get_nns_by_name(name=seed_track, n=4)
      try: nns.remove(seed_track)
      except ValueError: pass
      track_id = random.choices(nns)[0]
    if secondary_mode == SecondaryMode.Wild:
      filt = {'_id': {'$nin': [ObjectId(o) for o in played]}}
      cursor = db.get_tcol().find(filt, projection=['_id'])
      track_id = random.choice(list(cursor))['_id']

  if primary_mode == PrimaryMode.Discover:
    if secondary_mode == SecondaryMode.Organic:
      # TODO: Hardcoded filter
      pipeline.insert(0, {'$match': {'_id': {'$nin': [ObjectId(o) for o in interest_control.history if o != seed_track]}}})
      cursor = db.get_tcol().aggregate(pipeline)
      tspace = factory.get_tspace2(token=token, cursor=cursor)
      nns, dists = tspace.index.get_nns_by_name(name=seed_track, n=4)
      try: nns.remove(seed_track)
      except ValueError: pass
      track_id = random.choices(nns)[0]
    if secondary_mode == SecondaryMode.Wild:
      filt = {'_id': {'$nin': [ObjectId(o) for o in interest_control.history]}}
      cursor = db.get_tcol().find(filt, projection=['_id'])
      track_id = random.choice(list(cursor))['_id']

  tdoc = db.get_tdoc_from_tspace_key(track_id)
  return MusicTag.from_tdoc(tdoc)
