from recommender import *
from .get_track_metadata import get_track_metadata

@log_time
def track_metadata(token:str, track_id:str):
  return get_track_metadata(token, track_id=track_id)

# def seed_track_metadata(token:str):
#   udoc = db.get_udoc(token=token)
#   tdoc_seed = db.get_tdoc_from_tspace_key(db.get_seed_track(udoc)['_id'])
#   return get_feats(tdoc_seed)


