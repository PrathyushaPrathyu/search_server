from recommender import *
import neo_db as nd
import mongo_manager as mm


def get_track_metadata(token:str, track_id:str):
  # tags = MusicTagController.from_tdoc(tdoc).tags_to_json()
  # TODO: Use UserManager from db_manager
  tdoc = mm.Track.from_id(id=track_id).to_dict()
  track = nd.Track.from_mongodb_doc(tdoc)
  user = nd.User.from_mongo_id(mongo_id=token)
  tags = [o.to_dict() for o in user.get_expertises_for_track(track) if o.track_feature.tag_type != 'emotion']
  features = {k.name.lower(): tdoc[k.db_field] for k in Features}
  return {'id':str(tdoc['_id']), 'name': tdoc['name'], 'tags': tags, **features}
