import pymongo
from flask import abort
import neo_db as nd

from recommender import *
from .db_ops import *
from .history import *
from .user import new_user_doc


# def sign_in(username:str, password:str, info:dict) -> str:
  # # Old implementation #
  # udoc = db.get_udoc_by_credentials(username=username, password=password)
  # writer = db.UserDBWriter(_id=udoc['_id'])
  # # writer.register_update(add_history_sign_in(username=username, info=info))
  # writer.register_update(new_session())
  # writer.write()
  # ####################
  # # Reset session
  # playback_control = PlaybackController.from_no_context(user_id=str(udoc['_id']))
  # playback_control.reset_session()
  # playback_control.save()
  # # Get user from neo_db
  # neo_user = nd.User.from_name(name=username)
  # return neo_user.to_dict(simplified=False)

# def sign_up(username:str, password:str, info:dict):
  # ucol = db.get_user_col()
  # new_udoc = new_user_doc(username=username)
  # try:
  #   _id = ucol.insert_one(new_udoc).inserted_id
  #   writer = db.UserDBWriter(_id=_id)
  #   # writer.register_update(add_history_sign_up(username=username, info=info))
  #   # Add to neo4j
  #   neo_user = nd.User(name=username, mongo_id=str(_id)).create()
  #   _user_perks(neo_user)
  # except pymongo.errors.DuplicateKeyError: abort(409, 'Username already exists')

# def get_user(token:str):
#   neo_user = nd.User.from_mongo_id(mongo_id=token)
#   return neo_user.to_dict(simplified=False)

# def _user_perks(neo_user:nd.User):
  # neo_user.modify_currency(200)
  # neo_user.add_badge(badge=nd.Badges.beta_user_badge)
  #
  # # TODO:TEST Next lines only for testing function
  # # Add a track to have some expertise
  # tdoc = db.get_tdoc(name='hot mama blues')
  # track = nd.Track.from_mongodb_doc(tdoc)
  # context_manager = nd.ContextManager(user=neo_user, context=nd.Context('Testing'))
  # context_manager.register_action(track=track, action='RightSwipe', reward=0.7)
