from recommender import *
import neo_db as nd
import mongo_manager as mm
import db_manager as dm


class TrackMetadata:
  def __init__(
      self,
      id: str,
      name: str,
      valence: float,
      loudness: float,
      deepness: float,
      speechiness: float,
      duration: float,
      album: str,
      spotifyUri: str,
      popularity:int,
      date:int,
      expertise,
      trackFriends,
      tags,
  ):
    self.id = id
    self.name = name
    self.valence = valence
    self.loudness = loudness
    self.deepness = deepness
    self.speechiness = speechiness
    self.popularity = popularity
    self.date = date
    self.duration = duration
    self.expertise = expertise
    self.album = album
    self.trackFriends = trackFriends
    self.tags = tags
    self.spotifyUri = spotifyUri
    # self.appleMusicUri = appleMusicUri

  def to_dict(self):
    return self.__dict__

  @classmethod
  def from_track_id(cls, token: str, track_id: str):
    tdoc = mm.Track.from_id(id=track_id).to_dict()
    udoc = mm.UserInfo.from_user_id(id=token) #added by aditya
    user_manager = dm.UserManager.from_id(user_id=token)
    #neo_user = user_manager.neo_user
    # expertise = {'count': 2, 'score': 2, 'is_expert': True, 'score_threshold': 3, 'count_threshold':3}
    expertise = nd.api_alpha.get_track_expertise(user_id=token,track_id=track_id)
    trackFriends = nd.api_alpha.get_friends_who_love_this_track(user_id=token,track_id=track_id)

    trackExpertise = expertise["is_track_expert"]

    final_tags = cls._get_final_tags(tdoc=tdoc, expertise = expertise)


    if(udoc.has_apple_music):
      uri = "appleMusic:track:" + str(tdoc['applemusic_uri'])
    else:
      uri = tdoc["uri"]

    return cls(
      id=track_id,
      name=tdoc['name'],
      album=tdoc['album']['name'] if 'album' in tdoc and 'name' in tdoc['album'] else None,
      duration=tdoc['duration_ms'] if 'duration_ms' in tdoc else None,
      valence=tdoc[Features.valence.db_field],
      loudness=tdoc[Features.loudness.db_field],
      deepness=tdoc[Features.deepness.db_field],
      speechiness = tdoc[Features.speechiness.db_field],
      spotifyUri= uri,  #TODO: refactor spotifyUri->Uri
      popularity = TrackMetadata.transform_popularity(tdoc['popularity']),
      date = tdoc['release_year'] if 'release_year' in tdoc else None,
      expertise=trackExpertise,
      trackFriends= trackFriends,
      tags=final_tags,
    )


  # @staticmethod
  # def _get_final_tags(tdoc, expertise):
  #   final_tags = []
  #   for tag_type in MusicTagType:
  #     if tag_type == MusicTagType.album:
  #       tag = tdoc[tag_type.db_field]
  #       if tag:
  #         final_tags.append({'tag': {'tagType': tag_type.name, 'name': tag['name']}})
  #     elif not tag_type in [MusicTagType.track, MusicTagType.emotion]:
  #       tags = tdoc[tag_type.db_field]
  #       for tag in tags: final_tags.append({'tag': {'tagType': tag_type.name, 'name': tag['name']}})
  #   return final_tags

  #edited by aditya
  @staticmethod
  def _get_final_tags(tdoc, expertise):
    final_tags = []
    expertise_placeholder = {'canEdit': False, 'myBadge': 0, 'hasEditedBadge': 0,
                         "myExpertise": 0, 'hasEditedExpertise': 0}
    for tag_type in MusicTagType:
      if tag_type == MusicTagType.album:
        tag = tdoc[tag_type.db_field]
        if tag:
          final_tags.append({'tag': {'tagType': tag_type.name, 'name': tag['name'],'expertise':expertise_placeholder}})
      elif not tag_type in [MusicTagType.track, MusicTagType.emotion]:
        tags = tdoc[tag_type.db_field]
        for tag in tags:
          expertise_key = tag_type + "::" + tag['name']
          if(expertise_key in expertise.keys()):
            final_tags.append({'tag': {'tagType': tag_type.name, 'name': tag['name'], 'expertise' : expertise[expertise_key]}})
          else:
            final_tags.append({'tag': {'tagType': tag_type.name, 'name': tag['name'], 'expertise' :expertise_placeholder}})

    return final_tags

  @staticmethod
  def transform_popularity(popularity):
    if popularity <= 5:
      return 1
    elif popularity >5 and popularity < 30:
      return 2
    elif popularity >= 30:
      return 3

class Track2:
  def __init__(
      self,
      id: str,
      name: str,
      url: str,
      cover_image_url: str,
      recommended_by: dict,
      metadata: TrackMetadata,
  ):
    self.id = id
    self.name = name
    self.url = url
    self.cover_image_url = cover_image_url
    self.recommended_by = recommended_by
    self.metadata = metadata

  def to_dict(self):
    return {
      'id': self.id,
      'name': self.name,
      'url': self.url,
      'cover_image_url': self.cover_image_url,
      'recommended_by': self.recommended_by,
      'metadata': self.metadata.to_dict()
    }

  @classmethod
  def from_music_tag(cls, token: str, music_tag: MusicTag, shared_by: dict = None):
    metadata = TrackMetadata.from_track_id(token=token, track_id=music_tag.id)
    return cls(
      id=str(music_tag.id),
      name=music_tag.name,
      url=music_tag.preview_url,
      cover_image_url=music_tag.cover_image,
      recommended_by=shared_by,
      metadata=metadata,
    )

  def get_track_from_isrc(isrc):
    pipeline = [{'$match': {'isrc': isrc}},
                {'$limit': 1},
                {'$project': {'_id': 1}}
                ]
    track_list = list(mm.Track.objects.aggregate(*pipeline))
    for i in track_list:
      tid = str(i['_id'])
    if not track_list:
      tid = None
    return tid

