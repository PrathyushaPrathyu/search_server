import neo_db as nd
from recommender import *
import db_manager as dm
import mongo_manager as mm

def get_friends(token:str):
  user = nd.User.from_mongo_id(mongo_id=token)
  friends = user.get_friends()
  return {'users': [o.to_dict(simplified=True) for o in friends]}

def get_friends(token:str):
  user = nd.User.from_mongo_id(mongo_id=token)
  friends = user.get_friends()
  return {'users': [o.to_dict(simplified=True) for o in friends]}


def share_track(token:str, to_user:str, track_id):
  user_manager = dm.UserManager.from_id(user_id=token)
  tdoc = mm.Track.from_id(id=track_id).to_dict()
  user_manager.share_track(tdoc=tdoc, to_user=to_user)

def send_chat_notification(token:str,to_user:str, track_id:str, message):
  friend_user_manager = dm.UserManager.from_id(user_id=to_user)
  my_user_manager = dm.UserManager.from_id(token)
  friend_fire_user = friend_user_manager.fire_user
  message_title = my_user_manager.mongo_user.username
  track_name = mm.Track.from_id(track_id).name
  message_body = message
  data = {'page': '/chatPage',
          'username': str(my_user_manager.mongo_user.username),
          'trackName': track_name,
          'trackId' : str(track_id),
          'user_id': str(my_user_manager.mongo_user.id)}
  friend_fire_user.create_notification(message_title=message_title,message_body=message_body,data=data)



def add_friend(token:str, friend_id):
  user1 = nd.User.from_mongo_id(mongo_id=token)
  user2 = nd.User.from_name(name=friend_id)
  user1.add_friend(friend=user2)
  dm.UserManager.from_id(user_id=user2.mongo_id).fire_user.send_new_friend_add_message(name=user1.name)

def friends_reactions(token:str, track_id:str):
  user = nd.User.from_mongo_id(mongo_id=token)
  friends_history_manager = nd.FrieneddsHistoryManager(user=user)
  return {'items': friends_history_manager.friends_reaction(track_id=track_id).to_dict()}

