import neo_db as nd
from recommender import *
import mongo_manager as mm

def received_broadcasted_tracks(token:str):
  neo_user = nd.User.from_mongo_id(mongo_id=token)
  broad_tracks_manager = nd.BroadcastedTracksManager(user=neo_user)
  broad_tracks = broad_tracks_manager.get_received_broadcasted_tracks()

  for broad_track in broad_tracks:
    track_id = broad_track.pop('track_mongo_id')
    broad_track['track'] = MusicTag.from_tdoc_id(track_id=track_id).to_json()

  return broad_tracks

def visualized_broadcasted_tracks(token:str):
  neo_user = nd.User.from_mongo_id(mongo_id=token)
  broad_tracks_manager = nd.BroadcastedTracksManager(user=neo_user)
  broad_tracks_manager.visualized()

def broadcast_track(token:str, track_id):
  tdoc = mm.Track.from_id(id=track_id).to_dict()
  from_user = nd.User.from_mongo_id(mongo_id=token)
  track = nd.Track.from_mongodb_doc(doc=tdoc)
  broad_tracks_manager = nd.BroadcastedTracksManager(user=from_user)
  broad_tracks_manager.broadcast_track(track=track)
