from recommender import *
from bson.regex import Regex
import mongo_manager as mm
import neo_db as nd
from .track2 import Track2
from recommender.tag.db_enums import MusicTagType, MeTagType
from recommender.tag.me_tag import MeTag
from recommender.tag.music_tag import MusicTag
from recommender import api

# ADDED_BY PRATHYUSHA
# from pymongo import MongoClient
import re
import requests
import json
import bson
from tekore.auth import Credentials

# ADDED BY ADITYA
from mongo_manager.core.models.track import Track
from bson import ObjectId


# ADD COMPLETE BY ADITYA

# def search_item(token:str, query:str, limit:int, item_type:str=None,):
#   if item_type: item_type = [MusicTagType[o] for o in [e.strip() for e in item_type.split(',')]]
#   else:         item_type = [MusicTagType.languages, MusicTagType.genres, MusicTagType.about, MusicTagType.instruments, MusicTagType.track, MusicTagType.artists]
#   query = query.lower()
#
#   tags = []
#   # Search for tags (Emotion, about, etc)
#   res = list(mm.Tag.objects(__raw__={
#     'name': Regex(f'^{query}', flags='i'),
#     'tag_type': {'$in': [o.db_field for o in item_type]},
#   }))
#   tags += [MusicTag.from_doc(o.to_dict()).to_json() for o in res]
#   # Search for artists
#   if MusicTagType.artists in item_type:
#     res = list(mm.Artist.objects(__raw__={'name_lower': Regex(f'^{query}')}).limit(3))
#     tags += [MusicTag.from_artist_doc(o.to_dict()).to_json() for o in res]
#   # Search for tracks
#   if MusicTagType.track in item_type:
#     res = []
#     res += list(mm.Track.objects(__raw__={'name_lower': Regex(f'^{query}')}).limit(3))
#     res += list(mm.Track.objects(__raw__={'artists.name': Regex(f'^{query}')}).limit(3))
#     res += list(mm.Track.objects(__raw__={'album.name': Regex(f'^{query}')}).limit(3))
#     tags += [MusicTag.from_tdoc(o.to_dict()).to_json() for o in res]
#
#   return tags



def search_item(token: str, query: str, limit: int, item_type: str = None):
    global gen_names

    if item_type:
        item_type = [MusicTagType[o] for o in [e.strip() for e in item_type.split(',')]]
    else:
        item_type = [MusicTagType.languages,
                     MusicTagType.genres,
                     MusicTagType.about,
                     MusicTagType.instruments,
                     MusicTagType.track,
                     MusicTagType.artists,
                     MusicTagType.album]

    query = query.lower()
    query = query.strip()



    tags = []
    # other_tracks = []
    # other_artists = []
    # other_albums = []
    other_genres = []
    other_about = []
    other_ins = []
    other_lans = []
    apple_id_list, apple_doc = check_apple_id_in_track_final(query, limit)

    gen_names = []
    genre = ''

    if MusicTagType.genres in item_type:
        regex_query = bson.regex.Regex(f'^{query}')
        pipeline = [{'$match': {'genres.name': regex_query}},
                    {"$limit": 15},
                    {"$sort": {'popularity': -1}},
                    # {'$limit': limit},
                    {'$project': {'genres': 1}},
                    {'$limit': limit}
                    ]

        res = []
        for o in mm.Track.objects.aggregate(*pipeline):
            _id = str(o['_id'])
            # doc[_id] = {'name': o['name']}
            # for each
            for each in o['genres']:
                if each['name'] == query:
                    res = [{'name': query}]
                    genre = query
        code = f"""
    CALL db.index.fulltext.queryNodes("genreName", "{query}*") YIELD node, score
    RETURN node.name as name, score LIMIT 3 
    """
        code = f"""
    MATCH (g:Genre) WHERE g.name STARTS WITH "{query}" RETURN g.name as name LIMIT 10
    """
        if not res:
            res = nd.api_alpha.read(code).data()

        for o in res:
            gen_names.append(o['name'])
        tags += [MusicTag(name=o['name'], tag_type=MusicTagType.genres).to_json() for o in res[:2]]
        other_genres = [MusicTag(name=o['name'], tag_type=MusicTagType.genres).to_json() for o in res[2:]]

    if MusicTagType.languages in item_type:

        # languages
        code = f"""
    CALL db.index.fulltext.queryNodes("languageName", "'{query}' OR {query}*") YIELD node, score
    RETURN node.name as name, score LIMIT 3 
    """
        code = f"""
    MATCH (g:Language) WHERE g.name STARTS WITH "{query}" RETURN g.name as name LIMIT 10
    """
        res = nd.api_alpha.read(code).data()
        tags += [MusicTag(name=o['name'], tag_type=MusicTagType.languages).to_json() for o in res[:2]]
        other_lans = [MusicTag(name=o['name'], tag_type=MusicTagType.languages).to_json() for o in res[2:]]

    if MusicTagType.instruments in item_type:

        # instruments
        code = f"""
    CALL db.index.fulltext.queryNodes("instrumentName", "'{query}' OR {query}*") YIELD node, score
    RETURN node.name as name, score LIMIT 3 
    """
        code = f"""
    MATCH (g:Instrument) WHERE g.name STARTS WITH "{query}" RETURN g.name as name LIMIT 10
    """
        res = nd.api_alpha.read(code).data()
        tags += [MusicTag(name=o['name'], tag_type=MusicTagType.instruments).to_json() for o in res[:2]]
        other_ins = [MusicTag(name=o['name'], tag_type=MusicTagType.instruments).to_json() for o in res[2:]]

    if MusicTagType.about in item_type:

        # about
        code = f"""
    CALL db.index.fulltext.queryNodes("aboutName", "'{query}' OR {query}*") YIELD node, score
    RETURN node.name as name, score LIMIT 3 
    """
        code = f"""
    MATCH (g:About) WHERE g.name STARTS WITH "{query}" RETURN g.name as name LIMIT 10
    """
        res = nd.api_alpha.read(code).data()
        tags += [MusicTag(name=o['name'], tag_type=MusicTagType.about).to_json() for o in res[:2]]
        other_about = [MusicTag(name=o['name'], tag_type=MusicTagType.about).to_json() for o in res[2:]]

    if MusicTagType.artists in item_type:

        # artists
        # art_doc = mm.Artist.objects().get(id=ObjectId(tid))
        # art_service_id = []
        # res = list(mm.Artist.objects(__raw__={'name_lower': Regex(f'^{query}')}).limit(3))
        # tags += [MusicTag.from_artist_doc(o.to_dict()).to_json() for o in res]
        query = query.lower()
        regex_query = bson.regex.Regex(f'^{query}')
        pipeline = [{'$match': {'name_lower': regex_query}},
                    {"$limit": 3},
                    {"$project": {"service_id": 1, "name": 1, "cover_image_url": 1}},
                    ]
        if not genre and not gen_names:
            for o in mm.Artist.objects.aggregate(*pipeline):
                art_service_id = o['service_id']
                art_name = o['name'].lower()
                if 'cover_image_url' in o:
                    cover_image_url = o['cover_image_url']
                else:
                    cover_image_url = get_artist_info_using_art_id(art_service_id)
                    art_doc = mm.Artist.objects().get(id=o['_id'])
                    art_doc.cover_image_url = cover_image_url
                    art_doc.save()
                tags.append(MusicTag(name=art_name, tag_type=MusicTagType.artists,
                                     cover_image=cover_image_url).to_json())
            # print("art_tags:",tags)

        code = f"""
    CALL db.index.fulltext.queryNodes("artistName", "{query}*") YIELD node, score
    RETURN node.name as name, score LIMIT 3 
    """
        code = f"""
    MATCH (g:Artist) WHERE g.name STARTS WITH "{query}"
    WITH g LIMIT 25
    MATCH (g)<-[:HAS_ARTIST]-(t:Track)
    WITH g, count(DISTINCT t) as score 
    RETURN g.name as name, score ORDER BY score DESC LIMIT 10
    """
        if not tags:
            res = nd.api_alpha.read(code).data()
            tags += [MusicTag(name=o['name'], tag_type=MusicTagType.artists).to_json() for o in res[:3]]
        # other_artists = [MusicTag(name=o['name'], tag_type=MusicTagType.artists).to_json() for o in res[3:]]

        if len(apple_id_list) == 0:
            query_list = query.split(" ")
            tags_list = []
            for i, each in enumerate(query_list):
                tmp_query = " ".join(query_list[i:])
                tmp_query = bson.regex.Regex(f'^{tmp_query}')
                # print(tmp_query)
                tags_list.extend(get_fam_art_tracks(tmp_query))
                if len(tags_list)>3:
                    break
            print("art_tracks_from_track_final:", tags_list)
            for o in tags_list:
                print(o)
                tags.append(MusicTag(name=o["name"], tag_type=MusicTagType.track,
                                     artist_name=o['artists'], id=str(o['id']),
                                     cover_image=o['cover_image_url']).to_json())

    if MusicTagType.track in item_type:
        if len(tags) > 0:
            res, doc = track_search_item(query, 7, apple_id_list, apple_doc)
        else:
            res, doc = track_search_item(query, 10, apple_id_list, apple_doc)
        print(f"search results : {res}")
        for o in res:
            # prnt(doc[o])
            _id = o
            name = doc[o]['name']
            artist_names = ', '.join([a['name'].title() for a in doc[o]['artists']])
            cover_image = doc[o]['cover_image_url']
            tags.append(MusicTag(name=name, tag_type=MusicTagType.track,
                                 artist_name=artist_names, id=_id,
                                 cover_image=cover_image).to_json())
        # for o in res:
        #     _id = o['_id']
        #     name = o['name']
        #
        #     artist_names = ', '.join([a['name'].title() for a in o['artists']])
        #     cover_image = o['cover_image_url']
        #     tags.append(MusicTag(name=name, tag_type=MusicTagType.track,
        #                          artist_name=artist_names, id=_id,
        #                          cover_image=cover_image).to_json())

    if MusicTagType.album in item_type:

        # res_album =[]
        # res_album += list(mm.Track.objects(__raw__={'album.name': Regex(f'^{query}')}).limit(3))
        query = query.lower()
        regex_query = bson.regex.Regex(f'^{query}')
        pipeline = [{'$match': {'album.name': regex_query}},
                    {"$limit": 25},
                    {"$project": {"album": 1, "cover_image_url": 1}},
                    ]
        id_list = []
        for o in mm.Track.objects.aggregate(*pipeline):
            # # alb = list(mm.Album.objects(__raw__={'album.name': Regex(f'^{query}')}).limit(3))
            # # if not alb:
            # # album_id = mm.Album.from_id(track_id).id
            album_id = o['album']['id']
            if album_id not in id_list:
                id_list.append(o['album']['id'])
                #
                #     album_name = o['album']['name']
                # cover_image_url = get_album_info_using_album_id(album_id)
                # album_ses_doc = mm.Album(name = album_name.title(),
                #                         name_lower = album_name,
                #                           service_id = album_id,
                #                           cover_image_url = cover_image_url)
                # album_ses_doc.save()
                cover_image_url = o['cover_image_url']
                album_name = o['album']['name'].lower()
                tags.append(MusicTag(name=album_name, tag_type=MusicTagType.album,
                                     cover_image=cover_image_url).to_json())

        code = f"""
    MATCH (g:Album) WHERE g.name STARTS WITH "{query}" RETURN g.name as name LIMIT 10
    """
        # if not res_album:
        res = nd.api_alpha.read(code).data()
        # res = []
        # for a in res_album:
        #     if not a['name'] in gen_names:
        #         res.append({'name':a['name']})

        # tags += [MusicTag.from_tdoc(o).to_json() for o in res_album]
        if not tags:
            tags += [MusicTag(name=o['name'], tag_type=MusicTagType.album).to_json() for o in res[:3]]
        # other_albums = [MusicTag(name=o['name'], tag_type=MusicTagType.album).to_json() for o in res[3:]]

    tags += other_about
    tags += other_lans
    tags += other_ins
    tags += other_genres
    # tags += other_artists
    # tags += other_albums
    # tags += other_tracks

    if not tags:
        code = f"""
    CALL db.index.fulltext.queryNodes('names', '"{query}"^100 {query}') YIELD node, score
    RETURN DISTINCT node.name as name, node.id as id, labels(node) as labels LIMIT 20
    """
        res = nd.api_alpha.read(code).data()
        for o in res:
            # if 'Track' in o['labels'] and MusicTagType.track in item_type:
            #   tags.append(MusicTag(name=o['name'], tag_type=MusicTagType.track, id=o['id']).to_json())
            if 'Artist' in o['labels'] and MusicTagType.artists in item_type:
                tags.append(MusicTag(name=o['name'], tag_type=MusicTagType.artists).to_json())
            elif 'Album' in o['labels'] and MusicTagType.album in item_type:
                tags.append(MusicTag(name=o['name'], tag_type=MusicTagType.album).to_json())
            elif 'Genre' in o['labels'] and MusicTagType.genres in item_type:
                tags.append(MusicTag(name=o['name'], tag_type=MusicTagType.genres).to_json())
            elif 'About' in o['labels'] and MusicTagType.about in item_type:
                tags.append(MusicTag(name=o['name'], tag_type=MusicTagType.about).to_json())
            elif 'Instrument' in o['labels'] and MusicTagType.instruments in item_type:
                tags.append(MusicTag(name=o['name'], tag_type=MusicTagType.instruments).to_json())
            elif 'Language' in o['labels'] and MusicTagType.languages in item_type:
                tags.append(MusicTag(name=o['name'], tag_type=MusicTagType.languages).to_json())

    return tags


def search_me(token: str, query: str, limit: int) -> List[dict]:
    res = mm.History.objects(__raw__={'context.name': Regex(f'^{query}')})
    tags = set([MeTag(name=o.context.name, type=MeTagType[o.context.type]) for o in res])
    tags = [tag.to_json() for tag in tags]
    return tags


def capitalize(artists_list):
    upped_list = [o.title() for o in artists_list]
    return ', '.join(upped_list)


def search_using_ngrams(query, limit=4):
    print("###neodb_search_using_ngrams###")
    query = query.lower()
    query = ' '.join(make_ngrams(query))
    doc = {}

    code = f"""
    CALL db.index.fulltext.queryNodes("TitleNgramsIndex", $query)
    YIELD node, score
    RETURN node.mongoId as mongoId ORDER BY score DESC LIMIT 10
    """
    res = nd.api_alpha.read(code, query=query).data()
    ids = [ObjectId(doc['mongoId']) for doc in res]

    pipeline = [
        {"$match": {"_id": {"$in": ids}}},
        {"$project": {"name": 1, "cover_image_url": 1, "artists": 1, "popularity": 1}},
        {"$sort": {"popularity": -1}},
        {"$limit": limit}
    ]
    # print(list(mm.Track.objects.aggregate(*pipeline))
    res = []
    for o in list(mm.Track.objects.aggregate(*pipeline)):
        _id = str(o['_id'])
        doc[_id] = {'name': o['name'], 'cover_image_url': o['cover_image_url'], 'artists': o['artists']}
        res.append(_id)
        # print(res)

    return res, doc


def make_ngrams(word, min_size=3, edge_ngram=False):
    word = re.sub(r" ?\([^)]+\)", " ", word)
    word = re.sub(r'\[[^()]*\]', ' ', word)
    word = word.replace('"', ' ')
    word = word.replace(",", ' ')
    word = word.replace(":", " ")
    word = word.replace("!", " ")
    word = word.replace("-", " ")
    word = word.replace("'", '')

    word_list = word.split(" ")

    ngrams_set = set()
    # for each word calculate the edge ngrams
    for word in word_list:
        length = len(word)
        size_range = range(min_size, max(length, min_size) + 1)
        for size in size_range:
            for i in range(0, max(0, length - size) + 1):
                if edge_ngram:
                    ngrams_set.add(word[0:i + size])
                else:
                    ngrams_set.add(word[i:i + size])
    return ngrams_set


def track_search_item(query, limit, apple_id_list, apple_doc):
    print('####track_search_started######')
    query = query.lower()

    res_list = []
    doc = {}

    regex_query = bson.regex.Regex(f'^{query}')
    pipeline = [{'$match': {'name_lower': regex_query}},
                {"$limit": 50},
                {"$sort": {'popularity': -1}},
                # {'$limit': limit},K
                {'$limit': 2}
                ]


    for o in mm.Track.objects.aggregate(*pipeline):
        _id = str(o['_id'])
        cover_image_url = o['cover_image_url'] if 'cover_image_url' in o else None
        doc[_id] = {'name': o['name'], 'cover_image_url': cover_image_url,
                    'artists': o['artists']}

        res_list.append(_id)


    if len(res_list)< limit:
        print('#####using_apple_id_search#####')
        # apple_id_list, apple_doc = check_apple_id_in_track_final(query, limit)
        # print(apple_id_list)
        doc.update(apple_doc)
        res_list.extend(apple_id_list)
        # res_list = sorted(set(res_list), key=res_list.index)
        # res = sorted(set(apple_id_list), key=apple_id_list.index)

        # res.update(apple_id_list)
        print(res_list)

    if len(res_list) < limit:
        print('#####using_neodb_ngrams#####')
        neodb_id_list, neodb_doc = search_using_ngrams(query, limit=limit)
        # print(neodb_id_list)

        doc.update(neodb_doc)
        res_list.extend(neodb_id_list)
        # res = sorted(set(neodb_id_list), key=neodb_id_list.index)
        # res.update(neodb_id_list)
        # print(res)
    # name_list=[]
    # for each in list(res_list):
    #     name_list.append(doc[each]['name'])
    res = sorted(set(res_list), key=res_list.index)
    # print('final_track_search_res: ', res)
    # print(name_list)

    return res, doc


def fuzzy_search_item_with_apple(query, limit):
    ####track_search_started####
    # limit = limit + 3
    URL = "https://api.music.apple.com/v1/catalog/us/search"
    token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IjNIRzk3UUs4WDQifQ.eyJpc3MiOiIyOTU1U0o3UDJSIiwiaWF0IjoxNTg2NDIxNTU4LCJleHAiOjE2MDIxODk1NTh9.QciZoAPw_pf5vB2Kk6GvkLgtYJVuM2xYCbqmHYnGP6sE79qO3zB183v_c8uJb4lWCiIqFYfJOYuh2FULPWi_Yg'
    hed = {'Authorization': 'Bearer ' + token}
    cover_size = '600'
    # location given here
    #     term = "dil tho paagal hai"
    # defining a params dict for the parameters to be sent to the API
    params = {'limit': limit, 'term': query, 'types': 'songs'}
    query_name = query.split(" ")[0]
    # l = len(query_name)
    # new_query = query_name[:l-2] + query.split(" ")[:1]

    # params_new = {'limit': limit, 'term': new_query, 'types': 'songs'}
    res = json.loads(requests.get(url=URL, params=params, headers=hed).text)

    # if not res['results']:
    #     res = json.loads(requests.get(url=URL, params=params_new, headers=hed).text)

    res_names_list = []
    res_id_list = []
    art_list = []
    cover_img_list = []
    res_isrc_list = []

    try:
        for item in res['results']['songs']['data']:
            isrc = item['attributes']['isrc']
            spotify_res = api.check_if_isrc_exists_in_spotify(isrc)
            if spotify_res:
                res_isrc_list.append(item['attributes']['isrc'])
                res_names_list.append(item['attributes']['name'])
                res_id_list.append(int(item['id']))
                cover_img_list.append(
                    item['attributes']['artwork']['url'].replace("{w}", cover_size).replace("{h}", cover_size))
                arts = re.split(", | & ", item['attributes']['artistName'])
                art_list.append(arts)

    except KeyError:
        pass
    return tuple(zip(res_id_list, res_isrc_list, res_names_list, cover_img_list,
                     art_list))  ##return isrc and id as list of tuples


def check_apple_id_in_track_final(query, limit):
    print('apple_id_search_started')
    doc = {}
    # res = set()
    res = []
    apple_tup = fuzzy_search_item_with_apple(query, limit)
    for id, isrc, name, cover_img, arts in apple_tup:

        # print(name, 'output from applemusic search')
        pipeline = [{'$match': {'$or': [{'applemusic_uri': id}, {'isrc': isrc}]}},
                    {'$limit': 1}
                    ]
        # o = mm.Track.objects.get('applemusic_uri'=id)
        # if not o:
        #     print('ids_are_not_in_our_db')
        #     o = mm.Track.objects.get('isrc' = isrc)
        # tmp_doc = [str(o['_id'])
        tmp_res = list(mm.Track.objects.aggregate(*pipeline))

        # print(tmp_res)
        # print('apple_id_search_started_for_tracks_in_our_db')
        for o in tmp_res:

            # print(o['_id'])
            cover_img_url = o['cover_image_url'] if 'cover_image_url' in o else None
            _id = str(o['_id'])
            doc[_id] = {'name': o['name'], 'cover_image_url': cover_img_url, 'artists': o['artists']}

            res.append(_id)
            # print(res)
        # print('assigning_special_char_to_ids')
        if not tmp_res:
            art_list = []
            new_track_id = str(isrc) + '+' + str(isrc)[:11]
            for each in arts:
                art_list.append({'name': each})
            # print(new_track_id)
            doc[new_track_id] = {'name': name, 'cover_image_url': cover_img, 'artists': art_list}
            # print(doc[new_track_id]['artists'])
            res.append(new_track_id)
            # print(res)

    res = sorted(set(res), key=res.index)
    # print("apple_search_res: ", res)
    return res, doc


def get_artist_info_using_art_id(art_id):
    SPOTIPY_CLIENT_SECRET = "8a375348c3d341cd918ad0ccc02ef658"
    SPOTIPY_CLIENT_ID = "985e8f22ccfd487cb4fb07eb22b32fe6"
    URL = "https://api.spotify.com/v1/artists/" + art_id

    cred = Credentials(SPOTIPY_CLIENT_ID, SPOTIPY_CLIENT_SECRET, 'lishash.com/about')
    token = cred.request_client_token()
    hed = {'Authorization': 'Bearer ' + str(token)}

    res = json.loads(requests.get(url=URL, headers=hed).text)
    if res['images']:
        img = res['images'][0]['url']
    else:
        img = None
    return img


def get_track2_from_id(token: str, track_id: str):
    track_music_tag = MusicTag.from_tdoc_id(track_id=track_id)
    # todo: for newly searched song, from apple music and add?

    track = Track2.from_music_tag(token=token, music_tag=track_music_tag)
    res = track.to_dict()
    return res


def get_fam_art_tracks(regex_query):
    # print("get_fam_art_tracks_search_started")
    tags = []
    track_pipeline = [{'$match': {'artists.name': regex_query}},
                      {"$limit": 4},
                      {"$sort": {"popularity": -1}},
                      {"$project": {"name": 1, "cover_image_url": 1, "artists": 1}},
                      ]
    cursor = mm.Track.objects.aggregate(*track_pipeline)

    for o in cursor:
        # print(o)
        _id = o['_id']
        artist_names = ', '.join([a['name'].title() for a in o['artists']])
        cover_image_url = o['cover_image_url'] if 'cover_image_url' in o else None
        tags.append({'id': _id,'name': o['name'], 'cover_image_url': cover_image_url,
                    'artists': artist_names})
    # print("art_tags_in_function: ", tags)
    return tags
