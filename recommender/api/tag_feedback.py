import db_manager as dm
from recommender import *

def tag_feedback(token:str, track_id:str, feedback:str, body):
  # Modify currency
  user = dm.UserManager.from_id(user_id=token)
  user.modify_currency(v=1)
  # Modify tag
  tag = MusicTag.from_json(body['musicTag'])
  if feedback == 'positive':   change = 50
  elif feedback == 'negative': change = -50
  else: raise ValueError(f'Feedback {feedback} not supported')
  up = {'$inc': {f'{tag.tag_type.db_field}.$.count': change}}
  db.get_tcol().update_one({
    '_id': ObjectId(track_id),
    f'{tag.tag_type.db_field}.name': tag.name,
  }, update=up)

