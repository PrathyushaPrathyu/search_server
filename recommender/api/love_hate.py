import neo_db as nd
from recommender import *
from .track2 import Track2
import db_manager as dm
from pprint import pprint

def tracks_for_improve_recommendation(token:str):
    print("getting tracks to improve recommendation")
    user_manager = dm.UserManager.from_id(user_id=token)
    neo_user = user_manager.neo_user

    disliked_tracks = nd.api_alpha.get_tracks_for_improve_recommendation(user=neo_user)
    res=[]
    for disliked_track in disliked_tracks:
        track_id = disliked_track.pop('disliked_mongo_id')
        music_tag = MusicTag.from_tdoc_id(track_id=track_id)
        track = Track2.from_music_tag(token=token, music_tag=music_tag)
        disliked_track['track'] = track.to_dict()
        res.append(disliked_track['track'])

    print("returning tracks")

    return res
