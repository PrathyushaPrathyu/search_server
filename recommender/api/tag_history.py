from recommender import *
import mongo_manager as mm
import neo_db as nd
from bson import ObjectId
from recommender.tag.db_enums import MeTagType


def get_user_tags(token:str, current_hour, max_top, max_recent):
  # TODO: Tag History Refactor
  tag_history_control = TagsHistoryController.load(token)
  # This will come from neo4j for now
  code = """
  MATCH (u:User{id: $id})-[r:LISTENED_TO]->(t) WHERE NOT 'Language' IN labels(t)
  WITH t, r, r.score AS score ORDER BY r.score DESC LIMIT 25
  RETURN t.name as name, labels(t) as labels, t.id as id ORDER BY r.last_listened DESC LIMIT 10
  """
  res = nd.api_alpha.read(code, id=token).data()

  tags = []
  for o in res:
      if "Artist" in o['labels']:
          tags.append(MusicTag(name=o['name'], id= o['id'], tag_type=MusicTagType.artists).to_json())
      elif "Track" in o['labels']:
          tags.append(MusicTag(name=o['name'], id= o['id'], tag_type=MusicTagType.track).to_json())
      elif "Album" in o['labels']:
          tags.append(MusicTag(name=o['name'], tag_type=MusicTagType.album).to_json())
      elif "About" in o['labels']:
          tags.append(MusicTag(name=o['name'], tag_type=MusicTagType.about).to_json())
      elif "Instrument" in o['labels']:
          tags.append(MusicTag(name=o['name'], tag_type=MusicTagType.instruments).to_json())
      elif "Genre" in o['labels']:
          tags.append(MusicTag(name=o['name'], tag_type=MusicTagType.genres).to_json())

  if not tags:
      tags = [MusicTag(name=o, tag_type=MusicTagType.genres).to_json()
              for o in mm.UserInfo.objects().get(id=ObjectId(token)).base_pref['genre']]
  music_tags = {
      'favoritesMusicTags': tags,
      'recentMusicTags': [],
    }

  # This will come from mongodb for now

  # pipeline = [
  #   {"$group": {"_id": "$context", "time": {"$avg": "$time.user.hour"}}},
  #   {"$project": {"diff": {"$abs": {"$subtract": [current_hour, "$time"]}}}},
  #   {"$sort": {"diff": 1}},
  #   {"$limit": 2}
  # ]

  pipeline = [
      {"$match": {"user_id": token}},
      {"$match": {"context.name": {"$ne": "UNSPECIFIED"}}},
      {"$group": {"_id": "$context", "min_diff": {"$min": {"$abs": {"$subtract": [current_hour, "$time.user.hour"]}}},
                  "hour": {"$avg": "$time.user.hour"}, "latest": {"$max": "$time.server"}}},
      {"$addFields": {"diff": {"$abs": {"$subtract": [current_hour, "$hour"]}}}},
      {"$sort": {"min_diff": 1, "latest": -1, "diff": 1}},
      {"$limit": 3},
  ]

  me_tags = set([MeTag(name=hdoc['_id']['name'], type=hdoc['_id']['type']) for hdoc in mm.History.objects.aggregate(*pipeline)])

  if len(me_tags) < 3:
    for o in mm.UserInfo.objects().get(id=ObjectId(token)).base_pref['context']:
        me_tags.add(MeTag(name=o['name'], type=MeTagType[o['type']]))

  me_tags = [o.to_json() for o in me_tags]

  me_tags = {
    'favoritesMeTags': me_tags[:3],
    'recentMeTags': me_tags[:3],
  }

  return {
    **music_tags,
    **me_tags,
  }

def register_tags(token:str, body):

  udoc = mm.UserInfo.objects().get(id=ObjectId(token))
  music_tags = MusicTag.from_list_json(body.get('musicTags', []))
  me_tags = MeTag.from_list_json(body.get('meTags', []))

  base_genres = []
  base_languages = []
  base_contexts = [o.to_json() for o in me_tags]

  for o in music_tags:
    if o.tag_type == MusicTagType.genres: base_genres.append(o.name)
    elif o.tag_type == MusicTagType.languages: base_languages.append(o.name)

  base_pref = {'genre': base_genres, 'language': base_languages, 'context': base_contexts}
  udoc.base_pref = base_pref
  udoc.has_spotify = body.get('hasSpotify', False)
  udoc.has_apple_music = body.get('hasAppleMusic', False)
  udoc.save()
