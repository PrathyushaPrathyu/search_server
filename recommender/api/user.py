from recommender import *


# TODO: Use db.keys instead of hardcoded fields
# TODO: Hardcoded last_arm
_immediate_arm = bandit.Arm(mode=enums.ModeTag.immediate, interest=enums.InterestTag.immediate)
def new_user_doc(username:str) -> db.DBOperation:
  udoc = {
    'username': f'{username}',
    'current_country': None,
    'music': {
      'session': {'tracks': [], 'context': '', 'last_arm': _immediate_arm.to_dict()},
      'contexts': [],
      'history': [],
    },
  }
  return db.DBOperation(udoc)
