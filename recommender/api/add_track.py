from recommender import api
import neo_db as nd


def check_and_add_new_track_to_db(trackId:str):
    print("inside check_and_add_new_track_to_db for track " +  trackId )
    isrc = trackId.split('+')[0]

    #todo:call function to fetch track from mongo DB if it exists; if isrc exists, then just get id..otherwise below lines to add to db's
    mongo_id = api.Track2.get_track_from_isrc(isrc=isrc)

    if (mongo_id is None):
        sampled_track_id, name_lower, popularity, genre, cover_image_url = api.get_id_from_am_api(isrc=isrc)
        nd.api_alpha.load_track_to_neo4j(_id=str(sampled_track_id), name_lower=name_lower, popularity=popularity,
                                         genre=genre, cover_image_url=cover_image_url)
        tid = sampled_track_id

    else:
        tid = mongo_id

    return str(tid)

def add_or_get_track_for_song(token:str, trackId: str):
    #writing to mongoDB and Neo4j if trackId was passed to search with a + after ISRC
    if '+' in trackId:
        print("track does not exist...adding")
        sampled_track_id = check_and_add_new_track_to_db(trackId=trackId)
        # isrc = trackId.split('+')[0]
        #
        # sampled_track_id, name_lower, popularity, genre, cover_image_url = api.get_id_from_am_api(isrc=isrc)
        # nd.api_alpha.load_track_to_neo4j(_id=str(sampled_track_id), name_lower=name_lower, popularity=popularity, genre=genre, cover_image_url=cover_image_url)

        new_id = sampled_track_id

        #music_tag.id = sampled_track_id
        #self.tag_filters[i] = music_tag
    else:
        print("track already exists")
        new_id = trackId

    #to get the track2 for the trackId
    res = api.get_track2_from_id(token=token, track_id=new_id)


    return res
