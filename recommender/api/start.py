from recommender import *
from .track2 import Track2
from recommender import api_alpha
import  neo_db as nd
import db_manager as dm

def start(token:str, body):
  try:
    session_id = body['sessionId'] if ('sessionId' in body) else None
    reset = body['reset'] if ('reset' in body) else False
    #todo check lastupdated date time in playstore...for every 2 hours,decrement 2 currency
    user_manager = dm.UserManager.from_id(user_id=token)
    fire_user = user_manager.fire_user
    fire_user.last_login_deduct_currency()

    alpha_playback = api_alpha.AlphaPlayback.from_body(user_id=token, body=body, session_id=session_id)
    track_id, session_id, instructions, shared_by = alpha_playback.start(reset=reset)
    track_music_tag = MusicTag.from_tdoc_id(track_id=track_id)

    if (not shared_by):
      code = """
      MATCH (friend:User)-[r:SHARES_TRACK{track_mongo_id:$track_id}]->(u:User {id: $user_id})
      WHERE NOT (u)-[:LISTENED_TO]->(:Track {id: $track_id})
      RETURN r.track_mongo_id as tid, friend.username as friend_name,friend.id as friend_id
      """
      res = nd.api_alpha.read(code, user_id=token, track_id=track_id).data()
      shared_by = []
      for o in res:
        shared_by.append({'friend': o['friend_name'], 'friendId': o['friend_id']})


    track = Track2.from_music_tag(token=token, music_tag=track_music_tag, shared_by=shared_by)
    res = {
      'track': track.to_dict(),
      'success': True,
      'sessionId': session_id,
      'instructions': instructions,
    }
    return res

  except NoTracksAvailable:
    return {'success': False}


