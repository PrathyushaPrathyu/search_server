from recommender import *

# def mark_as_played(tdoc:dict) -> db.DBOperation:
#   return db.DBOperation({'$addToSet': {db.PLAYED_TRACKS: tdoc['seq_id']}})

# def mark_as_played2(tdoc:dict) -> db.DBOperation:
#   return db.DBOperation({'$addToSet': {db.PLAYED_TRACKS: str(tdoc[TSPACE_ID_FIELD])}})

# TODO: Temporary hack, should use ImmediateInterest
# def update_seed_track(udoc:dict, tdoc:dict, score:float) -> db.DBOperation:
#   current_seed = db.get_seed_track(udoc)
#   if current_seed and score < current_seed['score']:
#    return db.DBOperation()
#   seed_track = {'_id': str(tdoc[TSPACE_ID_FIELD]), 'score': score}
#   return db.DBOperation({'$set': {db.SEED_TRACK: seed_track}})

# def register_last_arm(arm) -> db.DBOperation:
#   return db.DBOperation({'$set': {db.LAST_ARM: arm.to_dict()}})

# def add_bonus_reward(v:float) -> db.DBOperation:
#   return db.DBOperation({'$inc': {db.BONUS_REWARD: v}})

# def clear_bonus_reward() -> db.DBOperation:
#   return db.DBOperation({'$set': {db.BONUS_REWARD: 0}})

# def register_context_op(context:str) -> db.DBOperation:
#     return db.DBOperation({'$set': {db.CURRENT_CONTEXT: context}, '$addToSet': {db.PAST_CONTEXTS: context}})

# def new_session() -> db.DBOperation:
#   return db.DBOperation(
#     {'$set': {
#       'music.session.tracks': [],
#       'music.session.bonus_reward': 0,
#       db.SEED_TRACK: None,
#       }
#     }
#   )

