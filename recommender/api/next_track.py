from recommender import *

@log_time
def next_track3(token:str, last_track_id:str, body:dict):
  logger.debug('Next track called')
  playback_control = PlaybackController.from_body(user_id=token, body=body)
  if body['forceSeedTrackUpdate']: new_seed_track_id = last_track_id
  else: new_seed_track_id=None
  track = playback_control.next_track(
    primary_mode=PrimaryMode[body['primaryMode']],
    secondary_mode=SecondaryMode[body['secondaryMode']],
    new_seed_track_id=new_seed_track_id,
  ).to_json()

  # OPTIMIZE: This lines can be executed after the response has been sent
  playback_control.save()
  # Register tags
  tag_history_control = TagsHistoryController.load(user_id=token)
  music_tags = MusicTag.from_list_json(body['musicTags'])
  me_tags = MeTag.from_list_json(body['meTags'])
  tag_history_control.register_tags(music_tags)
  tag_history_control.register_tags(me_tags)
  tag_history_control.save(user_id=token)

  return {'track': track, 'instructions': playback_control.instructions_recorder.to_dict()}
