# from datetime import datetime
#
# from recommender import *
#
# def parse_info(func) -> callable:
#   def _wrapper(*args, info:dict, **kwargs):
#     print('Registering history')
#     info = {
#       'client_time': datetime.strptime(info['time'], '%Y-%m-%d %H:%M:%S.%f'),
#       'utc_time': datetime.utcnow(),
#     }
#     return func(*args, **kwargs, info=info)
#   return _wrapper
#
#
# @parse_info
# def add_history_sign_in(username:str, info:dict) -> db.DBOperation:
#   return add_to_history({
#     'action': 'sign_in',
#     'username': username,
#   }, info)
#
# @parse_info
# def add_history_sign_up(username:str, info:dict) -> db.DBOperation:
#   return add_to_history({
#     'action': 'sign_up',
#     'username': username,
#   }, info)
#
# @parse_info
# def add_history_right_swipe(track_id:str, love_count:int, play_ratio:float, info:dict) -> db.DBOperation:
#   return add_to_history({
#     'action': 'right_swipe',
#     'track_id': track_id,
#     'love_count': love_count,
#     'play_ratio': play_ratio,
#   }, info)
#
# @parse_info
# def add_history_left_swipe(track_id:str, love_count:int, play_ratio:float, info:dict) -> db.DBOperation:
#   return add_to_history({
#     'action': 'left_swipe',
#     'track_id': track_id,
#     'love_count': love_count,
#     'play_ratio': play_ratio,
#   }, info)
#
# # @parse_info
# # def add_history_complete(track_id:str, love_count:int, info:dict) -> db.DBOperation:
# #   return add_to_history({
# #     'action': 'complete',
# #     'track_id': track_id,
# #     'love_count': love_count,
# #   }, info)
#
# @parse_info
# def add_history_track_search(track_id:str, info:dict) -> db.DBOperation:
#   return add_to_history({
#     'action': 'track_search',
#     'track_id': track_id,
#   }, info)
#
# @parse_info
# def add_history_start_from_features(features:str, info:dict) -> db.DBOperation:
#   return add_to_history({
#     'action': 'start_from_features',
#     'features': features,
#   }, info)
#
# @parse_info
# def add_history_register_context(context:str, info:dict) -> db.DBOperation:
#   return add_to_history({
#     'action': 'register_context',
#     'context': context,
#   }, info)
#
# @parse_info
# def add_history_start_from_context(context:str, info:dict) -> db.DBOperation:
#   return add_to_history({
#     'action': 'start_from_context',
#     'context': context,
#   }, info)
#
# @parse_info
# def add_history_start_from_nothing(info:dict) -> db.DBOperation:
#   return add_to_history({
#     'action': 'start_from_nothing',
#   }, info)
#
# def add_to_history(doc:dict, info:dict) -> db.DBOperation:
#   doc.update(info)
#   return db.DBOperation({'$push': {'music.history': doc}})
