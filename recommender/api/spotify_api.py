import requests
from tekore.auth import Credentials
import json
import mongo_manager as mm

def get_spotify_info_using_isrc(term):
    SPOTIPY_CLIENT_SECRET = "8a375348c3d341cd918ad0ccc02ef658"
    SPOTIPY_CLIENT_ID = "985e8f22ccfd487cb4fb07eb22b32fe6"
    URL = "https://api.spotify.com/v1/search?type=track&q=isrc:" + term
    cred = Credentials(SPOTIPY_CLIENT_ID, SPOTIPY_CLIENT_SECRET, 'lishash.com/about')
    token = cred.request_client_token()
    hed = {'Authorization': 'Bearer ' + str(token)}
    PARAMS = {'id': term}
    res = json.loads(requests.get(url=URL, headers=hed).text)
    uri = ""
    popularity = 0
    for each in res['tracks']['items']:
        uri = each['uri']
        popularity = each['popularity']
    return uri, popularity

def check_if_isrc_exists_in_spotify(isrc):
    SPOTIPY_CLIENT_SECRET = "8a375348c3d341cd918ad0ccc02ef658"
    SPOTIPY_CLIENT_ID = "985e8f22ccfd487cb4fb07eb22b32fe6"
    URL_isrc = "https://api.spotify.com/v1/search?type=track&q=isrc:" + isrc
    cred = Credentials(SPOTIPY_CLIENT_ID, SPOTIPY_CLIENT_SECRET, 'lishash.com/about')
    token = cred.request_client_token()
    hed = {'Authorization': 'Bearer ' + str(token)}
    res_isrc = json.loads(requests.get(url=URL_isrc, headers=hed).text)
    res_list = res_isrc['tracks']['items']
    return res_list

def add_new_track_using_spotify_with_isrc(isrc):

    seq_id_pipeline = [
        {"$sort": {"seq_id": -1}},
        {"$limit": 1},
        {"$project": {"seq_id": 1, "_id": 0}}]
    seq_id = list(mm.Track.objects().aggregate(*seq_id_pipeline))[0]['seq_id']
    seq_id += 1

    SPOTIPY_CLIENT_SECRET = "8a375348c3d341cd918ad0ccc02ef658"
    SPOTIPY_CLIENT_ID = "985e8f22ccfd487cb4fb07eb22b32fe6"
    URL_isrc = "https://api.spotify.com/v1/search?type=track&q=isrc:" + isrc
    cred = Credentials(SPOTIPY_CLIENT_ID, SPOTIPY_CLIENT_SECRET, 'lishash.com/about')
    token = cred.request_client_token()
    hed = {'Authorization': 'Bearer ' + str(token)}
    res_isrc = json.loads(requests.get(url=URL_isrc, headers=hed).text)

    if len(res_isrc['tracks']['items']) > 1:
        add_track_later_list = res_isrc['tracks']['items']
    else:
        add_track_later_list = []

    res_list = res_isrc['tracks']['items']
    name = res_list[0]['name']
    name_lower = name.lower()
    spotify_uri = res_list[0]['uri']
    popularity = res_list[0]['popularity']
    duration_ms = res_list[0]["duration_ms"]
    preview_url = res_list[0]["preview_url"]
    cover_image_url = res_list[0]['album']['images'][0]['url']
    release_date = res_list[0]['album']['release_date'] if 'release_date' in res_list[0]['album'] else None
    release_year = release_date.split("-")[0] if 'release_date' in res_list[0]['album'] else None
    gen_list = []
    art_list = []
    for art in res_list[0]['artists']:
        art_list.append({"name": art['name'].lower(), "id": art['id']})
    print(art_list)
    valence = 3
    loudness = 5
    deepness = 2
    speechiness = 0
    embed = [0.0, 0.0, 0.0, 0.0, 0.0]

    new_track_ses_doc = mm.Track(uri=spotify_uri,
                                 name=name,
                                 name_lower=name_lower,
                                 popularity=popularity,
                                 valence=valence,
                                 loudness=loudness,

                                 deepness=deepness,
                                 embed=embed,
                                 artists=art_list,
                                 genres=gen_list,
                                 languages=[],

                                 about=[],
                                 instruments=[],
                                 speechiness=speechiness,
                                 seq_id=seq_id,
                                 album=[],

                                 duration_ms=duration_ms,
                                 release_date=release_date,
                                 preview_url=preview_url,
                                 cover_image_url=cover_image_url,
                                 art_gens=[],

                                 release_year=release_year,
                                 applemusic_uri=None,
                                 isrc=isrc)
    new_track_ses_doc.save()
    track_id = str(new_track_ses_doc.id)

    return track_id, name_lower, popularity, gen_list, cover_image_url,add_track_later_list