import db_manager as dm

def reset_listening_history(token:str):
    dm.UserManager.from_id(user_id=token).reset_listening_history()