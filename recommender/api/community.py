import neo_db as nd

def create_community(token:str, community_name):
  user = nd.User.from_mongo_id(mongo_id=token)
  community = nd.Community(name=community_name).create(person=user)

def join_community(token:str, community_name):
  user = nd.User.from_mongo_id(mongo_id=token)
  community = nd.Community.from_name(name=community_name)
  community.add_user(user=user)

def add_community_post(token:str, community_name, body):
  user = nd.User.from_mongo_id(mongo_id=token)
  community = nd.Community.from_name(name=community_name)
  post = nd.CommunityPost(person=user, title=body['title'], content=body['content']).create()
  community.add_post(post=post)

def community_posts(token:str, community_name):
  community = nd.Community.from_name(name=community_name)
  return community.to_dict(simplified=False)

def community_users(token:str, community_name):
  community = nd.Community.from_name(name=community_name)
  return [o.to_dict(simplified=True) for o in community.get_users()]

def get_communities(token:str):
  communities = nd.Community.get_communities()
  return [o.to_dict(simplified=True) for o in communities]
