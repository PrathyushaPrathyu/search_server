import mongo_manager as mm
from recommender import *
from recommender.controller import ContextController


# This class just works as an adapter
class SeedTrackController:
  def __init__(self, seed_track_model=None):
    self.seed_track_model = seed_track_model or mm.SeedTrackController

  @property
  def seed_tdoc(self): return self.seed_track_model.seed_tdoc

  @property
  def seed_v(self): return np.array(self.seed_track_model.seed_v)

  def reset_session(self):
    return self.seed_track_model.reset_session()

  def get_seed_v(self):
    try: return self.seed_track_model.get_seed_v()
    except mm.errors.SeedTrackNotAvailable: raise SeedTrackNotAvailable

  def register_action(
      self,
      action:Action,
      context_control:ContextController,
      feat_preference_control:FeaturePreferenceController,
      embedder:TrackEmbedder,
  ):
    seed_track = context_control.seed_track
    if seed_track:
      #TODO: REFACTOR we already have a mm method for this right?
      seed_tdoc = mm.Track.from_id(id=seed_track.id).to_dict()
      seed_feats = embedder.tdoc2feats(seed_tdoc)
      seed_v = embedder.feats2v(seed_feats)
    else:
      seed_tdoc = None
      seed_v = None
    return self.seed_track_model.register_action(seed_tdoc=seed_tdoc, seed_v=seed_v, action=action.name)

  def save(self, user_id):
    return self.seed_track_model.save()

  def register_seed_track(self, seed_tdoc, embedder:TrackEmbedder):
    seed_feats = embedder.tdoc2feats(seed_tdoc)
    seed_v = embedder.feats2v(seed_feats)
    self.seed_track_model.set_seed(seed_tdoc=seed_tdoc, seed_v=seed_v)

  @classmethod
  def load(cls, user_id):
    seed_track_model = mm.SeedTrackController.load(user_id=user_id)
    return cls(seed_track_model=seed_track_model)

# from recommender import *
# from recommender.controller import ContextController
#
#
# class SeedTrackController(Serializable2):
#   base_path = Path(f'/mnt/disks/dev/tmp/seed_track_controller')
#   def __init__(self):
#     super().__init__()
#     self.seed_tdoc = None
#     self.seed_v = None
#
#   def reset_session(self):
#     self.seed_v = None
#     self.seed_tdoc = None
#
#   def get_seed_v(self):
#     if self.seed_v is None: raise SeedTrackNotAvailable
#     return self.seed_v
#
#   def register_action(
#       self,
#       action:Action,
#       context_control:ContextController,
#       feat_preference_control:FeaturePreferenceController,
#       embedder:TrackEmbedder,
#   ):
#     seed_track = context_control.seed_track
#     if seed_track is None: return
#     seed_tdoc = db.get_tdoc_from_tspace_key(seed_track.id)
#     seed_feats = embedder.tdoc2feats(seed_tdoc)
#     # TODO: For showing artists
#     if action == LeftSwipe:
#       pass
#       # seed_feats.update(feat_preference_control.sample())
#     self.seed_tdoc = seed_tdoc
#     self.seed_v = embedder.feats2v(seed_feats)
#
#   def save(self, user_id):
#     return super().save(self.base_path/user_id)
#
#   @classmethod
#   def load(cls, user_id):
#     try:
#       obj = super(SeedTrackController, cls).from_file(cls.base_path/user_id)
#       if obj.seed_tdoc is not None: logger.debug('Seed Track: %s', obj.seed_tdoc['name'])
#       return obj
#
#     except FileNotFoundError:
#       return cls()
