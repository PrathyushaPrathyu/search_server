from recommender.enums import *

class Arm:
  def __init__(self, mode:ModeTag, interest:InterestTag):
    self.mode, self.interest = mode, interest

  @property
  def name(self): return f'{self.mode.name}_{self.interest.name}'

  def to_dict(self):
    return dict(mode=self.mode.name, interest=self.interest.name)

  @classmethod
  def from_dict(cls, x:dict):
    return cls(mode=ModeTag[x['mode']], interest=InterestTag[x['interest']])

  def __eq__(self, other):
    return isinstance(other, Arm) and other.mode == self.mode and other.interest == self.interest

  def __hash__(self):
    return hash((self.mode, self.interest))

  def __repr__(self): return f'<Arm: (mode:{self.mode.name}, interest:{self.interest.name})>'


