import numpy as np
import matplotlib.pyplot as plt
from enum import Enum

from recommender.utils import ContextSerializable, ClassNameSerializableMixin


class Bandit(ClassNameSerializableMixin, ContextSerializable):
  def __init__(self, arms):
    self.arms = arms

  def sample_arm(self):
    samples = {k: v.sample() for k, v in self.arms.items()}
    arm = max(samples, key=lambda x: samples[x])
    dist = self.arms[arm]
    return arm, dist

  def update_arm(self, arm, reward):
    return self.arms[arm].update(reward=reward)

  def set_arm(self, arm, reward):
    return self.arms[arm].set(reward=reward)

  def plot(self):
    fig, ax = plt.subplots()
    for arm, dist in self.arms.items(): self._plot_dist(ax=ax, label=arm.name, dist=dist)
    ax.legend(loc='best', frameon=False)
    return fig, ax

  def print_arms(self):
    for arm, dist in self.arms.items():
      print(f'{arm.name}: {dist.mean}')

  def _plot_dist(self, ax, label, dist):
    # x = np.linspace(dist.ppf(0.01), dist.ppf(0.99), 100)
    x = np.linspace(0, 1, 200)
    y = dist.pdf(x)
    ax.set_ylim([0, 4])
    ax.plot(x, y, label=label)

