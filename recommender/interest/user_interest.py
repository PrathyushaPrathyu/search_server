from abc import ABC

from .interest import *
from recommender.index import Index

class GlobalInterestMixin(InfiniteInterest, ABC):
  def context_save(self, user_id, context):
    return super().context_save(user_id=user_id, context='global')

  @classmethod
  def from_context(cls, user_id, context):
    return super(GlobalInterestMixin, cls).from_context(user_id=user_id, context='global')

class AdaptiveTopNMixin(InfiniteInterest, ABC):
  @property
  def top_n(self):
    x = int(len(self._data) * 0.2)
    x = max(x, 5)
    return min(self._top_n, x)

class GlobalLongInterest(AdaptiveTopNMixin, GlobalInterestMixin, InfiniteInterest):
  def __init__(self): super().__init__(top_n=50)

class GlobalShortInterest(GlobalInterestMixin, HorizonInterest):
  def __init__(self): super().__init__(top_n=10, horizon=30)

class SpecificLongInterest(AdaptiveTopNMixin, InfiniteInterest):
  def __init__(self): super().__init__(top_n=25)

class SpecificShortInterest(HorizonInterest):
  def __init__(self): super().__init__(top_n=5, horizon=10)

class ImmediateInterest(Interest):
  def __init__(self):
    self.__last = None
    self._last_reward = -np.inf

  @property
  def favorites(self): return {}

  @property
  def _last(self):
    if self.__last is None: raise EmptyInterestError('Interest is empty')
    return self.__last

  def sample(self): return self._last

  def sample_near(self, index:Index, played:list, n:int=1):
    idxs, dists = index.get_nns_by_name(self._last, n=n+len(played))
    idxs = list(set(idxs) - set(played))
    return np.random.choice(idxs)

  def register_action(self, action: Action, track_id: str):
    reward = reward_from_action(action)
    if action == Search: self._update_last(track_id=track_id, reward=reward)
    if action == RightSwipe or action == Complete:
      if reward >= self._last_reward: self._update_last(track_id=track_id, reward=reward)

  def register_right(self, id, reward): self.__last = id

  def register_left(self, id, reward): pass

  def _update_last(self, track_id:str, reward:float):
    self.__last = track_id
    self._last_reward = reward

  def __contains__(self, item): return False
