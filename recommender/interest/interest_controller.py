from recommender import *
from .interest import *


class InterestController:
  def __init__(self, interests:Dict[InterestTag, Interest]):
    self.interests = interests

  @property
  def seed_track(self):
    return self.interests[InterestTag.immediate].sample()

  @property
  def history(self):
    return self.interests[InterestTag.glt].history

  def get_history(self, interest:InterestTag=InterestTag.glt):
    return self.interests[interest].history

  def get_scores(self, interest:InterestTag, track_ids:List[str]):
    return self.interests[interest].get_scores(track_ids=track_ids)

  def register_action(self, action:Action, track_id:str):
    for k, v in self.interests.items(): v.register_action(action=action, track_id=track_id)

