import numpy as np

from sortedcollections import *
from collections import defaultdict

from recommender import *
from recommender.errors import *
from recommender.index import *
from recommender.utils import *
from recommender.utils.data_structs import Deque


# TODO: Maybe we should have left, right and complete?
# TODO: Maybe we should store the action obj directly
class TrackItem:
  def __init__(self, reward, left, right):
    self.reward,self.left,self.right = reward,left,right

  @property
  def score(self):
    return self.reward

  def __repr__(self):
    return f'<TrackItem (score:{self.score:.2f}, right:{self.right}, left:{self.left})>'

class Interest(ClassNameSerializableMixin, ContextSerializable):
  def __contains__(self, item):         raise NotImplementedError
  def sample(self):                     raise NotImplementedError
  def register_right(self, id, reward): raise NotImplementedError
  def register_left(self, id, reward):  raise NotImplementedError
  def register_action(self, action:Action, track_id:str): raise NotImplementedError
  def sample_near(self, id, index:Index, played:list, n:int=10): raise NotImplementedError
  @property
  def favorites(self): raise NotImplementedError

class InfiniteInterest(Interest):
  def __init__(self, top_n:int):
    self._top_n = top_n
    self._data = defaultdict(lambda: TrackItem(reward=0, left=0, right=0))
    self._sorted = ValueSortedDict()

  def __contains__(self, item) -> bool:
    try:                       return item in self.get_items()
    except EmptyInterestError: return False

  @property
  def top_n(self): return self._top_n

  @property
  def favorites(self):
    i = self._sorted.bisect_key_left(2.0)
    return {k: self._data[k].score for k in self._sorted.keys()[i:]}

  @property
  def history(self): return list(self._data)

  def get_scores(self, track_ids:List[str]):
    return {k: self._data[k].score for k in track_ids}

  def register_action(self, action:Action, track_id:str):
    item = self._data[track_id]
    r = reward_from_action(action=action)
    item.reward += r
    if action == RightSwipe or action == Complete: item.right += 1
    if action == LeftSwipe: item.left += 1
    self._sorted[track_id] = item.score

  def register_right(self, id, reward):
    item = self._data[id]
    item.right += 1
    item.reward += reward
    self._sorted[id] = item.score

  def register_left(self, id, reward):
    item = self._data[id]
    item.left += 1
    item.reward += reward
    self._sorted[id] = item.score

  def sample(self):
    seq_ids, items = zip(*list(self.get_items().items()))
    weights = np.array([max(o.score, 1e-6) for o in items])
    return np.random.choice(seq_ids, p=weights/weights.sum())

  def get_items(self):
    ids = self._sorted.keys()[::-1][:self.top_n]
    if len(ids) == 0: raise EmptyInterestError('Interest is empty')
    return {i: self._data[i] for i in ids}

  def remove(self, id):
    del self._data[id]
    del self._sorted[id]

  def sample_near(self, id, index:Index, played:list, n:int=10):
    # Create index only with songs that are from this interest
    tmp_index = IndexMap(FaissIndex(dim=index.dim))
    for i in self.get_items():
      try:
        v = index.get_item_vector(i)
        tmp_index.add_one(name=i, v=v)
      except KeyError:
        print(f'{i} not found in index')
    # Get nns
    v = index.get_item_vector(id)
    idxs, dists = tmp_index.get_nns_by_vector(v=v, n=n+len(played))
    idxs = list(set(idxs) - set(played))
    if not idxs: raise ExhaustedTracksError('All tracks from this interest have already been played in this session')
    return self._sample_weighted(names=idxs)

  def _sample_weighted(self, names):
    items = [self._data[i] for i in names]
    weights = np.array([o.score for o in items])
    return np.random.choice(names, p=weights/weights.sum())

class HorizonInterest(InfiniteInterest):
  def __init__(self, top_n:int, horizon:int):
    super().__init__(top_n=top_n)
    self.horizon = horizon
    self._history = Deque(maxlen=horizon)

  def register_right(self, id, reward):
    self._update_history(id=id)
    super().register_right(id=id, reward=reward)

  def register_left(self, id, reward):
    self._update_history(id=id)
    super().register_left(id=id, reward=reward)

  def _update_history(self, id):
    removed = self._history.add(id)
    if removed is not None: self.remove(id=removed)


