from bson import ObjectId

from .model import Model


class TrackModel(Model):
  def __init__(self, lishash_id:str, spotify_id:str):
    self.lishash_id, self.spotify_id = lishash_id, spotify_id

  def to_dict(self) -> dict:
    return dict(lishashId=self.lishash_id, spotifyId=self.spotify_id)

  @classmethod
  def from_tdoc(cls, tdoc):
    spotify_id = tdoc['most_popular_track']['_id'].split(':')[-1]
    return cls(lishash_id=str(tdoc['_id']), spotify_id=spotify_id)

  @classmethod
  def from_seq_id(cls, seq_id:int):
    tdoc = db.get_track_col().find_one({'seq_id': int(seq_id)})
    return cls.from_tdoc(tdoc)

class TrackModel2(Model):
  def __init__(self, name:str, id:str, uri:str):
    self.tagType, self.tagSubtype = 'music', 'track'
    self.name,self.id,self.uri = name,id,uri

  def to_dict(self) -> dict:
    return self.__dict__

  @classmethod
  def from_tdoc(cls, tdoc):
    return cls(name=tdoc['name'], id=str(tdoc['_id']), uri=tdoc['uri'])

