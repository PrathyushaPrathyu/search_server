from recommender.tag import MusicTag

class Action:
  def __init__(self, love:float=None, play_ratio:float=None):
    self.love, self.play_ratio = love, play_ratio

  @property
  def name(self): return self.__class__.__name__

  def __repr__(self):
    attrs = ', '.join([f'{k}: {v}' for k, v in self.__dict__.items() if v])
    return f'<{self.name}({attrs})>'

  def __eq__(self, other):
    return isinstance(self, other)

  def __hash__(self):
    return hash(self.__class__)

  @staticmethod
  def get_values(): return [Love, RightSwipe, LeftSwipe, Complete, Search]

class Love(Action):
  def __init__(self, love:float):
    super().__init__(love=love)

class RightSwipe(Action):
  def __init__(self, love:float, play_ratio:float):
    super().__init__(love=love, play_ratio=play_ratio)

class LeftSwipe(Action):
  def __init__(self, love:float, play_ratio:float):
    super().__init__(love=love, play_ratio=play_ratio)

class Complete(Action):
  def __init__(self, love:float):
    super().__init__(love=love)

class Search(Action):
  def __init__(self, tag:MusicTag = None):
    super().__init__()
    self.tag = tag
