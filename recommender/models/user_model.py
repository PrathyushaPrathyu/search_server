from recommender import db
from .user_keys import UserKeys


class UserModel:
  def __init__(self, _id: str, username: str, friends_recom):
    self.writer = db.UserDBWriter(_id=_id)
    self.username = username
    self.friends_recom = friends_recom

  def register_update(self, op: db.DBOperation) -> None:
    self.writer.register_update(op=op)

  def save(self):
    self.writer.write()

  @classmethod
  def from_token(cls, token: str):
    udoc = db.get_udoc(token=token)
    return cls.from_udoc(udoc=udoc)

  @classmethod
  def from_udoc(cls, udoc):
    return cls(
      _id=str(udoc['_id']),
      username=db.get_nested_key(d=udoc, k=UserKeys.username),
      friends_recom=db.get_nested_key(d=udoc, k=UserKeys.friends_recom, default=[])
    )
