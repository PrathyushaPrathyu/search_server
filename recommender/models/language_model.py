class LanguageModel :
  def __init__(self, name:str, id:str):
    self.tagType, self.tagSubtype = 'music', 'language'
    self.name,self.id = name,id

  def to_dict(self) -> dict:
    return self.__dict__

  @classmethod
  def from_tdoc(cls, doc):
    return cls(name=doc['name_lower'], id=str(doc['_id']))

