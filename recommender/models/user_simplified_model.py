# from recommender import *
# from recommender import db
#
# from .user_keys import UserKeys
#
#
# class UserSimplifiedModel(SerializableClass):
#   def __init__(self, _id: str, username: str):
#     super().__init__()
#     self._writer = db.UserDBWriter(_id=_id)
#     self._id = _id
#     self.username = username
#
#   def register_update(self, op: db.DBOperation) -> None:
#     self._writer.register_update(op=op)
#
#   def save(self):
#     self._writer.write()
#
#   @classmethod
#   def from_token(cls, token: str):
#     udoc = db.get_udoc(token=token)
#     return cls.from_dict(udoc=udoc)
#
#   @classmethod
#   def from_dict(cls, d:dict):
#     return cls(
#       _id=str(d['_id']),
#       username=db.get_nested_key(d=d, k=UserKeys.username),
#     )
