from recommender import *


class BaseFeaturePreference(Serializable2, ABC):
  """
  Records what feature level the user likes to listen to. With sampling functionallity.
  """
  base_path = Path(f'/mnt/disks/dev/tmp/feature_preference')
  def __init__(self, user_id:str, context:str=None):
    super().__init__()
    self.user_id, self.context = user_id, context
    self._prefs = (1./self.n) * np.ones(self.n)

  def __repr__(self): return f'<{self.__class__.__name__}: ({self.probs})>'

  @property
  def n(self) -> int: raise NotImplementedError

  def register_right(self, i, bonus:float=0):
    self._prefs[int(i)-1] += 1 + bonus

  def register_left(self, i):
    self._prefs[int(i)-1] -= 1

  def register_action(self, action:Action, i):
    if action == RightSwipe or action == Complete:
      self.register_right(i=i, bonus=action.love*5)
    if action == Search:
      self.register_right(i=i, bonus=3.0)
    if action == LeftSwipe:
      self.register_left(i=i)

  @property
  def probs(self):
    weights = self._prefs - self._prefs.min() + (1./self.n)
    return weights / weights.sum()

  def sample(self):
    return np.random.choice(self.n, p=self.probs) + 1

  def save_as_context(self):
    if not self.context: raise ValueError('Context cannot be None')
    return self.save(self._get_context_path(user_id=self.user_id, context=self.context))

  def save_as_global(self):
    return self.save(self._get_global_path(user_id=self.user_id))

  @classmethod
  def _load(cls, path:Path, user_id:str, context:str):
    try:                      return super(BaseFeaturePreference, cls).from_file(path)
    except FileNotFoundError: return cls(user_id=user_id, context=context)

  @classmethod
  def from_context(cls, user_id:str, context:str):
    return cls._load(
      path=cls._get_context_path(user_id=user_id, context=context),
      user_id=user_id,
      context=context,
    )

  @classmethod
  def from_global(cls, user_id:str):
    return cls._load(
      path=cls._get_global_path(user_id=user_id),
      user_id=user_id,
      context=None,
    )

  @staticmethod
  def _get_context_path(user_id:str, context:str): return BaseFeaturePreference.base_path/f'{user_id}/context/{context}'

  @staticmethod
  def _get_global_path(user_id:str): return BaseFeaturePreference.base_path/f'{user_id}/global'


class ValencePreference(BaseFeaturePreference):
  @property
  def n(self) -> int: return 5

class LoudnessPreference(BaseFeaturePreference):
  @property
  def n(self) -> int: return 8

class DeepnessPreference(BaseFeaturePreference):
  @property
  def n(self) -> int: return 4
