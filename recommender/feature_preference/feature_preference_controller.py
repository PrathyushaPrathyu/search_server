from recommender import *
from .feature_preference import *



# DEPRECATED
class FeaturesPreference(Serializable2):
  base_path = Path(f'/mnt/disks/dev/tmp/features_preference')
  def __init__(self, user_id:str, context:str=None):
    super().__init__()
    self.user_id, self.context = user_id, context
    self.feats = {
      Features.valence: ValencePreference(user_id=user_id, context=context),
      Features.loudness: LoudnessPreference(user_id=user_id, context=context),
      Features.deepness: DeepnessPreference(user_id=user_id, context=context),
    }

  def __repr__(self): return f'{self.__class__.__name__}: {self.feats}'

  def sample(self): return {k: v.sample() for k, v in self.feats.items()}

  def register_action(self, action:action, tdoc):
    for k, v in self.feats.items(): v.register_action(action=action, i=tdoc[k.db_field])

  def save_as_context(self):
    if not self.context: raise ValueError('Context cannot be None')
    return self.save(self._get_context_path(user_id=self.user_id, context=self.context))

  def save_as_global(self):
    return self.save(self._get_global_path(user_id=self.user_id))

  @classmethod
  def _load(cls, path:Path, user_id:str, context:str):
    try:                      return super(FeaturesPreference, cls).from_file(path)
    except FileNotFoundError: return cls(user_id=user_id, context=context)

  @classmethod
  def from_context(cls, user_id:str, context:str):
    return cls._load(
      path=cls._get_context_path(user_id=user_id, context=context),
      user_id=user_id,
      context=context,
    )

  @classmethod
  def from_global(cls, user_id:str):
    return cls._load(
      path=cls._get_global_path(user_id=user_id),
      user_id=user_id,
      context=None,
    )

  @staticmethod
  def _get_context_path(user_id:str, context:str): return FeaturesPreference.base_path/f'{user_id}/context/{context}'

  @staticmethod
  def _get_global_path(user_id:str): return FeaturesPreference.base_path/f'{user_id}/global'


class FeaturePreferenceController:
  def __init__(self, global_feats_pref:FeaturesPreference, feats_prefs:List[FeaturesPreference]):
    self.global_feats_pref = global_feats_pref
    self.feats_prefs = feats_prefs

  def sample(self):
    # TODO: How to combine multiple?
    if len(self.feats_prefs) > 0: return self.feats_prefs[0].sample()
    return self.global_feats_pref.sample()

  def register_action(self, action:action, tdoc):
    for o in self.feats_prefs: o.register_action(action=action, tdoc=tdoc)
    self.global_feats_pref.register_action(action=action, tdoc=tdoc)

  def save(self):
    self.global_feats_pref.save_as_global()
    for o in self.feats_prefs: o.save_as_context()

  @classmethod
  def load(cls, user_id:str, contexts:List[str]):
    # global_feats_pref = FeaturesPreference.from_global(user_id=user_id)
    # feats_prefs = [FeaturesPreference.from_context(user_id=user_id, context=c) for c in contexts]
    return cls(global_feats_pref=None, feats_prefs=None)




