import pickle
from pathlib import Path
from typing import *

from recommender.utils import Serializable2
from .index import Index


class IndexMap(Serializable2):
  def __init__(self, index:Index):
    super().__init__()
    self.index = index
    self.name2i, self.i2name = {}, {}
    self._count = 0

  def __len__(self): return len(self.index)

  @property
  def dim(self): return self.index.dim

  def add_one(self, name, v):
    i = self._add_name(name=name)
    self.index.add_one(v=v)

  def add_batch(self, name:list, v):
    i = [self._add_name(name=n) for n in name]
    self.index.add_batch(v=v)

  def get_distance_by_vector(self, v1, v2):
    return self.index.get_distance(v1=v1, v2=v2)

  def get_nns_by_name(self, name, n:int):
    idx = self.name2i[name]
    idxs, dists = self.index.get_nns_by_item(i=idx, n=n)
    return [self.i2name[i] for i in idxs], dists

  def get_nns_by_vector(self, v, n:int):
    idxs, dists = self.index.get_nns_by_vector(v=v, n=n)
    return self.i2name_batch(idxs), dists

  def get_item_vector(self, name):
    return self.index.get_item_vector(i=self.name2i[name])

  def i2name_batch(self, idxs):
    return [self.i2name[i] for i in idxs]

  # # TODO: Understand how Serializable does this
  # def save(self, path:Union[str, Path], **kwargs):
  #   self.index.save(path, **kwargs)
  #   _attrs = dict(name2i=self.name2i, i2name=self.i2name, _count=self._count)
  #   path = self._get_save_path(path)
  #   with open(path, 'wb') as f:
  #     pickle.dump(_attrs, f)
  #
  # def load(self, path, **kwargs):
  #   self.index.load(path, **kwargs)
  #   path = self._get_save_path(path)
  #   with open(path, 'rb') as f:
  #     _attrs = pickle.load(f)
  #   self.__dict__.update(_attrs)
  #
  # def _get_save_path(self, path):
  #   path = Path(path)
  #   path.mkdir(parents=True, exist_ok=True)
  #   return str(path/'/index_map.pkl')

  def _add_name(self, name:str) -> int:
    assert name not in self.name2i, f'{name} was already added to index'
    self.name2i[name] = self._count
    self.i2name[self._count] = name
    self._count += 1
    return self._count
