import faiss
import numpy as np
from .index import Index
from recommender.utils import *

class FaissIndex(Index):
  def __init__(self, dim:int, metric:str='euclidean'):
    self.metric = metric
    super().__init__(dim=dim)

  def __len__(self): return self.index.ntotal

  def _create_index(self):
    if self.metric == 'angular':   return faiss.IndexFlatIP(self.dim)
    if self.metric == 'euclidean': return faiss.IndexFlatL2(self.dim)

  def add_one(self, v):
    self.add_batch(v=v[None])

  def add_batch(self, v):
    if self.metric == 'angular': v = math.unit_v(v)
    v = np.array(v, dtype=np.float32).copy()
    self.index.add(v)

  def get_distance(self, v1, v2):
    v1, v2 = np.asarray(v1, dtype=np.float32), np.asarray(v2, dtype=np.float32)
    if self.metric == 'euclidean':
      return np.sum((v1 - v2) ** 2)
    raise NotImplementedError

  def get_nns_by_vector(self, v, n:int):
    v = np.asarray(v, dtype=np.float32)[None]
    dists, idxs = self.index.search(v, k=n)
    dists, idxs = dists[0], idxs[0]
    exists = idxs != -1
    return idxs[exists], dists[exists]

  def get_nns_by_item(self, i, n:int):
    v = self.get_item_vector(i)
    return self.get_nns_by_vector(v=v, n=n)

  def get_item_vector(self, i):
    return self.index.reconstruct(i)
