from pathlib import Path
from boltons.cacheutils import cachedproperty

from .annoy_index import AnnoyIndex
from .index_map import IndexMap

class _SavedIndex:
  # path_v0_0 = Path('/mnt/disks/dev/data/bandit/tspaces/spot_feats_only_v2_1')
  path_v0_0 = Path('/mnt/disks/dev/data/bandit/tspaces/v2_0')
  path_spot_feats_v6_1 = Path('/mnt/disks/dev/data/bandit/tspaces/spot_feats_v6_1')
  path_v6_2 = Path('/mnt/disks/dev/data/bandit/tspaces/v6_2')
  path_v6_3 = Path('/mnt/disks/dev/data/bandit/tspaces/1genre_05emot_v6_3')
  path_v6_4 = Path('/mnt/disks/dev/data/bandit/tspaces/1genre_1emot_v6_4')
  path_v6_5 = Path('/mnt/disks/dev/data/bandit/tspaces/1genre_05emot_v6_5')
  path_v6_6 = Path('/mnt/disks/dev/data/bandit/tspaces/1genre_all_05emot_v6_6')
  path_v6_7 = Path('/mnt/disks/dev/data/bandit/tspaces/1genre_intersect_1emot_v6_7')
  path_v6_8 = Path('/mnt/disks/dev/data/bandit/tspaces/1genre_intersect_1emot_v6_8')
  path_v6_9 = Path('/mnt/disks/dev/data/bandit/tspaces/1genre_intersect_2emot_v6_9')

  @property
  def comb_index(self): return self.comb_index_v6_2 # TODO: CAREFUL
  @property
  def genre_index(self): return self.genre_index_v0_0
  @property
  def emot_index(self): return self.emot_index_v0_0

  @cachedproperty
  def comb_index_v6_9(self):
    index = IndexMap(AnnoyIndex(dim=33, n_trees=10, metric='euclidean'))
    index.load(self.path_v6_9/'comb')
    return index

  @cachedproperty
  def comb_index_v6_8(self):
    index = IndexMap(AnnoyIndex(dim=33, n_trees=10, metric='euclidean'))
    index.load(self.path_v6_8/'comb')
    return index

  @cachedproperty
  def comb_index_v6_7(self):
    index = IndexMap(AnnoyIndex(dim=33, n_trees=10, metric='euclidean'))
    index.load(self.path_v6_7/'comb')
    return index

  @cachedproperty
  def comb_index_v6_6(self):
    index = IndexMap(AnnoyIndex(dim=33, n_trees=10, metric='euclidean'))
    index.load(self.path_v6_6/'comb')
    return index

  @cachedproperty
  def comb_index_v6_5(self):
    # 1 and 0 tags
    index = IndexMap(AnnoyIndex(dim=33, n_trees=10, metric='euclidean'))
    index.load(self.path_v6_5/'comb')
    return index

  @cachedproperty
  def comb_index_v6_4(self):
    # 1 and 0 tags
    index = IndexMap(AnnoyIndex(dim=34, n_trees=10, metric='euclidean'))
    index.load(self.path_v6_4/'comb')
    return index

  @cachedproperty
  def comb_index_v6_3(self):
    index = IndexMap(AnnoyIndex(dim=34, n_trees=10, metric='euclidean'))
    index.load(self.path_v6_3/'comb')
    return index

  @cachedproperty
  def comb_index_v6_2(self):
    index = IndexMap(AnnoyIndex(dim=34, n_trees=10, metric='euclidean'))
    index.load(self.path_v6_2/'comb')
    return index

  @cachedproperty
  def comb_index_spot_feats_v6_1(self):
    index = IndexMap(AnnoyIndex(dim=31, n_trees=10, metric='euclidean'))
    index.load(self.path_spot_feats_v6_1/'comb')
    return index

  @cachedproperty
  def comb_index_v6_1(self):
    index = IndexMap(AnnoyIndex(dim=33, n_trees=10, metric='euclidean'))
    index.load(self.path_v0_0/'comb')
    return index

  @cachedproperty
  def comb_index_v0_0(self):
    index = IndexMap(AnnoyIndex(dim=33, n_trees=10))
    index.load(self.path_v0_0/'comb')
    return index

  @cachedproperty
  def genre_index_v0_0(self):
    index = IndexMap(AnnoyIndex(dim=30, n_trees=10))
    index.load(self.path_v0_0/'genre')
    return index

  @cachedproperty
  def emot_index_v0_0(self):
    index = IndexMap(AnnoyIndex(dim=3, n_trees=10))
    index.load(self.path_v0_0/'emotion')
    return index

SavedIndex = _SavedIndex()
