import annoy
import numpy as np
from pathlib import Path
from typing import *

from .index import Index
from recommender.utils import Serializable2

class AnnoyIndex(Serializable2, Index):
  def __init__(self, dim, n_trees:int, metric:str='angular'):
    self.n_trees, self.metric = n_trees, metric
    self._current_n = 0
    super().__init__(dim=dim)

  def _create_index(self):
    return annoy.AnnoyIndex(f=self.dim, metric=self.metric)

  def add_one(self, v):
    self.index.add_item(self._current_n, v)
    self._current_n += 1

  def add_batch(self, v):
    for row in v: self.add_one(row)

  def get_nns_by_vector(self, v, n: int):
    seq_ids, distances = self.index.get_nns_by_vector(v, n=n, include_distances=True)
    return np.asarray(seq_ids), np.asarray(distances)

  def get_nns_by_item(self, i, n: int):
    seq_ids, distances = self.index.get_nns_by_item(i, n=n, include_distances=True)
    return np.asarray(seq_ids), np.asarray(distances)

  def get_item_vector(self, i):
    v = self.index.get_item_vector(i)
    return np.asarray(v)

  def build(self):
    self.index.build(n_trees=self.n_trees)

  def load(self, path:Union[str, Path], prefault:bool=False):
    # print('Deprecated, use .from_file instead')
    self.index.load(str(path)+'.ann', prefault=prefault)

  def __getstate__(self):
    state = super().__getstate__()
    del state['index']
    return state

  def save(self, path:Union[str, Path]):
    path = Path(path)
    path.mkdir(exist_ok=True, parents=True)
    self.index.save(self.annoy_index_path(path=path))
    super().save(path=path)

  @classmethod
  def from_file(cls, path:Union[str, Path], prefault:bool=False):
    obj = super(AnnoyIndex, cls).from_file(path)
    obj.index = annoy.AnnoyIndex(f=obj.dim, metric=obj.metric)
    obj.index.load(cls.annoy_index_path(path))
    return obj

  @staticmethod
  def annoy_index_path(path:Union[str, Path]): return str(Path(path)/'annoy_index.ann')
