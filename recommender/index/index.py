from pathlib import Path
from typing import *

from recommender.utils import Serializable


class Index:
  def __init__(self, dim:int):
    self.dim = dim
    self.index = self._create_index()

  def __len__(self):
    raise NotImplementedError

  def _create_index(self):
    raise NotImplementedError

  def add_one(self, v):
    raise NotImplementedError

  def add_batch(self, v):
    raise NotImplementedError

  def get_distance(self, v1, v2):
    raise NotImplementedError

  def get_nns_by_vector(self, v, n:int):
    raise NotImplementedError

  def get_nns_by_item(self, i, n:int):
    raise NotImplementedError

  def get_item_vector(self, i):
    raise NotImplementedError

  def save(self, path:Union[str, Path]):
    raise NotImplementedError

  def load(self, path:Union[str, Path]):
    raise NotImplementedError


