from enum import Enum
import mongo_manager as mm
from recommender.utils import *


class DBEnum(str, Enum):
  @property
  def db_field(self):
    return camel2snake(self.name)

  @classmethod
  def from_db_field(cls, name):
    return cls[snake2lowerCamel(name)]


FilterState = mm.FilterState
# class FilterState(DBEnum):
#   seed = 'seed'
#   include = 'include'
#   exclude = 'exclude'

class TagType(DBEnum):
  me = 'me'
  music = 'music'


MusicTagType = mm.MusicTagType
MeTagType = mm.MeTagType
# class MusicTagType(AddNameHackMixin, DBEnum):
#   language = 'language'
#   genres = 'genres'
#   emotion = 'emotion'
#   about = 'about'
#   instrument = 'instrument'
#   track = 'track'
#   artist = 'artist'

class SpecialFeatures(DBEnum):
  genres_embed = 'genres_embed'


Features = mm.Features
# class Features(DBEnum):
#   valence = 'valence'
#   loudness = 'loudness'
#   deepness = 'deepness'
#   speechiness = 'speechiness'

# class MusicTagType(DBEnum):
#   Language = 'Language'
#   Genres = 'Genres'
#   Emotion = 'Emotion'
#   About = 'About'
#   Instrument = 'Instrument'
#   Track = 'Track'
#
# class Features(DBEnum):
#   Valence = 'Valence'
#   Loudness = 'Loudness'
#   Deepness = 'Deepness'
