from typing import *
from .db_enums import Features

class UserFeature:
  def __init__(self, feature: Features, min: float, max: float):
    self.feature = feature
    self.min, self.max = min, max

  def __eq__(self, other):
    if isinstance(other, self.__class__):
      return (
          self.feature == other.feature
          and self.min == other.min
          and self.max == other.max
      )
    return False

  def to_filter(self):
    return {self.feature.db_field: {'$gte': self.min, '$lte': self.max}}

  @classmethod
  def from_json(cls, x: dict):
    return cls(
      feature=Features[x['feature']],
      min=x['min'],
      max=x['max'],
    )

  @classmethod
  def from_dict_json(cls, x: dict):
    return [UserFeature(feature=Features[k], min=v['min'], max=v['max'])
            for k, v in x.items()]

  def __repr__(self):
    return f'<{self.__class__.__name__} | feature: {self.feature}, min: {self.min}, max: {self.max}>'
