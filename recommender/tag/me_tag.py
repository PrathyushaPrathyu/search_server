from mongo_manager.tmp.me_tag import MeTag

MeTag = MeTag

# from recommender import *
#
# class MeTag:
#   def __init__(self, name:str):
#     self.name = name
#
#   def to_json(self): return json.loads(json.dumps({snake2lowerCamel(k): v for k, v in self.__dict__.items()}))
#
#   def __eq__(self, other):
#     if isinstance(other, self.__class__):
#       return self.name.lower() == other.name.lower()
#     return False
#
#   def __hash__(self):
#     return hash(self.name.lower())
#
#   @classmethod
#   def from_json(cls, x):
#     return cls(name=x['name'])
#
#   @staticmethod
#   def from_list_json(list_json):
#     return [MeTag.from_json(o) for o in list_json]
