import logging
from recommender.logger import logger


TSPACE_ID_FIELD = '_id'

def set_debug(v:bool):
  if v:
    logger.setLevel(logging.DEBUG)
