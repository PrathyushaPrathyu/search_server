import operator
from functools import reduce
from collections import *

from .action_log import ActionLog
from recommender.utils import *
from recommender.models import *

class TrackActionHistory:
  def __init__(self, action2log:dict=None):
    self.action2log = defaultdict(lambda: ActionLog(), action2log or {})

  def register_action(self, action:Action):
    action_log = self.action2log[action.name]
    action_log.register_new()
    if action.love: self.action2log[Love.name].register_new()

  def __add__(self, other):
    if not isinstance(other, self.__class__): raise NotImplemented
    action2log = combine_dicts(self.action2log, other.action2log, values_reduce=lambda x: reduce(operator.add, x))
    new = self.__class__(action2log=action2log)
    return new

  def __repr__(self):
    return f'<{self.__class__.__name__} ({dict(self.action2log)})>'
