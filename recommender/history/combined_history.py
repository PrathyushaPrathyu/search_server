import operator
from functools import reduce

from .history import History
from recommender.models import *
from recommender.utils import *

class CombinedHistory(History):
  def __init__(self, hists:List[History], keys_reduce=set.union):
    raise NotImplementedError
    self.hists = hists
    self.keys_reduce = keys_reduce
    super().__init__(
      data=combine_dicts(*[h.data for h in self.hists], values_reduce=lambda x: reduce(operator.add, x), keys_reduce=self.keys_reduce),
      date_sorted=combine_dicts(*[h.date_sorted for h in self.hists], values_reduce=max, keys_reduce=self.keys_reduce),
      score_sorted=combine_dicts(*[h.score_sorted for h in self.hists], values_reduce=sum, keys_reduce=self.keys_reduce),
    )

  def register_action(self, action:Action, track_id:str):
    super().register_action(action=action, track_id=track_id)
    for hist in self.hists: hist.register_action(action=action, track_id=track_id)

  def save_as_context(self, user_id:str): raise NotImplementedError

  def save_as_global(self, user_id:str): raise NotImplementedError
