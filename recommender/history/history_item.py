from datetime import datetime

from .track_action_history import TrackActionHistory
from recommender.reward import reward_from_action
from recommender.models import *

class HistoryItem:
  def __init__(self, id:str):
    self.id:str = id
    self.reward:float = 0.0
    self.n_played:int = 0
    self.last_played = None
    self.last_action = None
    self.last_reward = None
    self.action_history = TrackActionHistory()

  @property
  def score(self):
    return self.reward

  def register_action(self, action:Action):
    reward = reward_from_action(action)
    self.reward += reward
    self.n_played += 1
    self.last_played = datetime.utcnow()
    self.last_action = action
    self.last_reward = reward
    self.action_history.register_action(action=action)

  def __add__(self, other):
    if not isinstance(other, self.__class__): raise NotImplemented
    assert self.id == other.id
    new = self.__class__(id=self.id)
    new.reward = self.reward + other.reward
    new.n_played = self.n_played + other.n_played
    new.last_played = max(self.last_played, other.last_played)
    new.action_history = self.action_history + other.action_history
    return new

  def __repr__(self):
    return f'<{self.__class__.__name__} (id:{self.id}, score:{self.score}, last_played:{self.last_played}>'