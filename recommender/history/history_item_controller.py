from bson import ObjectId
from typing import *

from .history_item import HistoryItem

class HistoryItemController:
  def __init__(self, items:List[HistoryItem]):
    self.items = items

  def __len__(self): return len(self.items)

  def get_pipeline_stages(self):
    ids = [ObjectId(o.id) for o in self.items]
    return [{'$match': {'_id': {'$in': ids}}}]
