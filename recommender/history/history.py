import mongo_manager as mm

from recommender import *


class History:
  def __init__(self, history_model: mm.History):
    self.history_model = history_model

  def __len__(self): return NotImplementedError

  @property
  def user_id(self): return self.history_model.user_id

  @property
  def context(self): return self.history_model.context

  @property
  def data(self): return self.history_model.data

  @property
  def seed_track(self): return self.history_model.seed_track

  @property
  def seed_track_reward(self): return self.history_model.seed_track_reward

  @property
  def played_tracks(self): return self.history_model.played_tracks

  @property
  def favorites(self):
    return self.history_model.favorites

  @property
  def short(self):
    return self.history_model.short

  @property
  def long(self):
    return self.history_model.short

  def get_items(self, ids):
    return self.history_model.get_items(ids=ids)

  def set_seed_track(self, track_id):
    reward = reward_from_action(Complete(love=0.0))
    return self.history_model.set_seed_track(track_id=track_id, reward=reward)

  def register_action(self, action: Action, track_id: str):
    reward = reward_from_action(action)
    return self.history_model.register_action(action=action.name, track_id=track_id, reward=reward)

  def reset_session(self):
    return self.history_model.reset_session()

  def get_played_tracks_pipeline_stage(self):
    return self.history_model.get_played_tracks_pipeline_stage()

  def get_new_tracks_pipeline_stage(self):
    return self.history_model.get_new_tracks_pipeline_stage()

  def save_as_context(self, user_id: str):
    self.history_model.save()

  def save_as_global(self, user_id: str):
    self.history_model.save()

  @classmethod
  def from_context(cls, user_id: str, context: str):
    history_model = mm.History.from_context(user_id=user_id, context=context)
    return cls(history_model=history_model)

  @classmethod
  def from_global(cls, user_id: str):
    history_model = mm.History.from_global(user_id=user_id)
    return cls(history_model=history_model)

# import numpy as np
# from datetime import datetime
#
# from bson import ObjectId
# from sortedcollections import *
# from pathlib import Path
# from typing import *
#
# from recommender import InterestTag
# from recommender.reward import reward_from_action
# from recommender.utils import ContextSerializable, ClassNameSerializableMixin, Serializable2
# from recommender.models import *
# from recommender.db import *
# from .history_item import HistoryItem
# from .history_item_controller import HistoryItemController
#
# def register_context_op(context:str) -> DBOperation:
#   return DBOperation({'$set': {CURRENT_CONTEXT: context}, '$addToSet': {PAST_CONTEXTS: context}})
#
# class History(Serializable2):
#   base_path = Path('/mnt/disks/dev/tmp/history')
#
#   def __init__(self, context:str=None, data=None, date_sorted=None, score_sorted=None):
#     super().__init__()
#     self.context = context
#     self.data = data or {}
#     self.date_sorted = ValueSortedDict(date_sorted)
#     self.score_sorted = ValueSortedDict(score_sorted)
#     self.seed_track:HistoryItem = None
#     self.seed_track_reward = None
#     self.played_tracks:List[HistoryItem] = []
#
#   def __len__(self): return len(self.data)
#
#   @property
#   def favorites(self):
#     left = self.score_sorted.bisect_key_left(2.0)
#     ids = self.score_sorted.keys()[left:][::-1]
#     return self.get_items(ids=ids)
#
#   @property
#   def short(self):
#     ids = self.date_sorted.keys()[::-1][:30]
#     return self.get_items(ids=ids)
#
#   @property
#   def long(self):
#     ids = self.score_sorted.keys()[::-1][:30]
#     return self.get_items(ids=ids)
#
#   def get_items(self, ids):
#     return HistoryItemController(items=[self.data[id] for id in ids])
#
#   def set_seed_track(self, track_id):
#     try: item = self.data[track_id]
#     except KeyError: item = HistoryItem(id=track_id)
#     # Simulate reward for this item
#     item.last_reward = reward_from_action(Complete(love=0.0))
#     self.seed_track = item
#
#   def register_action(self, action:Action, track_id:str):
#     try: item = self.data[track_id]
#     except KeyError: item = HistoryItem(id=track_id)
#     item.register_action(action=action)
#     self._update(item=item)
#     self._register_played(item=item, action=action)
#
#   def reset_session(self):
#     self.seed_track:HistoryItem = None
#     self.played_tracks:List[HistoryItem] = []
#
#   def get_played_tracks_pipeline_stage(self):
#     return {'$match': {'_id': {'$nin': [ObjectId(o.id) for o in self.played_tracks]}}}
#
#   def get_new_tracks_pipeline_stage(self):
#     return {'$match': {'_id': {'$nin': [ObjectId(o.id) for o in self.data.values()]}}}
#
#   def _register_played(self, item: HistoryItem, action:Action):
#     if self.seed_track is None or action == Search or item.last_reward >= self.seed_track_reward:
#       if action != LeftSwipe:
#         self.seed_track = item
#         self.seed_track_reward = item.last_reward
#     self.played_tracks.append(item)
#
#   def _update(self, item:HistoryItem):
#     self.data[item.id] = item
#     self.date_sorted[item.id] = datetime.utcnow()
#     self.score_sorted[item.id] = item.score
#
#   @staticmethod
#   def _get_context_path(user_id:str, context:str): return History.base_path/f'{user_id}/context/{context}'
#
#   @staticmethod
#   def _get_global_path(user_id:str): return History.base_path/f'{user_id}/global'
#
#   def save_as_context(self, user_id:str):
#     if not self.context: raise ValueError('Context cannot be None')
#     # Write context to db
#     writer = UserDBWriter(_id=user_id)
#     writer.register_update(register_context_op(context=self.context))
#     writer.write()
#
#     return self.save(self._get_context_path(user_id=user_id, context=self.context))
#
#   def save_as_global(self, user_id:str):
#     return self.save(self._get_global_path(user_id=user_id))
#
#   @classmethod
#   def from_context(cls, user_id:str, context:str):
#     return super(History, cls).from_file(cls._get_context_path(user_id=user_id, context=context))
#
#   @classmethod
#   def from_global(cls, user_id:str):
#     return super(History, cls).from_file(cls._get_global_path(user_id=user_id))
#
#

