import random
from recommender import *

def track_preview(token:str, body:dict):
  music_tag_control = MusicTagController.from_list_json(body['musicTags'])
  user_feats_control = UserFeaturesController.from_dict_json(body['userFeatures'])
  track_previewer = TrackPreviewer(music_tag_control=music_tag_control, user_feats_control=user_feats_control)
  return track_previewer.get_track().to_json()


class TrackPreviewer:
  def __init__(
      self,
      music_tag_control:MusicTagController,
      user_feats_control:UserFeaturesController,
  ):
    self.music_tag_control = music_tag_control
    self.user_feats_control = user_feats_control

  def get_track(self) -> MusicTag:
    pipe_control = TrackPreviewPipelineController(music_tag_control=self.music_tag_control)
    pipeline = db.Pipeline()
    pipeline.add_stages(self.user_feats_control.get_pipeline_stages())
    pipeline = pipe_control.get_pipeline(seeding=False, pipeline=pipeline)
    tdocs = list(pipeline.run(projection=['_id']))
    selected_id = random.choice(tdocs)['_id']
    return MusicTag.from_tdoc_id(selected_id)

class TrackPreviewPipelineController:
  def __init__(
      self,
      music_tag_control:MusicTagController,
  ):
    self.music_tag_control = music_tag_control

  def get_pipeline(self, seeding: bool, pipeline:db.Pipeline=None):
    # Define the order of operations
    ops = [
      lambda: True,
      lambda: self._remove_tag_type(MusicTagType.track),
      lambda: self._remove_tag_type(MusicTagType.instrument),
      lambda: self._remove_tag_type(MusicTagType.artist),
      lambda: self._remove_tag_type(MusicTagType.about),
      lambda: self._remove_tag_type(MusicTagType.emotion),
      lambda: self._remove_tag_type(MusicTagType.genres),
      lambda: self._remove_tag_type(MusicTagType.language),
    ]
    for op in ops:
      changed = op()
      if changed:
        available, new_pipeline = self._has_available_tracks(seeding=seeding, starting_pipeline=pipeline)
        if available: return new_pipeline

    raise NoTracksAvailable

  def _remove_tag_type(self, music_tag_type:MusicTagType):
    # print(f'Removing tag: {music_tag_type}')
    return self.music_tag_control.remove_tag_type(music_tag_type)

  def _has_available_tracks(self, seeding:bool, starting_pipeline:db.Pipeline=None) -> [bool, db.Pipeline]:
    for inner_match in ['$and', '$or']:
      pipeline = starting_pipeline.copy() if starting_pipeline else db.Pipeline()
      pipeline.add_stages(self.music_tag_control.get_pipeline_stages(seeding=seeding, inner_match=inner_match))
#       logger.debug(f'Pipeline: {pipeline}')

      # OPTIMIZE: Running filters only to get document count
      n_tracks = len(list(pipeline.run(projection=['_id'])))
      # logger.debug(f'AvailableTracks({inner_match}): {n_tracks}')
      if n_tracks > 0: break

    return n_tracks > 0, pipeline
