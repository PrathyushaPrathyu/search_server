import pymongo
import neo_db as nd
import mongo_manager as mm
import db_manager as dm
from recommender import *
from ..api import auth

from flask import abort
from ..api.db_ops import *
from ..api.history import *
from ..api.user import new_user_doc

def sign_up(email:str, name:str, username:str, password:str, info:dict, device_token):
  # if (username.to_lower() not in reserved_usernames):
  dm.UserManager.sign_up(email=email, name=name, username=username, password=password, device_token=device_token)

def username_exists_check(username:str):
  username_exists = dm.UserManager.username_exists_check(username=username)

  return username_exists


def sign_in(email:str, password:str, info:dict, device_token) -> str:
  user_manager = dm.UserManager.sign_in(email=email, password=password, device_token=device_token)
  return user_manager.to_dict(simplified=False)

def get_user(token:str):
  neo_user = dm.UserManager.from_id(user_id=token)
  return neo_user.to_dict(simplified=False)

# def _user_perks(neo_user:nd.User):
#   neo_user.modify_currency(200)
#   neo_user.add_badge(badge=nd.Badges.beta_user_badge)
#
#   # TODO:TEST Next lines only for testing function
#   # Add a track to have some expertise
#   tdoc = db.get_tdoc(name='hot mama blues')
#   track = nd.Track.from_mongodb_doc(tdoc)
#   context_manager = nd.ContextManager(user=neo_user, context=nd.Context('Testing'))
#   context_manager.register_action(track=track, action='RightSwipe', reward=0.7)

# def _backwards_sign_up(username, _id: str, info):
#   # TODO: We can delete all of this code right?
#   ucol = db.get_user_col()
#   new_udoc = new_user_doc(username=username)
#   new_udoc['_id'] = ObjectId(_id)
#   try:
#     _id = ucol.insert_one(new_udoc).inserted_id
#     writer = db.UserDBWriter(_id=_id)
#     writer.register_update(add_history_sign_up(username=username, info=info))
#   except pymongo.errors.DuplicateKeyError:
#     abort(409, 'Username already exists')

# def _backwards_sign_in(username:str, password:str, info:dict) -> str:
#   # Old implementation #
#   # TODO: EVEN IF WE DELETE THESE, NOTHING SHOULD BE AFFECTED RIGHT?
#   udoc = db.get_udoc_by_credentials(username=username, password=password)
#   writer = db.UserDBWriter(_id=udoc['_id'])
#   # writer.register_update(add_history_sign_in(username=username, info=info))
#   # writer.register_update(new_session())
#   writer.write()
#   ####################
#   # Reset session
#   #TODO: This is already being called in sign in. Delete?
#   playback_control = PlaybackController.from_no_context(user_id=str(udoc['_id']))
#   playback_control.reset_session()
#   playback_control.save()
