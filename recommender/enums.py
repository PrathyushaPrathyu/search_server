from enum import Enum

InterestTag = Enum('InterestTag', 'glt gst slt sst immediate favorites')
ModeTag = Enum('ModeTag', 'exploit explore immediate')
ActionTag = Enum('ActionTag', 'right left complete quick_right search')

EmotionTag = Enum('EmotionTag', 'valence activation deepness')
TrackFeaturesTag = Enum('TrackFeaturesTag', 'genre valence activation deepness')

PrimaryMode = Enum('PrimaryMode', 'Favorites Discover Magic')
SecondaryMode = Enum('SecondaryMode', 'Organic Wild')
TertiaryMode = Enum('TertiaryMode', 'All Indie')